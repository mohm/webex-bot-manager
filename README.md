# Webex Bot Manager

Webex Bot Manager (WBM) is a feature rich tool for managing your bots and your day to day workflows in Webex Teams. Webex Bot Manager is designed to make it easy to start using your Webex Teams bots. Once you have successfully deployed WBM, all you need to get started is the bot access token to bring it alive.

Webex Bot Manager requires minimium internet connection and be able to talk to the webex teams APIs to work. If you want the full set of features you should set it up in a public deployment which will enable you to create chatbots. 

## What can Webex Bot Manager offer?

* Responsive web design viewable on desktop, tablet and mobile web browsers.
* Easy configuration setup
* Popup explaination of the different features 
* Manage all your bots and add as many as you like!
* Full access control and restrictions (restrict who can chat with your bot)
    * Set access domains to define which domains the bot will reply to
    * Set access group on the webhook to restrict communication with the bot to a limited set of people
    * Set access group on command, define which command is off limit for unauthorized users
    * Restrict communication in group spaces
        * Maybe you want a lot of restriction but allow one person in a single space to communicate with the bot to avoid spam? Webex Bot Manager can enable that!
        * Maybe you want everyone in a particular space to be able to chat with the bot? Webex Bot Manager can enable that!
* Manage users, all user data is fetched from Webex Teams
* Create multipurpose groups, add users and spaces to the groups to broadcast messages to them or use the group as an access group for a command or a webhook. 
    * The groups can also be used as subscription groups for notifications where users can opt-in or opt-out via the bot if you have 2 way communication (just enable it on the bot, no programming needed)
* View the bot spaces, see the chatlog, administer the space, create new spaces / teams / team spaces and add thousands of users to a space or team in minutes or delete/archive spaces. 
* Create chatbots by enabling webhooks on the bots
* Create customer keywords that trigger the API for custom tasks, response or both.  
* Send messages via your preferred bot (just select which bot should send the message) to individual or groups of users and spaces
* Markdown Editor
    * Write your message, save it and send it later. Send a test to your own Webex Teams account before broadcast to make sure it looks OK! Select multiple groups of people and spaces or individuals to broadcast. 
* Message queue that will collect each request and response for the messages to let you know that the message was sent or not and the reason for the message to fail. After the broadcast task has been created, you can monitor the progress. 
* Support for integration, add your custom integration to the Webex Bot Manager
* Authorize any user or org-admin to broadcast messages from that user instead of a bot 
* Device management with full cloud xAPI wrapper (list devices and search, get device status, send commands, fetch configurations and set configurations) - requires integration
* Place managment, create new, update existing or delete places in your org and get activations codes for Webex Room Devices (requires integration) 
* If you have 2 way communication you can enable these features on the bot it self using the native bot features, see below for some of the cool built in bot features. No programming needed, once the webhook is up and running (single button click) each of the features can be individually enabled on any and all of your bots! You can add access groups to any of the features to control who can actually execute it, some features makes sense to restrict while others should be open. 
    * Manage Webex Bot Manager from your bot, add/remove users +++ etc. 
    * Space controls, create spaces from the bot, add groups of users to a space
    * Integration controls, authorize a user with custom scopes on the fly
    * Place controls, create places and activation codes on the fly
    * Device controls, look up devices, get status, send commands, configure a device on the fly
    * Usage report (how many times each keyword the bot responded to)
    * Remove the last reply from the bot
    * Allow users to subscribe / unsubscribe to groups that belongs to the bot
    * Allow users to add them self to Webex Bot Manager as a user
    * Allow users to look up their own user to see what groups they are member of in Webex Bot Manager
    * Allow users to purge themself from Webex Bot Manager
* Active Directory Integration for Authentication and adding users via AD groups
* Proxy support
* And more..!

GET STARTED AND BECOME THE ULTIMATE PUPPETMASTER! 

## Create custom bot interactions or trigger notifications with the WBM APIs

* Webex Bot Manager comes with a very simple API that allow you to forward the response to an API where your third party application can pick it up and use the information to execute custom actions!
* Webex Bot Manager has a notification API that allows an authenticated request to trigger a message broadcast to one or more groups of people. Maybe you have a service where you want a group of users to be alerted if something happens? 

## Screenshots

![login](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/93e0cd5c6196b769e641b0bb44063d09/CropperCapture_13_.png)
![bot_dashboard](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/a5a323a8e0aee27006821ddf4682720b/CropperCapture_16_.png)
![users_page](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/c2231069210e936b44e3e728757b89d0/CropperCapture_17_.png)
![groups_page](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/fe2b6b39854a89772811c4edc04feaee/CropperCapture_18_.png)
![spaces_teams_page](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/a9e84dc3f74d2eac61cc5fed8933ee97/CropperCapture_19_.png)
![users_page](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/893ea56de1bc946b90540e8307d5f54f/CropperCapture_20_.png)
![bot_page](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/81818ec53064ee650a1993ad3a60b4c8/CropperCapture_21_.png)
![webhooks_page](https://gitlab.com/mohm/webex-bot-manager/-/wikis/uploads/76fab11bb1bc368eaa525aa013ae6bc3/CropperCapture_22_.png)



## Installation

* Read the [Installation Guide](https://gitlab.com/mohm/webex-bot-manager/-/wikis/Installation) for instructions to install webex-bot-manager.

## Support
"Webex Bot Manager" is NOT a Cisco Webex supported tool. Please do not call Cisco for support with WBM. For support, file an issue within gitlab. 

## Credits

* Creator and Lead Developer Magnus Ohm (mohm@cisco.com)
* Secondary Developer Yusof Yaghi (yyaghi@cisco.com)