-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 28, 2023 at 02:06 PM
-- Server version: 5.5.68-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easybot-template`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(100) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `command` text NOT NULL,
  `user` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ad_groups`
--

CREATE TABLE `ad_groups` (
  `id` int(100) NOT NULL,
  `group_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ad_group_mapping`
--

CREATE TABLE `ad_group_mapping` (
  `id` int(100) NOT NULL,
  `ad_group_id` int(100) NOT NULL,
  `local_group_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blocked_contacts`
--

CREATE TABLE `blocked_contacts` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bots`
--

CREATE TABLE `bots` (
  `id` varchar(255) NOT NULL,
  `emails` varchar(100) NOT NULL,
  `displayName` varchar(255) NOT NULL,
  `avatar` varchar(400) NOT NULL,
  `type` varchar(50) NOT NULL,
  `access` varchar(255) NOT NULL,
  `main` int(10) NOT NULL,
  `defres` text NOT NULL,
  `card_mode` int(1) NOT NULL,
  `same_org_response` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bot_allowed_domain`
--

CREATE TABLE `bot_allowed_domain` (
  `botid` varchar(100) NOT NULL,
  `domainid` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bot_webhook`
--

CREATE TABLE `bot_webhook` (
  `id` int(11) NOT NULL,
  `webhookid` varchar(100) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `groupid` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `card_body` text NOT NULL,
  `reply` text NOT NULL,
  `accessgroup` int(25) NOT NULL,
  `input_active` int(1) NOT NULL,
  `delete_active` int(1) NOT NULL,
  `echo_active` int(1) NOT NULL,
  `update_active` int(1) NOT NULL,
  `redirect_reply` int(1) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `datacode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `card_data`
--

CREATE TABLE `card_data` (
  `card_id` varchar(255) NOT NULL,
  `person_id` varchar(255) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `person_email` varchar(255) NOT NULL,
  `input_data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` varchar(200) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `emails` varchar(100) NOT NULL,
  `avatar` varchar(500) NOT NULL,
  `orgId` varchar(200) NOT NULL,
  `type` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_access_group_response`
--

CREATE TABLE `contact_access_group_response` (
  `id` varchar(200) NOT NULL,
  `contactid` varchar(200) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `debug_log`
--

CREATE TABLE `debug_log` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `request` text NOT NULL,
  `response` text NOT NULL,
  `bot` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `customfield1` text NOT NULL,
  `customfield2` text NOT NULL,
  `customfield3` text NOT NULL,
  `customfield4` text NOT NULL,
  `customfield5` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `id` int(100) NOT NULL,
  `domain` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `excluded_spaces`
--

CREATE TABLE `excluded_spaces` (
  `id` varchar(200) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `spacetitle` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `usage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES
(1, 'Admin controls', 'admin', 'The \"Admin Controls\" feature will allow a bot to manage the WebEx Teams Bot Managment site from the client using the assigned bot. The admin controls feature is quite powerful and should be protected with an access group!', '## ADMINISTRATOR COMMANDS\r\n---\r\n\r\n<blockquote class=primary>System</blockquote>\r\n\r\n- **admin maintenance** [**enable**/**disable**] \r\n    - Enables or disables maintenance mode, can be enabled to prevent new external tasks to be created in case of modifications or downtime. \r\n\r\n<blockquote class=primary>Message</blockquote>\r\n\r\n- **admin message list [messageid]** \r\n    - Get a list of stored messages with message id. You can supply a message id to the list command to preview a message.\r\n- **admin message announce [groupid] [messageid]**\r\n    - Announce a stored message specified groups. You can issue multiple groups in csv (no spaces)\r\n- **admin message echo [groupid],[email] [free text here..]**\r\n    - Sends an echo of your message to the specified groups or emails. You can issue a combo of multiple groups and emails in csv (no spaces)\r\n\r\n<blockquote class=primary>Feedback</blockquote>\r\n\r\n- **admin feedback create [Topic text..]** \r\n    - Creates a new feedback topic for this bot\r\n- **admin feedback allow_statement [topicId:0|1]**\r\n    - Turn a rule off or on.\r\n- **admin feedback topic [topicId]**\r\n    - View topic details and rules\r\n\r\n<blockquote class=primary>User management</blockquote>\r\n\r\n- **admin user list** \r\n    - Lists all the users added to the database\r\n- **admin check** [**email_address**] \r\n    - Checks if a user exists in the database and any memberships\r\n- **admin add** [**email_address**],[**email_address**]..\r\n    - Adds one or more new users to the database (if not exists)\r\n- **admin delete** [**email_address**],[**email_address**].. \r\n    - Deletes one or more users from the database\r\n- **admin block** [**email_address**] \r\n    - Blocks all my communication with this user regardless \r\n- **admin unblock** [**email_address**] \r\n    - Unblocks user \r\n\r\n<blockquote class=primary>Group and membership management</blockquote>\r\n\r\n- **admin group list** \r\n    - Lists available groups\r\n- **admin group members [groupid]**\r\n    - Lists all members of a group\r\n- **admin group create [groupname:groupid]**\r\n    - Creates a new group. The groupname and the groupid cannot contain spaces and must be separated with colon only if you create a groupid. Groupid is optional but it makes it easier to add users to the group, else the ID will be an incremental number.\r\n- **admin group delete [groupid]**\r\n- **admin group mention [groupid]** \r\n    - Creates a mention tag for all members of the group and will be mentioned by the bot in the current space.\r\n- **admin group addtospace [groupid]**\r\n    - Adds all the members of the group to the current space\r\n- **admin group add** [**groupid:email_addess**]  \r\n    - Adds a user or a group space to a group, you can separate the groupid and or emails with comma to add multiple users to multiple groups (groupid,groupid,groupid:email_address,email_address). To add a space you have to type **this** instead of an e-mail: [**groupid:this**] the space must be a group space and the group must be owned by the bot you are talking to. \r\n- **admin group remove** [**groupid:email_addess**] - Removes a user from a group, you can separate the groupid and or e-mails with comma to remove multiple groups (XX,XX,XX:email_address,email_address)\r\n\r\n<blockquote class=primary>Space controls (admin commands)</blockquote>\r\n\r\n- **admin spaceresponse status** \r\n    - Checks the response status in the current space (command must be issued in the space you wish to see the status)\r\n- **admin spaceresponse enable|disable** \r\n    - Enables or disables group response for any members in the space (command must be issued in the space you wish to enable)\r\n- **admin spaceresponse [email_address]** \r\n    - Enables spaceresponse for that particular user in the particular space (command must be issued in the space you wish to enable)\r\n- **admin joinable** \r\n    - Make a group space joinable with the **space join** command (must be issued inside the space you want to make joinable)\r\n\r\n<blockquote class=primary>Task handler</blockquote>\r\n\r\n- **admin queue report** \r\n    - Checks number of tasks in the queue\r\n- **admin queue purge** \r\n    - Purge task queue\r\n'),
(2, 'Space controls', 'space', 'Space controls lets you interact with a bot in a space and control spaces. Add csv of users, kick, create.', '## SPACE CONTROLS\r\n---\r\n\r\n<blockquote class=primary>Space commands</blockquote>\r\n\r\n- **space create** \r\n    - *Creates a new space with you and the bot with a autogenerated title*\r\n- **space create [title text ..]** \r\n    - *Creates a new space with you and the bot. The space will be named with UTC timestamp and the specified title*\r\n- **space create [email,email..]** \r\n    - *Creates a new space and adds the users specified with CSV at the same time*\r\n    - *If a new space is created in a team space, the space will be created in the team.*\r\n- **space add [email,email..]** \r\n    - *Adds users to the current space*\r\n- **space kick [email]** \r\n    - Removes a user from a space, note that if you write only \"space kick\" it will remove you. \r\n- **space gencsv** \r\n    - Generates a list of emails based on who is in the space the command is issued. The list is CSV format and will be given to you 1:1 in bulks of 100 emails. If there are too many people > 1000 in the space I might not be able to provide the full list. \r\n- **space list** \r\n    - Lists spaces marked as **joinable** for this bot (shows spaceid)\r\n- **space join [spaceid]**\r\n    - I will invite you to the space \r\n\r\n\r\n'),
(3, 'Random friendly jokes', 'joke', 'By issuing the <b>joke</b> command to a bot, it will reply with a joke. \r\n\r\nEntertainment feature ', '## JOKE\r\n---\r\n\r\n<blockquote class=primary>JOKE - ENTERTAINMENT</blockquote>\r\n\r\n- **joke** - *The bot will tell a random friendly joke*'),
(4, 'Random yomomma jokes', 'yomomma', 'A series of insulting jokes. \r\n\r\nCan be inappropriate in certain scenarios. \r\n\r\nEntertainment feature. ', '## YOMOMMA\r\n---\r\n\r\n<blockquote class=primary>YOMOMMA - ENTERTAINMENT</blockquote>\r\n\r\n- **yomomma** - *The bot will tell a random  yomomma joke*'),
(5, 'Random Chuck Norris jokes', 'chuck', 'A series of Chuck Norris jokes. \r\n\r\nSome of the jokes can be inappropriate. \r\n\r\nEntertainment feature.', '## CHUCK\r\n---\r\n\r\n<blockquote class=primary>CHUCK NORRIS - ENTERTAINMENT</blockquote>\r\n\r\n- **chuck** - *The bot will tell a random Chuck Norris joke*'),
(6, 'Remove last message', 'remove', 'This feature will when issued make the bot delete its latest response, can be issued multiple times to remove more content. ', '## REMOVE MESSAGE\r\n---\r\n\r\n<blockquote class=primary>REMOVE LAST MESSAGES</blockquote>\r\n\r\n- **remove last** - *The bot will remove its latest reply in the space*'),
(7, 'Service report', 'service', 'Gives feedback on the task listener (requires third party script to report correct status)', '## SERVICE\r\n---\r\n\r\n<blockquote class=primary>SERVICE REPORT</blockquote>\r\n\r\n- **service** - *The bot will let you know if the external task service is still alive*'),
(8, 'Usage reports', 'usage', 'Reports back the amount of commands the bot has responded to. ', '## USAGE\r\n---\r\n\r\n<blockquote class=primary>USAGE REPORTS</blockquote>\r\n\r\n- **usage** - *The bot will go through the activity logs and give an estimate of how many times a command has been issues and what command is used most frequently*'),
(9, 'Who am I', 'whoami', 'The user who issues the command will see what access groups he is currently member of in the Bot Manager. ', '## WHO AM I\r\n---\r\n\r\n<blockquote class=primary>WHOAMI - SELF CHECK</blockquote>\r\n\r\n- **whoami** - *The bot will check what groups a user is part of in the bot management system and provide the list of memberships to the user*'),
(10, 'Lookup spark user', 'whois', 'Issues a query in spark for a username or email. The bot will return details about this user. ', '## WHO IS\r\n---\r\n\r\n<blockquote class=primary>WHO IS PERSON</blockquote>\r\n\r\n- **whoid** [**email|name**] - *The bot will look up the user in spark and provide som trivial information about the user*'),
(11, 'Request being added', 'request', 'With the request feature you can let the user decide if he or she wants to receive notifications etc. The feature will automatically add the user to the database. You can also tag groups as \"default groups\". Whenever a user typed requests the user will be added to the default groups as well. ', '## REQUEST\r\n---\r\n\r\n<blockquote class=primary>Request participation</blockquote>\r\n\r\n- **request** - *The bot will add the user that issues this command to the database and to the pre-configured default groups. Makes administration of users easier*'),
(12, 'Subscribe to groups', 'subscribe', 'With subscription groups enabled you allow the users to decide what groups they should to be part of.\r\n\r\nIf you use a specific group for notifications frequently you might want to allow users to unsubscribe if they don\'t want more notification. \r\n\r\nSubscription groups can also be used for other purposes.  ', '## SUBSCRIPTION GROUPS\r\n---\r\n\r\n<blockquote class=primary>Usage:</blockquote>\r\n\r\n- **subscribe** '),
(13, 'Delete self', 'resign', 'A user that has been added in the bot management system will be able to delete him self from the database (only him self). ', '## RESIGN AS A USER\r\n---\r\n\r\n<blockquote class=primary>Usage:</blockquote>\r\n\r\n- **resign** - *Deletes the issuer as a user (if exists).*  '),
(14, 'Collect user feedback', 'feedback', 'Setup a bot to collect user input or save data from users. Create topics and let the users create entries of which others can vote on topic entries or comment on the entries.', '## FEEDBACK COMMANDS\r\n---\r\n\r\n<blockquote class=primary>Introduction</blockquote>\r\n\r\nThis feature opens up for user input. An admin can create one or more topics for the bot. \r\n\r\nEach topic has a set of rules that can allow a user to view and create entries of which other users can vote and comment on.\r\n\r\nEach topic may have a different set of rules, for example a topic can be private (only accessible to a specific group of people) or certain features may be turned off, like commenting or voting. I will notify you if such a rule applies. Only admins can create topics and setup rules. \r\n\r\n<blockquote class=primary>Topic commands</blockquote>\r\n\r\nNote: **[placeholders]** are typed without the **[]** \r\n\r\n- **feedback topics** \r\n    - View all available topics and the correlating topicId.\r\n- **feedback topic [topicId]** \r\n    - View all entries in the specified topicId. This also includes an overview of how many comments and votes per entry.\r\n- **feedback topic [topicId] add [entry text ..]**\r\n    - Create a new entry in the specified topicId\r\n\r\n<blockquote class=primary>Entry commands</blockquote>\r\n\r\n- **feedback entry [entryId]** \r\n    - View the full details of a specified entry including votes and comments. \r\n- **feedback entry [entryId] delete**\r\n    - Deletes the specified entryId if you have created it. Deleting an entry will also delete all votes and comments on the specified entry. This command will not provide a confirmation prompt and cannot be undone.  \r\n\r\n<blockquote class=primary>Votes and Comments</blockquote>\r\n\r\n- **feedback vote [entryId]** \r\n    - Place your vote on the specified entryId. You can revoke the vote by placing the same command a second time.   \r\n- **feedback comment [entryId] [comment text ...]**\r\n    - Place a comment on the specified entryId. \r\n- **feedback comment delete [commentId]**\r\n    - Deletes a comment that you have created using the commentId. The commentId can be found in the comment by viewing the entry details.   \r\n\r\n\r\n'),
(15, 'Devices ', 'device', 'Enable device control', '## DEVICE COMMANDS\r\n---\r\n\r\nWith the device commands you can find your device based on your current integration user.If no integration user is configured to your account the bot will use its own user token to look for devices it has been granted control for. Configuration require admin scopes.  \r\n\r\n<blockquote class=primary>View devices</blockquote>\r\n\r\n- **device list** [**displayName**] or JSON (see example)\r\n     - *List the available devices controlled by this bot or integration (including deviceId)* \r\n\r\n<blockquote class=primary>Status, configurations and commands</blockquote>\r\n\r\n- **device ui** [**deviceId**]\r\n    - *Returns an adaptive card that is locked to the deviceId you type in. Makes it easier to get status, send commands and configs to the device.*\r\n- **device status** [**deviceId**] [**statusKey**] \r\n     - *Get status from the device on a statuskey*\r\n- **device command** [**deviceId**] [**commandKey**] [**{\"attribute\":\"value\"}**]*\r\n     - *Send a command to a device with a command key*\r\n- **device config get** [**deviceId**] [**configKey**] [**-v**]*\r\n     - *Fetch configuration details from device*\r\n     - *The flag -v will display fewer nodes but more info*\r\n- **device config set** [**deviceId**] [**configKey**] [**value**]\r\n     - *Modifies a configuration key on the specified device*\r\n\r\nExample usage:\r\n\r\nThe **deviceId** is acquired using **device list**\r\n\r\n- **device list My system**\r\n     - *Searches for a device named My system and lists the deviceId if found*\r\n- **device list {\"product\":\"SX10\",\"start\":\"100\"}**\r\n     - *Returns a list of devices that matches the product and starts the list from device number 100*\r\n- **device show [deviceId]**\r\n     - *Lists the details of a device, most of the detail keys can be used when searching for devices using the device list command with JSON*\r\n\r\n- **device status [deviceId] audio.volume** \r\n     - *Returns the current volume on the device*\r\n- **device command [decviceId] audio.volume.set {\"Level\": 60}** \r\n     - *Set the volume on the device*\r\n- **device config get [deviceId] SerialPort.Mode**\r\n     - *Gets the configuration details of xConfiguration SerialPort Mode*\r\n- **device config set [deviceId] SerialPort.Mode Off**\r\n     - *Sets the configuration node xConfiguration SerialPort Mode to Off*\r\n\r\n'),
(16, 'Integration controls', 'integration', 'Allow authorizing with integration using the bot. ', '## INTEGRATION COMMANDS\r\n---\r\n\r\nIntegration allow you to connect any Webex Teams user to your bot user. Since device and place management is the primary features you get from integration, it is recommended to authorize a device admin or org admin to perform device activation.  \r\n\r\nWhen authorizing new users you might have to log out of your currently logged in user in your browser.\r\n\r\n<blockquote class=primary>Authorize</blockquote>\r\n\r\n- **integration authorize** [**scope** **scope**..] \r\n     - *Generates an authorization URL for your user, login with any Webex Teams user to attach the token to your current user. If no scopes are issued the system default ones will be used.*\r\n- **integration refresh** \r\n     - *Refreshes your currently attached access token*\r\n\r\n<blockquote class=primary>View</blockquote>\r\n\r\n- **integration show**\r\n     - *Displays your current integration settings including the system default scopes.*\r\n- **integration scopes**\r\n     - *Displays the available scopes with description that can be used when generating custom authrorization URLs*\r\n- **integration defaults** \r\n     - *Displays the system default scopes*\r\n\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **integration delete**\r\n     - *Removes your current integration authorization*'),
(17, 'Place controls', 'place', 'Enables control over places, requires autorized integration token to use. The authorized user needs to be a site admin. \r\n\r\nScopes: spark-admin:places_read, spark-admin:places_write and identity:placeonetimepassword_create\r\n\r\n', '## PLACES COMMANDS\r\n---\r\n\r\nThis feature may require you to authrorize a user with the following scopes: spark-admin:places_read, spark-admin:places_write. In order to get an activation code you need the identity:placeonetimepassword_create\r\n\r\n<blockquote class=primary>Get details</blockquote>\r\n\r\n- **place list** [**displayName**]\r\n    - List places, search for displayName (optional). \r\n- **place show** [**placeId**] \r\n    - List details for the place, requires placeId.\r\n\r\n<blockquote class=primary>Create</blockquote>\r\n\r\n- **place create** [**displayName**] \r\n    - Creates a new place with the specified displayName\r\n- **place update** [**placeId**] [**newDisplayName**] \r\n    - Updates an existing place with the specified displayName\r\n- **place activate** [**displayName**]\r\n    - Creates a new place using the displayName and returns an activation code for a Cisco Webex Room Device\r\n- **place code** [**placeId**]\r\n    - Activates an existing place and returns an activation code for a Cisco Webex Room Device\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **place delete** [**placeId**]\r\n    - Deletes a place'),
(18, 'Workspace integration control', 'ws', 'Control the workspace integrations. Use the bot to switch between integrations and check status. Enable certain features on the workspace integration you have currently mounted. ', '## WORKSPACE INTEGRATIONS\r\n---\r\n\r\n<blockquote class=primary>Workspace integration commands</blockquote>\r\n\r\n- **ws** \r\n    - *Display this list of commands*\r\n- **ws list**\r\n    - *List activated workspace integrations (with ID) that are available for you to mount.*\r\n- **ws show**\r\n    - *Display limited information about the workspace integration currently attached to your account*\r\n- **ws show [id]**\r\n    - *Display limited information about the workspace integration specified by ID. The specified workspace integration do not have to be mounted to your account*\r\n- **ws mount [id]**\r\n    - *Mount an activated workspace integration to your account*\r\n- **ws unmount** \r\n    - *Remove the mounted workspace integration from your account* \r\n- **ws refresh**\r\n    - *The access token will automatically be refreshed when running commands that require the workspace integration access token. This command refreshes the token manually.* \r\n- **ws monitor [deviceId]**\r\n    - *Will send you a message when a webhook is received from this device. Just a notification. You may monitor more than one device.* \r\n\r\nUse the commands **place** and **device** when a workspace integration is mounted to your account.\r\n\r\n\r\n\r\n\r\n ');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_entries`
--

CREATE TABLE `feedback_entries` (
  `id` int(100) NOT NULL,
  `topic_id` int(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_entry_comment`
--

CREATE TABLE `feedback_entry_comment` (
  `id` int(100) NOT NULL,
  `entry_id` int(100) NOT NULL,
  `comment` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_entry_vote`
--

CREATE TABLE `feedback_entry_vote` (
  `id` int(100) NOT NULL,
  `entry_id` int(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `voted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_topic`
--

CREATE TABLE `feedback_topic` (
  `id` int(100) NOT NULL,
  `botid` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `comments_allowed` int(1) NOT NULL DEFAULT '1',
  `votes_allowed` int(1) NOT NULL DEFAULT '1',
  `entry_create_allowed` int(1) NOT NULL DEFAULT '1',
  `entry_view_allowed` int(1) NOT NULL,
  `entry_delete_allowed` int(1) NOT NULL DEFAULT '1',
  `accessgroup` int(100) NOT NULL,
  `public` int(1) NOT NULL DEFAULT '0',
  `publickey` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `generic_feedback`
--

CREATE TABLE `generic_feedback` (
  `id` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `default_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generic_feedback`
--

INSERT INTO `generic_feedback` (`id`, `message`, `default_message`) VALUES
('maintenance', '**Maintenance mode is currently enabled:** The requested task is blocked and purged when this mode is enabled due to ongoing maintenance or downtime. Please try again later.', '**Maintenance mode is currently enabled:** The requested task is blocked and purged when this mode is enabled due to ongoing maintenance or downtime. Please try again later.'),
('warning', '**Warning** We are experiencing temporary network issues!', '**Warning** We are experiencing temporary network issues!');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `sub_id` varchar(100) NOT NULL,
  `groupname` varchar(200) NOT NULL,
  `subscribable` int(10) NOT NULL,
  `default_group` int(1) NOT NULL,
  `description` text NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `sub_id`, `groupname`, `subscribable`, `default_group`, `description`, `botid`) VALUES
(15, '', 'ADMINS', 0, 0, 'Generic group', '0');

-- --------------------------------------------------------

--
-- Table structure for table `group_contacts`
--

CREATE TABLE `group_contacts` (
  `groupid` int(100) NOT NULL,
  `contactid` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_groups`
--

CREATE TABLE `group_groups` (
  `groupid` varchar(100) NOT NULL,
  `nestedid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_spaces`
--

CREATE TABLE `group_spaces` (
  `groupid` int(100) NOT NULL,
  `spaceid` varchar(200) NOT NULL,
  `botid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `heartbeat`
--

CREATE TABLE `heartbeat` (
  `id` int(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(100) NOT NULL,
  `who` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heartbeat`
--

INSERT INTO `heartbeat` (`id`, `time`, `ip`, `who`) VALUES
(1, '2017-12-10 23:26:14', '173.38.220.47', 'internal_pi');

-- --------------------------------------------------------

--
-- Table structure for table `integration`
--

CREATE TABLE `integration` (
  `id` varchar(100) NOT NULL,
  `client_id` varchar(100) NOT NULL,
  `client_secret` varchar(100) NOT NULL,
  `redirect_url` varchar(100) NOT NULL,
  `default_scopes` text NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `integration`
--

INSERT INTO `integration` (`id`, `client_id`, `client_secret`, `redirect_url`, `default_scopes`, `name`) VALUES
('1', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `integration_scopes`
--

CREATE TABLE `integration_scopes` (
  `scope` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `integration_scopes`
--

INSERT INTO `integration_scopes` (`scope`, `description`) VALUES
('audit:events_read', 'Access to the audit log for an organization'),
('identity:placeonetimepassword_create', 'Access to a one time password to a place to create an activation code'),
('spark-admin:call_qualities_read', 'Access to read organization\'s call qualities'),
('spark-admin:devices_read', 'See details for any device in your organization'),
('spark-admin:devices_write', 'Delete any device in your organization'),
('spark-admin:hybrid_clusters_read', 'Access to read hybrid clusters for your organization'),
('spark-admin:hybrid_connectors_read', 'Access to read hybrid connectors for your organization'),
('spark-admin:licenses_read', 'Access to read licenses available in your user\'s organizations'),
('spark-admin:organizations_read', 'Access to read your user\'s organizations'),
('spark-admin:people_read', 'Access to read your user\'s company directory'),
('spark-admin:people_write', 'Access to write to your user\'s company directory'),
('spark-admin:resource_groups_read', 'Access to read your organization\'s resource groups'),
('spark-admin:resource_group_memberships_read', 'Access to read your organization\'s resource group memberships'),
('spark-admin:resource_group_memberships_write', 'Access to update your organization\'s resource group memberships'),
('spark-admin:roles_read', 'Access to read roles available in your user\'s organization'),
('spark-admin:workspaces_read', 'See details for workspaces'),
('spark-admin:workspaces_write', 'Create, modify and delete workspaces'),
('spark-compliance:events_read', 'Access to read events in your user\'s organization'),
('spark-compliance:memberships_read', 'Access to read memberships in your user\'s organization'),
('spark-compliance:memberships_write', 'Access to create/update/delete memberships in your user\'s organization'),
('spark-compliance:messages_read', 'Access to read messages in your user\'s organization'),
('spark-compliance:messages_write', 'Post and delete messages in all spaces in your user\'s organization'),
('spark-compliance:rooms_read', 'Access to read rooms in your user\'s organization'),
('spark-compliance:teams_read', 'Access to read teams in your user\'s organization'),
('spark-compliance:team_memberships_read', 'Access to read team memberships in your user\'s organization'),
('spark-compliance:team_memberships_write', 'Access to update team memberships in your user\'s organization'),
('spark:all', 'Full access to your Webex Teams account'),
('spark:devices_read', 'See details for your devices'),
('spark:devices_write', 'Modify and delete your devices'),
('spark:memberships_read', 'List people in the rooms you are in'),
('spark:memberships_write', 'Invite people to rooms on your behalf'),
('spark:messages_read', 'Read the content of rooms that you are in'),
('spark:messages_write', 'Post and delete messages on your behalf'),
('spark:people_read', 'Read your users\' company directory'),
('spark:rooms_read', 'List the titles of rooms that you are in'),
('spark:rooms_write', 'Manage rooms on your behalf'),
('spark:teams_read', 'List the teams your user\'s a member of'),
('spark:teams_write', 'Create teams on your users\' behalf'),
('spark:team_memberships_read', 'List the people in the teams your user belongs to'),
('spark:team_memberships_write', 'Add people to teams on your users\' behalf'),
('spark:xapi_commands', 'Execute all commands on RoomOS-enabled devices.'),
('spark:xapi_statuses', 'Retrieve all information from RoomOS-enabled devices.');

-- --------------------------------------------------------

--
-- Table structure for table `integration_tokens`
--

CREATE TABLE `integration_tokens` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `expires_in` int(255) NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `refresh_token_expires_in` int(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `joinable_space`
--

CREATE TABLE `joinable_space` (
  `id` int(100) NOT NULL,
  `spaceid` varchar(200) NOT NULL,
  `spacetitle` varchar(400) NOT NULL,
  `access_group` int(100) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_bot`
--

CREATE TABLE `log_bot` (
  `id` int(1) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log_bot`
--

INSERT INTO `log_bot` (`id`, `botid`) VALUES
(0, 'none');

-- --------------------------------------------------------

--
-- Table structure for table `log_users`
--

CREATE TABLE `log_users` (
  `id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `queue_subtasks`
--

CREATE TABLE `queue_subtasks` (
  `subtask_id` varchar(100) NOT NULL,
  `task_id` varchar(100) NOT NULL,
  `bot_id` varchar(255) NOT NULL,
  `subtask_type` varchar(25) NOT NULL,
  `task_json` mediumtext NOT NULL,
  `group_id` varchar(25) NOT NULL,
  `task_code` varchar(3) NOT NULL,
  `payload` text NOT NULL,
  `response` text NOT NULL,
  `task_results` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `queue_task`
--

CREATE TABLE `queue_task` (
  `task_id` varchar(100) NOT NULL,
  `task_type` varchar(50) NOT NULL,
  `task_name` varchar(35) NOT NULL,
  `task_submitter` varchar(35) NOT NULL,
  `botresponse` int(1) NOT NULL,
  `botid` varchar(150) NOT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clear` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `response` text NOT NULL,
  `file_url` varchar(300) NOT NULL,
  `card_id` int(11) NOT NULL,
  `is_task` int(1) NOT NULL,
  `is_feature` int(1) NOT NULL,
  `accessgroup` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(100) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `saved_messages`
--

CREATE TABLE `saved_messages` (
  `id` int(100) NOT NULL,
  `title` text NOT NULL,
  `message` text NOT NULL,
  `files` varchar(400) NOT NULL,
  `card_attachment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_status`
--

CREATE TABLE `service_status` (
  `id` int(1) NOT NULL,
  `maintenance_mode` int(1) NOT NULL,
  `warning_mode` int(1) NOT NULL,
  `task_monitor` int(11) NOT NULL,
  `maintenance_message` text NOT NULL,
  `debug_mode` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_status`
--

INSERT INTO `service_status` (`id`, `maintenance_mode`, `warning_mode`, `task_monitor`, `maintenance_message`, `debug_mode`) VALUES
(1, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(25) NOT NULL,
  `settings` varchar(25) NOT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `settings`, `value`) VALUES
(1, 'dbversion', '603');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `taskId` varchar(100) NOT NULL,
  `attr1` text NOT NULL,
  `attr2` text NOT NULL,
  `attr3` text NOT NULL,
  `attr4` text NOT NULL,
  `attr5` text NOT NULL,
  `attr6` text NOT NULL,
  `text` text NOT NULL,
  `user` varchar(200) NOT NULL,
  `user_details` text NOT NULL,
  `bot` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `roomid` varchar(400) NOT NULL,
  `files` text NOT NULL,
  `card_data` text NOT NULL,
  `parentMessageId` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `webex_response_codes`
--

CREATE TABLE `webex_response_codes` (
  `code` varchar(3) NOT NULL,
  `status` varchar(50) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `webex_response_codes`
--

INSERT INTO `webex_response_codes` (`code`, `status`, `label`, `description`) VALUES
('100', 'Continue', '<span class=\"label label-success\">Continue</span>', 'The server could not confirm that the request was successful'),
('200', 'OK', '<span class=\"label label-success\">Success</span>', 'Successful request with body content.'),
('204', 'No Content', '<span class=\"label label-success\">No Content</span>', 'Successful request without body content.'),
('400', 'Bad Request', '<span class=\"label label-warning\">Bad Request</span>', 'The request was invalid or cannot be otherwise served. An accompanying error message will explain further.'),
('401', 'Unauthorized', '<span class=\"label label-warning\">Unauthorized</span>', 'Authentication credentials were missing or incorrect.'),
('403', 'Forbidden', '<span class=\"label label-warning\">Forbidden</span>', 'The request is understood, but it has been refused or access is not allowed.'),
('404', 'Not Found', '<span class=\"label label-warning\">Not Found</span>', 'The URI requested is invalid or the resource requested, such as a user, does not exist. Also returned when the requested format is not supported by the requested method.'),
('405', 'Method Not Allowed', '<span class=\"label label-warning\">Method Not Allowed</span>', 'The request was made to a resource using an HTTP request method that is not supported.'),
('409', 'Conflict', '<span class=\"label label-warning\">Conflict</span>', 'The request could not be processed because it conflicts with some established rule of the system. For example, a person may not be added to a room more than once.'),
('415', 'Unsupported Media Type', '<span class=\"label label-warning\">Unsupported Media Type</span>', 'The request was made to a resource without specifying a media type or used a media type that is not supported.'),
('429', 'Too Many Requests', '<span class=\"label label-warning\">Too Many Requests</span>', 'Too many requests have been sent in a given amount of time and the request has been rate limited. A Retry-After header should be present that specifies how many seconds you need to wait before a successful request can be made.'),
('500', 'Internal Server Error', '<span class=\"label label-danger\">Internal Server Error</span>', 'Something went wrong on the server'),
('502', 'Bad Gateway', '<span class=\"label label-danger\">Bad Gateway</span>', 'The server received an invalid response from an upstream server while processing the request. Try again later.'),
('503', 'Service Unavailable', '<span class=\"label label-danger\">Service Unavailable</span>', 'Server is overloaded with requests. Try again later.');

-- --------------------------------------------------------

--
-- Table structure for table `workspace_integrations`
--

CREATE TABLE `workspace_integrations` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `client` varchar(255) NOT NULL DEFAULT '',
  `secret` varchar(255) NOT NULL DEFAULT '',
  `refresh_token` varchar(255) NOT NULL DEFAULT '',
  `access_token` varchar(255) NOT NULL DEFAULT '',
  `expires_in` varchar(255) NOT NULL DEFAULT '',
  `refresh_token_expires_in` varchar(255) NOT NULL DEFAULT '',
  `token_timestamp` timestamp NULL DEFAULT NULL,
  `scopes` text NOT NULL,
  `accessgroup` int(11) NOT NULL DEFAULT '0',
  `activation_jwt` text NOT NULL,
  `organization` text NOT NULL,
  `status` text NOT NULL,
  `error` text NOT NULL,
  `whs` varchar(255) NOT NULL DEFAULT '',
  `health_timestamp` timestamp NULL DEFAULT NULL,
  `health_status` int(11) NOT NULL DEFAULT '0',
  `activation_status` int(11) NOT NULL DEFAULT '0',
  `webhook_logging` int(11) NOT NULL DEFAULT '0',
  `actions_logging` int(11) NOT NULL DEFAULT '0',
  `data_collection` int(11) NOT NULL DEFAULT '0',
  `feature_signage_enabled` int(11) NOT NULL DEFAULT '0',
  `signage_base_url` text NOT NULL,
  `signage_content_url` text NOT NULL,
  `signage_assign_url` text NOT NULL,
  `feature_pwa_enabled` int(11) NOT NULL DEFAULT '0',
  `pwa_base_url` text NOT NULL,
  `pwa_content_url` text NOT NULL,
  `pwa_assign_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ws_action_log`
--

CREATE TABLE `ws_action_log` (
  `id` int(11) NOT NULL,
  `appId` varchar(255) NOT NULL,
  `jti` varchar(255) NOT NULL DEFAULT '',
  `action` text NOT NULL,
  `iat` int(11) DEFAULT NULL,
  `full` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ws_device_collector`
--

CREATE TABLE `ws_device_collector` (
  `appId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `deviceInfo` text NOT NULL,
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latestStatus` text NOT NULL,
  `lastEvent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ws_device_monitoring`
--

CREATE TABLE `ws_device_monitoring` (
  `appId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `notification_groups` text NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ws_integration_user`
--

CREATE TABLE `ws_integration_user` (
  `userid` varchar(255) NOT NULL,
  `appid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ws_webhook_log`
--

CREATE TABLE `ws_webhook_log` (
  `id` int(255) NOT NULL,
  `appId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_groups`
--
ALTER TABLE `ad_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_group_mapping`
--
ALTER TABLE `ad_group_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocked_contacts`
--
ALTER TABLE `blocked_contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `bots`
--
ALTER TABLE `bots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `bot_allowed_domain`
--
ALTER TABLE `bot_allowed_domain`
  ADD UNIQUE KEY `botid` (`botid`,`domainid`);

--
-- Indexes for table `bot_webhook`
--
ALTER TABLE `bot_webhook`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `webhookid` (`webhookid`,`botid`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_data`
--
ALTER TABLE `card_data`
  ADD UNIQUE KEY `card_id` (`card_id`,`person_id`),
  ADD UNIQUE KEY `card_id_2` (`card_id`,`person_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emails` (`emails`);

--
-- Indexes for table `contact_access_group_response`
--
ALTER TABLE `contact_access_group_response`
  ADD UNIQUE KEY `id` (`id`,`contactid`,`botid`);

--
-- Indexes for table `debug_log`
--
ALTER TABLE `debug_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `domain` (`domain`);

--
-- Indexes for table `excluded_spaces`
--
ALTER TABLE `excluded_spaces`
  ADD UNIQUE KEY `id` (`id`,`botid`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `keyword` (`keyword`);

--
-- Indexes for table `feedback_entries`
--
ALTER TABLE `feedback_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_entry_comment`
--
ALTER TABLE `feedback_entry_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_entry_vote`
--
ALTER TABLE `feedback_entry_vote`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `entry_id` (`entry_id`,`email`);

--
-- Indexes for table `feedback_topic`
--
ALTER TABLE `feedback_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generic_feedback`
--
ALTER TABLE `generic_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_contacts`
--
ALTER TABLE `group_contacts`
  ADD UNIQUE KEY `unique_index` (`contactid`,`groupid`);

--
-- Indexes for table `group_groups`
--
ALTER TABLE `group_groups`
  ADD UNIQUE KEY `groupid` (`groupid`,`nestedid`);

--
-- Indexes for table `group_spaces`
--
ALTER TABLE `group_spaces`
  ADD UNIQUE KEY `groupid` (`groupid`,`spaceid`,`botid`);

--
-- Indexes for table `heartbeat`
--
ALTER TABLE `heartbeat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `integration`
--
ALTER TABLE `integration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `integration_scopes`
--
ALTER TABLE `integration_scopes`
  ADD PRIMARY KEY (`scope`);

--
-- Indexes for table `integration_tokens`
--
ALTER TABLE `integration_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`userid`);

--
-- Indexes for table `joinable_space`
--
ALTER TABLE `joinable_space`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `spaceid` (`spaceid`,`botid`),
  ADD KEY `id` (`spaceid`);

--
-- Indexes for table `log_bot`
--
ALTER TABLE `log_bot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_users`
--
ALTER TABLE `log_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue_subtasks`
--
ALTER TABLE `queue_subtasks`
  ADD PRIMARY KEY (`subtask_id`);

--
-- Indexes for table `queue_task`
--
ALTER TABLE `queue_task`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`keyword`,`botid`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_messages`
--
ALTER TABLE `saved_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_status`
--
ALTER TABLE `service_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webex_response_codes`
--
ALTER TABLE `webex_response_codes`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `workspace_integrations`
--
ALTER TABLE `workspace_integrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ws_action_log`
--
ALTER TABLE `ws_action_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ws_device_collector`
--
ALTER TABLE `ws_device_collector`
  ADD PRIMARY KEY (`appId`,`deviceId`);

--
-- Indexes for table `ws_device_monitoring`
--
ALTER TABLE `ws_device_monitoring`
  ADD UNIQUE KEY `appId` (`appId`,`deviceId`,`userid`);

--
-- Indexes for table `ws_integration_user`
--
ALTER TABLE `ws_integration_user`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `userid` (`userid`,`appid`);

--
-- Indexes for table `ws_webhook_log`
--
ALTER TABLE `ws_webhook_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appId` (`appId`,`deviceId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ad_groups`
--
ALTER TABLE `ad_groups`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ad_group_mapping`
--
ALTER TABLE `ad_group_mapping`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blocked_contacts`
--
ALTER TABLE `blocked_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bot_webhook`
--
ALTER TABLE `bot_webhook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `debug_log`
--
ALTER TABLE `debug_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_entries`
--
ALTER TABLE `feedback_entries`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_entry_comment`
--
ALTER TABLE `feedback_entry_comment`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_entry_vote`
--
ALTER TABLE `feedback_entry_vote`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_topic`
--
ALTER TABLE `feedback_topic`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `integration_tokens`
--
ALTER TABLE `integration_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `joinable_space`
--
ALTER TABLE `joinable_space`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_users`
--
ALTER TABLE `log_users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `saved_messages`
--
ALTER TABLE `saved_messages`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ws_action_log`
--
ALTER TABLE `ws_action_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ws_webhook_log`
--
ALTER TABLE `ws_webhook_log`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
