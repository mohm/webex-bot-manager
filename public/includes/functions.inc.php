<?php
include 'messagelist.inc.php';

if(!empty($_SERVER['DOCUMENT_ROOT'])) {
	
	$rootdoc = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
	$rootdoc = str_replace("/public", "", $rootdoc);
	$configfile = "config_ini.php";
	$fullfilepath = $rootdoc.'/'.$configfile;
	
	if (!file_exists($fullfilepath)) {
		
		echo "I have issues finding $configfile which is required, the document root must be set to the webex-bot-manager folder. I am looking for the file in the following root path I got from the server: $rootdoc . Can you please verify that your web server has the correct root directory configured?";
	
	} 
	
	require $fullfilepath;

} else {
	
	if(php_sapi_name() === 'cli') {
		
		require 'config_ini.php';
	
	} else {
require '../config_ini.php';
	
	}
	
}

#WORKSPACE_INTEGRATION FUNCTIONS

function jwtDecode($jwtarr, $num) {
    if (in_array($num, array('0','1'))) {
        return json_decode(base64_decode(explode('.', $jwtarr['jwt'])[$num]), true);
    }
    return array();
}

function base64UrlEncode($text)
{
    return str_replace(
        ['+', '/', '='],
        ['-', '_', ''],
        base64_encode($text)
        );
    
}

function readconfigfile() {
	
	global $config;
	$configVariable = array();
	
	foreach ($config as $k => $v) {
		
		$configVariable[$k] = $v;
	
	}

	return $configVariable;

}

function codeBlock($string) {
	
	return "<pre><code>" . $string . "</code></pre>";

}

function filterArray($data, $filter) {
    
    $newData = array();
           
    foreach ($filter as $k => $v) {
        if (isset($data[$v])) {
            $newData[$v] = $data[$v];
        }
    }
        
    return $newData;
}


function verifyConfigFile($config) {	
	
	$required_fields = array(
			'dbhost'=>'r', 
			'dbname'=>'r', 
			'username'=>'r', 
			'password'=>'r', 
			'base_dir'=>'r',
	        'pkey'=>'r',
			'proxyurl'=>'o', 
			'proxyuserpass'=>'o', 
			'ADserver'=>'o', 
			'ADport'=>'o', 
			'BaseDN'=>'o', 
			'AdminGroup'=>'o', 
			'ADuser'=>'o', 
			'ADpass'=>'o'
	);
	
	$returnconfig = array();
	
	foreach ($required_fields as $k => $v) {
		
		if ($v == 'r') {
			
			if (isset($config[$k]) and !empty($config[$k])) {
				
				$returnconfig[$k] = $config[$k];
			
			} else {
				
				return false;
			
			}
		
		} else {
			
			$returnconfig[$k] = issetor($config[$k]);
		
		}
	
	}
	
	return $returnconfig; 

}

function writeconfigfile(array $args = array()) {
	
	$data = verifyConfigFile($args);
	$base_dir = $data['base_dir'];
	$configfile = $data['base_dir']."/config_ini.php";

	try {
		
		$config_ini = fopen($configfile, "w") or die("Unable to create config.ini file. Please check apache has write permissions to the directory.");
		$fields = "";
		
		foreach ($data as $k => $v) {
			
			$fields .= '"'.$k.'"=>"'.$v.'",';
		
		}
		
		$config_data = '<?php 
$config = array(
	'.$fields.'
);
?>';
		fwrite($config_ini, $config_data);

		return true;
		
	} catch (Exception $e)  {
		
		return "Issue writing to config file. Error message: " . $e;
	
	}

}

function secondsToTime($seconds) {
	
	$s = (int)$seconds;
	
	return sprintf('%d:%02d:%02d:%02d', $s/86400, $s/3600%24, $s/60%60, $s%60);

}

function calculateHmac($secret, $payload) {
    
}

function checkinternet( $proxyurl, $proxycreds ) {
	
	$url = 'www.webex.com';
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_PROXY, $proxyurl);
	curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxycreds);
	curl_exec($ch);
	
	$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	curl_close($ch);

	return $retcode;

}

function queueLock($action='') {
	
	global $config;
	$lockfile = $config['base_dir'] . '/temp/queue.lock';

	switch ($action) {
		
		case "lock":
			return (mkdir($lockfile));
			break;
		
		case "unlock":
			
			if (queueLock()) {
				
				return(rmdir($lockfile));
			
			}
			
			break;
		//Check if lockfile has existed for more than 1 hour (assuming deadlock)
		case "isdeadlocked":
			
			if (queueLock("check")) {
				
				return (time()-filemtime($lockfile) > 1 * 3600);
			
			}
			
			break;
		
		default:
			
			return (file_exists($lockfile));
			break;
			
	}

}

function check_is_ip($ipdata) {
	
	return (filter_var($ipdata, FILTER_VALIDATE_IP)) ? true : false;

}

function tooltip($headline, $text, $icon="fa-info-circle", $placement='bottom') {
	
	global $tooltips;
	return "<a tabindex='0' role='button' data-placement='$placement' data-toggle='popover' data-trigger='focus' title='$headline' data-content='$text'><i class='icon fa $icon'></i></a>";
	
}

function getHostUrl() {
	
	if(isset($_SERVER['HTTPS'])){
	
		$protocol = ($_SERVER['HTTPS'] and $_SERVER['HTTPS'] != "off") ? "https" : "http";
	
	} else {
		
		$protocol = "http";
	
	}
	
	return $protocol.'://'.$_SERVER['HTTP_HOST'];

}

function quote($c) {
	
	return array("<blockquote class='$c'>","</blockquote>");

}

//Cred https://stackoverflow.com/questions/8689471/alternative-to-header-for-re-directs-in-php
function redirect($url) {
	
	if (!headers_sent()) {
		
		header('Location: '.$url);
		exit;
	
	} else {
		
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$url.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
		echo '</noscript>'; exit;
	
	}

}

function blockquote($input, $class, $card=false, array $kwargs = array()) {
    $c = new Cards();
    
    $text = "<blockquote class='$class'>$input</blockquote>\n\n";
       
	if ($card) {
	    
	    $styles = [
	        'style' => $class
	    ];
	    
	    if (isset($kwargs['headline'])) {
	        $styles['headline'] = $kwargs['headline']; 
	    }
   
		return array("markdown"=>$text,"attachments"=>$c->msgCard($input, $styles));
    
	}
   
	return $text;

}

function formUrl($get) {
	
	return $_SERVER["PHP_SELF"]."?".http_build_query($_GET);

}

function validateEmail($emaildata) {
	
	return (filter_var($emaildata, FILTER_VALIDATE_EMAIL)) ? true : false;

}

function validateUrl($urldata) {

	return (filter_var($urldata, FILTER_VALIDATE_URL)) ? true : false;

}

function validateDomain($domain) {

	return (preg_match('^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$^', $domain)) ? true : false;

}

function get_timestamp($zone="Europe/Oslo") {

	date_default_timezone_set($zone);
	$timestamp = date('Y-m-d H:i:s');

	return $timestamp;

}

function colorize_value($color, $value, $title='') {

	return "<font title='$title' color='$color'>$value</font>";

}

function alerts($type, $title='', $message='', $redirectURL = false) {
	
	if (empty($title) or empty($message)) {
		
		if ($type == "error") {
			
			$title = "Oops, something went wrong!";
			$message = "Generic error, please try again!";
		
		} elseif ($type == "success") {
			
			$title = "Success";
			$message = "The request was successful!";
		
		} elseif ($type == "warning") {
		
			$title = "Warning!";
			$message = "Something was not quite right...";
		
		}
	
	}

	if(!empty($redirectURL)) {
		
		$redirect = ".then(function (result) {
		if (true) {
		window.location = '".$redirectURL."';
		}
		})";

	} else {
		
		$redirect = '';
	
	}
	
	$result  = "<script>Swal.fire({
		title: '".$title."',
		html: '".$message."',
		type: '".$type."',
		confirmButtonText: 'Close',
		allowOutsideClick: true,
		})" . $redirect . "
		</script>";
	
	return $result;

}

function feedbackMsg($headline,$msg,$style, $botr=0) {
	$webstyle = array(
			'danger'=>array(
					'style'=>"alert alert-danger alert-dismissible",
					'icon' =>"<h4><i class='icon fa fa-ban'></i>"),
			'alert'=>array(
					'style'=>"alert alert-danger alert-dismissible",
					'icon' =>"<h4><i class='icon fa fa-ban'></i>"),
			'success'=>array(
					'style'=>"alert alert-success alert-dismissible",
					'icon' =>"<h4><i class='icon fa fa-check-circle'></i>"),
	        'info'=>array(
					'style'=>"alert alert-info alert-dismissible",
					'icon' =>"<h4><i class='icon fa fa-info-circle'></i>"),
			'warning'=>array(
					'style'=>"alert alert-warning alert-dismissible",
					'icon' =>"<h4><i class='icon fa fa-exclamation-triangle'></i>"));
	
	return ($botr == 0) ? "<div class='{$webstyle[$style]['style']}' style='margin-left: 10px; margin-right: 10px;'>
		<button type='button' class='close' data-dismiss='alert'>&times;</button>
		{$webstyle[$style]['icon']}$headline</h4>
					$msg
				</div>":"<blockquote class='$style'><strong>$headline</strong><br>$msg</blockquote>\n";
}

function msgList($keyword) {
	
	global $messageList, $headlineList;
	list($section, $id) = explode('-',$keyword);
	
	return feedbackMsg($headlineList[$section],$messageList[$keyword],$section);
	
}

function get_guid() {
    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // Set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // Set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function generatePassword() {
	
	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	$special = '%&!?#$@*|=+';
	$randompass = array();
	$alphaLength = strlen($alphabet) - 1;
	$specialLength = strlen($special) - 1;
	
	for ($i = 0; $i < 8; $i++) {
		
		if ($i === rand(0,8)) {
			
			$n = rand(0, $specialLength);
			$randompass[] = $special[$n];
			
		} else {
			
			$n = rand(0, $alphaLength);
			$randompass[] = $alphabet[$n];
			
		}
		
	}
	
	return implode($randompass);
	
}

function multiSort($array, $sortvalue) {
	
	array_multisort(array_map('strtolower', array_column($array, $sortvalue)), SORT_ASC, $array);
	
	return $array;
	
}

function multi_implode(array $glues, array $array){
	$out = "";
	$g = array_shift($glues);
	$c = count($array);
	$i = 0;
	foreach ($array as $val){
		if (is_array($val)){
			$out .= multi_implode($glues,$val);
		} else {
			$out .= (string)$val;
		}
		$i++;
		if ($i<$c){
			$out .= $g;
		}
	}
	return $out;
}

function statusBoolPill($bool, $trueText='Operational', $falseText='Inoperative') {
    $color = ($bool) ? "success" : "danger"; 
    $text = ($bool) ? $trueText:$falseText; 
    return '<span class="badge badge-pill badge-'.$color.'">'.$text.'</span>';
}
function statusPill($text, $color='primary') {
    return '<span class="badge badge-pill badge-'.$color.'>'.$text.'</span>';
}

function onoff($bool, $titleTrue='', $titleFalse='', $textTrue='', $textFalse='', $h='20', $w='20') {
	global $pos_color, $neg_color;
	//return ($bool) ? '<img class="img-circle" src="images/static/green_light_1.png" title="'.$titleTrue.'" height="'.$h.'" width="'.$w.'"> <font color="'.$pos_color.'">'.$textTrue .'</font>':'<img class="img-circle" src="images/static/red_light_1.png" title="'.$titleFalse.'" height="'.$h.'" width="'.$w.'"> <font color="'.$neg_color.'">'.$textFalse.'</font>';
	return ($bool) ? '<font color="'.$pos_color.'"><i class="fas fa-toggle-on" title="'.$titleTrue.'"></i> '.$textTrue .'</font>':'<font color="'.$neg_color.'"><i class="fas fa-toggle-off" title="'.$titleFalse.'"></i> '.$textFalse .'</font>';
	//$checked = ($bool) ? "checked":"";
	//$text = ($bool) ? $textTrue:$textFalse;
	//$title = ($bool) ? $titleTrue:$titleFalse;
	//return '<div class="custom-control custom-switch">
  //<input type="checkbox" title="'.$title.'" class="custom-control-input" '.$checked.' id="customSwitch1">
  //<label class="custom-control-label" for="customSwitch1">'.$text.'</label>
//</div>';
}
function lockunlock($bool, $titleTrue='', $titleFalse='', $textTrue='', $textFalse='', $h='20', $w='20') {
	global $pos_color, $neg_color;
	return ($bool) ? '<font color="'.$pos_color.'"><i class="fas fa-lock" title="'.$titleTrue.'"></i></font>':'<font color="'.$neg_color.'"><i class="fas fa-lock-open" title="'.$titleFalse.'"></i></font>';
}
function warning($title='', $h='20', $w='20') {
	return '<img src="images/static/warning.png" title="'.$title.'" height="'.$h.'" width="'.$w.'">';
}
function actionButton($type, $title='', $h='30', $w='30') {
	global $pos_color, $neg_color;
	$src = "";
	switch ($type) {
		case "edit":
			$src = "<font color='".$pos_color."''><i class='fas fa-pencil-alt' title='Edit'></i>";
			break;
		case "delete":
			$src = "<font color='".$neg_color."'><i class='fas fa-trash' title='Delete'></i>";
			break;
		case "question":
			$src = "<i class='fas fa-question'></i>";
			break;
		default:
			return false;
	}
	//return "<img style='background: none; border: none;' title='$title' height='$h' width='$w' class='img-circle' $src>";
	return $src;
}

function error_header($code, $message, $tracking='not applicable') {
    header("Status: {$code}");
    echo json_encode(array(
        'description'=> $message,
        'trackingId' => $tracking
    ));
    die();
}

function verify() {
	return ($_SESSION['status'] == "logged") ? true : false;
}
function issetor(&$var, $default = "") {
	return isset($var) ? $var : $default;
}
function statusdigger($nested) {
	$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($nested));
	$keys = array();
	foreach ($iterator as $key => $value) {
		// Build long key name based on parent keys
		for ($i = $iterator->getDepth() - 1; $i >= 0; $i--) {
			$key = $iterator->getSubIterator($i)->key() . '.' . $key;
		}
		$keys[] = $key .': <b>'. $value . '</b>';
	}
	return implode('<br>', $keys);
}
function removeHTML($str) {
	return preg_replace('/\s+/', ' ', str_replace(['<','>'], ' ', $str));
}

//CARD GENERIC/FEATURE ACTIONS

//DRAW A CARD
function cardDraw($identifier) {
    global $rootdoc;
    include $rootdoc.'/public/includes/cards.inc.php';
    return json_decode($cardDeck[$identifier], true);
}

function cardSwitchStyle($style) {
    switch ($style) {
        case "warning":
            return "attention";
            break;
        case "danger":
            return "warning";
            break;
        case "success":
            return "good";
            break;
        case "primary":
            return "accent";
            break;
        case "info":
            return "accent";
            break;
        default: 
            return $style;
            break;
    }
}

function cardWhoisSearchResult($items, $meta = False) {
	
	//WHOIS RESULT CARD
	$numResult = count($items);
	
	if (!$numResult) {
		return "No result";
	}
	
	$card = cardDraw('emptyCard');
	
	
	if ($numResult == 1) {
		
		$resultItem = cardDraw('whoisProfile');
		$resultItem['columns'][0]['items'][0]['url'] = issetor($items[0]['avatar'], 'http://notfound.com');
		$resultItem['columns'][0]['items'][0]['selectAction']['url'] = issetor($items[0]['avatar'], 'http://notfound.com');
		$resultItem['columns'][1]['items'][0]['text'] = $items[0]['displayName'];
		$resultItem['columns'][1]['items'][1]['text'] = $items[0]['emails'][0];
		$resultItem['columns'][1]['items'][2]['actions'][0]['data'] = array(
				"wbm_command" => "whois " .$items[0]['emails'][0] . " meta"
		);
		
		$card['body'][] = $resultItem;
		
		if ($meta) {
			return codeBlock(json_encode($items[0], JSON_PRETTY_PRINT));
		}
		
	} else {
		
		$headlineBox = cardDraw('textBlock');
		$headlineBox['size'] = "ExtraLarge";
		$headlineBox['color'] = "Accent";
		$headlineBox['text'] = "Search Result";
		$card['body'][] = $headlineBox;
		
		for ($i = 0; $i < $numResult; $i++) {
			
			$resultItem = cardDraw('whoisSearchResult');
			
			$resultItem['columns'][0]['items'][0]['text'] = $i+1 . '.';
			$resultItem['columns'][1]['items'][0]['text'] = $items[$i]['displayName'];
			$resultItem['columns'][1]['items'][1]['text'] = $items[$i]['emails'][0];
			$resultItem['selectAction']['data'] = array("wbm_command" => "whois ".$items[$i]['emails'][0]);
			
			$card['body'][] = $resultItem;
			
			if ($i == 19) {
				break;
			}
			
		}
		
	}
	
	
	$card = cardPrepareCard($card);
	
	return array("markdown" => "## Sorry\n\n Your client does not support cards and are unable to render card results. Type: **whois [email] meta** for string output", "attachments" => $card);
	
}

function cardProfileBox($headline, $text) {
    
    $card = cardDraw('emptyCard');
    $columnSet1 = $columnSet2 = cardDraw('columnSet');
    $column1 = $column2 = cardDraw('column');  
    $textblock1 = $textblock2 = cardDraw('textBlock');
    
    $textblock1['text'] = $headline;
    $textblock1['size'] = "Large";
    $textblock1['weight'] = "Bolder";
    $textblock1['color'] = "Accent";
    
    $textblock2['text'] = $text;
    
    $column1['items'][] = $textblock1;
    $column2['items'][] = $textblock2;
    
    $columnSet1['columns'][] = $column1;
    $columnSet2['columns'][] = $column2;
    
    $card['body'][] = $columnSet1;
    $card['body'][] = $columnSet2;
    
    $card = cardPrepareCard($card); 
    
    return array("markdown" => "## $headline\n\n $text", "attachments" => $card);
}

function cardMessageBox($input, $class) {
   
    
   $class = cardSwitchStyle($class);
    
   
   $card = cardDraw('emptyCard');
   $columnSet = cardDraw('columnSet');
   $column1 = $column2 = cardDraw('column');
   $textblock = cardDraw('textBlock');
   
   $textblock['text'] = $input;
   $column1['style'] = $class;
   $column1['width'] = "auto";
   $column2['items'][] = $textblock; 
   
   $columnSet['columns'][] = $column1;
   $columnSet['columns'][] = $column2;
   
   $card['body'][] = $columnSet;
   
   return cardPrepareCard($card); 
  
}

function cardPrepareCard($card) {
    $cardmanifest = array(
        "contentType" => "application/vnd.microsoft.card.adaptive",
        "content" => $card
    );
    return $cardmanifest;
}

//CLASSES
class Cards {
    public $cardDeck;
    
    public function __construct() {
       $this->cardDeck = array(
           'new_card'=>array('type'=>'AdaptiveCard', '$schema'=>'http://adaptivecards.io/schemas/adaptive-card.json', 'version'=>'1.2'),
           'textblock'=>array('type'=>'TextBlock'),
           'columnset'=>array('type'=>'ColumnSet'),
           'column'=>array('type'=>'Column'),
           'image'=>array('type'=>'Image'),
           'container'=>array('type'=>'Container'),
           'actionset'=>array('type'=>'ActionSet'),
           'submit'=>array('type'=>'Action.Submit'),
           'show'=>array('type'=>'Action.ShowCard'),
           'open'=>array('type'=>'Action.OpenUrl'),
           'textinput'=>array('type'=>'Input.Text'),
           'choiceset'=>array('type'=>'Input.ChoiceSet'),
           'time'=>array('type'=>'Input.Time'),
           'toggle'=>array('type'=>'Input.Toggle'),
           'number'=>array('type'=>'Input.Number'),
           'factset'=>array('type'=>'FactSet')            
       );
    }
    
    public function cardNew($headline, $color='accent') {
        return $this->cardItem('new_card', [
            'body' => [
                $this->cardItem('container', [
                    'style' => $color,
                    'spacing' => 'None',
                    'minHeight' => '5px',
                    'bleed' => true,
                    'items' => [
                        $this->cardItem('columnset', [
                            'columns'=> [
                                $this->cardItem('column', [
                                    'items'=>[
                                        $this->cardItem('textblock', [
                                            'text' => $headline,
                                            'weight' => 'bolder'
                                        ])
                                    ]
                                ]),
                            ]
                        ])
                    ]
                ])
            ]
        ]);
    }
    
    public function cardSwitchStyle($style) {
        $styles = [
            'warning' => 'warning', 
            'error' => 'attention',
            'danger' => 'attention',
            'success' => 'good',
            'primary' => 'accent',
            'info' => 'accent',
            'default' => 'accent'
        ];
             
        return (in_array($style, $styles)) ? $style : ((array_key_exists($style, $styles)) ? $styles[$style] : $styles['default']);
    }
    
    
    public function card_prepare($card) {
        $cardmanifest = array(
            "contentType" => "application/vnd.microsoft.card.adaptive",
            "content" => $card
        );
        return $cardmanifest;
    }
    
    public function load($type) {
        return $this->cardDeck[$type];
    }
    
    public function cardItem($type, array $kwargs=array()) {
        return array_merge($this->load($type), $kwargs);
    }
    
    //Pre-defined cards
    
    public function msgCard($text, array $kwargs = array()) {
               
        $style = (isset($kwargs['style'])) ? $kwargs['style'] : 'info';   
        $color = $this->cardSwitchStyle($style);
        
        $card = $this->cardItem('new_card', [
            "body" => [
                $this->cardItem('container', [
                    'style' => $color,
                    'spacing' => 'None',
                    'minHeight' => '5px',
                    'bleed' => true,
                    'items' => [
                        $this->cardItem('columnset', [
                            'columns'=> [
                                $this->cardItem('column', [
                                    'items'=>[
                                        $this->cardItem('textblock', [
                                            'text' => (isset($kwargs['headline'])) ? $kwargs['headline'] : ucfirst($style),
                                            'weight' => 'bolder'
                                        ]) 
                                    ]
                                ]),    
                            ]
                        ])         
                    ]                    
               ]),
               $this->cardItem('textblock', [
                   'text' => $text,
                   'wrap' => true
               ])
           ]
     ]);
        
     if (isset($kwargs['extra'])) {
          $card['body'] = array_merge($card['body'], $kwargs['extra']);  
     }
        
     return $this->card_prepare($card);
        
   }
   
   public function featureAdminCard($level, array $kwargs = array()) {
       //Admin control main card
       $card = $this->cardItem('new_card', [
           'body' => [
               $this->cardItem('textblock', [
                   'text' => 'Admin controls',
                   'size' => 'large',
                   'weight' => 'bolder'
               ])
           ]
       ]);
       
       //Admin control parts
       
       $userManagement = $this->cardItem('container', [
           'items' => [
               $this->cardItem('textblock', [
                   'text' => 'User Management',
                   'weight' => 'bolder'
               ]),
               $this->cardItem('textblock', [
                   'text' => 'Type in one or more e-mail adresses and click the preferred operation below.',
                   'wrap' => true
               ]),
               $this->cardItem('textinput', [
                   'id' => 'emails',
                   'placeholder' => 'E-mail(s) CSV'
               ]),
               $this->cardItem('actionset', [
                   'spacing' => 'none',
                   'actions' => [
                       $this->cardItem('submit', [
                           'title' => 'Add',
                           'data' => ['wbm_command' => 'admin user add']
                       ]),
                       $this->cardItem('submit', [
                           'title' => 'Delete',
                           'data' => ['wbm_command' => 'admin user delete']
                       ]),
                       $this->cardItem('submit', [
                           'title' => 'List', 
                           'data' => ['wbm_command' => 'admin user list']
                       ]),
                       $this->cardItem('submit', [
                           'title' => 'Check',
                           'data' => ['wbm_command' => 'admin user check']
                       ])
                   ]
               ]),
               $this->cardItem('actionset', [
                   'spacing' => 'none',
                   'actions' => [
                       $this->cardItem('submit', [
                           'title' => 'Block',
                           'data' => ['wbm_command' => 'admin block']
                       ]),
                       $this->cardItem('submit', [
                           'title' => 'Unblock',
                           'data' => ['wbm_command' => 'admin unblock']
                       ])
                   ]
               ])
           ]
       ]);
              
       $spaceManagement = "";
       $groupManagement = "";
       $feedbackManagement = "";
       $messageManagement = "";
       $generic = "";
       
       switch ($level) {
           case 'user':
               $card['body'][] = $userManagement; 
               break;
           default: 
               $card['body'][] = $userManagement;
               $card['body'][] = $spaceManagement;
               $card['body'][] = $feedbackManagement;
               $card['body'][] = $messageManagement;
               $card['body'][] = $generic;
               break;
       }
       
       return $this->card_prepare($card); 
       
   }
     
   public function featurePlaceCard(array $kwargs = array()) {
       //Card flow for the "place" feature
       $card = $this->cardItem('new_card', [
           'body' => [
               $this->cardItem('textblock', [
                   'text' => 'Workspace actions',
                   'size' => 'large',
                   'weight' => 'bolder'
               ])
           ]
       ]);
       
       //Place initial actions
       
       $place_actions = $this->cardItem('container', [
           'items' => [
               $this->cardItem('textblock', [
                   'text' => 'Create or search for a workspace'
               ]),
               $this->cardItem('textinput', [
                   'id' => 'input',
                   'placeholder' => 'Workspace displayName'
               ]),
               $this->cardItem('actionset', [
                   'spacing' => 'none',
                   'actions' => [
                       $this->cardItem('submit', [
                           'title' => 'Create & Activate',
                           'data' => ['wbm_command' => 'place activate']
                       ]),
                       $this->cardItem('submit', [
                           'title' => 'Create',
                           'data' => ['wbm_command' => 'place create']
                       ]),
                       $this->cardItem('submit', [
                           'title' => 'Search',
                           'data' => ['wbm_command' => 'place list']
                       ])
                   ]
               ])
              
           ]
       ]);
       
       $card['body'][] = $place_actions;
        
       return $this->card_prepare($card); 
       
   }
        
}

class LDAP{
	//Credit goes to https://www.veritech.net/simple-ldap-class-for-php/
	//Modified to fit needs
	function connect($server,$port){

		$connection = ldap_connect($server,$port);  // must be a valid LDAP server!	
		ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($connection, LDAP_OPT_REFERRALS, 0);

		return $connection;
	}

	function bind($connection,$basedn,$basepass){

		$ldaprdn  = $basedn;    // ldap rdn or dn 
		$ldappass = $basepass;  // associated password
		$bind = ldap_bind($connection, $ldaprdn, $ldappass);

		return $bind;
	}

	function search($connection, $searchdn, $filter, $attributes = array()){

		$pageSize = 1000;

		 $cookie = '';
		 do {
			 ldap_control_paged_result($connection, $pageSize, true, $cookie);

			 $result  = ldap_search($connection, $searchdn, $filter, $attributes);
			 $entries = ldap_get_entries($connection, $result);
			
			 return $entries;

			 ldap_control_paged_result_response($connection, $result, $cookie);
		   
		 } while($cookie !== null && $cookie != '');
	}
	function close($connection){
		echo '<hr><br>';
	    echo "Closing connection";
	    ldap_close($connection);
	}

}
class Db {
	protected static $connection;
	
	public function connect() {
		if(!isset(self::$connection)) {
		    global $config;
			self::$connection = new mysqli($config['dbhost'],$config['username'],$config['password'],$config['dbname']);
		}
		if(self::$connection === false) {
			return false;
		}
		return self::$connection;
	}
	public function query($query, $last_id=false) {
		$connection = $this -> connect();
		if ($last_id) {
			$connection -> query($query);
			return mysqli_insert_id($connection);
		}
		if ($result = $connection -> query($query)) {
			return $result;
		}
		else
			return $this -> error();
	}
	public function select($query) {
		$rows = array();
		$result = $this -> query($query);
		if($result === false) {
			return false;
		}
		while ($row = $result -> fetch_assoc()) {
			$rows[] = $row;
		}
		return $rows;
	}
	
	public function select_specific($query) {
		$row = array();
		$result = $this -> query($query);
		if($result === false) {
			return false;
		}
		$row = $result -> fetch_assoc();
		return $row;
	}
	
	public function error() {
		$connection = $this -> connect();
		return $connection -> error;
	}
	
	//Discontinue
	public function errorHandler($boolean){
		if ($boolean === true) {
			return msgList('success-Generic');
		}
		else {
			return msgList('alert-Generic');
		}
	}
	
	public function quote($value) {
		$connection = $this -> connect();
		return "" . $connection -> real_escape_string($value) . "";
	}
	
	public function quoteArray($array) {
		foreach ($array as $k => $v) {
			$array[$k] = (is_string($array[$k])) ? $this->quote($array[$k]):$array[$k];
		}
		return $array;
	}
	
	public function constructInsertValues($table, $data_array) {
		return "INSERT INTO {$table} (`".implode("`, `", array_keys($data_array))."`) VALUES ('".implode("', '", $data_array)."')";
	}
	
	public function constructUpdateValues($table, $data_array, $clause) {
		$result = "UPDATE {$table} SET ";
		$last_key = key(array_slice($data_array, -1, true));
		foreach($data_array as $key => $value) {
			$result .= "{$key} = '{$value}'";
			if ($key != $last_key) $result .= ",";
		}
		$result .= " {$clause}";
		return $result;
	}
	//Debug
	
	public function debugIsEnabled() {
		$status = $this->select_specific("SELECT debug_mode FROM service_status"); 
		return $status['debug_mode'];
	}
	
	public function debugClear() {
	    return $this->query("DELETE FROM debug_log");
	}
	
	public function debugEnable() {
	    return $this->query("UPDATE service_status SET debug_mode = '1'");
	}
	
	public function debugDisable() {
	    return $this->query("UPDATE service_status SET debug_mode = '0'");
	}
	
	public function debugLog(array $args = array()) {
		$filter_valid = array('user', 'type', 'request', 'response', 'bot', 'description', 'customfield1', 'customfield2', 'customfield3', 'customfield4', 'customfield5');
		$insertvalues = array();
		foreach ($filter_valid as $k => $v) {
			if (isset($args[$v])) {
				$insertvalues[$v] = $args[$v];
			}
		}
		if (count($insertvalues)) {
			$insertvalues['user'] = (issetor($insertvalues['user'])) ? $insertvalues['user'] : ((issetor($_SESSION['email'])) ? $_SESSION['email']:'unknown');
			return $this->query($this->constructInsertValues('debug_log', $insertvalues));
		}	
	}
	
	//Accepted domains
	public function acceptedDomainAdd($data) {
		$data['domain'] = strtolower($data['domain']);
		return $this->query($this->constructInsertValues("domains", $data), true);
	}
	public function acceptedDomainDelete($id) {
		$this->query("DELETE FROM domains WHERE id = '$id'");
		$this->query("DELETE FROM bot_allowed_domain WHERE domainid = '$id'");
	}
	public function acceptedDomainFetch($domainid='') {
		return (empty($domainid)) ? $this->select("SELECT * FROM domains") : $this->select("SELECT * FROM domains WHERE id = '{$domainid}'") ;
	}
	//Group handlers
	public function groupAdd($data) {
		$data['subscribable'] = intval($data['subscribable']);
		$data['default_group'] = intval($data['default_group']);
		if(empty($data['id'])) { unset($data['id']); }
		return $this->query($this->constructInsertValues("groups", $data));
	}
	public function groupRemove($groupid) {
		$groupinfo = $this->groupFetchGroups($groupid);
		if (count($groupinfo)>0) {
			//$this->query("UPDATE response SET accessgroup = '0' WHERE accessgroup = '{$groupinfo[0]['id']}'");
			$this->query("UPDATE bot_webhook SET groupid = '0' WHERE groupid = '$groupid'");
			$e = $this->query("DELETE FROM groups WHERE id = '{$groupid}' or sub_id = '{$groupid}'");
			$e .= $this->query("DELETE FROM group_contacts WHERE groupid = '{$groupinfo[0]['id']}'");
			$e .= $this->query("DELETE FROM space_contacts WHERE groupid = '{$groupinfo[0]['id']}'");
			$e .= $this->query("DELETE FROM group_groups WHERE groupid = '{$groupinfo[0]['id']}' or nestedid = '{$groupinfo[0]['id']}'");
			return $e;
		} else return "failed";
	}
	public function groupUpdate($data) { //Update group details
		$table = "groups";
		$group_data = $data['groupname'];
		$updateid = $data['id'];
		$clause = "WHERE id = '{$updateid}'";
		if (isset($data['botid']) and !empty($data['botid'])) {
			if ($this->groupCheckHasLinkedGroups($updateid)) {
				return false;
			}
		} elseif (!isset($data['botid']) or empty($data['botid'])) {
			if ($this->groupCheckIfGroupHasSpaceMember($updateid)) {
				return false;
			}
		}
		$this->query($this->constructUpdateValues($table, $data, $clause));
		return true;
	}
	//Adds a user to a local group
	public function groupAddContact($data) { //groupid & contactid
		return $this->query($this->constructInsertValues("group_contacts", $data));
	}
	//Adds a space to a local group
	public function groupAddSpace($data) { //groupAddSpace(array('groupid' => $groupid, 'spaceid' => $spaceid, 'botid' => $botid));
		$table = "group_spaces";
		return $this->query($this->constructInsertValues($table, $data));
	}
	//Adds a group to a local group (nested membership)
	public function groupAddGroup($data) { //groupid & nestedid
		$table = "group_groups";
		return $this->query($this->constructInsertValues($table, $data));
	}
	//Removes a contact from a group
	public function groupRemoveContact($groupid, $contactid) {
		return $this->query("DELETE FROM group_contacts WHERE groupid = '{$groupid}' and contactid = '{$contactid}'");
	}
	//Removes a space from a group
	public function groupRemoveSpace($groupid, $spaceid, $botid) {
		return $this->query("DELETE FROM group_spaces WHERE groupid = '{$groupid}' and spaceid = '{$spaceid}' and botid = '{$botid}'");
	}
	//Removes a group from a group
	public function groupRemoveGroup($groupid) {
		return $this->query("DELETE FROM group_groups WHERE groupid = '{$groupid}'");
	}
	//Returns the number of total members for a group id (including nested groups)
	public function groupMembershipNumber($groupid) {
		return count($this->groupGetMembers($groupid));
	}
	//Returns the user members for a group id
	public function groupUserMembershipNumber($groupid) {
		return count($this->select("SELECT * FROM group_contacts WHERE groupid = '{$groupid}'"));
	}
	//Returns the number of spaces for a group that belongs to a particular bot
	public function groupSpaceMembershipNumber($groupid, $botid) {
		return count($this->select("SELECT * FROM group_spaces WHERE groupid = '{$groupid}' and botid = '{$botid}'"));
	}
	//Get all (including linked) user members of a particular local group
	public function groupGetMembers($groupid) {
		//Fetch the groups that are member of this groupid
		$extra_options = "";
		$membergroups = $this->groupGetGroupMembers($groupid);
		if (count($membergroups)) {
			foreach ($membergroups as $key => $value) {
				$extra_options .=  "or groupid = '{$value['nestedid']}' ";
			}
		}
		//Fetch the users that are member of this groupid
		$list = $this->select("SELECT DISTINCT contactid FROM group_contacts WHERE groupid = '{$groupid}' $extra_options");
		//Fetch the members of the groups linked to this group id
		
		return $list;
	}
	//ID not alias - generates an array of member IDs of a group
	public function groupGetMemberIdArray($groupid) {
		$members = $this->groupGetMembers($groupid);
		$returnvalue = [];
		if (count($members)) {
			foreach ($members as $key => $value) {
				$returnvalue[] = $value['contactid'];
			}
		}
		return $returnvalue;
	}
	//Get actual user members of a particular local group
	public function groupGetActualMembers($groupid) {
		return $this->select("SELECT contactid FROM group_contacts WHERE groupid = '{$groupid}'");
	}
	//Get space members of a particular local group
	public function groupGetSpaceMembers($groupid, $botid) {
		return $this->select("SELECT spaceid FROM group_spaces WHERE groupid = '{$groupid}' and botid = '{$botid}'");
	}
	public function groupGetSpaceMemberIdArray($groupid, $botid) {
		$spacemembers = $this->groupGetSpaceMembers($groupid, $botid);
		$returnvalue = [];
		if (count($spacemembers)) {
			foreach ($spacemembers as $key => $value) {
				$returnvalue[] = $value['spaceid'];
			}
		}
		return $returnvalue;
	}
	//Get member (nested) groups of a particular local group
	public function groupGetGroupMembers($groupid) {
		return $this->select("SELECT nestedid FROM group_groups WHERE groupid = '{$groupid}'");
	}
	//Check if the group has other groups linked
	public function groupCheckHasLinkedGroups($groupid) {
		return (count($this->select("SELECT nestedid FROM group_groups WHERE nestedid = '{$groupid}'"))) ? true:false;
	}
	//Return groupname of the group the contact is linked from
	public function groupCheckContactGroupLink($groupid, $contactid) {
		$linked_groups = $this->groupGetGroupMembers($groupid);
		foreach ($linked_groups as $key => $value) {
			if ($this->groupCheckIfActualMember($contactid, $value['nestedid'])) {
				$group_details = $this->groupFetchGroups($value['nestedid']);
				return $group_details;
			}
		}
		return False;
	}
	//Check if a contact is member of a particular group
	public function groupCheckIfMember($contactid, $groupid) {
		$membership = $this->groupGetMembers($groupid);
		if (validateEmail($contactid)) {
			$userinfo = $this->contactFetchContacts($contactid);
			if (count($userinfo)) {
				$contactid = $userinfo[0]['id'];
			} else {
				return false;
			}
		}
		return (in_array($contactid, array_column($membership, 'contactid'))) ? true : false;
	}
	//Check if a contact is member of a particular group
	public function groupCheckIfActualMember($contactid, $groupid) {
		return (count($this->select("SELECT contactid FROM group_contacts WHERE groupid = '{$groupid}' and contactid = '{$contactid}'"))>0) ? true:false;
	}
	//Check if a space is part of a particular group
	public function groupCheckIfSpaceMember($spaceid, $groupid, $botid) {
		$membership = $this->select("SELECT * FROM group_spaces WHERE (groupid = '{$groupid}' and spaceid = '{$spaceid}' and botid = '{$botid}')");
		return (count($membership)>0) ? true : false;
	}
	public function groupCheckIfGroupHasSpaceMember($groupid) {
		return (count($this->select("SELECT * FROM group_spaces WHERE groupid = '$groupid'"))) ? true : false;
	}
	public function groupGetGroupOwner($botid='0') {
		return $this->select("SELECT * FROM groups WHERE botid = '$botid'");
	}
	public function groupFetchGroups($groupid="") {
		return (!empty($groupid)) ? $this->select("SELECT * FROM groups WHERE id = '{$groupid}' or sub_id = '{$groupid}'") : $this->select("SELECT * FROM groups ORDER BY groupname ASC");
	}
	//Returns groups that can be subscribed to
	public function groupFetchSubscriptionGroups($groupid="") {
		return (!empty($groupid)) ? $this->select("SELECT * FROM groups WHERE (id = '{$groupid}' or sub_id = '{$groupid}') and subscribable = '1'") : $this->select("SELECT * FROM groups WHERE subscribable = '1' ORDER BY groupname ASC");
	}
	public function groupFetchDefaultGroups() {
		return $this->select("SELECT * FROM groups WHERE default_group = '1' ORDER BY groupname ASC");
	}
	//Cards (feature)
	
	//Add a card
	public function cardAdd($data, $last_id=false) {
		if(empty($data['datacode'])) { $data['datacode'] = ''; }
		if(empty($data['accessgroup'])) { $data['accessgroup'] = 0; }
		return $this->query($this->constructInsertValues("cards", $this->cardFilterArray($data)), $last_id);
	}
	
	//Update a card
	public function cardUpdate($data, $cardid) {
		$clause = "WHERE id = '{$cardid}'";
		if(empty($data['datacode'])) { $data['datacode'] = ''; }
		if(empty($data['accessgroup'])) { $data['accessgroup'] = 0; }
		return $this->query($this->constructUpdateValues("cards", $this->cardFilterArray($data), $clause));
	}
	
	//Fetch cards
	public function cardFetchCards($cardid="") {
		return ($cardid) ? $this->select("SELECT * FROM cards WHERE id = '{$cardid}'") : $this->select("SELECT * FROM cards ORDER BY title ASC");
	}
	
	//Fetch cards
	public function cardFetchBotCards($botid) {
		return $this->select("SELECT * FROM cards WHERE botid = '{$botid}' ORDER BY title ASC");	
	}
	
	public function cardAPIGetData($cardId) {
		$returnvalue = array();
		$cardInfo = $this->cardFetchCards($cardId);
		if (count($cardInfo)) {
			unset($cardInfo[0]['card_body']);
			$returnvalue['card'] = $cardInfo[0];
			
			$cardData = $this->cardFetchCardData($cardId);
			$returnvalue['items'] = array();
			
			if (count($cardData)) {
				foreach ($cardData as $key => $value) {
					$item = array(
							"personId" => $value['person_id'],
							"personEmail" => $value['person_email'],
							"displayName" => $this->contactGetName($value['person_email']),
							"submitData" => json_decode($value['input_data']),
							"created" => $value['created']
					);
					$returnvalue['items'][] = $item;
				}
			} 
		} else $returnvalue['error'] = "Card was not found";
		
		return $returnvalue;
	}
	
	public function cardAPIGetCards($botid="") {
		$returnvalue = array();
		$returnvalue['items'] = array(); 
		$cardInfo = ($botid) ? $this->cardFetchBotCards($botid) : $this->cardFetchCards();
		if (count($cardInfo)) {
			foreach ($cardInfo as $key => $value) {
				unset($value['card_body']);
				$value['botDisplayName'] = $this->botGetName($value['botid']); 
				$returnvalue['items'][] = $value;
			}
		} else $returnvalue['error'] = "No cards found";
		
		return $returnvalue;
	}
	
	//Filter array
	public function cardFilterArray($data) {
		$newData = array();		
		$filter = array('botid', 'userid', 'title', 'delete_active', 'card_body', 'reply', 'input_active', 'update_active', 'echo_active', 'redirect_reply', 'accessgroup', 'datacode');
			
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = $data[$v];
			}
		}
		
		return $newData;
	}
	
	//Delete card and card data
	public function cardDelete($cardid) {
		$this->query("DELETE FROM cards WHERE id = '{$cardid}'");
		$this->query("DELETE FROM card_data WHERE card_id = '{$cardid}'");
		$this->query("UPDATE saved_messages SET card_attachment = '0' WHERE card_attachment = '$cardid'");
		$this->query("UPDATE response SET card_id = '0' WHERE card_id = '$cardid'");
	}
	
	//Insert card data from submission [card_id, person_id, botid, person_email, input_data, created]
	public function cardDataCollectAdd($data) {
		return $this->query($this->constructInsertValues("card_data", $data));
	}
	
	//Update card data from submission [card_id, person_id, botid, person_email, input_data, created]
	public function cardDataCollectUpdate($data) {
		$clause = "WHERE card_id = '{$data['card_id']}' and person_id = '{$data['person_id']}'";
		return $this->query($this->constructUpdateValues("card_data", $data, $clause));
	}
	
	//Fetch card data
	public function cardFetchCardData($cardid, $personId = "") {
		return (!empty($personId)) ? $this->select("SELECT * FROM card_data WHERE card_id = '$cardid' and person_id = '$personId'") : $this->select("SELECT * FROM card_data WHERE card_id = '$cardid'");
	}
	
	//Fetch card data
	public function cardDeleteDataEntry($cardId, $personId) {
		return $this->query("DELETE from card_data WHERE card_id = '$cardId' and person_id = '$personId'");
	}
	
	//Fetch card data
	public function cardDeleteData($cardId) {
		return $this->query("DELETE from card_data WHERE card_id = '$cardId'");
	}
	
	//Feedback (feature)
	//Add a feedback topic
	public function feedbackTopicAdd($data, $last_id=false) {
		$data['accessgroup'] = intval($data['accessgroup']);
		return $this->query($this->constructInsertValues("feedback_topic", $this->feedbackFilterArray($data, 'topic')), $last_id);
	}
	//Updates a feedback topic
	public function feedbackTopicUpdate($data, $topicid) {
		$data['accessgroup'] = intval($data['accessgroup']);
		$clause = "WHERE id = '{$topicid}'";
		return $this->query($this->constructUpdateValues("feedback_topic", $this->feedbackFilterArray($data, 'topic'), $clause));
	}
	//Deletes many feedback topics - all entries, votes and comments based on bot owner
	public function feedbackBotTopicsDelete($botid) {
		$topics = $this->select("SELECT * FROM feedback_topic WHERE botid = '$botid'");
		if (count($topics)>0) {
			foreach ($topics as $key => $value) {
				$entries = $this->feedbackFetchTopicEntries($value['id']);
				if (count($entries)>0) {
					foreach ($entries as $key1 => $value1) {
						$this->feedbackEntryDelete($value1['id']);
					}
				}
				$this->query("DELETE FROM feedback_topic WHERE id = '{$value['id']}'");
			}
		}
	}
	//Deletes a feedback topic - all entries, votes and comments
	public function feedbackTopicDelete($topicid) {
		$this->query("DELETE FROM feedback_topic WHERE id = '{$topicid}'");
		$entries = $this->feedbackFetchTopicEntries($topicid);
		foreach ($entries as $key => $value) {
			$this->feedbackEntryDelete($value['id']);
		}
	}
	//Check if operation is allowed
	public function feedbackTopicCheckAllowList($action, $topic) {
		return ($topic[0][$action]) ? true : false;
	}
	//Check access group
	public function feedbackTopicCheckAccessGroup($user, $topic) {
		$groupid = $topic[0]['accessgroup'];
		if ($groupid) {
			$contact = $this->contactFetchContacts($user);
			if (count($contact) > 0) {
				$contactid = $contact[0]['id'];
				return ($this->groupCheckIfMember($contactid, $groupid)) ? true : false;
			} else return false;
		}
		return true;
	}
	//Filter array
	public function feedbackFilterArray($data, $type) {
		$newData = array();
		switch ($type) {
			case "topic":
				$filter = array('botid', 'title', 'comments_allowed', 'votes_allowed', 'entry_create_allowed', 'entry_view_allowed', 'entry_delete_allowed', 'accessgroup');
				break;
			case "entry":
				$filter = array('description', 'topic_id', 'created_by');
				break;
		}
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = $data[$v];
			}
		}
		return $newData;
	}
	//Add a feedback entry
	public function feedbackEntryAdd($data, $last_id=false) {
		$data = $this->quoteArray($data);
		$q = $this->constructInsertValues("feedback_entries", $this->feedbackFilterArray($data, 'entry'));
		return $this->query($q, $last_id);
	}
	//Update a feedback entry
	public function feedbackEntryUpdate($data, $entryid) {
		$data = $this->quoteArray($data);
		$clause = "WHERE id = '{$entryid}'";
		return $this->query($this->constructUpdateValues("feedback_entries", $this->feedbackFilterArray($data, 'entry'), $clause));
	}
	//Delete feedback entries
	public function feedbackEntryDelete($entryid) {
		$this->query("DELETE FROM feedback_entries WHERE id = '{$entryid}'");
		$this->query("DELETE FROM feedback_entry_vote WHERE entry_id = '{$entryid}'");
		$this->query("DELETE FROM feedback_entry_comment WHERE entry_id = '{$entryid}'");
	}
	//Fetch topic entries
	public function feedbackFetchTopicEntries($topicid="") {
		return ($topicid) ? $this->select("SELECT * FROM feedback_entries WHERE topic_id = {$topicid}") : $this->select("SELECT * FROM feedback_entries");
	}
	//Fetch entry details
	public function feedbackFetchEntry($entryid) {
		return $this->select("SELECT * FROM feedback_entries WHERE id = '{$entryid}'");
	}
	//Fetch topics
	public function feedbackFetchTopics($botid, $topicid="") {
		return (!empty($topicid)) ? $this->select("SELECT * FROM feedback_topic WHERE botid = '{$botid}' and id = '{$topicid}'") : $this->select("SELECT * FROM feedback_topic WHERE botid = '{$botid}' ORDER BY title ASC");
	}
	//Fetch topics
	public function feedbackFetchAllTopics() {
	    return $this->select("SELECT * FROM feedback_topic ORDER BY title ASC");
	}
	//Add vote to entry
	public function feedbackEntryVote($email, $entryid) {
		return $this->query($this->constructInsertValues('feedback_entry_vote', array('entry_id'=>$entryid, 'email'=>$email)));
	}
	//Remove vote from entry
	public function feedbackEntryVoteDelete($id="", $email="", $entryid="") {
		if ($id) {
			return $this->query("DELETE FROM feedback_entry_vote WHERE id = '{$id}'");
		} elseif ($email and $entryid) {
			return $this->query("DELETE FROM feedback_entry_vote WHERE email = '{$email}' and entry_id = {$entryid}");
		} else return;
	}
	//Fetch entry votes
	public function feedbackFetchEntryVotes($entryid="") {
		return (!empty($entryid)) ? $this->select("SELECT * FROM feedback_entry_vote WHERE entry_id = '{$entryid}'") : $this->select("SELECT * FROM feedback_entry_vote");
	}
	//Check if vote exist
	public function feedbackFetchEntryVoteExists($entryid, $email) {
		return (count($this->select("SELECT * FROM feedback_entry_vote WHERE entry_id = '$entryid' and email = '$email'"))) ? true : false;
	}
	//Add comment to entry
	public function feedbackEntryComment($entryid, $comment, $email, $last_id=false) {
		return $this->query($this->constructInsertValues('feedback_entry_comment', array('entry_id'=>$this->quote($entryid), 'email'=>$email, 'comment'=>removeHTML($this->quote($comment)))), $last_id);
	}
	//Delete a comment by id
	public function feedbackEntryCommentDelete($commentid) {
		return $this->query("DELETE FROM feedback_entry_comment WHERE id = '{$commentid}'");
	}
	//Fetch entry comments
	public function feedbackFetchEntryComments($entryid="") {
		return (!empty($entryid)) ? $this->select("SELECT * FROM feedback_entry_comment WHERE entry_id = '{$entryid}'") : $this->select("SELECT * FROM feedback_entry_comment");
	}
	//Fetch entry comments
	public function feedbackFetchEntryComment($commentid) {
		return $this->select("SELECT * FROM feedback_entry_comment WHERE id = '{$commentid}'");
	}
	//Feedback API
	public function feedbackAPI($botId = "", $topicId = "", $entryId = "", $commentId = "") {
	    
	    $returnvalue = array(
	        
	        "bots" => array()
	        
	    );
	    
	    $bots = $this->botFetchBots($botId); 
        $entries = array();
        
        foreach ($bots as $k => $v) {
            
            if ($v['type'] == "bot") {
                
                $bot = array(
                    'botId' => $v['id'],
                    'name' => $v['displayName'],
                    'email' => $v['emails'],
                    'topics' => array()
                );
                
                $topics = $this->feedbackFetchTopics($v['id'], $topicId);
                
                foreach ($topics as $k => $v) {
                    
                    $topicEntries = $this->feedbackFetchTopicEntries($v['id']);
                    $entryCount = count($topicEntries);
                    $accessGroup = ($v['accessgroup']) ? $this->groupFetchGroups($v['accessgroup']) : array(); 
                    
                    $topic = array(
                        'topicId' => $v['id'],
                        'title' => $v['title'],
                        'accessgroup' => (count($accessGroup)) ? array('id' => $v['accessgroup'], 'name' => $accessGroup[0]['groupname'], 'restricted' => True) : array('restricted' => False),
                        'entryCount' => $entryCount, 
                        'entries' => array()
                    );
                     
                    foreach($topicEntries as $k => $v) {
                        
                        $entryComments = $this->feedbackFetchEntryComments($v['id']);
                        $entryVotes = $this->feedbackFetchEntryVotes($v['id']); 
                        $votenum = count($entryVotes);
                        $commentCount = count($entryComments);
                                               
                        $entry = array(
                            'entryId' => $v['id'],
                            'description' => $v['description'],
                            'voteCount' => $votenum,
                            'votes' => $entryVotes,
                            'commentCount' => $commentCount,
                            'comments' => $entryComments
                        );
                        
                        $topic['entries'][] = $entry;
                        
                    }
                    
                    $bot['topics'][] = $topic;
                    
                }
                
            } else {
                
                continue;
                
            }
            
            $entries[] = $bot;
            
        }
        
        $returnvalue['bots'] = $entries;
        
        return $returnvalue;
        
    }

	//User Login
	public function userLogin($username, $password){
		$userinfo = $this->select("SELECT * FROM contacts WHERE emails = '$username'");
		if (count($userinfo)>0){
			if ($userinfo[0]['type'] != ""){
				if ($this->adminGetUserRoles($userinfo[0]['type'])[0]['role'] === 'admin') {
					return (password_verify($password, $userinfo[0]['password'])) ?  true : false;
				}
			}
		}
		return false;
	}
	//Contact/User handlers
	public function userUpdateSettings($action, $hash_value, $userid, $role='') {
		switch ($action) {
			case 'token':
				$data = array('token' => $hash_value);
				break;
			case 'update':
				$data = array('type'=>$role,'password'=>$hash_value);
				break;
			default:
				$data = array('type'=>'','password'=>'', 'token'=>'');
				break;
		}
		return $this->query($this->constructUpdateValues("contacts", $data, "WHERE id = '$userid'"));
	}
	public function contactAdd($data, $botid) {
		//Filter not needed data
		$data = $this->contactFilterArray($data['items'][0]);
		$data = $this->quoteArray($data);
		if(!array_key_exists('type', $data)) { $data['type'] = ''; }
		if(!array_key_exists('password', $data)) { $data['password'] = ''; }
		if(!array_key_exists('token', $data)) { $data['token'] = ''; }
		$default_groups = $this->groupFetchDefaultGroups();
		foreach ($default_groups as $key => $group){
			if ($group['botid'] == $botid or $group['botid'] == "0") $this->groupAddContact(array('groupid' => $group['id'], 'contactid' => $data['id']));
		}
		return $this->query($this->constructInsertValues("contacts", $data));
	}
	public function contactUpdate($data) {
		foreach ($data['items'] as $k => $v) {
			$meta = $this->contactFilterArray($data['items'][$k]);
			$meta = $this->quoteArray($meta);
			$this->query($this->constructUpdateValues("contacts", $meta, "WHERE id = '{$meta['id']}'"));
		}
	}
	public function contactRemove($userid) {
		$this->query("DELETE from contacts WHERE id = '{$userid}'");
		$this->query("DELETE FROM group_contacts WHERE contactid = '{$userid}'");
		$this->query("DELETE FROM contact_access_group_response WHERE contactid = '{$userid}'");
		$this->query("DELETE FROM ws_integration_user WHERE userid = '{$userid}'");
		$this->integrationDeleteToken($userid);
	}
	//Only keep the keys that we want to insert, remove all others
	public function contactFilterArray($data) {
		$newData = array();
		$filter = array('id','firstName','lastName','orgId', 'avatar', 'emails');
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = ($v == 'emails') ? $data[$v][0] : $data[$v];
			} else {
			    $newData[$v] = "";
			}
		}
		if ($newData['firstName'] == "" or $newData['lastName'] == "") {
		    if (isset($data['displayName'])) {
		        $newData['firstName'] = $data['displayName'];
		    }
		}
		return $newData;
	}
	//Get added contacts based on e-mail, id or all contact
	public function contactFetchContacts($contactid="") {
		if (!empty($contactid) and filter_var($contactid, FILTER_VALIDATE_EMAIL)) {
			return $this->select("SELECT * FROM contacts WHERE emails = '{$contactid}'");
		}
		elseif(!empty($contactid)) {
			return $this->select("SELECT * FROM contacts WHERE id = '{$contactid}'");
		}
		else {
			return $this->select("SELECT * FROM contacts ORDER BY firstName ASC");
		}
	}
	
	public function contactExists($contactid) {
	    return (count($this->select("SELECT * FROM contacts WHERE id = '{$contactid}'"))) ? true:false;
	}
	
	public function contactGetName($id) {
		$contactinfo = $this->contactFetchContacts($id);
		$value = (count($contactinfo)) ? $contactinfo[0]['firstName'] . " " . $contactinfo[0]['lastName'] :  $id;
		if (trim($value) == "") $value = $contactinfo[0]['emails'];
		return $value;
	}
	public function contactGetEmail($id) {
		$contactinfo = $this->contactFetchContacts($id);
		$value = (count($contactinfo)) ? $contactinfo[0]['emails'] :  $id;
		return $value;
	}
	
	public function contactGetNameLink($id, $avatar='', $avatarsize='20') {
		$contactinfo = $this->contactFetchContacts($id);
		if ($avatar) { 
			$avatar = "<img src='images/static/noimagefound.jpeg' class='img-circle' width='$avatarsize' height='$avatarsize'>";
		}
		if ($avatar and count($contactinfo)) {
			$avatar = "<img src='{$contactinfo[0]['avatar']}' class='img-circle' width='$avatarsize' height='$avatarsize'>";
		}
		if (count($contactinfo)) {
			$email = $contactinfo[0]['emails'];
			$name = $this->contactGetName($email);
			return "$avatar <a href='index.php?id=contacts&contactid={$contactinfo[0]['id']}' title='$email'>$name</a>";
		}
		return ($avatar) ? $avatar . " " . $id : $id;
	}
	
	public function contactGetGroupResponseAcl($contactid) {
		$returnvalue = "";
		$acl = $this->select("SELECT * FROM contact_access_group_response WHERE contactid = '{$contactid}'");
		if (count($acl) > 0) {
			foreach ($acl as $key => $value) {
				$botinfo = $this->botFetchBots($value['botid']);
				$returnvalue .= "{$botinfo[0]['emails']}:{$value['id']} <a href='index.php?id=contacts&special_access_remove={$contactid}&botid={$value['botid']}&spaceid={$value['id']}'>Delete</a><br>";
			}
			return $returnvalue;
		}
		else{
			return "None";
		}
	}
	
	public function contactGetGroupMemberships($contactid) {
	    
	    $contactinfo = $this->contactFetchContacts($contactid);
	    
	    $returnvalue = array(
	        "userId"=>"",
	        "groups"=>array()
	    );
	    
	    if (count($contactinfo)) {
	        
	        $returnvalue['userId'] =  $contactid;
	        
	        $groups = $this->groupFetchGroups();
	  	        
	        foreach ($groups as $key => $value) {
	            
	            if ($this->groupCheckIfMember($contactinfo[0]['id'], $value['id'])) {
	            
	                array_push($returnvalue['groups'], array($value['id'] => $value['groupname']));
	            
	            }
	        
	        }
	        
	    }
	    
	    return $returnvalue;
	    
	}
	
	public function contactDetailsAPI($contactid = '') {
	    
	    $returnvalue = array(
	        "items" => array()
	    );
	    
	    $contactinfo = ($contactid) ? $this->contactFetchContacts($contactid) : $this->contactFetchContacts();
	   
	    if (count($contactinfo)) {
	        
	        foreach ($contactinfo as $key=>$value) {
	            	            
	            $value = filterArray($value, array('id', 'firstName', 'lastName', 'emails', 'avatar', 'orgId'));
	            
	            $invalid = array("'");
	            
	            $value['firstName'] = str_replace($invalid, "", $value['firstName']);
	            $value['lastName'] = str_replace($invalid, "", $value['lastName']);
	            
	            $integrationExists = $this->integrationAccessTokenExists($value['id']);
	            
	            if ($integrationExists) {
	                $value['integration'] = $this->integrationAPIGetTokenData($value['id']);
	            } else {
	                $value['integration'] = array();
	            }
	            
	            $memberships = filterArray($this->contactGetGroupMemberships($value['id']), array('groups'));
	            $value['wbmGroupMemberships'] = $memberships;
	            
	            array_push($returnvalue['items'], $value);
	            
	        }   
	       
	    }
	    
	    return $returnvalue; 
	    	    
	}
	//Message handlers
	public function messagesLoad($id="") {
		return (empty($id)) ?
		$this->select("SELECT * FROM saved_messages") :
		$this->select("SELECT * FROM saved_messages WHERE id = '$id'");
	}
	public function messagesSave($data) {
		$result = $this->query($this->constructInsertValues("saved_messages", $data));
		if($result == 1)
		{
			echo alerts('success', 'Success!', 'Message has been saved');
		}
	}
	public function messagesUpdate($data, $id) {
		$this->query($this->constructUpdateValues("saved_messages", $data, "WHERE id = '$id'"));
	}
	public function messagesDelete($id) {
		$this->query("DELETE FROM saved_messages WHERE id = '$id'");
	}
	
	// Workspace integration handlers
	
	public function wsIntegrationAdd($data) {
	    return $this->query($this->constructInsertValues('workspace_integrations', $data));
	}
	
	public function wsIntegrationLinkUser($appId, $userId) {
	    return $this->query($this->constructInsertValues('ws_integration_user', array("userid" => $userId, "appid" => $appId)));
	}
	
	public function wsIntegrationLinkUserExists($userId) {
	    return (count($this->wsIntegrationGetUserLink($userId))) ? true : false;
	}
		
	public function wsIntegrationWebhookLog($data_array) {
	    //$data_array = array("appId", "deviceId", "data");
	    return $this->query($this->constructInsertValues('ws_webhook_log', $data_array));
	}
	
	public function wsIntegrationWebhookLogClear($appId='') {
	    //$data_array = array("appId", "deviceId", "data");
	    $query = ($appId) ? "DELETE FROM ws_webhook_log WHERE appId = '{$appId}'" : "DELETE FROM ws_webhook_log";
	    return $this->query($query);
	}
	
	public function wsIntegrationWebhookLogGet($appId, $deviceId='') {
	    //$data_array = array("appId", "deviceId", "data");
	    $query = ($deviceId) ? "SELECT data FROM ws_webhook_log WHERE appId = '$appId' and deviceId = '$deviceId' ORDER BY id DESC LIMIT 100" : "SELECT data FROM ws_webhook_log WHERE appId = '$appId' ORDER BY id DESC LIMIT 100";
	    $result = $this->select($query);
	    $returnvalue = array();
	    if (count($result)) {
	        foreach ($result as $k => $v) {
	            $returnvalue[] = json_decode($v['data'], true);
	        }
	    }
	    
	    return $returnvalue; 
	}
	
	public function wsIntegrationActionLog($data_array) {
	    //$data_array = array("appId", "jti", "action", "iat", "time");
	    return $this->query($this->constructInsertValues('ws_action_log', $data_array));
	}
	public function wsIntegrationActionLogGet($appId) {
	    return $this->select("SELECT * FROM ws_action_log WHERE appId = '$appId'");
	}
	
	public function wsIntegrationActionLogClear($appId) {
	    return $this->query("DELETE FROM ws_action_log WHERE appId = '$appId'");
	}
		
	//Add a new device to the overview for data collection
	public function wsIntegrationDeviceCollectorAddDevice($data_array) {
	    //$data_array = array("appId", "deviceId", "latestStatus", "lastEvent");
	    return $this->query($this->constructInsertValues('ws_device_collector', $data_array));
	}
	
	public function wsIntegrationDeviceCollectorClear($appId) {
	    return $this->query("DELETE FROM ws_device_collector WHERE appId = '$appId'");
	}
	
	//Update existing devices where a webhook is received
	public function wsIntegrationDeviceCollectorUpdateDevice($appId, $deviceId, $data) {
	    //$data_array = array("appId", "deviceId", "data");
	    return $this->query($this->constructUpdateValues('ws_device_collector', $data, "WHERE appId = '$appId' and deviceId = '$deviceId'"));
	}
	
	//Get the devices from the database
	public function wsIntegrationDeviceCollectorGetDevices($appId, $deviceId='') {
	    return ($deviceId) ? $this->select("SELECT * FROM ws_device_collector WHERE appId = '$appId' and deviceId = '$deviceId'") : $this->select("SELECT * FROM ws_device_collector WHERE appId = '$appId'");
	}
	
	//Add a new device to the overview for data collection
	public function wsIntegrationDeviceCollectorGetDevicesAPI($appId, $deviceId='') {
	    //$data_array = array("appId", "deviceId", "latestStatus", "lastEvent")
	    $devices = $this->wsIntegrationDeviceCollectorGetDevices($appId, $deviceId);
	    $returnvalue = array(
	        "items" => array()
	    );
	    
	    if (count($devices)) {
	        foreach ($devices as $k => $v) {
	            $returnvalue['items'][] = array(
	                'deviceId' => $v['deviceId'],
	                'deviceInfo' => json_decode($v['deviceInfo'], true),
	                'latestStatus' => json_decode($v['latestStatus'], true),
	                'lastEvent' => json_decode($v['lastEvent'], true),
	                'lastUpdated' => $v['lastUpdated']
	            );
	        }
	    }
	    
	    return $returnvalue;
	    
	}
	
	public function wsIntegrationAddDeviceMonitoring($data_array) {
	    //data_array = array('appId'=>$appid, 'userId'=>$userid, 'deviceId'=>$deviceid, 'groups'=>csv of groupids);
	    $this->wsIntegrationDeviceMonitoringDelete($data_array['appId'], $data_array['deviceId'], $data_array['userId']);
	    return $this->query($this->constructInsertValues('ws_device_monitoring', $data_array));
	}
		
	public function wsIntegrationGetDeviceMonitoring($appId, $deviceId) {
	    return $this->select("SELECT * FROM ws_device_monitoring WHERE deviceId = '{$deviceId}' and appId = '{$appId}'");
	}
	
	public function wsIntegrationDeviceMonitoringDeleteFromDevice($deviceId) {
	    return $this->query("DELETE FROM ws_device_monitoring WHERE deviceId = '{$deviceId}'");
	}
	public function wsIntegrationDeviceMonitoringDeleteFromApp($appId) {
	    return $this->query("DELETE FROM ws_device_monitoring WHERE appId = '{$appId}'");
	}
	public function wsIntegrationDeviceMonitoringDeleteFromUser($userId) {
	    return $this->query("DELETE FROM ws_device_monitoring WHERE userid = '{$userId}'");
	}
	public function wsIntegrationDeviceMonitoringDelete($appId, $deviceId, $userId) {
	    return $this->query("DELETE FROM ws_device_monitoring WHERE deviceId = '$deviceId' and appId = '$appId' and userId = '$userId'");
	}
	
	public function wsIntegrationCheckUserAccess($appId, $userId) {
	    $details = $this->wsIntegrationInfoAPI($appId);
	    
	    if ($details['integration']['accessgroup']) {
	        
	        return $this->groupCheckIfMember($userId, $details['integration']['accessgroup']);
   
	    } 
	    
	    else { return true; }
	    
	}
	
	public function wsIntegrationUpdateLinkUser($appId, $userId) {
	    
	    if ($this->wsIntegrationLinkUserExists($userId)) {
	          
    	    if (!empty($appId)) {
    	        
    	        if ($this->contactExists($userId) and $this->wsIntegrationExists($appId)) {
    	            
    	            $data_array = array(
    	                'appid' => $appId
    	            );
    	            
    	            return $this->query($this->constructUpdateValues('ws_integration_user', $data_array, "WHERE userid = '{$userId}'"));
    	        
    	        }
    	            
    	    } else if (empty($appId)) {
    	        return $this->wsIntegrationLinkDelete($userId);
    	    }
	    } else {
	        
	        if (!empty($appId) and !empty($userId)) {
	           
	            if ($this->contactExists($userId) and $this->wsIntegrationExists($appId)) {
	                return $this->wsIntegrationLinkUser($appId, $userId);
	            }
	            
	        }
	        
	    }
	}
		
	public function wsIntegrationLinkDelete($userId) {
	    return $this->query("DELETE FROM ws_integration_user WHERE userid = '{$userId}'");
	}
	
	public function wsIntegrationGetUserLink($userId) {
	    return $this->select("SELECT * FROM ws_integration_user WHERE userid = '{$userId}'");
	}
	
	public function wsIntegrationJwtInfo($appId) {
	    $returnvalue = array(
	        "headers_json"=>"",
	        "payload_json"=>"",
	        "signature"=>"",
	        "headers"=>"",
	        "payload"=>""
	    );
	    
	    $result = $this->wsIntegrationGet($appId);
	   
	    if (count($result)) {
	        $result = $result[0];
	        if (!empty($result['activation_jwt'])) {
	            $jwt_dec = json_decode($result['activation_jwt'], true);
	            $headers = jwtDecode($jwt_dec, 0);
	            $payload = jwtDecode($jwt_dec, 1);
	            $returnvalue['headers'] = $headers;
	            $returnvalue['payload'] = $payload;
	            $returnvalue['headers_json'] = json_encode($headers);
	            $returnvalue['payload_json'] = json_encode($payload);
	            $returnvalue['signature'] = explode('.', $jwt_dec['jwt'])[2];
	        }
	    }
	    return $returnvalue;
	}
	
	public function wsIntegrationInfoAPI($appId) {
	    $returnvalue = array(
	        "integration" => array(),
	        "jwt" => array(),
	        "token" => array()
	    );
	    $details = $this->wsIntegrationGet($appId);
	    
	    if (count($details)) {
	        
	        $details = $details[0];
	        $jwt = $this->wsIntegrationJwtInfo($appId);
	        
	        $returnvalue['integration'] = $details;
	        $returnvalue['jwt'] = $jwt;
	        
	        if (issetor($details['access_token']) and issetor($details['refresh_token'])) {
	            
	            $at_ex = $this->integrationCheckExpiration($details['expires_in'], $details['token_timestamp']);
	            $rt_ex = $this->integrationCheckExpiration($details['refresh_token_expires_in'], $details['token_timestamp']);
	            
	            $returnvalue['token'] = array(
	                "at" => $at_ex,
	                "rt" => $rt_ex
	            );
	          
	        }
	        		        
	    } else {
	        $returnvalue = array();
	    }
	    
	    return $returnvalue; 
	    
	}
	
	public function wsIntegrationInfoAPISensor($details) {
	    $details['integration']['activation_jwt'] = '***';
	    $details['integration']['access_token'] = '***';
	    $details['integration']['secret'] = '***';
	    $details['integration']['refresh_token'] = '***';
	    $details['integration']['whs'] = '***';
	    $details['jwt'] = '***';
	    
	    return $details;
	}
	
	public function wsIntegrationExists($appid) {
	    return (count($this->select("SELECT * FROM workspace_integrations WHERE id = '{$appid}'"))) ? true:false;
	}
	
	public function wsIntegrationUpdate($id, $data, $type='') {
	   
	    if ($type) {
	        
	        $filterTypes = array(	    
	            'webform' => array('name', 'description', 'client', 'secret', 'accessgroup', 'data_collection', 'webhook_logging', 'actions_logging', 'signage_base_url', 'signage_content_url', 'signage_assign_url')
	        );
	   	    
	        $data = filterArray($data, $filterTypes[$type]);
	        
	    }
	    	    
	    return $this->query(
	        $this->constructUpdateValues('workspace_integrations', $data, "WHERE id = '{$id}'")
	    );
	}
		
	public function wsIntegrationGet($id='') {
	    return (!empty($id)) ? $this->select("SELECT * FROM workspace_integrations WHERE id = '$id'") : $this->select("SELECT * FROM workspace_integrations");
	}
	
	public function wsIntegrationGetValue($id, $value) {
	    $result = $this->select_specific("SELECT {$value} FROM workspace_integrations WHERE id = '{$id}'");
	    return $result[$value];
	}
	
	public function wsIntegrationDelete($appId) {
	    $this->query("DELETE FROM workspace_integrations WHERE id = '{$appId}'");
	    $this->query("DELETE FROM ws_integration_user WHERE appid = '{$appId}'");
	    $this->integrationDeleteRepresentation($appId);
	    $this->wsIntegrationActionLogClear($appId);
	    $this->wsIntegrationWebhookLogClear($appId);
	    $this->wsIntegrationDeviceCollectorClear($appId);
	    $this->wsIntegrationDeviceMonitoringDeleteFromApp($appId);
	}
	
	public function wsIntegrationDeactivate($id) {
	    $data = array(
	        "refresh_token"=>"",
	        "access_token"=>"", 
	        "expires_in"=>"",
	        "refresh_token_expires_in"=>"",
	        "token_timestamp"=>"",
	        "activation_jwt"=>"",
	        "activation_status"=>"0",
	        "health_status"=>"0",
	        "health_timestamp"=>"",
	        "organization"=>"",
	        "status"=>"Deactivated",
	        "error"=>"",
	        "whs"=>""
	    );
	 	    
	    return $this->wsIntegrationUpdate($id, $data);
	    
	}
	
	//Integration handlers
	
	public function integrationFilterTokenResult($data) {
	    $newData = array();
	    $filter = array('access_token', 'expires_in', 'refresh_token', 'refresh_token_expires_in');
	    foreach ($filter as $k => $v) {
	        if (isset($data[$v])) {
	            $newData[$v] = $data[$v];
	        }
	    }
	    return $newData;
	}
	
	
	public function integrationFetchSettings() {
		return $this->select_specific("SELECT * FROM integration WHERE id = '1'");
	}
	public function integrationUpdateSettings($data) {
		$data['default_scopes'] = implode(' ', $data['default_scopes']);
		return $this->query($this->constructUpdateValues("integration", $data, "WHERE id = '1'"));
	}
	public function integrationDeleteSettings() {
		$current = $this->integrationFetchSettings();
		unset($current["id"]);
		foreach ($current as $key => $value) {
			$current[$key] = "";
		}
		$this->integrationDeleteToken();
		return $this->query($this->constructUpdateValues("integration", $current, "WHERE id = '1'"));
	}
	public function integrationDeleteToken($userid='') {
		if ($userid) {
			$this->query("DELETE from integration_tokens WHERE userid = '$userid'");
			$this->integrationDeleteRepresentation($userid);
		} else {
			$this->query("DELETE from integration_tokens");
			$this->query("DELETE from bots WHERE type = 'person'");
		}
	}
	public function integrationDeleteRepresentation($userid) {
		$query = "DELETE from bots WHERE id = '$userid' and type != 'bot'";
		return $this->query($query);
	}
	public function integrationAddToken($data) {
		return $this->query($this->constructInsertValues("integration_tokens", $data));
	}
	public function integrationUpdateToken($data, $userid) {
		return $this->query($this->constructUpdateValues("integration_tokens", $data, "WHERE userid = '$userid'"));
	}
	public function integrationFetchToken($userid='') {
		if ($userid) {
			return $this->select_specific("SELECT * FROM integration_tokens WHERE userid = '$userid'");
		}
		return $this->select("SELECT * FROM integration_tokens");
	}
	public function integrationFetchScopes($scope='') {
		if ($scope) {
			return $this->select_specific("SELECT * FROM integration_scopes WHERE scope = '$scope'");
		}
		return $this->select("SELECT * FROM integration_scopes");
	}
	public function integrationAccessTokenExists($userid) {
		$check = $this->integrationFetchToken($userid);
		if(!is_null($check)) { return (count($this->integrationFetchToken($userid))) ? true:false; }
	}
	public function integrationScopeExists($scope) {
		return (count($this->integrationFetchScopes($scope))) ? true:false;
	}
	public function integrationCheckConfiguration() {
		$integrationConfig = $this->integrationFetchSettings();
		foreach ($integrationConfig as $key => $value) {
			if (empty($value)) {
				return false;
			}
		}
		return true;
	}
	
	public function integrationCheckExpiration($expires_in, $timestamp) {
	    $returnvalue = array(
	        "days_left" => "",
	        "expiration_date" => "",
	        "expired" => True
	    );
	    
	    $future = (strtotime($timestamp)+$expires_in);
	    $curtime = time();
	    $time_left = $future - $curtime;
	    
	    $returnvalue['expiration_date'] = date("Y-m-d h:s", $future);
	    $returnvalue['days_left'] = round((($time_left/24)/60)/60);
	    $returnvalue['expired'] = ($curtime>$future) ? True:False; 
	    
	    return $returnvalue;
	    
	}
	
	public function integrationAPIGetTokenData($userid) { 
		$returnvalue = array();
		$tokendata = $this->integrationFetchToken($userid);
		$integrationUserData = $this->botFetchBots($userid);
		$name = $this->contactGetName($userid);
		$curtime = time();
			
		$access_token_future = (strtotime($tokendata['timestamp'])+$tokendata['expires_in']);
		$refresh_token_future = (strtotime($tokendata['timestamp'])+$tokendata['refresh_token_expires_in']);
		
		$timeleft_at = $access_token_future-$curtime;
		$expiration_date_at = date("Y-m-d h:s", $access_token_future);
		$days_left_at = round((($timeleft_at/24)/60)/60);
		
		$timeleft_rt = $refresh_token_future-$curtime;
		$expiration_date_rt = date("Y-m-d h:s", $refresh_token_future);
		$days_left_rt = round((($timeleft_rt/24)/60)/60);
		
		$at_expired = ($curtime > $access_token_future) ? true:false;
		$rt_expired = ($curtime > $refresh_token_future) ? true:false;
		unset($tokendata['userid']);
		$returnvalue = array(
				'userid' => $userid,
				'displayName' => $name,
				'authorizedUser' => array(
					'userEmail' => (isset($integrationUserData[0])) ?  $integrationUserData[0]['emails'] : "N/A",
				    'displayName' => (isset($integrationUserData[0])) ? $integrationUserData[0]['displayName'] : "N/A"
				),
				'tokendata' => $tokendata,
				'access_token' => array(
						'expiration_date' => $expiration_date_at,
						'days_left' => $days_left_at,
						'expired' => $at_expired),
				'refresh_token' => array(
						'expiration_date' => $expiration_date_rt,
						'days_left' => $days_left_rt,
						'expired' => $rt_expired),
				);
		return $returnvalue;
	}
	
	//Admin handlers
	public function adminCheckWebhookExists($webhookid, $botid) {
		return (count($this->select("SELECT * FROM bot_webhook WHERE webhookid = '$webhookid' and botid = '$botid'"))>0) ? true:false;
	}
	public function adminCheckIsBotMain($id) {
		return (count($this->select("SELECT * FROM bots WHERE id = '$id' and main = '1'"))>0) ? true:false;
	}
	public function adminCheckIsLoginUser($id){
		return (count($this->select("SELECT * FROM contacts WHERE type = '1' and password != '' and id = '$id'"))>0) ? true:false;
	}
	public function adminCheckUserExists($id){
		$key = (validateEmail($id)) ? "emails":"id"; 
		return (count($this->select("SELECT * FROM contacts WHERE ".$key." = '$id'"))) ? true:false;
	}
	public function adminCheckSiteAdminExists(){
		return (count($this->select("SELECT * FROM contacts WHERE type = '1' and password != ''"))>0) ? true:false;
	}
	public function adminCheckTaskQueue() {
		return count($this->select("SELECT * FROM tasks"));
	}
	public function adminPurgeTaskQueue(){
		$this->query("DELETE FROM tasks");
	}
	public function adminCheckIfWarning() {
		$warning_status = $this->select_specific("SELECT warning_mode FROM service_status WHERE id = '1'");
		return $warning_status['warning_mode'];
	}
	public function adminGetWarningMessage() {
		$warning_message = $this->select_specific("SELECT message FROM generic_feedback WHERE id = 'warning'");
		return $warning_message['message'];
	}
	public function adminSetWarning($value) {
		if(empty($value)) { $value = 0; }
		return $this->query("UPDATE service_status SET warning_mode = '$value' WHERE id = '1'");
	}
	public function adminSetWarningMessage($value) {
		return $this->query("UPDATE generic_feedback SET message = '$value' WHERE id = 'warning'");
	}
	public function adminCheckIfMaintenance() {
		$maintenance_status = $this->select_specific("SELECT maintenance_mode FROM service_status WHERE id = '1'");
		return $maintenance_status['maintenance_mode'];
	}
	public function adminGetMaintenanceMessage() {
		$maintenance_status = $this->select_specific("SELECT message FROM generic_feedback WHERE id = 'maintenance'");
		return $maintenance_status['message'];
	}
	public function adminCheckBotRestriction($botid) {
		return (count($this->select("SELECT * FROM bot_allowed_domain WHERE botid = '{$botid}'"))) ? true:false;
	}
	public function adminSetMaintenance($value) {
		if(empty($value)) { $value = 0; }
		return $this->query("UPDATE `service_status` SET `maintenance_mode` = $value WHERE id = '1'");
	}
	public function adminSetMaintenanceMessage($value) {
		return $this->query("UPDATE generic_feedback SET message = '$value' WHERE id = 'maintenance'");
	}
	public function adminCheckIfTaskMonitor() {
		$taskmonitor_status = $this->select_specific("SELECT task_monitor FROM service_status WHERE id = '1'");
		return $taskmonitor_status['task_monitor'];
	}
	public function adminSetTaskMonitor($value) {
		if(empty($value)) { $value = 0; }
		$this->query("UPDATE service_status SET task_monitor = '$value' WHERE id = '1'");
	}
	public function adminCheckServiceStatus($type='') { //report to get text output, nothing to get true or false
		$heartbeat = $this->select_specific("SELECT * FROM heartbeat WHERE id = '1'");
		$time = strtotime($heartbeat['time']);
		$curtime = time();
		if ($type === 'report') {
			return (($curtime-$time) >= 30) ? "Task service is <strong>DOWN</strong> for: ><strong>" . ($curtime-$time) . "</strong> seconds, last heartbeat received on {$heartbeat['time']}" :
			"Task service is **UP** | last heartbeat received {$heartbeat['time']}";
		}
		else {
			return (($curtime-$time) >= 30) ? true : false;
		}
	}
	public function adminCheckIfValidAPIToken($token) {
		$token = $this->quote($token);
		if (empty($token)) return false;
		return count($this->select("SELECT * FROM contacts WHERE token = '$token'"));
	}
	public function adminCheckIfValidDomain($email, $botid) {
		list($user, $domain) = explode("@", $email);
		$domains = $this->select("SELECT d.domain, b.* FROM domains as d, bot_allowed_domain as b WHERE d.id = b.domainid and b.botid = '{$botid}'");
		return (in_array($domain,array_column($domains,'domain'))) ? true : false;
	}
	public function adminCheckGroupResponseAcl($botid, $spaceid) {
		return (count($this->select("SELECT * FROM excluded_spaces WHERE id = '$spaceid' and botid = '$botid'"))>0) ? true:false;
	}
	public function adminGetGroupResponseAcl($botid="") {
		return ($botid == "") ? $this->select("SELECT * FROM excluded_spaces") : $this->select("SELECT * FROM excluded_spaces WHERE botid = '$botid'");
	}
	public function adminCheckUserGroupResponseAcl($botid, $spaceid="", $contactid="") {
		if (empty($spaceid) and empty($contactid)) {
			$query = "SELECT * FROM contact_access_group_response WHERE (botid = '$botid')";
		}
		elseif (!empty($spaceid) and empty($contactid)) {
			$query = "SELECT * FROM contact_access_group_response WHERE (botid = '$botid' and id = '$spaceid')";
		}
		elseif (!empty($spaceid) and !empty($contactid)) {
			$query = "SELECT * FROM contact_access_group_response WHERE (id = '$spaceid' and botid = '$botid' and contactid = '{$contactid}')";
		}
		$result = $this->select($query);
		return $result;
	}
	public function adminAddGroupResponseAcl($botid, $spaceid, $title='') {
		$title = $this->quote($title);
		$this->query("INSERT INTO excluded_spaces (id, botid, spacetitle) VALUES ('$spaceid', '$botid', '$title')");
	}
	public function adminAddUserGroupResponseAcl($botid, $spaceid, $contactid) {
		$this->query("INSERT INTO contact_access_group_response (id, botid, contactid) VALUES ('$spaceid', '$botid', '$contactid')");
	}
	public function adminRemoveUserGroupResponseAcl($botid, $spaceid, $contactid) {
		$this->query("DELETE FROM contact_access_group_response WHERE (id = '$spaceid' and botid = '$botid' and contactid = '$contactid')");
	}
	public function adminRemoveGroupResponseAcl($botid, $spaceid) {
		$this->query("DELETE FROM excluded_spaces WHERE (id = '$spaceid' and botid = '$botid')");
	}
	public function adminUpdateGroupResponseAcl($spaceid, $title) {
		$this->query("UPDATE excluded_spaces SET spacetitle = '$title' WHERE id = '$spaceid'");
	}
	public function adminBlockContact($email) {
		$this->query("INSERT INTO blocked_contacts (email) VALUES ('$email')");
	}
	public function adminUnblockContact($email) {
		$this->query("DELETE FROM blocked_contacts WHERE email = '$email'");
	}
	public function adminGetBlockedContacts($email=""){
		if (!empty($email)){
			return (count($this->select("SELECT * FROM blocked_contacts WHERE email='$email'"))>0) ? true : false;
		}
		else {
			return $this->select("SELECT * FROM blocked_contacts");
		}
	}
	public function adminAddJoinableSpace($data) {
		$this->query($this->constructInsertValues("joinable_space",$data));
	}
	public function adminUpdateJoinableSpace($spaceid, $title) {
		$this->query($this->query("UPDATE joinable_space SET spacetitle = '$title' WHERE spaceid = '$spaceid'"));
	}
	public function adminRemoveJoinableSpace($botid, $spaceid) {
		return $this->query("DELETE FROM joinable_space WHERE spaceid='$spaceid' and botid='$botid'");
	}
	public function adminGetJoinableSpace($botid, $roomid="") {
		return (!empty($roomid)) ? $this->select("SELECT * FROM joinable_space WHERE botid='$botid' and spaceid='$roomid'") : $this->select("SELECT * FROM joinable_space WHERE botid='$botid'");
	}
	public function adminGetJoinableSpaceById($botid, $id) {
		return $this->select("SELECT * FROM joinable_space WHERE botid='$botid' and id='$id'");
	}
	public function adminCheckJoinableSpace($botid, $spaceid) {
		return (count($this->select("SELECT * FROM joinable_space WHERE spaceid='$spaceid' and botid='$botid'"))>0) ? true : false;
	}
	//Checks if a given space via id, is joinable from bot
	public function adminCheckJoinableSpaceById($botid, $id) {
		return (count($this->select("SELECT * FROM joinable_space WHERE id='$id' and botid='$botid'"))>0) ? true : false;
	}
	//Logs a user event
	public function adminAddLogUser($email) {
		return (validateEmail($email)) ? $this->query("INSERT INTO log_users (email) VALUES ('$email')") :
		false;
	}
	//Get available features
	public function adminGetFeatures($id="") {
		return (!empty($id)) ? $this->select("SELECT * FROM features WHERE id = '$id'"):
		$this->select("SELECT * FROM features");
	}
	public function adminGetFeatureUsage($keyword) {
	    $featureUsage = $this->select_specific("SELECT * FROM features WHERE keyword = '$keyword'");
	    return ($featureUsage) ? $featureUsage : array();
	}
	public function adminSetFeature($featureid, $botid) {
		$feature = $this->adminGetFeatures($featureid)[0];
		$current = $this->responseFetchResponse($botid, $feature['keyword']);
		if(empty($current)) {
			$current = array();
		}
		if (count($current)>0) {
			($current['is_feature'] == "0") ? $this->responseUpdate(array("is_feature"=>"1", "is_task"=>"0","id"=>$current['id'])):$this->responseDelete($current['id']);
		}
		else {
			$this->query($this->constructInsertValues('response', array("botid"=>$botid,"keyword"=>$feature['keyword'], "response"=>$feature['usage'], "is_task"=>"0", "is_feature"=>'1', "accessgroup"=>"0", "file_url" =>"", "card_id"=>0)));
		}
	}
	//Get one or all user events
	public function adminGetLogUsers($email="") {
		return (!empty($email)) ? $this->select("SELECT * FROM log_users WHERE email = '$email'"):
		$this->select("SELECT * FROM log_users");
	}
	public function adminGetLogBot() {
		$logbotid = $this->select("SELECT * FROM log_bot")[0]['botid'];
		return (!empty($logbotid)) ? $logbotid:false;
	}
	//Fetch one or all of the User roles (Related to login to the web page)
	public function adminGetUserRoles($id="") {
		return (!empty($id)) ? $this->select("SELECT * FROM roles WHERE id = '$id'"):
		$this->select("SELECT * FROM roles");
	}
	public function adminFilterMentions($botname, $text) {
		list($filter) = explode(' ',$botname);
		$text = str_replace($botname." ", "", $text);
		$text = str_replace($filter." ", "", $text);
		return $text;
	}
	public function adminReportLogs($botid='') {
		$command_usage = array();
		$month_q = date('Y-m');
		$last_month_q = date('Y-m', strtotime('last month'));
		$year_q = date('Y');
		$day_q = date('Y-m-d');
		$total_number = count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' ORDER BY date ASC"));
		$year_number =  count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$year_q}%' ORDER BY date ASC"));
		$month_number =  count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$month_q}%' ORDER BY date ASC"));
		$last_month_number = count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$last_month_q}%' ORDER BY date ASC"));
		$day_number = count($this->select("SELECT * FROM activity_log WHERE botid = '{$botid}' and date LIKE '{$day_q}%' ORDER BY date ASC"));
		$bot_keywords = $this->select("SELECT keyword FROM response WHERE botid = '{$botid}'");
		
		$returnvalue = "## Usage report (Commands responded to)\n<hr> <blockquote class=success>Generic usage</blockquote>\n\n- Usage today: **{$day_number}**\n- Usage this year: **{$year_number}**\n- Lifetime: **{$total_number}**\n\n<blockquote class=success>Per command usage</blockquote>\n\n";
		foreach ($bot_keywords as $key => $value) {
			$command_usage[$value['keyword']] = count($this->select("SELECT * FROM activity_log WHERE botid = '$botid' and command LIKE '%{$value['keyword']}%'"));
		}
		arsort($command_usage);
		foreach ($command_usage as $key => $value){
			$returnvalue .= '<li> ' . $key . ':<b>' . $value . '</b>';
		}
		return $returnvalue;
	}
	//Bot handlers
	//Returns the bot id that owns the webhook id.
	public function botGetWebhookOwner($webhookid){
		$webhookowner = $this->select("SELECT * FROM bot_webhook WHERE webhookid = '{$webhookid}'");
		return $webhookowner[0]['botid'];
	}
	
	//Check if the bot has the same_org_response flag set
	public function botCheckSameOrgResponse($botid){
	    
	    $data = $this->botFetchBots($botid);
	    
	    if (count($data)) {
	        return ($data[0]['same_org_response']) ? true : false;
	    }
	   	
	    return false;
	    
	}
	
	//Fetches and returns the main bot info. The main bot is used by the system to fetch information from the API so you don't have to select a bot to perform small tasks.
	public function botGetMainInfo() {
		$mainbot = $this->select("SELECT * FROM bots WHERE main = '1'");
		return (isset($mainbot[0]) and !empty($mainbot[0])) ? $mainbot[0]:false;
	}
	//Fetches and returns the bot display name
	public function botGetName($id) {
		$botinfo = $this->botFetchBots($id);
		$value = (count($botinfo)) ? $botinfo[0]['displayName'] : $id;
		if (trim($value) == "") $value = $botinfo[0]['emails'];
		return $value;
	}
	//Add bot based on data from the API
	public function botAdd($data) {
		//Filter not needed data
		if (!isset($data['items'][0])) return False;
		$data = $this->botFilterArray($data);
		return $this->query($this->constructInsertValues("bots", $data['items'][0]));
	}
	public function wizardAddMainBot($token) {
		$url = $this->getApiUrl("people").'/me';
		$botvalues = $this->data_get(array(), $this->build_spark_headers($token, "GET"), $url);
		if (isset($botvalues['id'])) {
			$botid = $botvalues['id'];
			$this->db->wizardAddBot($botvalues);
			$this->db->botUpdateSettings($botid, $token);
			$this->db->botSetPrimary($botid);
			return "<img src='{$botvalues['avatar']}' class='img-circle' height='50' width='50'> <b>{$botvalues['displayName']}</b> was added as the primary bot!";
		}
		else {
			return false;
		}
	}
	public function botAddAllowedDomain($domainid, $botid) {
		$this->query("INSERT INTO bot_allowed_domain (domainid, botid) VALUES ('$domainid', '$botid')");
	}
	public function botDeleteAllowedDomain($botid) {
		return $this->query("DELETE FROM bot_allowed_domain WHERE botid = '$botid'");
	}
	public function botCheckDomain($domainid, $botid) {
		return (count($this->select("SELECT * FROM bot_allowed_domain WHERE domainid = '{$domainid}' and botid = '{$botid}'")) > 0) ? true : false;
	}
	public function wizardAddBot($data) {
		//Filter not needed data
		$data = $this->wizardAddBotFilterArray($data);
        $q = $this->query($this->constructInsertValues("bots", $data));
		return $q;
	}
	public function botSetPrimary($botid) {
		$this->query("UPDATE bots SET main = '0' WHERE main='1'");
		$this->query("UPDATE bots SET main = '1' WHERE id = '$botid'");
	}
	public function botSetDefaultResponse($response ,$botid) {
		$this->query("UPDATE bots SET defres = '$response' WHERE id = '$botid'");
	}
	//Purge a bot from the system including all responses, webhook references, owned groups etc.
	public function botDelete($id) {
		$this->query("DELETE FROM bot_webhook WHERE botid = '$id'");
		$this->feedbackBotTopicsDelete($id);
		$this->query("UPDATE groups SET botid = '0' WHERE botid ='$id'");
		$this->query("DELETE FROM contact_access_group_response WHERE botid='$id'");
		$this->query("DELETE FROM excluded_spaces WHERE botid = '$id'");
		$this->query("DELETE FROM joinable_space WHERE botid = '$id'");
		$this->query("DELETE FROM response WHERE botid = '$id'");
		$this->query("DELETE FROM group_spaces WHERE botid = '$id'");
		$this->query("DELETE FROM bot_allowed_domain WHERE botid = '$id'");
		$this->query("DELETE FROM bots WHERE id = '$id'");
		return true;
	}
	
	public function botFetchBots($botid='', $integration='') { //integration = botrepresentationid
		if ($botid) {
			return $this->select("SELECT * FROM bots WHERE id = '$botid'");
		}
		if ($integration) {
			return $this->select("SELECT * FROM bots WHERE type = 'bot' or id = '$integration' ORDER BY displayName ASC");
		}
		return $this->select("SELECT * FROM bots WHERE type = 'bot' ORDER BY displayName ASC");
	}
	
	public function botUpdateValues($botid, $data) {
	    return $this->query($this->constructUpdateValues("bots", $data, "WHERE id = '$botid'"));
	}
	
	public function botUpdate($data) {
		//Filter not needed data
		$data = $this->botFilterArray($data);
		return $this->query($this->constructUpdateValues("bots", $data['items'][0], "WHERE id = '{$data['items'][0]['id']}'"));
	}
	public function botUpdateSettings($botid, $access="", $main="") {
		if (empty($main) and !empty($access)) {
			return $this->query("UPDATE bots SET access = '$access' WHERE id = '$botid'");
		} elseif (!empty($main) and empty($access)) {
			$this->query($this->query("UPDATE bots SET main = '0' WHERE main = '1'"));
			$this->query($this->query("UPDATE bots SET main = '1' WHERE id = '$botid'"));
		}
	}
	public function botFilterArray($data) {
		$newData = array(
				'items' => array()
		);
		$filter = array('id','displayName','avatar','type', 'emails');
		foreach ($filter as $k => $v) {
			if (isset($data['items'][0][$v])) {
				$newData['items'][0][$v] = ($v == 'emails') ? $data['items'][0][$v][0] : $data['items'][0][$v];
			}
		}
		return $newData;
	}
	public function wizardAddBotFilterArray($data) {
		$newData = array();
		if(!array_key_exists('access', $data)) { $data['access'] = ''; }
		if(!array_key_exists('main', $data)) { $data['main'] = '0';}
		if(!array_key_exists('defres', $data)) { $data['defres'] = '';}
		if(!array_key_exists('card_mode', $data)) { $data['card_mode'] = '1';}
		if(!array_key_exists('same_org_response', $data)) { $data['same_org_response'] = '0';}
		$filter = array('id','displayName','avatar','type', 'emails', 'access', 'main', 'defres', 'card_mode', 'same_org_response');
		foreach ($filter as $k => $v) {
			if (isset($data[$v])) {
				$newData[$v] = ($v == 'emails') ? $data[$v][0] : $data[$v];
			}
		}
		return $newData;
	}
	//Response handlers
	public function responseFetchResponse($botid="", $keyword="") {
		return $this->select_specific("SELECT * FROM response WHERE botid = '{$botid}' and keyword = '{$keyword}'");
	}
	
	public function responseFetchResponses($botid, $id="") {
		return (empty($id)) ? $this->select("SELECT * FROM response WHERE botid = '$botid' ORDER BY keyword ASC") : $this->select("SELECT * FROM response WHERE id = '$id'");
	}
	
	public function responseCreate($data) {
	    $is_feature = $this->adminGetFeatureUsage($data['keyword']);
	    if(!empty($is_feature)){
	        $is_feature = 1;
	    } else {
	        $is_feature = 0;
	    }
	    if ($data['is_task'] == 1 and $is_feature) {
	        return false;
	    }
		$data['keyword'] = explode(' ', $data['keyword'])[0];

		if(empty($data['card_id'])) { $data['card_id'] = 0; } //If card_id is empty, insert default value of 0
		if(empty($data['is_feature'])) { $data['is_feature'] = 0; } //If card_id is empty, insert default value of 0
		return $this->query($this->constructInsertValues("response", $data));
	}
	public function responseUpdate($data) {
		$is_feature = $this->adminGetFeatureUsage($data['keyword']);
		if(!empty($is_feature)){
			$is_feature = 1;
		} else {
			$is_feature = 0;
		}
		if ($data['is_task'] == 1 and $is_feature) {
			return false;
		}
		$data['is_feature'] = ($is_feature) ? 1 : 0;
		$data['keyword'] = explode(' ', $data['keyword'])[0];
		if(empty($data['card_id'])) { $data['card_id'] = 0; }
		return $this->query($this->constructUpdateValues("response", $data, "WHERE id = '{$data['id']}'"));
	}
	public function responseDelete($responseid) {
		return $this->query("DELETE FROM response WHERE id = '$responseid'");
	}
	//Task handlers
	public function taskAddNewTask($taskid, $task_attributes, $metadata) {
	    list($user, $userdetails, $type, $roomid, $botmail, $payload, $files, $parentid, $card_data) = $metadata;
		list($attr1, $attr2, $attr3, $attr4, $attr5, $attr6) = $task_attributes;
		$attr1 = $this->quote($attr1);
		$attr2 = $this->quote($attr2);
		$attr3 = $this->quote($attr3);
		$attr4 = $this->quote($attr4);
		$attr5 = $this->quote($attr5);
		$attr6 = $this->quote($attr6);
		$payload = $this->quote($payload);
		$query="INSERT INTO tasks (taskId, attr1, attr2, attr3, attr4, attr5, attr6, text, user, user_details, bot, type, roomid, files, parentMessageId, card_data, timestamp)
		VALUES('$taskid','$attr1','$attr2','$attr3','$attr4', '$attr5', '$attr6', '$payload', '$user', '$userdetails', '$botmail', '$type', '$roomid', '$files', '$parentid', '$card_data',  NOW())";
		return ($this->query($query)) ? true : false;
	}
	//Webhook handlers
	public function webhookDbLink($webhookid, $botid){
		$this->query("INSERT INTO bot_webhook (webhookid, botid, groupid) VALUES('$webhookid','$botid', 0)");
	}
	public function webhookDbUnlink($webhookid){
		$this->query("DELETE FROM bot_webhook WHERE webhookid = '$webhookid'");
	}
	public function webhookGetAccessGroup($webhookid){
		$result = $this->select("SELECT * FROM bot_webhook WHERE webhookid = '$webhookid'");
		return (count($result)) ? $result[0]['groupid'] : false;
	}
	public function webhookSetAccessGroup($webhookid, $groupid){
		return $this->query("UPDATE bot_webhook SET groupid = '$groupid' WHERE webhookid = '$webhookid'");
	}
	//Logging
	public function logsGet($botid="", $limit){
		return (!empty($botid)) ? $this->select("SELECT * FROM activity_log WHERE botid = '$botid' ORDER BY date DESC LIMIT $limit"):
		$this->select("SELECT * FROM activity_log ORDER BY date DESC, botid DESC LIMIT $limit");
	}
	//Add that LDAP is enabled
	public function addldapsettings($ldap=""){
		$this->query("INSERT INTO site_settings (settings, value) VALUES('ldap_enabled','1')");
	}
	//Add that LDAP is enabled
	public function checkifldapisenabled($ldap=""){
		return count($this->select("SELECT * FROM site_settings WHERE settings = 'ldap_enabled' AND value = '1'"));
	}
	//Check DB version_compare
	public function sitesettingsexist($sitesettings=""){
		return $this->query("DESCRIBE site_settings");
	}
	//Check DB version_compare
	public function checkdbversion($version=""){
		return $this->select("SELECT * FROM site_settings WHERE settings = 'dbversion'");
	}
	public function verifydbversion($newdbver)
	{
		global $config;
		$DBName = $config['dbname'];
		
		if ($this->sitesettingsexist() == "Table '{$DBName}.site_settings' doesn't exist")
		{
			$data = '<div class="alert alert-danger" style="margin-left: 10px; margin-right: 10px;">
					<h4><i class="icon fa fa-ban"></i> Table \'site_settings\' does not exist</h4>
					We\'ve detected the \'site_settings\' table does not exist. <br>
					This is needed for maintaining the core site. It is also needed for future upgrades.<br>
					<br>
					<a href="index.php?id=upgrade&function=loadsitesettings"><button class="btn btn-primary">Create site_settings table</button></a>
				</div>';
			return $data;
		}
		else
		{
			if($this->checkdbversion()[0]['value'] < $newdbver)
			{
				$currentDB = $this->checkdbversion()[0]['value'];
				$data = '<div class="alert alert-danger" style="margin-left: 10px; margin-right: 10px;">
						<h4><i class="icon fa fa-ban"></i> Database Needs to be upgraded!</h4>
						We\'ve detected the current DB version is ' . $currentDB . ' and it needs to be upgraded to version ' . $newdbver . '
						<br>
						<br>
						<a href="index.php?id=upgrade&function=upgradedb&currentdbver='.$currentDB.'&newdbver='.$newdbver.'"><button class="btn btn-primary">UPGRADE DATABASE NOW</button></a>
					</div>';
				return $data;
			}
			elseif($this->checkdbversion()[0]['value'] == $newdbver)
			{
				return "";
			}
			else
			{
				$data = '<div class="alert alert-danger" style="margin-left: 10px; margin-right: 10px;">
						<h4><i class="icon fa fa-ban"></i> Database version issue!</h4>
						We\'ve detected an issue with the database version
					</div>';
				return $data;
			}
		}
		
	}
	function insertsitesettings() 
	{
		//Create site_settings table if it doesn't exist - Establishing baseline
		$createtable = $this->query("CREATE TABLE `site_settings` (
		  `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
		  `settings` varchar(25) NOT NULL,
		  `value` varchar(30) NOT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		//After table is created inserting dbversion 1
		
		if(empty($this->checkdbversion()))
		{
			$setdbver = $this->query("INSERT INTO site_settings(settings, value) VALUES ('dbversion','1')");
		}
		else
		{
			$setdbver = 'DB version already set';
		}
		
		return array($createtable, $setdbver);
	}
	function generatetaskid() {
		$str=rand(); 
		$randomgen = sha1($str);
		$count = count($this->select("SELECT * FROM queue_task WHERE task_id = '$randomgen'"));
		if($count == 0)
		{
			return $randomgen;
		}
		else
		{
			//if random gen exists in db, generate a new one.
			$this->generatetaskid();
		}
	}
	function generatesubtaskid() {
		$str=rand(); 
		$randomgen = sha1($str);
		$count = count($this->select("SELECT * FROM queue_subtasks WHERE subtask_id = '$randomgen'"));
		if($count == 0)
		{
			return $randomgen;
		}
		else
		{
			//if random gen exists in db, generate a new one.
			$this->generatesubtaskid();
		}
	}
	public function taskQueueGenerateBotResponse($taskid) {
		$failedcodes = array();
		$class="success";
		$subtasks = $this->taskQueueFetchSubtasks($taskid);
		$num_subtasks = count($subtasks);
		$num_success = count($this->taskQueueFetchSubtasks($taskid, "AND task_code = '200'"));
		$num_failed = ($num_subtasks - $num_success);
		if ($num_failed) {
			$class = "warning";
			foreach ($subtasks as $key => $value) {
				if ($value != 200) {
					$failedcodes[] = $value['task_code'];
				}
			}
			$failedcount = array_count_values($failedcodes);
			$failedcount = json_encode($failedcount);
		} 
		if (!$num_success) {
			$class = "danger";
		}
		return blockquote("Task with taskid <strong>$taskid</strong> was completed! \n <strong>$num_success</strong>/<strong>$num_subtasks</strong> was successfully executed (<strong>$num_failed</strong> errors)\n <code>$failedcount</code>", $class);
	}
	public function taskQueueFetchSubtask($subtaskid) {
		 return $this->select("SELECT * FROM queue_subtasks WHERE subtask_id = '$subtaskid'");
	}
	public function taskQueueFetchSubtasks($taskid, $clause="") {
		return $this->select("SELECT * FROM queue_subtasks WHERE task_id = '$taskid' $clause");
	}
	public function taskQueueFetchTask($taskid) {
		return $this->select("SELECT * FROM queue_task WHERE task_id = '$taskid'");
	}
	public function taskQueueClear($user, $taskid="") {
		$query = ($taskid) ? "UPDATE queue_task SET clear = 'yes' WHERE task_id = '$taskid' and task_submitter='$user'" : "UPDATE queue_task SET clear = 'yes' WHERE task_submitter='$user'";
		return $this->query($query);
	}
	public function taskQueuePurge($user) {
		$mytasks = $this->select("SELECT * FROM queue_task WHERE task_submitter = '$user'");
		foreach ($mytasks as $key => $value) {
			$this->query("DELETE FROM queue_subtasks WHERE task_id = '{$value['task_id']}'");
			$this->query("DELETE FROM queue_task WHERE task_id = '{$value['task_id']}'");
		}	
	}
	public function taskQueueResubmit($taskid, $subtaskid="") {
		$query = ($subtaskid) ? "UPDATE queue_subtasks SET task_code = '' WHERE subtask_id = '$subtaskid' and task_id = '$taskid' and task_code != '200'" : "UPDATE queue_subtasks SET task_code = '' WHERE task_id = '$taskid' and task_code != '200'";
		$this->query($query);
	}
	public function taskQueueInsert($taskid, $taskname, $type, $parsel, $botid, $custom) {
	    $botresponse = (isset($custom['botresponse'])) ? $custom['botresponse']:0;
	    if($type == 'messages')
	    {
	        $taskname = (empty($taskname)) ? "Unnamed message" : $taskname;
	        $inserttaskid = $this->query("INSERT INTO queue_task (task_id, task_type, task_name, task_submitter, botresponse, botid, submit_timestamp, clear) VALUES ('$taskid', '$type','$taskname', '$custom', '$botresponse', '$botid', now(), '')");
	        echo 'test '.$inserttaskid;
	        foreach ($parsel as $key => $value) {
	            $converttojson = $this->quote(json_encode($value));
	            $result[] = $this->query("INSERT INTO queue_subtasks (subtask_id, task_id, bot_id, subtask_type, task_json, group_id, task_code, payload, response, task_results) VALUES ('$key', '$taskid', '$botid', '$type', '$converttojson', '', '', '',  '', '')");
	        }
	        if(count(array_unique($result)) === 1)
	            return current($result);
	            else return;
	    }
	    elseif($type == 'addtolocal')
	    {
	        $loggedinuser = $custom['loggedinuser'];
	        $inserttaskid = $this->query("INSERT INTO queue_task (task_id, task_type, task_name, task_submitter, botresponse, botid, submit_timestamp, clear) VALUES ('$taskid', '$type','$taskname', '$loggedinuser', '$botresponse', '$botid', now(), '')");
	        $group = $custom['group'];
	        foreach($parsel as $key=>$value)
	        {
	            $subtaskid = $this->generatesubtaskid();
	            $result[] = $this->query("INSERT INTO queue_subtasks (subtask_id, task_id, bot_id, subtask_type, task_json, group_id, task_code, payload, response, task_results) VALUES ('$subtaskid', '$taskid', '$botid', '$type', '$value', '$group', '', '', '', '')");
	        }
	        
	        if(count(array_unique($result)) === 1)
	            return current($result);
	            else return;
	    }elseif($type == 'adgroup')
	    {
	        $result[] = $this->query("INSERT INTO ad_groups (group_name) VALUES ('$parsel')");
	        $getid = $this->select("SELECT id FROM ad_groups WHERE group_name = '$parsel'");
	        $adgroupid = $getid[0]['id'];
	        $group = $custom['group'];
	        $loggedinuser = $custom['loggedinuser'];
	        $result[] = $this->query("INSERT INTO ad_group_mapping (ad_group_id, local_group_id) VALUES ('$adgroupid', '$group')");
	        $result[] = $this->query("INSERT INTO queue_task (task_id, task_type, task_name, task_submitter, botresponse, botid, submit_timestamp, clear) VALUES ('$taskid', '$type','$taskname', '$loggedinuser', '$botresponse', '$botid', now(), '')");
	        $subtaskid = $this->generatesubtaskid();
	        $result[] = $this->query("INSERT INTO queue_subtasks (subtask_id, task_id, bot_id, subtask_type, task_json, group_id, task_code, payload, response, task_results) VALUES ('$subtaskid', '$taskid', '$botid', '$type', '$parsel', '$group', '', '', '', '')");
	        if(count(array_unique($result)) === 1)
	            return current($result);
	            else return;
	    }elseif($type == 'updatelocal')
	    {
	        $result[] = $this->query("INSERT INTO queue_task (task_id, task_type, task_name, task_submitter, botresponse, botid, submit_timestamp, clear) VALUES ('$taskid', '$type','$taskname', '$custom', '$botresponse', '$botid', now(), '')");
	        
	        foreach ($parsel as $key=>$value)
	        {
	            $subtaskid = $this->generatesubtaskid();
	            $result[] = $this->query("INSERT INTO queue_subtasks (subtask_id, task_id, bot_id, subtask_type, task_json, group_id, task_code, payload, response, task_results) VALUES ('$subtaskid', '$taskid', '$botid', '$type', '$value', '', '' ,'' ,'' ,'')");
	        }
	        if(count(array_unique($result)) === 1)
	            return current($result);
	            else return;
	    }
	}
	
	public function httpResponseLabel($code, $description=false) {
		$result = $this->select("SELECT * FROM webex_response_codes WHERE code = '$code'");
		return (!$description) ? $result[0]['label'] : $result[0]['label'] . " " . $code . " " .  $result[0]['status'];
	}
	
	public function messagesentcount() {
		$result = $this->select("SELECT COUNT(*) FROM queue_subtasks WHERE subtask_type = 'messages' and task_code = '200'");
		return $result;
	}
}

class SparkEngine {
	
	protected $db;
	protected $crd;
	protected $generate;
	
	//Create new objects
	public function __construct() {
		$this->db = new Db();
		$this->crd = new Cards();
		$this->generate = new OutputEngine();
	}
	
	public function getApiUrl($api){
		$apiList = array("messages"=>"https://webexapis.com/v1/messages",
				"devices" =>"https://webexapis.com/v1/devices",
				"devicestatus" => "https://webexapis.com/v1/xapi/status",
				"devicecommand" => "https://webexapis.com/v1/xapi/command",
				"deviceconfig" => "https://webexapis.com/v1/deviceConfigurations",
				"people"=>"https://webexapis.com/v1/people",
				"webhooks"=>"https://webexapis.com/v1/webhooks",
				"rooms"=>"https://webexapis.com/v1/rooms",
				"membership"=>"https://webexapis.com/v1/memberships",
				"teams"=>"https://webexapis.com/v1/teams",
				"teammemberships"=>"https://webexapis.com/v1/team/memberships",
				"integrationtoken"=>"https://webexapis.com/v1/access_token",
				"integrationauth"=>"https://webexapis.com/v1/authorize",
				"place"=>"https://webexapis.com/v1/places",
				"workspaces"=>"https://webexapis.com/v1/workspaces",
				"actions"=>"https://webexapis.com/v1/attachment/actions"
		);
		return $apiList[$api];
	}
	//HEADERS
	public function build_spark_headers($auth, $method, $content=NULL, $type='') {
		global $config;
		$authorization = "Authorization: Bearer {$auth}";
		$timeout = 10;
		if ($type == 'json') {
			$contentType = "application/json"; //Changed application/json -> Content-Type: application/json
			$content = http_build_query($content); 
		} else if ($type == "integration") {
			$contentType = "Content-Type: application/x-www-form-urlencoded";
			$content = http_build_query($content);
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_POSTFIELDS => $content,
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($contentType),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_PROXY => $config['proxyurl'],
					CURLOPT_PROXYUSERPWD => $config['proxyuserpass']
			);
		}
		else {
			$contentType = "Content-Type: application/json";
		}
		if ($method == "GET") {
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_PROXY => $config['proxyurl'],
					CURLOPT_PROXYUSERPWD => $config['proxyuserpass']
					
			);
		}
		elseif (($method == "POST" or $method == "PUT") and !empty($content)) {
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_POSTFIELDS => json_encode($content),
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_PROXY => $config['proxyurl'],
					CURLOPT_PROXYUSERPWD => $config['proxyuserpass']
			);
		}
		elseif ($method == "PATCH") {
			$contentType = "Content-Type: application/json-patch+json";
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_POSTFIELDS => json_encode($content),
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_PROXY => $config['proxyurl'],
					CURLOPT_PROXYUSERPWD => $config['proxyuserpass']
			);
		}
		elseif ($method == "DELETE") {
			return array(
					CURLOPT_TIMEOUT => $timeout,
					CURLOPT_CUSTOMREQUEST => $method,
					CURLOPT_SSL_VERIFYPEER => FALSE,
					CURLOPT_HTTPHEADER => array($authorization, $contentType),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_PROXY => $config['proxyurl'],
					CURLOPT_PROXYUSERPWD => $config['proxyuserpass']
			);
		}
	}
	//AUTHORIZATION / 
	public function authGet($id){
	    if ($this->db->wsIntegrationExists($id)) {
	           
	        $tokendata = $this->db->wsIntegrationInfoAPI($id); 
	        if (count($tokendata['token'])) {
               
	            if ($tokendata['token']['at']['expired'] and !$tokendata['token']['rt']['expired']) {
                   
	                $refreshData = $this->wsIntegrationRefreshAccessToken($id);
                    
                    if (isset($refreshData['access_token'])) {
                       
                        return $refreshData['access_token'];
                    
                    }
               
	            }
               
           }
           
           return $tokendata['integration']['access_token'];
	    
	    }
	    
	    else if ($this->db->integrationAccessTokenExists($id)) {
	        
			//Authorization should be integrated user
			$integrationdata = $this->db->integrationAPIGetTokenData($id);
			
			if ($integrationdata['access_token']['expired']) {
			    
				if ($integrationdata['refresh_token']['expired']) {
				    
					return false;
				
				}
				else {
				    
					$this->integrationRefresh($id);
					
				
				}
			
			} else {
				
			    return $integrationdata['tokendata']['access_token'];
			
			}
		
	    }
				
		$result = $this->db->select_specific("SELECT * FROM bots WHERE id = '{$id}'");
		
		return $result['access'];
		
	}
		
	
	//MESSAGES
	public function messageBlob($text, $recepient, $botid, $roomtype="", $spacetype="", $files="", $attachment = "") {
		if(empty($roomtype)) $roomtype = (validateEmail($recepient)) ? "toPersonEmail":"toPersonId";
		$data = array('sender'=>$botid,'recepientType'=>$roomtype,'recepientValue'=>$recepient,'text'=>$text,'type'=>$spacetype);
		if (!empty($files)) {
			$data['files'] = $files;
		}
		if (!empty($attachment)) {
			$data['attachments'] = $attachment;
		}
		return $data;
 	}
	public function messageSendEvent($text) {
		$log_users = $this->db->adminGetLogUsers();
		$logbotid = $this->db->adminGetLogBot();
		$m_query = array();
		$a = 0;
		
		if (count($log_users)>0 and $logbotid) {
			foreach ($log_users as $key => $loguser) {
				//array(array('url'=>'', 'method'=>'', 'type'=>'', 'auth'=>'', 'post'=>array()))
				$m_query[$a]['url'] = $this->getApiUrl("messages");
				$m_query[$a]['method'] = 'POST';
				$m_query[$a]['type'] = '';
				$m_query[$a]['auth'] = $this->authGet($logbotid);
				$m_query[$a]['post'] = array();
				$m_query[$a]['post']['toPersonEmail'] = $loguser['email'];
				$m_query[$a]['post']['markdown'] = $text;
				$a++;
				//$this->messageSend($this->messageBlob($text, $loguser['email'], $logbotid));
			}
			$this->data_multi_post($m_query);
		} else {
			return false;
		}
	}
	#kwargs = (sender(appId, botId, integration), receiver_type(roomId, toPersonId, toPersonEmail), receiver(roomId, personId, email), text, files*, attachments*, mention*, mention_self*, mention_self_name*)
	public function messageSendCustom(array $kwargs = array()) {
	    $url = $this->getApiUrl('messages');
	    $auth = (isset($kwargs['auth'])) ? $kwargs['auth'] : $this->authGet($kwargs['sender']);
	    
	    $message_payload = array(
	        $kwargs['receiver_type'] => $kwargs['receiver'],
	        "markdown" => $kwargs['text'],
	    );
	    
	    if(isset($kwargs['files'])) {
	        $message_payload_array['files'] = $kwargs['files'];
	    }
	    
	    if(isset($kwargs['attachments'])) {
	        $message_payload_array['attachments'] = $kwargs['attachments'];
	    }
	    
	    if(!isset($kwargs['parent'])) {
	        $message_payload_array['parentId'] = $kwargs['parent'];
	    }
	    
	    if (isset($kwargs['mention'])) {
	        foreach (explode(',', $kwargs['mention']) as $v) {
	            $message_payload['markdown'] .= $message_payload['markdown'] . "<@personEmail:$v|$v>";
	        }
	    }
	    
	    if (isset($kwargs['mention_self'])) {
	        $message_payload['markdown'] .= $message_payload['markdown'] . "<@personEmail:{$kwargs['mention_self']}|".(isset($kwargs['mention_self_name'])) ? $kwargs['mention_self_name']:$kwargs['mention_self'].">";
	    }
	     
	    return $this->data_post($this->build_spark_headers($auth, 'POST', $message_payload), $url);
	    
	}
	
	public function messageSend($data, $reply_id = "") {
		$auth = $this->authGet($data['sender']);
		$botinfo = $this->db->botFetchBots($data['sender']);
		$mentionself = $botinfo[0]['emails'];
		$type = issetor($data['type']);
		$url = $this->getApiUrl("messages");
		$recepient_type = $data['recepientType'];
		$recepient_value = $data['recepientValue'];
		$text = $data['text'];
		
		if ($type == "group" and !isset($data['attachments'])) {
			$text = $text . "<@personEmail:$mentionself|.>";
		}
		
		$message_payload_array = array("{$recepient_type}" => "{$recepient_value}", "markdown" => "{$text}");
		
		if(isset($data['files'])) {
			$message_payload_array['files'] = $data['files'];
		}
		if(isset($data['attachments'])) {
			$message_payload_array['attachments'] = $data['attachments'];
		}
		if(!empty($reply_id)) {
			$message_payload_array['parentId'] = $reply_id;
		}
				
		//Returns array of data for json conversion
		return $this->data_post($this->build_spark_headers($auth, 'POST', $message_payload_array), $url);
	}
	public function messageSendMultiple($list, $botid) {
		//$list is an array("rec"=>"ReceiverID", "Type" => "toPersonId, roomId", "message"=>"Markdown message", "files" => "fileurl")
		$m_query = $report = array();
		$errors = "<blockquote class='warning'>";
		$auth = $this->authGet($botid);
		$botinfo = $this->db->botFetchBots($botid);
		$mentionself = $botinfo[0]['emails'];
		$url = $this->getApiUrl("messages");
		
		$a = 0;
		
		foreach ($list as $key => $parsel) {
			$text = ($parsel['type'] == 'roomId') ? $parsel['message'] . "<@personEmail:$mentionself|.>" : $parsel['message'];
			$m_query[$a]['url'] = $url;
			$m_query[$a]['auth'] = $auth;
			$m_query[$a]['method'] = "POST";
			$m_query[$a]['type'] = "";
			$m_query[$a]['post'] = array();
			$m_query[$a]['post'][$parsel['type']] = $parsel['rec'];
			$m_query[$a]['post']['markdown'] = $text;
			if (!empty($parsel['files'])) $m_query[$a]['post']['files'] = $parsel['files'];
			if (!empty($parsel['card_attachment'])) $m_query[$a]['post']['attachments'] = $parsel['card_attachment'];
			$a++;
		}
		
		$result = $this->data_multi_post($m_query);
		$report['result'] = $result;
		
		return $report;
	}
	public function messageSendMultipleQueue($list, $botid, $custom) {
		//$list is an array("rec"=>"ReceiverID", "Type" => "toPersonId, roomId", "message"=>"Markdown message", "files" => "fileurl")
		$m_query = $report = array();
		$errors = "<blockquote class='warning'>";
		$auth = $this->authGet($botid);
		$botinfo = $this->db->botFetchBots($botid);
		$mentionself = $botinfo[0]['emails'];
		$url = $this->getApiUrl("messages");
		
		// $a = 0;

		foreach($list as $key=>$value)
		{
			$subtaskid = $value['subtask_id'];
			$task_json = json_decode(str_replace(array("\r\n", "\n", "\r"),'\n',$value['task_json']), true);

			$text = ($value['subtask_type'] == 'roomId') ? $task_json['message'] . "<@personEmail:$mentionself|.>" : $task_json['message'];
			$m_query[$subtaskid]['url'] = $url;
			$m_query[$subtaskid]['auth'] = $auth;
			$m_query[$subtaskid]['method'] = "POST";
			$m_query[$subtaskid]['type'] = ""; //changed from type = json
			$m_query[$subtaskid]['post'] = array();
			$m_query[$subtaskid]['post'][$task_json['type']] = $task_json['rec'];
			$m_query[$subtaskid]['post']['markdown'] = $text;
			if (issetor($task_json['files'])) $m_query[$subtaskid]['post']['files'] = $task_json['files'];
			if (!empty($task_json['card_attachment'])) $m_query[$subtaskid]['post']['attachments'] = $task_json['card_attachment'];
		}

		$result = $this->data_multi_post($m_query);
		
		foreach ($result as $key => $r) {
			if ($r['http_code'] == '200') {
				$this->db->query("UPDATE queue_subtasks SET task_results = 'Message sent successfully' WHERE subtask_id = '$key'");
			} else {
				$this->db->query("UPDATE queue_subtasks SET task_results = 'Failed' WHERE subtask_id = '$key'");
			}
		}
		
		$report['result'] = $result;

		return $report;
	}
	public function messageFetchMessages($botid, $roomid, $type=""){
		$url = $this->getApiUrl("messages");
		$auth = $this->authGet($botid);
		
		return ($type == "group") ? $this->data_get(array('roomId' => $roomid, 'mentionedPeople'=>'me', 'max'=>'50'), $this->build_spark_headers($auth, "GET"), $url) :
		$this->data_get(array('roomId' => $roomid, 'max'=>'50'), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function messageGetDetails($botid, $messageid){
		$url = $this->getApiUrl("messages").'/'.$messageid;
		$auth = $this->authGet($botid);
		
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function messageDelete($botid, $messageid){
		$url = $this->getApiUrl("messages")."/".$messageid;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//WEBHOOKS
	public function webhookGet($botid) {
		$url = $this->getApiUrl("webhooks");
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function webhookCreate($data) {
		$url = $this->getApiUrl("webhooks");
		$auth = $this->authGet($data['sender']);
		$webhook_data = array();
		foreach ($data as $key => $value) {
			if (!empty($value) and ($key != 'sender' and $key != 'submit')) {
				$webhook_data[$key] = $value;
			}
		}
		return $this->data_post($this->build_spark_headers($auth, 'POST', $webhook_data), $url);
	}
	public function webhookCreateQuick($botid, $type) {
		$query = array('name'=>'group', 'sender'=>$botid, 'targetUrl'=>getHostUrl().'/api/hooker.php','resource'=>'messages','event'=>'created', 'filter'=>'roomType=group');
		if ($type == 'direct') {
			$query['name'] = 'direct';
			$query['filter'] = 'roomType=direct';
		}
		else if ($type == 'card') {
			$query = array('name'=>'cardActions', 'sender'=>$botid, 'targetUrl'=>getHostUrl().'/api/handlers/card_handler.php','resource'=>'attachmentActions','event'=>'created');
		}
		$result = $this->webhookCreate($query);
		$this->db->webhookDbLink($result['id'], $botid);
		return $result['id'];
	}
	public function webhookDelete($webhook_id, $botid) {
		$url = $this->getApiUrl("webhooks")."/".$webhook_id;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	
	
	//Webex Teams Workspaces
	//List Workspaces
	public function placeGet($botid, $displayname = "", $max = "") {
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("workspaces");
		$search = array();
		$search["displayName"] = issetor($displayname);
		$search["max"] = issetor($max);
		
		return $this->data_get($search, $this->build_spark_headers($auth, "GET"), $url);
	}
	
	//List place details
	public function placeGetDetails($botid, $placeid) {
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("workspaces")."/".$placeid;		
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	
	//Create a place
	public function placeCreate($botid, $displayName) {
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("workspaces");
		$payload = array("displayName"=>$displayName);
		return $this->data_post($this->build_spark_headers($auth, "POST", $payload), $url);
	}
	
	//Update a place
	public function placeUpdate($botid, $placeId, $displayName) {
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("workspaces")."/".$placeId;
		$payload = array("displayName"=>$displayName);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $payload), $url);
	}
	
	//Delete a place
	public function placeDelete($botid, $placeId) {
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("workspaces")."/".$placeId;
		return $this->data_post($this->build_spark_headers($auth, "DELETE"), $url);
	}
	
	//Spark GET functions
	//Spark people list, takes recipient type (email, personId, displayName) and search string (email address or partial name)
	public function peopleGet($data) {
		$auth = $this->authGet($data['sender']);
		$url = $this->getApiUrl("people");
		
		$request_type = $data['recepientType'];
		$request_value = $data['recepientValue'];
		$request = array(
				"{$request_type}" => "{$request_value}"
		);
		
		return $this->data_get($request, $this->build_spark_headers($auth, "GET"), $url);
	}
	
	public function peopleGetDetails($botid, $personid) {
	    $auth = $this->authGet($botid);
	    $url = $this->getApiUrl("people").'/'.$personid;	    
	    return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	//$memberParsel must be a nested array: array(array("roomtype" => "teamId|roomId", "id" => "roomid|teamid", "method" => "personEmail|personId", "person"=>"id/email"))
	public function peopleGetCrossOrg($botid, $emails) {
	    
	    $roomdata = $this->roomCreate($botid, "tmp");
	    $parsel = array();
	    
	    foreach ($emails as $k=>$v) {
	        $parsel[] = array(
	            'roomtype'     => 'roomId',
	            'id'           => $roomdata['id'],
	            'method'       => 'personEmail',
	            'person'       => $v
	        );
	    }
	    
	    $result = $this->membershipCreateMultiple($parsel, $botid);
	    
	    return $result;
	    
	}
	
	//Adding multieple users without creating a task 
	public function peopleAddMultipleDirect($people, $botid, array $kwargs=array()) {
	    
	    if (in_array('groups', $kwargs)) {
	        
	        $groups = $kwargs['groups'];
	    
	    }
	    
	    if (in_array('botResponse', $kwargs)) {
	        $botResponse = $kwargs['botResponse'];
	    }
	    
	    $m_query = $report = array();
	    $errors = "";
	    $auth = $this->authGet($botid);
	    $url = $this->getApiUrl("people");
	    $a = 0;
	    
	    foreach ($people as $type => $person) {
	        
	        $m_query[$a]['url'] = $url;
	        $m_query[$a]['auth'] = $auth;
	        $m_query[$a]['method'] = "GET";
	        $m_query[$a]['type'] = "";
	        $m_query[$a]['get'] = array();
	        $m_query[$a]['get'][(validateEmail($person)) ? 'email':'id'] = $person;
	        $a++;
	        
	    }
	    
	    $result = $this->data_multi_get($m_query);
	    
	    foreach ($result as $key => $r) {
	        $userid = issetor($r['response']['items'][0]['id']);
	        $existing = 0;
	        
	        //Check is userid is empty or not
	        if (!empty($userid)) {
	            $existing = count($this->db->contactFetchContacts($userid));
	        } else {
	            $result[$key]['http_code']= 404;
	            continue;
	        }
	        
	        //Check if user is already in the database
	        if ($existing) {
	            //Create error if exists
	            $result[$key]['http_code'] = 409;
	        } else {
	            //Add user to DB if not exists
	            $this->db->contactAdd($r['response'], $botid);
	        }
	        
	        //Check if the user exists after adding it
	        $existing = count($this->db->contactFetchContacts($userid));
	        if (!$existing) {
	            $errors = "User was found but could not be added to db for an unknown reason";
	            $result[$key]['http_code']= 500;
	            continue;
	        }
	        //Add user to group if the user exists
	        if (count($groups) and $existing) {
	            foreach ($groups as $key => $group) {
	                $fields = array('groupid' => $this->db->quote($group),
	                    'contactid' => $this->db->quote($r['response']['items'][0]['id']));
	                $this->db->groupAddContact($fields);
	            }
	            
	        }
	    }
	    $report['result'] = $result;
	    $report['error_report'] = $this->check_multi_error($result);
	    $report['errors'] = $errors;
	    return $report;
	}
		
	public function peopleAddMultipleToLocal($csv, $botid, $group) {
		$m_query = $report = array();
		$errors = "";
		$auth = $this->authGet($botid);
		$url = $this->getApiUrl("people");
		$a = 0;

		foreach ($csv as $key => $value) {
			$subtaskid = $value['subtask_id'];
			$person = $value['task_json'];
			$m_query[$subtaskid]['url'] = $url;
			$m_query[$subtaskid]['auth'] = $auth;
			$m_query[$subtaskid]['method'] = "GET";
			$m_query[$subtaskid]['type'] = "";
			$m_query[$subtaskid]['get'] = array();
			if (validateEmail($person)) {
			    $m_query[$subtaskid]['get']['email'] = $person;
			} else {
			    $m_query[$subtaskid]['get']['id'] = $person;
			}			
		}

		$result = $this->data_multi_get($m_query);
		
		foreach ($result as $key => $r) {
			$userid = issetor($r['response']['items'][0]['id']);
			$existing = 0;
			
			//Check is userid is empty or not
			if (!empty($userid)) {
				$existing = count($this->db->contactFetchContacts($userid));
			} else {
				$result[$key]['http_code']= 404;
				$this->db->query("UPDATE queue_subtasks SET task_results = 'User Not Found in Webex Teams' WHERE subtask_id = '$key'");
				continue;
			}
			
			//Check if user is already in the database
			if ($existing) {
				//Create error if exists
				$result[$key]['http_code'] = 409;
				$this->db->query("UPDATE queue_subtasks SET task_results = 'User already exists in the database' WHERE subtask_id = '$key'");
			} else {
				//Add user to DB if not exists
				$this->db->contactAdd($r['response'], $botid);
				$this->db->query("UPDATE queue_subtasks SET task_results = 'User added successfully to database' WHERE subtask_id = '$key'");
			}
			
			//Check if the user exists after adding it
			$existing = count($this->db->contactFetchContacts($userid));
			if (!$existing) {
				$errors = "User was found but could not be added to db for an unknown reason";
				$result[$key]['http_code']= 500;
				$this->db->query("UPDATE queue_subtasks SET task_results = '$errors' WHERE subtask_id = '$key'");
				continue;
			}
			//Add user to group if the user exists
			if (!empty($group) and $existing) {
				$fields = array('groupid' => $this->db->quote($group),
						'contactid' => $this->db->quote($r['response']['items'][0]['id']));
				$this->db->groupAddContact($fields);
			}
		}
		$report['result'] = $result;
		$report['errors'] = $errors;
		return $report;
	}
	
	public function peopleUpdateMultipleToLocal($csv) {
		$m_query = $report = $void = array();
		$errors = "";
		
		$auth = $this->authGet($this->db->botGetMainInfo()['id']);
		$url = $this->getApiUrl("people");
		
		foreach ($csv as $key => $value) {
			$subtaskid = $value['subtask_id'];
			$person = $value['task_json'];
			$m_query[$subtaskid]['url'] = $url;
			$m_query[$subtaskid]['auth'] = $auth;
			$m_query[$subtaskid]['method'] = "GET";
			$m_query[$subtaskid]['type'] = "";
			$m_query[$subtaskid]['get'] = array();
			$m_query[$subtaskid]['get']['id'] = $person;
		}

		//Start simultaneous requests
		$result = $this->data_multi_get($m_query);

		foreach ($result as $key => $r) {
			$userid = issetor($r['response']['items'][0]['id']);
			$existing = 0;
			
			//Check is userid is empty or not
			if (!empty($userid)) {
				$this->db->contactUpdate($r['response']);
				$this->db->query("UPDATE queue_subtasks SET task_results = 'Contact Update' WHERE subtask_id = '$key'");
			} else {
				
				if($r['http_code'] == 429)
				{
					$errors = '';
				}
				else
				{
					$errors = "User Not Found in Webex Teams";
					$result[$key]['http_code'] = 404;
				}
				$this->db->query("UPDATE queue_subtasks SET task_results = '$errors' WHERE subtask_id = '$key'");
				continue;
			}
		}

		$report['result'] = $result;
		$report['errors'] = $errors;
		return $report;
	}
	
	public function peopleGetMe($botid) {
		$url = $this->getApiUrl("people").'/me';
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function peopleConfirmToken($accesstoken) {
		$url = $this->getApiUrl("people").'/me';
		return $this->data_get(array(), $this->build_spark_headers($accesstoken, "GET"), $url);
	}
	public function download_image($image_url, $image_file){
		global $config;
		$fp = fopen ($image_file, 'w+');              // open file handle
		$ch = curl_init($image_url);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
		curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
		curl_setopt($ch, CURLOPT_PROXY, $config['proxyurl']);
		curl_setopt($ch, CURLOPT_PROXYUSERPWD, $config['proxyuserpass']);
		// curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
		curl_exec($ch);

		curl_close($ch);                              // closing curl handle
		fclose($fp);                                  // closing file handle
	}
	public function wizardAddBot($token, $primary=False) {
		$url = $this->getApiUrl("people").'/me';
		$text = "was added!";
		$botvalues = $this->data_get(array(), $this->build_spark_headers($token, "GET"), $url);
		if (isset($botvalues['id'])) {
			$botid = $botvalues['id'];
			$this->db->wizardAddBot($botvalues);
			$this->db->botUpdateSettings($botid, $token);
			if ($primary) {
				$this->db->botSetPrimary($botid);
				$text = "was added as the primary bot!";
			}
			$this->download_image($botvalues['avatar'], 'images/bots/'.$botvalues['id'].'.jfif');
			return "<img src='images/bots/{$botvalues['id']}.jfif' class='img-circle' height='50' width='50'> <b>{$botvalues['displayName']}</b> $text";
		}
		else {
			return false;
		}
	}
	//CARD ACTIONS
	public function cardGetData($botid, $dataid) {
		$url = $this->getApiUrl("actions").'/'.$dataid;
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
		
	public function cardPrepareCard($cardid, array $card) {
		$identifier = array(
				"type"=>"Input.Text", 
				"isVisible"=>false,
				"id"=>"cardId",
				"value"=>$cardid		
		);
		if (isset($card['body'])) {
			$card['body'][] = $identifier;
			$cardmanifest = array(
					"contentType" => "application/vnd.microsoft.card.adaptive",
					"content" => $card
			);
			return $cardmanifest;
		}
	}
	
	public function cardPrepareFeatureCard($card, $feature = "") {
		$cardmanifest = array(
			"contentType" => "application/vnd.microsoft.card.adaptive",
			"content" => $card
		);
		return $cardmanifest;
	}
	
	public function cardTestCardMessage($cardid, $userid) {
		$cardinfo = $this->db->cardFetchCards($cardid);	
		$roomtype = (validateEmail($userid)) ? "toPersonEmail":"toPersonId";
		$preparedCard = $this->cardPrepareCard($cardid, json_decode($cardinfo[0]['card_body'], true));
		
		$data = array('sender'=>$cardinfo[0]['botid'],'recepientType'=>$roomtype,'recepientValue'=>$userid,'text'=>'If this message is rendered, the card failed','type'=>'direct', 'attachments'=>$preparedCard);
		
		return $this->messageSend($data);
	}
	
	//DEVICES
	public function deviceGenCard($botid, $deviceid) { //Create a device card for sending commands
		global $rootdoc;
		include $rootdoc.'/public/includes/cards.inc.php';
		
		$devicedata = $this->deviceGetDetails($botid, $deviceid);
		
		$card = json_decode($cardDeck['deviceKeyParam'], true);
		
		if (isset($devicedata['id'])) {
			$card['body'][0]['columns'][0]['items'][0]['text'] = "Cloud Device xAPI";
			$card['body'][1]['columns'][1]['items'][0]['text'] = $devicedata['displayName'];
			$card['body'][1]['columns'][1]['items'][1]['text'] = $devicedata['product'];
			$card['body'][1]['columns'][1]['items'][2]['text'] = $devicedata['software'];
			$card['body'][1]['columns'][1]['items'][3]['text'] = (isset($devicedata['upgradeChannel']) and strpos($devicedata['software'], "RoomOS") == false) ? $devicedata['upgradeChannel']:"Webex Edge for Devices"; 
			$card['body'][10]['actions'][0]['data']['deviceid'] = $deviceid;
			$card['body'][5]['placeholder'] = "key.path";
			$card['body'][7]['placeholder']= "{\"parameter\":\"value\",..} | Value";
			$card['body'][9]['placeholder'] = "xCommand multiline body";
			$card['body'][10]['actions'][0]['title'] = "Execute";
			
			return array(
					"markdown" => "Something went wrong!",
					"attachments" =>  $this->cardPrepareFeatureCard($card)
			);
		}
		return blockquote("I could not find details about the device with the provided device id, check your integration settings", "danger");
	}
	public function deviceList($botid, array $search = array()) { //List devices belonging to the bot
		$url = $this->getApiUrl("devices");
		$auth = $this->authGet($botid);
		return $this->data_get($search, $this->build_spark_headers($auth, "GET"), $url);
	}
	public function deviceGetDetails($botid, $deviceid) { //List devices belonging to the bot
		$url = $this->getApiUrl("devices")."/".$deviceid;
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function deviceStatus($botid, $deviceid, $key='*') { //Get status of a device for a particular key
		$url = $this->getApiUrl("devicestatus");
		return $this->data_get(array('name'=>$key, 'deviceId'=>$deviceid), $this->build_spark_headers($this->authGet($botid), "GET"), $url);
	}
	public function deviceGetConfig($botid, $deviceid, $key='*') { //Get status of a device for a particular key
		$url = $this->getApiUrl("deviceconfig");
		return $this->data_get(array('key'=>$key, 'deviceId'=>$deviceid), $this->build_spark_headers($this->authGet($botid), "GET"), $url);
	}
	public function deviceSetConfig($botid, $deviceid, $key, $value) { //Get status of a device for a particular key
		$url = $this->getApiUrl("deviceconfig")."?deviceId=".$deviceid;
		$key = $key . "/sources/configured/value";
		$payload = array(
				"path"=>$key,
				"op"=>"replace",
				"value"=>$value
		);
		return $this->data_post($this->build_spark_headers($this->authGet($botid), "PATCH", $payload), $url);
	}
	public function deviceGetActivationCode($botid, $placeid) { //Get activationcode for place
		$url = $this->getApiUrl("devices")."/activationCode";
        $payload = array("placeId"=>$placeid);
		return $this->data_post($this->build_spark_headers($this->authGet($botid), "POST", $payload), $url);
	}
	public function deviceCommand($botid, $deviceid, $commandkey, $args, $body = "") { //Send command to device
		$url = $this->getApiUrl("devicecommand").'/'.$commandkey;
		$payload = array(
				'deviceId'=>$deviceid
		);
		
		$args = trim($args);
		$payload['arguments'] = json_decode($args, true);
		
		if (!empty(trim($body))) {
			//$args = trim($args);
			$payload['body'] = $body;
		}
		return $this->data_post($this->build_spark_headers($this->authGet($botid), "POST", $payload), $url);
	}
	#public function deviceDelete($botid, $deviceid) { //Send command to device
	#	$url = $this->getApiUrl("devices").'/'.$deviceid;
	#	return $this->data_post($this->build_spark_headers($this->authGet($botid), "DELETE"), $url);
	#}
	//TEAMS
	public function teamGet($botid, $max) {
		$url = $this->getApiUrl("teams");
		$auth = $this->authGet($botid);
		return $this->data_get(array("max"=>$max), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function teamGetDetails($botid, $teamid) {
		$url = $this->getApiUrl("teams").'/'.$teamid;
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function teamCreate($botid, $name) {
		$url = $this->getApiUrl("teams");
		$data = array("name"=>$name);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "POST", $data), $url);
	}
	public function teamUpdate($botid, $teamid, $name) {
		$url = $this->getApiUrl("teams").'/'.$teamid;
		$data = array("name"=>$name);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $data), $url);
	}
	public function teamMembershipGet($teamid, $botid, $max='500') {
		$url = $this->getApiUrl("teammemberships");
		$auth = $this->authGet($botid);
		$request = array(
				"teamId" => $teamid,
				"max" => $max
		);
		return $this->data_get($request, $this->build_spark_headers($auth, "GET"), $url);
	}
	//Adds a user e-mail to a team
	public function teamMembershipCreate($teamid, $botid, $person) {
		$url = $this->getApiUrl("teammemberships");
		$auth = $this->authGet($botid);
		$request = array(
				"teamId" => $teamid,
				"personEmail" => $person
		);
		return $this->data_post($this->build_spark_headers($auth, "POST", $request, 'json'), $url);
	}
	//Takes a csv to add multiple user emails to a team
	public function teamMembershipCreateMultiple($botid, $teamid, $csv) {
		$csv = str_replace(" ", "", $csv);
		$personmail_array = explode(",", $csv);
		$report = array();
		foreach ($personmail_array as $key => $person) {
			if (validateEmail($person)){
				$report[] = $this->teamMembershipCreate($teamid, $botid, $person);
			}
			else{
				continue;
			}
		}
		return $report;
	}
	//ROOMS
	public function roomGet($data) {//Dataarray maxresults, sender
		$url = $this->getApiUrl("rooms");
		$auth = $this->authGet($data['sender']);
		$query = (isset($data['teamId']) and !empty($data['teamId'])) ? array("max"=>$data['max'], "teamId"=>$data['teamId']):array("max"=>$data['max'], "type"=>$data['type']);
		return $this->data_get($query, $this->build_spark_headers($auth, "GET"), $url);
	}
	public function roomGetDetails($botid, $roomid) {
		$url = $this->getApiUrl("rooms").'/'.$roomid;
		$auth = $this->authGet($botid);
		return $this->data_get(array(), $this->build_spark_headers($auth, "GET"), $url);
	}
	public function roomCreate($botid, $title, $teamid="") {
		$url = $this->getApiUrl("rooms");
		$data = ($teamid != "") ? array("title"=>$title, "teamId"=>$teamid):array("title"=>$title);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "POST", $data), $url);
	}
	public function roomUpdate($botid, $roomid, $title) {
		$url = $this->getApiUrl("rooms").'/'.$roomid;
		$data = array("title"=>$title);
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $data), $url);
	}
	public function roomDelete($botid, $roomid){
		$url = $this->getApiUrl("rooms").'/'.$roomid;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//MEMBERSHIPS
	public function membershipGet($roomid, $botid, $email="") {
		$url = $this->getApiUrl("membership");
		$auth = $this->authGet($botid);
		$request = array(
				"roomId" => $roomid,
				"max" => "1000"
		);
		if (validateEmail($email)){
			$request["personEmail"] = $email;
		}
		return $this->data_get($request, $this->build_spark_headers($auth, "GET"), $url);
	}
	public function membershipSetMod($membershipid, $botid, $bool) {
		$url = $this->getApiUrl("membership").'/'.$membershipid;
		$auth = $this->authGet($botid);
		$request = array( 
				"isModerator" => $bool
		);
		return $this->data_post($this->build_spark_headers($auth, "PUT", $request), $url);
	}
	public function membershipDelete($botid, $membership_id){
		$url = $this->getApiUrl("membership")."/".$membership_id;
		$auth = $this->authGet($botid);
		return $this->data_post($this->build_spark_headers($auth, 'DELETE'), $url);
	}
	//Personmail comma separated example@ex.com, test@test.com...
	public function membershipCreate($botid, $roomid, $personmail){
		$url = $this->getApiUrl("membership");
		$auth = $this->authGet($botid);
		
		$data = array("roomId"=>$roomid,  (validateEmail($personmail)) ? "personEmail":"personId" => $personmail);
		$result = $this->data_post($this->build_spark_headers($auth, 'POST', $data), $url);
		
		if (!isset($result['errors'])) {
		    return $result;
		} else {
		
		  return "User not valid";
		  
		}
		
	}
	//Split up multi requests in smaller bulks
	public function bulkSplitRequestPost($type, $parsel, $botid, $botr=0, $interval=50, $sleep_counter=200, $sleeptime=15, $sleep_mark=300, $custom="") {
		$b = $err = $mark = $total_finished = $succ = 0;
		$timetosleep = 0;
		$delete = $retry_action = $delete_list = array();
		$base_report = $error_report = $extra = $localerrors = $retryhtml = $deletehtml = $files = $messagePayload = $payloads = "";
		$max = count($parsel);
		
		$start = microtime(true); //Timer start
		
		//Execute bulk requests
		if ($max) {
			foreach ($parsel as $key => $value) {
				$request_bulk[$b] = $value;
				$b++;
				
				if ($b == $sleep_mark or $timetosleep == 1) {
					sleep($sleeptime);
					$sleep_mark = ($timetosleep == 1) ? $sleep_mark + $sleep_counter : $sleep_mark;
					$timetosleep = 0;
				}
				
				if ($b == ($mark + $interval) or $b == $max) {
					$mark = $mark + $interval;
					
					//Execute different contexts depending on the incoming data
					//Create memberships in spaces
					if ($type == "memberships"){
						$raw = $this->membershipCreateMultiple($request_bulk, $botid);
					}
					//Send messages to people or spaces
					elseif ($type == "messages") {
						$raw = $this->messageSendMultiple($request_bulk, $botid);
					}
					//Add users from WxT to local database
					elseif ($type == "addtolocal") {
						$raw = $this->peopleAddMultipleToLocal($request_bulk, $botid, $custom);
					}
					elseif ($type == "addtolocaldirect") {
					    $raw = $this->peopleAddMultipleDirect($request_bulk, $botid);
					}
					//Update userinfo in the local database
					elseif ($type == "updatelocal") {
						$raw = $this->peopleUpdateMultipleToLocal($request_bulk);
					}
					
					$request_bulk = array(); //Zero out the bulk to start over
					
					$report = $this->check_multi_error($raw['result']); //Create error list (check the responses for errors)
					
					$total_finished = count($raw['result']) + $total_finished; //Increase amounts of requests
					$succ = $report['success'] + $succ; //Increase amount of successful requests
					
					$localerrors .= issetor($raw['errors']);
					
					if ($type == "updatelocal" and isset($raw['delete'])) {
						if (count($raw['delete'])>0) {
							$delete_list = array_merge($raw['delete'], $delete_list);
						}
					}
					
					//If errors in the report generate details
					if ($report['errors'] > 0) {
						$persondata = array();
						$personid = "";
						
						//Go through all the errors
						foreach ($report['error_report'] as $key => $value) {
							if (issetor($value['outgoing_info']['personId'])) {
								$personid = $value['outgoing_info']['personId'];
							}
							if (issetor($value['outgoing_info']['toPersonId'])) {
								$personid = $value['outgoing_info']['toPersonId'];
							}
							if (issetor($value['outgoing_info']['personEmail'])) {
								$personid = $value['outgoing_info']['personEmail'];
							}
							if (issetor($value['outgoing_info']['email'])) {
								$personid = $value['outgoing_info']['email'];
							}
							if (issetor($value['outgoing_info']['id'])) {
								$personid = $value['outgoing_info']['id'];
							}
							if (issetor($value['outgoing_info']['toPersonEmail'])) {
								$personid = $value['outgoing_info']['toPersonEmail'];
							}
							if ($personid) {
								$persondata = $this->db->contactFetchContacts($personid);
							}
							
							$details = (count($persondata) > 0) ? $persondata[0]['firstName'] . " " . $persondata[0]['lastName'] . " (" . $persondata[0]['emails'] . ")":"({$personid})";
							$http_code = $value['http_code']; //Fetch HTTP response code
							
							//Explain the response codes
							switch ($http_code) {
								case 429:
									$timetosleep = 1;
									if ($type == 'messages') {
										$retry_action[] = $personid;
										if (empty($messagePayload)) $messagePayload = issetor($value['outgoing_info']['markdown']);
										if (isset($value['outgoing_info']['files']) and empty($files)) $files = $value['outgoing_info']['files'];
									}
									if ($type == 'addtolocal') {
										$retry_action[] = $personid;
									}
									$extra = " - Too fast (i.e. the request I sent was too much for Webex Teams to handle)";
									break;
								case 400:
									if (count($persondata) > 0) {
										$extra = " - Missing (i.e. the person may no longer exist in Webex Teams, the person can be deleted from my database)";
									}
									if ($type == 'updatelocal') {
										$delete_list[] = $personid;
									}
									break;
								case 403:
									$extra = " - Response understood but failed (i.e. operation was not allowed)";
									break;
								case 409:
									$extra = " - Conflict (i.e. the person is already in the space or the person does not exist)";
									break;
								case 404:
									$extra = " - Not found (i.e. the person/space cannot be found in Webex Teams)";
									break;
								case 100:
									$extra = " - Delayed response (i.e. the request is most likely successful, but may have failed.)";
									break;
								case 0:
									$extra = " - Delayed response (i.e. the request is most likely successful, but may have failed.)";
									break;
								default:
									$extra = "";
									break;
							}
							
							//If HTTP Code error explain error and increase error count
							$base_report = $err + 1 . ". Issue with processing {$details} -> HttpResponseCode: <strong>{$http_code} {$extra}</strong>";
							$error_report .= ($botr == 1) ? "<li>$base_report</li>":"<br> $base_report";
							$err++;
						}
					}
				}
			}
		}
		$end = number_format((microtime(true) - $start), 2); //Timer end
		if ($succ == $total_finished) $success_code = "success";
		if ($err > 0) $success_code = "warning";
		if ($succ == 0) $success_code = "danger";
		if ($localerrors) $localerrors = feedbackMsg('Local errors:',$localerrors, 'warning', $botr);
		
		if (count($delete_list) > 0 and $type == 'updatelocal') {
			$num_delete = count($delete_list);
			$deletehtml .= "<br><div id='input'><form action='index.php?id=contacts&botid=$botid' method='POST' enctype='multipart/form-data'>";
			foreach ($delete_list as $key => $value) {
				$deletehtml .= "<input type='hidden' name='personids[]' value='$value'>";
			}
			$deletehtml .= "<input type='submit' name='delete_$type' value='Delete $num_delete users from the database? Click here'></form></div>";
		}
		
		if (count($retry_action)) {
			if ($type == 'messages') {
				$formaction = "index.php?id=messages&botid=$botid";
			} else if ($type == 'addtolocal') {
				$formaction = "index.php?id=contacts";
			}
			$num_429 = count($retry_action);
			$retryhtml .= "<br><div id='input'><form action='$formaction' method='POST' enctype='multipart/form-data'>";
			foreach ($retry_action as $key => $value) {
				$retryhtml .= "<input type='hidden' name='personids[]' value='$value'>";
			}
			if ($type == 'messages') {
				$retryhtml .= "<input type='hidden' name='messagepayload' value='$messagePayload'>";
				if (!empty($files)) $retryhtml .= "<input type='hidden' name='files' value='$files'>";
			}
			if ($type == 'addtolocal') {
				$retryhtml .= "<input type='hidden' name='groups' value='$custom'>";
			}
			$retryhtml .= "<input type='submit' name='retry_$type' value='Retry $num_429 requests? Click here'></form></div>";
		}
		if ($type == "updatelocal") $payloads = "(<b>$b payloads</b>)";
		$returnvalue = ($botr == 1) ? "<blockquote class=$success_code>API Response report: <b>$succ/$total_finished</b> successful requests. Finished $total_finished requests in $end seconds, <b>$err</b> errors. <br>$error_report</blockquote>  \n\n$localerrors":feedbackMsg("API Response report: ", "<b>$succ/$total_finished</b> successful requests $payloads. Finished $total_finished requests in $end seconds, <b>$err</b> errors. $error_report $retryhtml <br>$localerrors $deletehtml", $success_code);
		return $returnvalue;
	}
	//Split up multi requests adin smaller bulks
	public function bulkSplitRequestPost_new($type, $parsel, $botid, $interval=50, $sleep_counter=200, $sleeptime=15, $sleep_mark=300, $custom="") {
		$b = $err = $mark = $total_finished = $succ = 0;
		$timetosleep = 0;
		$delete = $retry_action = $delete_list = array();
		$base_report = $error_report = $extra = $localerrors = $retryhtml = $deletehtml = $files = $messagePayload = $payloads = "";
		$max = count($parsel);
		
		$start = microtime(true); //Timer start
		
		//Execute bulk requests
		if ($max) {
			foreach ($parsel as $key => $value) {
				$request_bulk[$b] = $value;
				$b++;
				
				if ($b == $sleep_mark or $timetosleep == 1) {
					sleep($sleeptime);
					$sleep_mark = ($timetosleep == 1) ? $sleep_mark + $sleep_counter : $sleep_mark;
					$timetosleep = 0;
				}
				
				if ($b == ($mark + $interval) or $b == $max) {
					$mark = $mark + $interval;
					
					//Execute different contexts depending on the incoming data
					//Create memberships in spaces
					if ($type == "memberships"){
						$raw = $this->membershipCreateMultiple($request_bulk, $botid);
					}
					//Send messages to people or spaces
					elseif ($type == "messages") {
						$raw = $this->messageSendMultipleQueue($request_bulk, $botid, $custom);
					}
					//Add users from WxT to local database
					elseif ($type == "addtolocal") {
						$raw = $this->peopleAddMultipleToLocal($request_bulk, $botid, $custom);
					}
					//Update userinfo in the local database
					elseif ($type == "updatelocal") {
						$raw = $this->peopleUpdateMultipleToLocal($request_bulk);
					}
					
					$request_bulk = array(); //Zero out the bulk to start over
										
					// $succ = $report['success'] + $succ; //Increase amount of successful requests
					
					// if ($type == "updatelocal" and isset($raw['delete'])) {
					// 	if (count($raw['delete'])>0) {
					// 		$delete_list = array_merge($raw['delete'], $delete_list);
					// 	}
					// }
					
					foreach($raw['result'] as $key => $value)
					{
						if($value['http_code'] == 0)
						{
							$http_code = '200';
						}
						else
						{
							$http_code = $value['http_code'];
						}
						$response = $this->db->quote(json_encode($value['response']));
						$payload = $this->db->quote(json_encode($value['outgoing_info']));
						$insert_code = $this->db->query("UPDATE queue_subtasks SET task_code = '$http_code', response = '$response', payload = '$payload' WHERE subtask_id = '$key'");
					}
					return $insert_code;
					//return $raw['result'];
				}
			}
		}

		
	}
	
	public function membershipCreateMultiple($memberParsel, $botid) {
		//$memberParsel must be a nested array: array(array("roomtype" => "teamId|roomId", "id" => "roomid|teamid", "method" => "personEmail|personId", "person"=>"id/email"))
		$report = $m_query = array();
		$auth = $this->authGet($botid);
		$a = 0;
		foreach ($memberParsel as $key => $parsel) {
			$url = ($parsel['roomtype'] == "teamId") ? $this->getApiUrl("teammemberships"): $this->getApiUrl("membership");
			$m_query[$a]['url'] = $url;
			$m_query[$a]['auth'] = $auth;
			$m_query[$a]['method'] = "POST";
			$m_query[$a]['type'] = "";
			$m_query[$a]['post'] = array();
			$m_query[$a]['post'][$parsel['roomtype']] = $parsel['id'];
			$m_query[$a]['post'][$parsel['method']] = $parsel['person'];
			$a++;
		}
		
		$report['result'] = $this->data_multi_post($m_query);
		return $report;
	}
	
	public function featureExecute($id, $feature_attributes=array(), $info=array()) {
		//Executes a local hardcoded feature not handled by external listener
		if (count($info) == 6) list($botid, $user, $personid, $roomid, $type, $botmail) = $info;
		elseif (count($info) == 7) list($botid, $user, $personid, $roomid, $type, $botmail, $files) = $info;
		else return "Feature fatal error!";
		
		$botinfo = $this->db->botFetchBots($botid);
		$card_mode = ($botinfo[0]['card_mode']) ? true:false;
		$conditions = array(
		    "end" => array("stop", "end", "halt"),
		    "delete" => array("delete", "remove", "erase", "del")
		);
		$card_styles = array(
		  "list_item" => "emphasis"  
		);
		
		list($s, $e) = quote('info');
		
		switch ($id) {
			//Internal feature listener
			//Workspace integration
		    case "ws": 
		        
		        $details = $access = $appId = "";
		        
		        if (!$this->db->contactExists($personid)) {
		            return blockquote("You must be a registered user to use this feature!", "warning", $card_mode);
		        }
		        
		        $personinfo = $this->db->contactDetailsAPI($personid);
		        
		        if (!count($this->db->wsIntegrationGet())) {
		            return blockquote("There are no workspace integrations configured!", "warning", $card_mode);
		        }
		        
		        list($param1, $param2, $param3) = $feature_attributes;
		        $returnvalue = "";
		        
		        if ($param1 == "list") {
		            
		            $wsIntegrations = $this->db->wsIntegrationGet();
		            
		            if (count($wsIntegrations)) {
		                
		                $wsIntegrations = array_slice($wsIntegrations, 0, 19);
		                
		                $ws_list = $this->crd->cardItem('new_card', ["body" => [
		                    $this->crd->cardItem('textblock', [
		                        'text' => 'Active integrations',
		                        'size' => 'Large',
		                        'weight' => 'bolder',
		                        'separator' => true
		                    ])
		                ]]);
		                
		                foreach ($wsIntegrations as $key => $value) {
		                    
		                    if ($value['activation_status'] and $this->db->wsIntegrationCheckUserAccess($value['id'], $personid)) {
		                        $ws_list['body'][] = $this->crd->cardItem('container', [
		                            'style' => $card_styles['list_item'],
		                            'spacing' => 'Default',
		                            'minHeight' => '5px',
		                            'items' => [
		                                $this->crd->cardItem('columnset', [
		                                    'columns'=> [
		                                        $this->crd->cardItem('column', [
		                                            'items'=>[
		                                                $this->crd->cardItem('textblock', [
		                                                    'text' => "{$value['name']}",
		                                                    'weight' => 'bolder'
		                                                ]),
		                                                $this->crd->cardItem('factset', [
		                                                    'facts' => [
		                                                        ['title' => 'Org', 'value' => $value['organization']],
		                                                        ['title' => 'AppId', 'value' => $value['id']]
		                                                    ],
		                                                    'weight' => 'lighter',
		                                                    'size' => 'small'
		                                               ]),
		                                               $this->crd->cardItem('actionset', [
		                                                   'actions' => [
		                                                       $this->crd->cardItem('submit', [
		                                                           'title' => 'Mount',
		                                                           'data' => [
		                                                               'wbm_command' => 'ws mount '. $value['id']
		                                                           ]
		                                                       ]),
		                                                       $this->crd->cardItem('submit', [
		                                                           'title' => 'View',
		                                                           'data' => [
		                                                               'wbm_command' => 'ws show '. $value['id']
		                                                           ]
		                                                       ])
		                                                   ]
		                                                   
		                                               ])
		                                            ]
		                                        ]),
		                                    ]
		                                    
		                                ])
		                            ]
		                        ]);
		                        
		                    } 
        
		                }
		                
		            } else {
		                
		                return blockquote("No integrations found", "warning", $card_mode);
		                
		            }
		            
		            $ws_list['body'][] = $this->crd->cardItem(
		                'actionset' , [
		                    'actions' => [
		                        $this->crd->cardItem('submit', [
		                            'title' => 'Show mounted',
		                            'data' => [
		                                'wbm_command' => 'ws show'
		                            ]
		                        ]),
		                        $this->crd->cardItem('submit', [
		                            'title' => 'Unmount',
		                            'style' => 'destructive',
		                            'data' => [
		                                'wbm_command' => 'ws unmount'
		                            ]
		                        ])
		                    ]
		                
		            ]);
		            
		            return $this->crd->card_prepare($ws_list);
		            
		        } else if ($param1 == "show") {
		            
		            if ($param2) {
		                
		                $param2 = $this->db->quote($param2);
		                
		                if (!$this->db->wsIntegrationExists($param2)) {
		                    return blockquote("You have provided an invalid workspace integration id", "danger", $card_mode);
		                }
		                
		                $details = $this->db->wsIntegrationInfoAPI($param2);
		                                  
		                if(!$this->db->wsIntegrationCheckUserAccess($details['integration']['id'], $personid)) {
		                    return blockquote("You do not have access to this workspace integration!", "danger", $card_mode);
		                }
		                
		                if ($param3 != 'all') {
		                    $details = $this->db->wsIntegrationInfoAPISensor($details);
		                }
		                
		                return codeBlock(json_encode($details, JSON_PRETTY_PRINT));
		                
		            } else {
		                
		                $userapplink = $this->db->wsIntegrationGetUserLink($personid);
		                
		                if (count($userapplink)) {
		                    $userinfo = 
		                    $appId = $userapplink[0]['appid'];
		                    $details = $this->db->wsIntegrationInfoAPI($appId);
		                    
		                    if (!$this->db->wsIntegrationCheckUserAccess($details['integration']['id'], $personid)) {
		                        
		                        $this->db->wsIntegrationLinkDelete($personid);
		                       
		                        return blockquote("You no longer have access to this workspace integration and have been unmounted!", "danger", $card_mode);
		                        
		                    }
		                    
		                    if (!count($details['token'])) {
		                        
		                        $this->db->wsIntegrationLinkDelete($personid);
		                        
		                        return blockquote("Could not find a valid token on {$details['integration']['name']} - Automatically unmounted", "warning", $card_mode);
		                        
		                    }
		                    
		                    if ($param2 != 'all') {
		                        $details = $this->db->wsIntegrationInfoAPISensor($details);
		                    }
		                    
		                    $returnvalue = array(
		                        "user" => $personinfo['items'][0]['firstName'] ." ". $personinfo['items'][0]['lastName'] . " (" . $personinfo['items'][0]['emails'] . ")",
		                        "mounted_integration" => $details 
		                    );
		                    
		                    
		                    return codeBlock(json_encode($returnvalue, JSON_PRETTY_PRINT));
		                    
		                } else {
		                    return blockquote("There are no workspace integration mounted on your account", "warning", $card_mode);
		                }
		                
		            }
		            //Mount an integration
		        }  else if ($param1 == "mount") {
		            
		            if (!$param2) {
		                return blockquote("Please provide a workspace integration ID", "warning", $card_mode);
		            }
		            
		            if ($this->db->wsIntegrationExists($param2)) {
		                $appId = $param2;
		                $details = $this->db->wsIntegrationInfoAPI($appId);
		                
		                if (!$this->db->wsIntegrationCheckUserAccess($details['integration']['id'], $personid)) {
		                    
		                    return blockquote("Cannot mount the workspace integration because you do not have access!", "danger", $card_mode);
		                    
		                }
		                
		                //Verify token validity and refresh
		                //Make sure it is active
		                $this->db->wsIntegrationUpdateLinkUser($appId, $personid);
		                
		                return blockquote("Mounted <strong>{$details['integration']['name']}</strong> activated in <strong>{$details['integration']['organization']}</strong> to your account!", "warning", $card_mode);
		                
		            }
		            //Remove any mounted integration
		        } else if ($param1 == "unmount") {
		            
		            $userapplink = $this->db->wsIntegrationGetUserLink($personid);
		            
		            if (count($userapplink)) {
		                $appId = $userapplink[0]['appid'];
		                $details = $this->db->wsIntegrationInfoAPI($appId);
		                $this->db->wsIntegrationLinkDelete($personid);
		                $this->db->wsIntegrationDeviceMonitoringDeleteFromUser($personid);
		                return blockquote("<strong>{$details['integration']['name']}</strong> was unmounted from your account", "warning", $card_mode);
		                
		            } else {
		                return blockquote("There are no workspace integration mounted on your account", "warning", $card_mode);
		            }
		            
		            
		        } else if ($param1 == "monitor") {
		            
		            if ($param2) {
		                $userapplink = $this->db->wsIntegrationGetUserLink($personid);
		                $in_this_space = "";
		                
		                
		                if (in_array($param2, $conditions['end'])) {
		                    $this->db->wsIntegrationDeviceMonitoringDeleteFromUser($personid);
		                    return blockquote("Ended all monitoring tasks linked to your user", "success", $card_mode);
		                }
		                		                
		                if (count($userapplink)) {
		                    $appId = $userapplink[0]['appid'];
		                    $wsint = $this->db->wsIntegrationInfoAPI($appId);
		                    
		                    $deviceInfo = $this->deviceGetDetails($appId, $param2);
		                    
		                    if (!isset($deviceInfo['id'])) {
		                        return blockquote("I was unable to get any details on this deviceId", "warning", $card_mode);
		                    }
		                    
		                    $data_array = array(
		                        'appId'=>$appId, 'deviceId' => $param2, 'userId' => $personid
		                    );
		                    
		                    if (in_array($param3, $conditions['end'])) {
		                        if ($this->db->wsIntegrationDeviceMonitoringDelete($appId, $param2, $personid)) {
		                            return blockquote("Monitoring ended for {$deviceInfo['displayName']}", "success", $card_mode);
		                        } else {
		                            return blockquote("I encountered an issue with stopping the monitoring of {$deviceInfo['displayName']}. Check that the deviceId is correct.", "success", $card_mode);
		                        }
		                    }
		                      
		                    else if ($param3 == "this" and $type == "group") {
		                        		                        
		                        $integrationUserInfo = $this->wsIntegrationGetUserInfo($appId);
		                        
		                        $in_this_space = " in THIS space";
		                        
		                        if (isset($integrationUserInfo['id'])) {
    		                        $this->membershipCreate($botid, $roomid, $integrationUserInfo['id']);
    		                        $data_array['notification_groups'] = $roomid;
		                        }
		                        
		                    } 
		                    
		                    $this->db->wsIntegrationAddDeviceMonitoring($data_array);
		                    return blockquote($wsint['integration']['name']." is currently monitoring ".$deviceInfo['displayName'] . $in_this_space, "success", $card_mode);
		                    
		                } else {
		                    return blockquote("There are no workspace integration mounted on your account", "warning", $card_mode);
		                }
		            } 
		            
		            
		            
		        } else if ($param1 == "refresh") {
		            
		            $userapplink = $this->db->wsIntegrationGetUserLink($personid);
		            
		            if (count($userapplink)) {
		                $appId = $userapplink[0]['appid'];
		                $details = $this->db->wsIntegrationInfoAPI($appId);
		                
		                if (!$this->db->wsIntegrationCheckUserAccess($details['integration']['id'], $personid)) {
		                    
		                    $this->db->wsIntegrationLinkDelete($personid);
		                    return blockquote("Cannot refresh the access token, you do not have access to this workspace integration - unmounted", "danger", $card_mode);
		                    
		                }
		                
		                if (count($details['token'])) {
		                    if($details['token']['rt']['expired']) {
		                        return blockquote("Oh no! The refresh token has expired. The integration must be reactivated from Control Hub.", "warning", $card_mode);
		                    }
		                    
		                    $result = $this->wsIntegrationRefreshAccessToken($appId);
		                    
		                    if (isset($result['access_token'])) {
		                        return blockquote("Access token was refreshed!", "success", $card_mode);
		                    } else {
		                        return blockquote("Unable to refresh the access token", "success", $card_mode);
		                    }
		                    
		                }
		                
		                
		                return blockquote("<strong>{$details['integration']['name']}</strong> was unmounted", "warning", $card_mode);
		                
		            } else {
		                return blockquote("There are no integration mounted on your account", "warning", $card_mode);
		            }
		            
		            
		        } else {
		            
		            return $this->db->adminGetFeatureUsage($id)['usage'];
		            
		        }
		    		        
			//Intergration
			case "place":
			    
			    $userapplink = $this->db->wsIntegrationGetUserLink($personid);
				
				if ($this->db->integrationCheckConfiguration() or count($userapplink)) {
				    
				    $appId = $userapplink[0]['appid'];
				    
				    if (count($userapplink) and $this->db->wsIntegrationExists($appId)) {
				        
				        $appdata = $this->db->wsIntegrationInfoAPI($appId);
				        
				        if (!$this->db->wsIntegrationCheckUserAccess($appdata['integration']['id'], $personid)) {
				            
				            $this->db->wsIntegrationLinkDelete($personid);
				            
				            return blockquote("You no longer have access to this workspace integration and have been unmounted!", "danger", $card_mode);
				            
				        }
				        
				        if (!count($appdata['token'])) {
				            
				            return blockquote("Could not find a valid token on {$appdata['integration']['name']}", "warning", $card_mode);
				       
				        }
				        
				        $at_exp = $appdata['token']['at']['expired'];
				        $rt_exp = $appdata['token']['rt']['expired'];
				        
				        if (!$at_exp) {
				            
				            $botid = $appId;
				            
				        } 
				        
				        else if ($at_exp and !$rt_exp) {
				            
				            $this->wsIntegrationRefreshAccessToken($appId);
				            $botid = $appId;
				            
				        }
				        
				        else if ($at_exp and $rt_exp) {
				            
				            return blockquote("Attempting to use the workspace integration but the refresh token is expired, please mount another integration or re-activate the existing.", "warning", $card_mode);
				            
				        } else {
				            
				            return blockquote("Unknown error", "danger", $card_mode);
				            
				        }
				        
				   
				    }
					
					else if ($this->db->integrationAccessTokenExists($personid)) {
						
						$tokendata = $this->db->integrationAPIGetTokenData($personid);
						
						if (!$tokendata['access_token']['expired']) {
						
							$botid = $personid;
							
						}
						
					} else {
						    
				        return blockquote("You must have an authorized user or workspace integration with the scopes spark-admin:places_read, spark-admin:places_write, identity:onetimepassword_create linked to your account to use this feature", "warning");
			    
					}
							
						
					list($param1, $param2, $param3) = $feature_attributes;
					$returnvalue = "";
					
					if ($param1 == "ui") {
					    
					    return $this->crd->featurePlaceCard();
					    
					} else if ($param1 == "list") {
						
						$displayName = issetor($param2);
						$returnvalue = "<blockquote class=info>Places (displaying max 20)</blockquote><hr>\n\n";
						$places = $this->placeGet($botid, $displayName);
						
						$place_list = $this->crd->cardItem('new_card', ["body" => [
						    $this->crd->cardItem('textblock', [
						        'text' => 'Workspaces (Max 10)',
						        'size' => 'Large',
						        'weight' => 'bolder',
						        'separator' => true
						    ])
						]]);
						
						if (count($places['items'])) {
							
							$places['items'] = array_slice($places['items'], 0, 10);
							
							foreach ($places['items'] as $key => $value) {
							    
							    $place_list['body'][] = $this->crd->cardItem('container', [
							        'style' => $card_styles['list_item'],
							        'spacing' => 'Default',
							        'minHeight' => '5px',
							        'items' => [
							            $this->crd->cardItem('columnset', [
							                'columns'=> [
							                    $this->crd->cardItem('column', [
							                        'items'=>[
							                            $this->crd->cardItem('textblock', [
							                                'text' => "{$value['displayName']}",
							                                'weight' => 'bolder',
							                                'size' => 'large'
							                            ]),
							                            $this->crd->cardItem('actionset', [
							                                'actions' => [
							                                    $this->crd->cardItem('submit', [
							                                        'title' => 'Activate',
							                                        'data' => [
							                                            'wbm_command' => 'place code '. $value['id']
							                                        ]
							                                    ]),
					                                            $this->crd->cardItem('submit', [
					                                                'title' => 'Details',
					                                                'data' => [
					                                                    'wbm_command' => 'place show '. $value['id']
					                                                ]
					                                            ]),
					                                            $this->crd->cardItem('submit', [
					                                                'title' => 'Delete',
					                                                'style' => 'destructive',
					                                                'data' => [
					                                                    'wbm_command' => 'place delete '. $value['id']
					                                                ]
					                                            ])
					                                        ]
							                                        
					                                    ])
					                                ]
					                            ]),
					                        ]
							                                    
					                    ])
					                ]
					            ]);
								
								#$returnvalue .= "\n".blockquote("<strong>{$value['displayName']}</strong>\n\n <li> <strong>ID:</strong> {$value['id']} </li>\n<li> <strong>SIP:</strong> {$value['sipAddress']}</li>", "primary");
							
							}
						
						} else {
							
							return blockquote("Sorry, could not find any places based on the provided input", "warning", $card_mode);
						
						}
						
						#return $returnvalue;
						return $this->crd->card_prepare($place_list);
						
					} else if ($param1 == "show") {
						
						if ($param2) {
						
							$param2 = $this->db->quote($param2);
							return codeBlock(json_encode($this->placeGetDetails($botid, $param2), JSON_PRETTY_PRINT));
						
						} else {
							
							return blockquote("Please provide the placeId as a required parameter for this command!", "warning", $card_mode);
						
						}
					
					} else if ($param1 == "create") {
						
						if ($param2) {
						
							$name = $this->db->quote(implode(' ', array_slice($feature_attributes, 1)));
							return codeBlock(json_encode($this->placeCreate($botid, $name), JSON_PRETTY_PRINT));
						
						} else {
							
							return blockquote("Please provide a displayName for the place as a required parameter for this command!", "warning");
						
						}
					
					} else if ($param1 == "activate") {
						
						if ($param2) {
							
							$name = $this->db->quote(implode(' ', array_slice($feature_attributes, 1)));
							$newplace = $this->placeCreate($botid, $name);
							
							if ($newplace['id']) {
								
								$activation_code = $this->deviceGetActivationCode($botid, $newplace['id']);
								$result = array("result"=>array("place" => $newplace, "activation" => $activation_code));
								return codeBlock(json_encode($result, JSON_PRETTY_PRINT));
							
							}
						
						} else {
							
							return blockquote("Please provide a displayName for the place as a required parameter for this command!", "warning");
						
						}
					
					} else if ($param1 == "code") {
						
						if ($param2) {
							
							$place = $this->placeGetDetails($botid, $param2);
							
							if (issetor($place['id'])) {
								
								return codeBlock(json_encode($this->deviceGetActivationCode($botid, $place['id']), JSON_PRETTY_PRINT));
							
							}
						
						} else {
							
							return blockquote("Please provide a placeId for the place as a required parameter for this command!", "warning");
						
						}
					
					} else if ($param1 == "update") {
						
						if ($param2) {
							
							$place = $this->placeGetDetails($botid, $param2);
							
							if (issetor($place['displayName'])) {
								
								if ($param3) {
								
									$name = $this->db->quote(implode(' ', array_slice($feature_attributes, 2)));
									return codeBlock(json_encode($this->placeUpdate($botid, $param2, $name), JSON_PRETTY_PRINT));
								
								} else {
								
									return blockquote("Please provide a displayName for the place to update as a required parameter for this command!", "warning");
								
								}
								
							} else {
								
								return blockquote("That place could not be found", "warning");
							
							}
							
						} else {
							
							return blockquote("Please provide a placeId for the place as a required parameter for this command!", "warning");
						
						}
					
					} else if ($param1 == "delete") {
						
						if ($param2) {
							
							$placeId = $this->db->quote($param2);
							$place = $this->placeGetDetails($botid, $placeId);
							$this->placeDelete($botid, $placeId);
							
							if (issetor($place['displayName'])) {
								
								return blockquote("Deleted <strong>{$place['displayName']}</strong>", "success");
							
							} else {
								
								return blockquote("That place does not exist", "warning");
							
							}
						
						} else {
							
							return blockquote("Please provide a placeId for the place to delete as a required parameter for this command!", "warning");
						
						}
					
					} else {
						
						return $this->db->adminGetFeatureUsage($id)['usage'];
					
					}
				
				} else {
					
					return blockquote("You integration token has expired, please renew or refresh the token", "waring");
				
				} 
				
				break;
			// FEATURE INTEGRATION
			case "integration":
				
				if ($this->db->integrationCheckConfiguration()) {
					
					if ($this->db->adminCheckUserExists($personid)) {
					
						list($param1, $param2, $param3) = $feature_attributes;
						$returnvalue = "";
						
						if ($param1 == "authorize") {
							
							$returnvalue = "<blockquote class=info>Authorization URL</blockquote>\n\n This URL is personal and authroized users will be linked to **" . $this->db->contactGetName($personid)."**\n\n";
							
							if ($param2) {
								
								if ($param2 == "card") {
									
									$authCard = cardDraw("integrationAuthorizationCard");
									$url = $this->integrationGenAuthUrl($personid);
									$url = $url . "+bot";
									
									$authCard['body'][1]['actions'][0]['url'] = $url;
									$authCard['body'][2]['text'] = "Authorized user will be linked to " . $this->db->contactGetName($personid) . ". If this is not you, do not click this link!";
									
									$authCard = cardPrepareCard($authCard); 
									
									return array("markdown"=>"Your client do not support displaying cards, please type **integration authorize** to get a link in string format.", "attachments"=>$authCard);
										
								} else {
									
									$scopes = implode(' ', array_slice($feature_attributes, 1));
									$scopes = explode(" ", $scopes);
									$invalid_scopes = array();
									
									foreach ($scopes as $k => $scope) {
										
										if (!$this->db->integrationScopeExists($scope)) {
											
											$invalid_scopes[] = $scope;
											
										}
										
									}
									
									if (count($invalid_scopes)) {
										
										return blockqoute("The following scopes was invalid: " . implode(' ', $invalid_scopes), "danger");
										
									} else {
										
										$url = $this->integrationGenAuthUrl($personid, implode(' ', $scopes));
										$returnvalue .= "{$url}+bot ";
										
										return $returnvalue;
										
									}
									
								}
								
							} else {
								
								$url = $this->integrationGenAuthUrl($personid);
								$returnvalue .= "[{$url}+bot]";
								return $returnvalue;
							
							}
						
						} else if ($param1 == "defaults") {
							
							$integrationdata = $this->db->integrationFetchSettings();
							$scopes = explode(' ', $integrationdata['default_scopes']);
							$returnvalue = blockquote("Default scopes if no scope is specified");
							
							foreach ($scopes as $k => $v) {
								
								$scopeinfo = $this->db->integrationFetchScopes($v);
								$returnvalue .= "\n - <strong>{$scopeinfo['scope']}</strong>\n    - {$scopeinfo['description']}";
							
							}
							
							return $returnvalue;
							
							$returnvalue = blockquote("Available scopes", "primary");
							
							foreach ($scopes as $key => $scope) {
								
								$returnvalue .= "\n - <strong>{$scope['scope']}</strong>\n    - {$scope['description']}";
							
							}
							
							return $returnvalue;
						
						} else if ($param1 == "scopes") {
							
							$scopes = $this->db->integrationFetchScopes();
							$returnvalue = blockquote("Available scopes", "primary");
							
							foreach ($scopes as $key => $scope) {
							
								$returnvalue .= "\n - <strong>{$scope['scope']}</strong> \n    - {$scope['description']}";
							
							}
							
							return $returnvalue;
						
						} else if ($param1 == "ui") {
							
							return array(
									"markdown" => "Sorry, your client does not seem to support cards. Type **integration** for usage.",
									"attachments" => $this->cardPrepareFeatureCard(cardDraw('integrationUi'))
								   );
							
						} else if ($param1 == "show") {
							
							if ($this->db->integrationAccessTokenExists($personid)) {
								
								$userdata = $this->db->integrationAPIGetTokenData($personid);
								
								if ($param2 != "all") {
									
									$userdata['tokendata']['access_token'] = "***";
								
								}
								
								return codeBlock(json_encode($userdata, JSON_PRETTY_PRINT));
							
							} else {
								
								return blockquote("You currently dont have any integration attached! Please authorize a user first. <strong>integration authorize</strong>", "warning");
							
							}
						  
						} else if ($param1 == "refresh") {
							
							if ($this->db->integrationAccessTokenExists($personid)) {
								
								$userdata = $this->db->integrationAPIGetTokenData($personid);
								if (!$userdata['refresh_token']['expired']) {
									
									if ($this->integrationRefresh($personid)) {
										
										return blockquote("Successfully refreshed the access token!", "success");
									
									} else {
										
										return blockquote("Error: The access token was NOT refreshed!", "danger");
									
									}
								
								} else {
									
									return blockquote("Error: The refresh token is expired! Please re-authorize", "danger");
								
								}
							
							} else {
								
								return blockquote("You currently dont have any integration attached! Please authorize a user first. <strong>integration authorize</strong>", "warning");
							
							}
						} else if ($param1 == "delete") {
							
							if ($this->db->integrationAccessTokenExists($personid)) {
								
								$this->db->integrationDeleteToken($personid);
								
								return blockquote("Your integration authorization was deleted!", "success");
							
							} else {
								
								return blockquote("No integration authorization was found for your user. Nothing to delete.", "warning");
							
							}
						
						} else {
							
							return $this->db->adminGetFeatureUsage($id)['usage'];
						
						}	
					
					} else {
						
						return blockquote("You need to be a registered user to use the integration feature!", "warning");
						
					}
				
				} else {
					
					return blockquote("I am not configured for integrations! Please ask an administrator to configure the integration", "warning");
				
				}
			break;
			
			//FEATURE DEVICE MANAGEMENT
			case "device":
				
				list($param1, $param2, $param3, $param4, $param5) = $feature_attributes;
				$returnvalue = "";
				
				//Check if the user has a workspace integration linked
				
				$userapplink = $this->db->wsIntegrationGetUserLink($personid);
				
				if (count($userapplink)) {
				    $appId = $userapplink[0]['appid'];
				    
				    $appdata = $this->db->wsIntegrationInfoAPI($appId);
				    
				    if (!$this->db->wsIntegrationCheckUserAccess($appId, $personid)) {
				        
				        $this->db->wsIntegrationLinkDelete($personid);
				        
				        return blockquote("You no longer have access to this workspace integration and have been unmounted!", "danger", $card_mode);
				        
				    }
				    
				    $botid = $appId;
				    $integration=True;
				}
				
				else if ($this->db->integrationAccessTokenExists($personid)) {
					
					$userdata = $this->db->integrationAPIGetTokenData($personid);
					
					if ($userdata['access_token']['expired']) {
						
						return blockquote("Your integration token has expired! Please refresh, re-authorize or remove it to use the bot!", "danger");						
					
					} else {
						
						$botid = $personid;
						$integration = True;
					
					}
				
				} 
				//List devices
				if ($param1 == "list") {
					
					$returnvalue = "<blockquote class=info>Devices (displaying max 20)</blockquote><hr>";
					
					$device_list = $this->crd->cardItem('new_card', ["body" => [
					    $this->crd->cardItem('textblock', [
					        'text' => 'Devices (Max 10)',
					        'size' => 'Large',
					        'weight' => 'bolder',
					        'separator' => true
					    ])
					]]);
					
					$search = array();
					
					if ($param2) {
						
						$searchitems = implode(' ', array_slice($feature_attributes, 1));
						$search = json_decode($searchitems, true);
						
						if (!$search) {
							
							$search = array("displayName"=>$searchitems);
						
						} 
					
					}
					
					$devices = $this->deviceList($botid, $search);
					
					if (count($devices['items'])) {
						
						$devices['items'] = array_slice($devices['items'], 0, 10);
						
						foreach ($devices['items'] as $key => $value) {
						    
						    $device_list['body'][] = $this->crd->cardItem('container', [
						        'style' => $card_styles['list_item'],
						        'spacing' => 'Default',
						        'minHeight' => '5px',
						        'items' => [
						            $this->crd->cardItem('columnset', [
						                'columns'=> [
						                    $this->crd->cardItem('column', [
						                        'items'=>[
						                            $this->crd->cardItem('textblock', [
						                                'text' => "{$value['displayName']}",
						                                'weight' => 'bolder',
						                                'size' => 'large'
						                            ]),
						                            $this->crd->cardItem('factset', [
						                                 'facts' => [
						                                    ['title' => 'connectionStatus', 'value' => $value['connectionStatus']]
						                                  ],
						                                  'weight' => 'lighter',
						                                  'size' => 'small'
						                            ]),
						                            $this->crd->cardItem('actionset', [
						                                  'actions' => [
						                                      $this->crd->cardItem('submit', [
						                                          'title' => 'Cloud xAPI',
						                                          'data' => [
						                                              'wbm_command' => 'device ui '. $value['id']
						                                          ]
						                                      ]),
						                                      $this->crd->cardItem('submit', [
						                                          'title' => 'Monitor',
						                                          'data' => [
						                                              'wbm_command' => 'ws monitor '. $value['id']
						                                          ]
						                                      ]),
						                                      $this->crd->cardItem('submit', [
						                                          'title' => 'Details',
						                                          'data' => [
						                                              'wbm_command' => 'device show '. $value['id']
						                                          ]
						                            ])
						                            ]
						                                        
						                                    ])
						                        ]
						                    ]),
						                ]
						                                    
						            ])
						        ]
						    ]);	    
							
						    $returnvalue .= "\n<blockquote class=info><strong>{$value['displayName']}</strong>\n <li> <strong>ID:</strong> {$value['id']}\n <li> <strong>Connection:</strong> {$value['connectionStatus']}</blockquote>\n";
						
					}
						
						#return $returnvalue;
						return $this->crd->card_prepare($device_list);
					
					} else {
						
						return ($integration) ? blockquote("No devices found using your integration token!", "warning"):blockquote("No devices found for this bot!", "warning");
					
					}
					
				//Show device details
				} else if ($param1 == "show") {
					
					if ($param2) {
						
						$deviceid = $param2;
						$key = strtolower(str_replace(" ", ".", implode(' ', array_slice($feature_attributes, 2))));
						$result = $this->deviceGetDetails($botid, $deviceid);
						
						return codeBlock(json_encode($result, JSON_PRETTY_PRINT));
					
					} else {
						
						return blockquote("You need to include the deviceId to see the details", "warning");
					
					}
				//Get device status (xAPI)
				} else if ($param1 == "status") {
					
					if ($param2) {
						
						$deviceid = $param2;
						$key = strtolower(str_replace(" ", ".", implode(' ', array_slice($feature_attributes, 2))));
						
						if ($key) {
						
							$result = $this->deviceStatus($botid, $deviceid, $key);
							return codeBlock(json_encode($result, JSON_PRETTY_PRINT));
						
						} else {
							
							return blockquote("Missing status key, please provide a valid status key!", "warning");
						
						}
					
					} else {
						
						return blockquote("You need to include the deviceId as the second parameter (device status deviceId key)", "warning");
					
					}
				
				//Get or set device config (xAPI)
				} else if ($param1 == "config") {
					
					$returnvalue = array();
					if ($param2) {
						
						if ($param2 == "get") {
							
							if ($param3) {
								
								$deviceid = $param3;
								
								if ($param4) {
									
									$key = $param4;
									
									$result = $this->deviceGetConfig($botid, $deviceid, $key);
									
									if (count($result['items'])) {
										
										if (!$param5 == "-v") {
											
											foreach ($result['items'] as $k => $v) {
												
												$returnvalue[$k] = array(
												    
														"value"=>$v['value'],
														"source"=>$v['source']
												    
												);
											
											}
											
											$slice = 50;
										
										} else {
											
											$returnvalue = $result['items'];
											
											$slice = 5;
										
										}
										
										$returnvalue = array_slice($returnvalue, 0, $slice);
											
										return blockquote("Displaying max $slice nodes", "primary") . "\n\n" . codeBlock(json_encode($returnvalue, JSON_PRETTY_PRINT));
									
									} else {
										
										return blockquote("No result based on the input, keep in mind that the key is case sensitive, i.e: SystemUnit.* and not systemunit.*. Also make sure the deviceId is valid!", "warning");
									
									}
								
								} else {
									
									return blockquote("You need to include the key to reduce the amount of data, I am not able to return the full config scheme", "warning");
								
								}
							
							} else {
								
								return blockquote("You need to include the device ID as the second parameter", "warning");
							
							}
						
						} else if ($param2 == "set") {
							
							if ($param3) {
								
								$deviceid = $param3;
								
								if ($param4) {
									
									$key = $param4;
									
									if ($param5) {
										
										$value = implode(' ', array_slice($feature_attributes, 4));
									
										$configdata = $this->deviceGetConfig($botid, $deviceid, $key);
									
										$datatype = $configdata['items'][$key]['valueSpace']['type'];
										
										switch ($datatype) {
											
											case "string":
												
												$value = (string) $value;
												
												break;
											
											case "integer": 
												
												$value = (int) $value;
												
												break;
										}
																				
										$result = $this->deviceSetConfig($botid, $deviceid, $key, $value);
										
										if (count($result['items'])) {
											
											foreach ($configdata['items'] as $k1 => $v1) {
												
												if ($k1 == $key) {
													
													$returnvalue['old'][$k1] = array(
															"value"=>$v1['value'],
															"source"=>$v1['source']
													);
												
												}
											
											}
											
											foreach ($result['items'] as $k => $v) {
												
												if ($k == $key) {
													
													$returnvalue['new'][$k] = array(
															"value"=>$v['value'],
															"source"=>$v['source']
													);
												
												}
											
											}
											
											return codeBlock(json_encode($returnvalue, JSON_PRETTY_PRINT));
										
										} else {
											
											return blockquote("No result based on the input, keep in mind that the key is case sensitive, i.e: SystemUnit.* and not systemunit.*. Also make sure the deviceId is valid!", "warning");
										
										} 
									
									} else {
										
										return blockquote("You need to provide a value to the configuration", "warning");
									
									}
								
								} else {
									
									return blockquote("You need to provide a valid configuration key with the command", "warning");
								
								}
							
							} else {
								
								return blockquote("You need to provide a valid deviceId to the command", "warning");
							
							}
						
						} else  {
						    
							return $this->db->adminGetFeatureUsage($id)['usage'];
						
						}
					}  else  {
						
						return $this->db->adminGetFeatureUsage($id)['usage'];
					
					}
				} else if ($param1 == "command") {
					
					if ($param2) {
						
						$deviceid = $param2;
						
						if ($param3) {
								
							$key = $param3;
							$rest = implode(' ', array_slice($feature_attributes, 3));
							$args = substr($rest, strpos($rest, "{"), strpos($rest, "}")+1);
							
							if ($args[0] != "{" or !strpos($args, "}")) {
								
								$args = "";
							
							}
							
							$body = str_replace($args, "", $rest);
							
							return codeBlock(json_encode($this->deviceCommand($botid, $deviceid, $key, $args, $body), JSON_PRETTY_PRINT));
						
						} else {
							
							return blockquote("You need to provide a valid command key with the command", "warning");
						
						}
					
					} else {
						
						return blockquote("You need to provide a valid deviceId with the command", "warning");
					
					}
				
				} else if ($param1 == "ui") {
						
					if ($param2) {
			
						$deviceid = $param2;
						
						return $this->deviceGenCard($botid, $deviceid);
					
					} else {
						return blockquote("You need to provide a valid deviceId", "warning");
					}
				
				} else {
					
					return $this->db->adminGetFeatureUsage($id)['usage'];
				
				}
				
				break;
				
				//FEATURE SERVICE
			
			case "service":
				
				return $this->db->adminCheckServiceStatus("report");
				
				break;
				
				//FEATURE RESIGN
			case "resign":
				
				$userdetails = $this->db->contactFetchContacts($personid);
				
				if (count($userdetails)>0) {
					
					if ($userdetails[0]['type'] == '1') {
						
						return "You are a site administrator, you cannot remove yourself from here. You dont want to lock your self out!";
					
					} else {
						
						$this->db->contactRemove($personid);
						
						return "Success: You have been completely removed from the system!";
					
					}
					
				}
				else return "I cannot find $user in my database, so you have nothing to worry about!";
				break;
				
				//FEATURE SUBSCRIBE
			case "subscribe":
				list($param1) = $feature_attributes;
				
				$contactinfo = $this->db->contactFetchContacts($user);
				
				if (count($contactinfo)){
					
				    $contactid = $contactinfo[0]['id'];
				
				}
				
				else {
				
				    return blockquote("You are not a registered user!", "warning", true);
				
				}
				
				if (!empty($param1)) {
				
				    $groups = array_unique(explode(",", $param1));
				    
					$returnvalue = "**Subscription update**\n"; 
				
					foreach ($groups as $key => $value) {
				
					    $groupinfo = $this->db->groupFetchSubscriptionGroups($value);
					
						if (count($groupinfo)) {
						
						    $groupid = $groupinfo[0]['id'];
						    
							$groupcontact = array('groupid' => $groupid, 'contactid' => $contactid);
						
							if ($groupinfo[0]['botid'] == '0' or $groupinfo[0]['botid'] == $botid) {
						
							    if ($this->db->groupCheckIfMember($contactid, $groupid)) {
							      						
							        $this->db->groupRemoveContact($groupid, $contactid);
									$returnvalue .= "- Removed from: **" . $groupinfo[0]['groupname'] . "**\n";
							
							    }
							
								else {
								
								    $this->db->groupAddContact($groupcontact);
									$returnvalue .= "- Added to: **" . $groupinfo[0]['groupname'] . "**\n";
								
								}
							
							} else {
							
							    $returnvalue .= "- **$value**: No such subscription group\n";
							
							}
						
						}
						
						else {
						
						    $returnvalue .= "- **$value**: No such subscription group\n";
						
						}
					
					}
					
					return blockquote($returnvalue, "good", true); 
				
				} else {
				
				    $groupinfo = $this->db->groupFetchSubscriptionGroups();
					$subscribing = $unsubscribing = "";
					
					$subscribeCard = cardDraw('subscribeForm');
					$unsubscribeChoice = cardDraw('choiceSet');
					$subscribeChoice = cardDraw('choiceSet');
					
					$returnvalue = "<blockquote class='success'>You are subscribing to the following subscription groups:</blockquote>\n\n";
					
					foreach ($groupinfo as $key => $value) {
						
					    $id = (!empty($value['sub_id'])) ? $value['sub_id']:$value['id'];
					    
					    $desc = (!empty($value['description'])) ? "\n *{$value['description']}*" : "";
						
						if ($this->db->groupCheckIfMember($contactid, $value['id']) and ($value['botid'] == '0' or $value['botid'] == $botid)) {
							
						    $subscribing .= "- **$id** ({$value['groupname']})\n    - {$value['description']}</li></p>\n";
							
							$unsubscribeChoice['choices'][] = array(
							    "title" => "**{$value['groupname']}**$desc",
							    "value" => $value['id']
							);
						
						}
						
						elseif ($value['botid'] == '0' or $value['botid'] == $botid) {
							
						    $unsubscribing .= "- **$id** ({$value['groupname']})\n    * {$value['description']}</blockquote>\n";
							
							$subscribeChoice['choices'][] = array(
							    "title" => "**{$value['groupname']}**$desc",
							    "value" => $value['id']
							);
						
						}
					
					}
					
					if (empty($subscribing)) {
					    
					    $subscribing = "**You are not subscribing to any subscription groups!**";
					
					} else {
					    
					    $unsubscribeChoice['id'] = 'subscribe';
					    $subscribeCard['body'][2]['items'][] = $unsubscribeChoice;
					    
					}
					
					if (empty($unsubscribing)) {
					    
					    $unsubscribing = "**You are subscribing to all possible subscription groups!**";
					
					} else {
					    
					    $subscribeChoice['id'] = 'unsubscribe';
					    $subscribeCard['body'][3]['items'][] = $subscribeChoice;
					    
					}
					
					$returnvalue .= $subscribing;
					$returnvalue .= "\n\n<blockquote class='danger'>You can subscribe to the following subscription groups:</blockquote>\n\n" . $unsubscribing;
					$returnvalue .= "\n\n<blockquote class='info'>Usage: <b>subscribe [id]</b> to subscribe/unsubscribe to multiple groups, separate the id's with comma without spaces: <b>subscribe id,id,id</b></blockquote>";
					
					$cardPayload = array('markdown' => $returnvalue, 'attachments' => $this->cardPrepareFeatureCard($subscribeCard));
									
					return $cardPayload;
				
				}
				
				break;
				
				//FEATURE - REMOVE
			case "remove":
				
				list($param1) = $feature_attributes;
				
				if ($param1 == "last") {
					
					$messages = $this->messageFetchMessages($botid, $roomid, $type);
					
					foreach ($messages['items'] as $key=>$value) {
						
						if ($value['personEmail'] == $botmail) {
							
							$message_id = $value['id'];
							break;
						
						}
					
					}
					
					$this->messageDelete($botid, $message_id);
					
					return "removed";
				
				} else {
					
					return "$s <b>$id</b> usage: $e\n **remove last** - *Removes my latest reply in the space*";
				
				}
				
				break;
				
				//FEATURE - REQUEST
			case "request":
				$existing = $this->db->contactFetchContacts($user);
				if (count($existing)) {
					return $this->crd->msgCard("You are already in my system!");
				}
				else {
					$contactdata = $this->peopleGet(array('recepientValue'=>$personid, 'recepientType'=>'id', 'sender'=>$botid));
					$this->db->contactAdd($contactdata, $botid);
					$this->messageSendEvent("$user used the **request** feature. $user was automatically added to my database and assigned to the default groups (if any)");
					return $this->crd->msgCard("You have been successfully added to the system! It may take some time for an admin to process this request.", ['style'=>'success']);
				}
				break;
				//FEATURE - FEEDBACK
			case "feedback":
				
				//Define feature vars
				list($param1, $param2, $param3, $param4) = $feature_attributes;
				
				$fmsg = array(
						'error_entry_not_found' => blockquote("This entry does not exist, please verify the entryId", "danger", $card_mode),
				        'error_topic_not_found' => blockquote("This topic does not exist, please verify the topicId", "danger", $card_mode),
				        'error_comment_not_found' => blockquote("This comment does not exist, please verify the commentId", "danger", $card_mode),
				        'error_something_went_wrong' => blockquote("Something went wrong", "danger", $card_mode),
				        'error_empty_entries' => blockquote("Adding empty entries is not allowed", "danger", $card_mode),
				        'error_no_topics' => blockquote("No topics has been created yet! Only bot administrators can create topics.", "danger", $card_mode),
				        'error_delete_created' => blockquote("You can only delete entries created by yourself", "danger", $card_mode),
				        'error_delete_comment_created' => blockquote("You can only delete comments created by yourself", "danger", $card_mode),
				        'error_parameters' => blockquote("Incorrect parameters, type <strong>feedback</strong> for help.", "danger", $card_mode),
				        'acl_restricted' => blockquote("This topic is restricted by accessgroup, permission denied", "danger", $card_mode),
				        'acl_add_entries' => blockquote("Adding entries is disabled in this topic", "warning", $card_mode),
				        'acl_delete_entries' => blockquote("Deleting entries is disabled in this topic", "warning", $card_mode),
				        'acl_view_entries' => blockquote("Viewing entry details is disabled in this topic", "warning", $card_mode),
				        'acl_comment' => blockquote("Commenting entries is disabled in this topic", "warning", $card_mode),
						'help_vote' => "<i>add or revoke a vote on the specified entry</i>",
						'help_comment' => "<i>add a comment to the specified entry</i>",
						'help_entryview' => "<i>view details and comments for the specified entry</i>",
						'help_entryadd' => "<i>add a new entry to the specified topic</i>",
						'help_topic' => "<i>list entries in the specified topic</i>",
						'mark_voted' => " [<strong>you have voted</strong>]",
						'mark_commented' => " [<strong>you have commented</strong>]",
						'mark_lock' => "[<strong>private topic</strong>]"
				);
				
				$mark = 5;
				
				$counter = 0;
				
				$locked = "";
				
				$restricted = blockquote("This topic is restricted by accessgroup, permission denied", "danger", $card_mode);
				
				$entry_not_found = blockquote("This entry does not exist, please verify the entryId", "warning", $card_mode);
				
				$topic_not_found = blockquote("This topic does not exist, please verify the topicId", "warning", $card_mode);
				
				$something_went_wrong = blockquote("Something went wrong", "danger", $card_mode);
				
				$vote_help = "<i>add or revoke a vote on the specified entry</i>";
				
				$comment_help = "<i>add a comment to the specified entry</i>";
				
				$entryview_help = "<i>view details and comments for the specified entry</i>";
				
				$entryadd_help = "<i>add a new entry to the specified topic</i>";
				
				$topic_help = "<i>list entries in the specified topic</i>";
				
				//FEEDBACK -> TOPICS
				if (in_array($param1, array('topic', 'topics'))) {
					
					if ($param2) {
						
						if (is_numeric($param2)) {
							
							//assign
							$topicid = $this->db->quote($param2);
							
							$topic = $this->db->feedbackFetchTopics($botid, $topicid);
							
							$name = $this->db->contactGetName($user);
							
							//topic exists check
							if (!count($topic)) {
								
								return $fmsg['error_topic_not_found'];
							
							}
							
							//accessgroup check
							if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) {
								
								return $fmsg['acl_restricted'];
							
							}
														
							//add entry sub trigger
							if ($param3 == "add") {
								
								if (!$this->db->feedbackTopicCheckAllowList("entry_create_allowed", $topic)) 
									
									return $fmsg['acl_add_entries'];
								
								$desc = implode(' ', array_slice($feature_attributes, 3));
								
								if (empty($desc)) 
									
									return $fmsg['error_empty_entries'];
								
								$data = array('topic_id'=>$topic[0]['id'], 'created_by'=>$user, 'description'=> removeHTML($this->db->quote(implode(' ', array_slice($feature_attributes, 3)))));
								
								$last_id = $this->db->feedbackEntryAdd($data, true);
								
								if ($last_id) {
									
									$new_entry = $this->db->feedbackFetchEntry($last_id);
									
									return blockquote("$name <strong>created a new</strong> entryId: <strong>$last_id</strong> for topicId: <strong>{$topic[0]['id']}</strong><br><h3>{$new_entry[0]['description']}</h3></blockquote><hr><strong>feedback entry $last_id</strong> {$fmsg['help_entryview']}<br><strong>feedback comment $last_id [comment text...]</strong> {$fmsg['help_comment']}<br><strong>feedback vote $last_id</strong> {$fmsg['help_vote']}", "success");									
								
								}
								
								else return $fmsg['error_something_went_wrong'];
							
							}
							
							$entries = $this->db->feedbackFetchTopicEntries($param2);
												
							if (count($entries)) {
							    $entries = array_reverse($entries);
								$topic = $this->db->feedbackFetchTopics($botid, $param2);
								
								$entries_card = $this->crd->cardNew("Entries for topic [{$topic[0]['id']}]");
								$entries_card['body'][] = $this->crd->cardItem('textblock', [
								    'text' => $topic[0]['title'],
								    'size' => 'default',
								    'weight' => 'bolder'
								]);
								
								$returnvalue = "<blockquote class='success'><h1>{$topic[0]['id']} : {$topic[0]['title']}</h1></blockquote><hr>";
								foreach ($entries as $key => $value) {
									$votes = $this->db->feedbackFetchEntryVotes($value['id']);
									$comments = $this->db->feedbackFetchEntryComments($value['id']);
									$num_votes = count($votes);
									$num_comments = count($comments);
									$voted = (in_array($user, array_column($votes, 'email'))) ? $fmsg['mark_voted']:"";
									$commented = (in_array($user, array_column($comments, 'email'))) ? $fmsg['mark_commented']:"";
									$desc = (strlen($value['description'])>100) ? substr($value['description'], 0, 100) . "..." : $value['description'];
									$returnvalue .= "<blockquote class='warning'><h3><strong>{$value['id']}</strong> : {$desc}</h3></ul></blockquote><ul><blockquote class='info'>[<strong>$num_votes</strong> votes] [<strong>$num_comments</strong> comments] $voted $commented</ul></blockquote>\n";
									$counter++;
									
									$entry_container = $this->crd->cardItem('container', [
									    'items' => [
									        $this->crd->cardItem('textblock', [
									            'text' => $this->db->contactGetName($value['created_by']) . " ({$value['created']})",
									            'size' => 'small',
									            'weight' => 'bolder',
									            'color' => 'accent'
									        ]),
									        $this->crd->cardItem('columnset', [
									            'columns' => [
									                $this->crd->cardItem('column', [
									                    'items' => [
									                        $this->crd->cardItem('textblock', [
									                            'text' => $desc,
									                            'size' => 'small',
									                            'wrap' => true
									                        ])
									                    ]
									                ]),
									                $this->crd->cardItem('column', [
									                    'items' => [
									                        $this->crd->cardItem('factset', [
									                            'facts' => [
									                                [
									                                    'title' => 'Comments:', 'value' => "$num_comments"
									                                ],
									                                [
									                                    'title' => 'Votes:', 'value' => "$num_votes"
									                                ],
									                                [
									                                    'title' => 'EntryId:', 'value' => "{$value['id']}"
									                                ]
									                            ]
									                        ])
									                    ]
									                ])
									            ]
									        ]),
									        $this->crd->cardItem('actionset', [
									            'actions' => [
									                $this->crd->cardItem('submit', [
									                    'title' => 'View',
									                    'spacing' => 'none',
									                    'data' => [
									                        'wbm_command' => 'feedback entry ' . $value['id']
									                    ]
									                ]),
									                $this->crd->cardItem('submit', [
									                    'title' => 'Vote',
									                    'spacing' => 'none',
									                    'data' => [
									                        'wbm_command' => 'feedback vote ' . $value['id']
									                    ]
									                ])
									            ],
									            'spacing' => 'none',
									        ])
									    ],
									    'style' => 'emphasis'
									]);
									
									$entries_card['body'][] = $entry_container;
									
									if ($mark == $counter) {
									    break;
									    //send bulk to avoid messages getting to large
									    //$this->messageSend($this->messageBlob($returnvalue, $roomid, $botid, "roomId", $type));
									    //reset values for next bulk
									    //$counter = 0;
									    //$returnvalue = "";
									    //avoid out of order messages
									    //sleep(0.1);
									}
								}	
								
								$new_entry = $this->crd->cardItem('container', [
								    'items' => [
								        $this->crd->cardItem('textinput', [
								            'isMultiline' => True, 
								            'id' => 'feedback_new_entry',
								            'placeholder' => "Add entry to {$topic[0]['title']}" 
								        ]),
								        $this->crd->cardItem('actionset', [
								            'actions' => [
								                $this->crd->cardItem('submit', [
								                    'title' => "Add",
								                    'data' => [
								                        'wbm_command' => "feedback topic {$topic[0]['id']} add"
								                    ]
								                ]),
								                $this->crd->cardItem('submit', [
								                    'title' => "Refresh",
								                    'data' => [
								                        'wbm_command' => "feedback topic {$topic[0]['id']}",
								                        'wbm_delete_card' => ''
								                        ]
								                        ]),
								                $this->crd->cardItem('submit', [
								                    'title' => 'Close',
								                    'style' => 'destructive',
								                    'data' => [
								                        'wbm_cancel_card' => 'true'
								                    ]
								                ])
								            ],
								            'spacing' => 'none'
								        ])
								    ],
								    'style' => 'emphasis',
								    'bleed' => true
								]);
								
								$entries_card['body'][] = $new_entry;
								
								
								
								$returnvalue .= "<hr><strong>feedback topic $topicid add [new entry text...]</strong> {$fmsg['help_entryadd']}<br><strong>feedback entry [entryId]</strong> {$fmsg['help_entryview']}<br><strong>feedback comment [entryId] [comment text...]</strong> {$fmsg['help_comment']}<br><strong>feedback vote [entryId]</strong> {$fmsg['help_vote']}";
								
								return $this->crd->card_prepare($entries_card);
								
							} else return blockquote("Ups.. This topic has no entries yet! Add one with the following command: **feedback topic $topicid add [new entry text...]**", "warning", $card_mode);
						} 
					}
					$topics = $this->db->feedbackFetchTopics($botid);
					if (count($topics)) {
					    
					    $topics_card = $this->crd->cardNew("My topics");
					   
					    
						$returnvalue = "<h1>My topics</h1>";
						
						foreach ($topics as $key => $value) {
							$groupid = $value['accessgroup'];
							$locked = "";
							if ($groupid) {
								if (!$this->db->groupCheckIfMember($user, $groupid)) continue;
								$group = $this->db->groupFetchGroups($groupid);
								$groupname = $group[0]['groupname'];
								$locked = $fmsg['mark_lock'];
							}
							$v_a = ($value['votes_allowed']) ? "[**vote**]" : ""; 
							$c_a = ($value['comments_allowed']) ? "[**comment**]" : "";
							$e_c_a = ($value['entry_create_allowed']) ? "[**entry create**]" : "";
							$e_v_a = ($value['entry_view_allowed']) ? "[**entry view**]" : "";
							$e_d_a = ($value['entry_delete_allowed']) ? "[**entry delete**]" : "";
							$entries = $this->db->feedbackFetchTopicEntries($value['id']);
							$num_entries = count($entries);
							
							$topic_container = $this->crd->cardItem('container', [
							    'items' => [
							        $this->crd->cardItem('columnset', [
							            'columns' => [
							                $this->crd->cardItem('column', [
							                    'items' => [
							                        $this->crd->cardItem('textblock', [
							                            'text' => $value['title'],
							                            'size' => 'medium',
							                            'weight' => 'bolder'
							                        ])
							                    ]
							                ]),
							                $this->crd->cardItem('column', [
							                    'items' => [
							                        $this->crd->cardItem('factset', [
							                            'facts' => [
							                                ['title' => 'Entries:', 'value' => "$num_entries"]
							                            ]
							                        ])	
							                    ]
							                ])							                    
							            ]
							        ]),
							        $this->crd->cardItem('actionset', [
							            'actions' => [
							                $this->crd->cardItem('submit', [
							                    'title' => 'View', 
							                    'spacing' => 'none',
							                    'data' => [
							                        'wbm_command' => 'feedback topic ' . $value['id']
							                    ]
							                ])
							            ],
							            'spacing' => 'none',
							        ])
							    ],
							    'style' => 'emphasis'
							]);
							
							$topics_card['body'][] = $topic_container;
													
							$returnvalue .= "<blockquote class='success'><h2>{$value['id']} : {$value['title']}</h2></blockquote><ul><blockquote class='info'>[<strong>$num_entries</strong> entries] $v_a $c_a $e_c_a $e_v_a $e_d_a $locked</ul></blockquote>\n";
						}
						
						$body_actions = $this->crd->cardItem('actionset', [
						    'actions' => [
						        $this->crd->cardItem('submit', [
						            'title' => 'Close',
						            'style' => 'destructive',
						            'data' => [
						                'wbm_cancel_card' => ''
						            ]
						        ])
						    ]
						]);
						$topics_card['body'][] = $body_actions;
						
					} else return $fmsg['error_no_topics'];
					$returnvalue .= "<hr><strong>feedback topic [topicId]</strong> $topic_help<br>";
					return $this->crd->card_prepare($topics_card);
				}
				//FEEDBACK - ENTRIES
				elseif ($param1 == 'entry') {
					if ($param2) {
						if (is_numeric($param2)) {
							$entry = $this->db->feedbackFetchEntry($this->db->quote($param2));
							if (count($entry)) {
							    
							   	$topicid = $entry[0]['topic_id'];
								$entryid = $entry[0]['id'];
								$email = $entry[0]['created_by'];
								$stamp = $entry[0]['created'];
								$topic = $this->db->feedbackFetchTopics($botid, $topicid);
								
								if (count($topic) == 0) 								    
								    return $fmsg['error_topic_not_found'];
								
								if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) 
								    return $fmsg['acl_restricted'];
								
								$name = $this->db->contactGetName($email);
								
								if ($param3 == "delete") {
									if (!$this->db->feedbackTopicCheckAllowList("entry_delete_allowed", $topic)) 
									    return $fmsg['acl_delete_entries'];
									
									if ($email != $user) 						    
									    return $fmsg['error_delete_created'];		
									
									$this->db->feedbackEntryDelete($entryid);
									return blockquote("$name <strong>deleted</strong> entryId: <strong>$entryid</strong> in topicId: <strong>$topicid</strong><br><strong>{$entry[0]['description']}</strong>", "danger", $card_mode);
								}
								
								if (!$this->db->feedbackTopicCheckAllowList("entry_view_allowed", $topic)) 
								    return $fmsg['acl_view_entries'];
								
								$votes = $this->db->feedbackFetchEntryVotes($entryid);
								$comments = $this->db->feedbackFetchEntryComments($entryid);
								$num_votes = count($votes);
								$num_comments = count($comments);
								$voted = (in_array($user, array_column($votes, 'email'))) ? $fmsg['mark_voted']:"";
								$commented = (in_array($user, array_column($comments, 'email'))) ? $fmsg['mark_commented']:"";
								$returnvalue = "<blockquote class='success'><h2>Entry details for entryId: $entryid in topicId: $topicid</blockquote><hr>";
								$returnvalue .= "<blockquote class='warning'>$name $stamp - [<strong>$num_votes votes</strong>] [<strong>$num_comments comments</strong>] $voted $commented<br>";
								$returnvalue .= "<h3>{$entry[0]['description']}</h3></blockquote>\n";
								
								$entry_card = $this->crd->cardNew("Entry [$entryid] in topic [$topicid]");
								
								$entry_body = [
								    $this->crd->cardItem('textblock', [
								        'text' => "Submitted by $name [$stamp]", 
								        'size' => 'small',
								        'weight' => 'bolder',
								        'wrap' => true
								    ]),
								    $this->crd->cardItem('textblock', [
								        'text' => "[+$num_votes Votes]",
								        'size' => 'small',
								        'color' => ($num_votes) ? 'good':'warning',
								        'weight' => 'bolder',
								        'wrap' => true,
								        'spacing' => 'none'
								    ]),
								    $this->crd->cardItem('textblock', [
								        'text' => $entry[0]['description'],
								        'size' => 'small',
								        'wrap' => true
								    ]),
								    $this->crd->cardItem('actionset', [
								        'actions' => [
								            $this->crd->cardItem('submit', [
								                'title' => 'Vote',
								                'data' => [
								                    'wbm_command' => "feedback vote $entryid"
								                ]
								            ]), 
								            $this->crd->cardItem('submit', [
								                'title' => 'Delete',
								                'style' => 'destructive',
								                'data' => [
								                    'wbm_command' => "feedback entry $entryid delete",
								                    'wbm_delete_card' => ""
								                ]
								            ]) 
								        ]
								    ])
								];
								    
								
								$entry_card['body'] = array_merge($entry_card['body'], $entry_body);
								
								$entry_action_container = $this->crd->cardItem('container', [
								    'items' => [
								        $this->crd->cardItem('textinput', [
								            'id' => 'feedback_new_comment',
								            'isMultiline' => true,
								            'placeholder' => 'Comment this entry'
								        ]),
								        $this->crd->cardItem('actionset', [
								            'actions' => [
								                $this->crd->cardItem('submit', [
								                    'title' => 'Add',
								                    'data' => [
								                        'wbm_command' => "feedback comment $entryid"
								                    ]
								                ]),
								                $this->crd->cardItem('submit', [
								                    'title' => 'Refresh',
								                    'data' => [
								                        'wbm_command' => "feedback entry $entryid",
								                        'wbm_delete_card' => ""
								                    ]
								                ]),
								                $this->crd->cardItem('submit', [
								                    'title' => 'Close',
								                    'style' => 'destructive',
								                    'data' => [
								                        'wbm_cancel_card' => ""
								                    ]
								                ])
								            ]
								        ])
								    ],
								    'style' => 'emphasis',
								    'bleed' => true
								]);
								
								if ($num_comments) {
									$returnvalue .= "<p><strong>comments</strong></p>";
									
									$entry_comment_show = $this->crd->cardItem('show', [
									    'title' => "Comments ($num_comments)",
									    'card' => [
									        'type' => 'AdaptiveCard',
									        'body' => []
									    ]
									]);
									
									foreach ($comments as $key => $value) {
									    
										$name = $this->db->contactGetName($value['email']);
										$name = ($name) ? $name : $value['email'];
										$email = $value['email']; 
										$returnvalue .= blockquote("<strong>{$value['id']} : <a href=mailto:$email>$name</a></strong> {$value['created']}<br>{$value['comment']}", "info");
										$counter++;
										
										$comment_body = $this->crd->cardItem('container', [
										    'items' => [
										        $this->crd->cardItem('textblock', [
										            'text' => "$name" . " [{$value['created']}]",
										            'size' => 'small',
										            'weight' => 'bolder'
										        ]),
										        $this->crd->cardItem('textblock', [
										            'text' => $value['comment'],
										            'size' => 'small',
										            'spacing' => 'none'
										        ])
										    ],
										    'style' => 'emphasis',
										    'bleed' => true
										]);
										
										
										$entry_comment_show['card']['body'][] = $comment_body;
										
										
										if ($mark == $counter) {
											//send bulk to avoid messages getting to large
											$this->messageSend($this->messageBlob($returnvalue, $roomid, $botid, "roomId", $type));
											//reset values for next bulk
											$counter = 0;
											$returnvalue = "";
											//avoid out of order messages
											sleep(0.1);
										}
								    }
									
								$entry_action_container['items'][1]['actions'][] = $entry_comment_show;
									
								}
								
								$entry_card['body'][] = $entry_action_container;
								
								$returnvalue .= "\n\n";
								$returnvalue .= "<hr><strong>feedback comment $entryid [comment text...]</strong> {$fmsg['help_comment']}<br><strong>feedback vote $entryid</strong> {$fmsg['help_vote']}";
								return $this->crd->card_prepare($entry_card);
							} return $entry_not_found;
						}
					} else return $fmsg['error_parameters'];
				}
				//FEEDBACK COMMENT
				elseif ($param1 == "comment") {
					$name = $this->db->contactGetName($user);
					if ($param2) {
						if (is_numeric($param2)) {
							$entry = $this->db->feedbackFetchEntry($this->db->quote($param2));
							if (count($entry)) {
								$entryid = $entry[0]['id'];
								$topicid = $entry[0]['topic_id'];
								$topic = $this->db->feedbackFetchTopics($botid, $topicid);
								
								if (count($topic) == 0) 
								    return $fmsg['error_entry_not_found'];
								
								if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) 								    
								    return $fsmg['acl_restricted'];
								
								if (!$this->db->feedbackTopicCheckAllowList("comments_allowed", $topic)) 
								    return $fmsg['acl_comment'];
								
								if (count($topic)) {
									$comment = implode(' ', array_slice($feature_attributes, 2));
									$commentid = $this->db->feedbackEntryComment($entryid, $comment, $user, true);
									$comment = $this->db->feedbackFetchEntryComment($commentid);
									
									if (count($comment)) {
										return blockquote("$name created a new commentId: **$commentid** for entryId: **$entryid** in topicId: **$topicid**\n\n{$comment[0]['comment']}", "success", $card_mode);
									} else return $fmsg['error_something_went_wrong'];	
								} else return $fmsg['error_entry_not_found'];
							} else return $fmsg['error_entry_not_found'];
						} 
						elseif ($param2 == "delete") {
						    if (is_numeric($param3)) {
						        $comment = $this->db->feedbackFetchEntryComment($param3);
							
							if (count($comment)) {
								$commentid = $comment[0]['id'];
								$entryid = $comment[0]['entry_id'];
								$email = $comment[0]['email'];
								$entry = $this->db->feedbackFetchEntry($entryid);
								
								if (!count($entry)) 
								    return $fmsg['error_entry_not_found'];
								
								    $topicid = $entry[0]['topic_id'];
								$topic = $this->db->feedbackFetchTopics($botid, $entry[0]['topic_id']);
								
								if (!count($topic)) 
								    return $fmsg['error_topic_not_found'];
								
								if ($email != $user) 
								    return $fmsg['error_delete_comment_created'];
								
								$this->db->feedbackEntryCommentDelete($commentid);
								return blockquote("$name <strong>deleted</strong> commentId: <strong>$commentid</strong> in entryId: <strong>$entryid</strong> in topicId: <strong>$topicid</strong><br><strong>{$comment[0]['comment']}</strong>", "danger");
							}else return $fmsg['error_comment_not_found'];
						} else return blockquote("Please provide a numeric commentId to delete", "warning");
					} else return blockquote("Please provide a numeric entryId to comment on", "warning");
				} else return blockquote("Please provide a numeric entryId to comment on", "warning");
			}
				//FEEDBACK VOTE
			elseif ($param1 == "vote") {
				if ($param2) {
					$entry = $this->db->feedbackFetchEntry($this->db->quote($param2));
					if (count($entry) > 0) {
						$entryid = $entry[0]['id'];
						$topicid = $entry[0]['topic_id'];
						$topic = $this->db->feedbackFetchTopics($botid, $topicid);
						if (count($topic) == 0) return $entry_not_found;
						if (!$this->db->feedbackTopicCheckAccessGroup($user, $topic)) return $restricted;
						if (!$this->db->feedbackTopicCheckAllowList("votes_allowed", $topic)) return blockquote("Voting on entries is disabled in this topic", "warning", $card_mode);
						$name = issetor($this->db->contactGetName($user));
						$name = ($name) ? $name : $user;
						if ($this->db->feedbackFetchEntryVoteExists($entryid, $user)) {
							$this->db->feedbackEntryVoteDelete("", $user, $entryid);
							$num_votes = count($this->db->feedbackFetchEntryVotes($entryid));
							$curr_votes = "[**$num_votes votes registered**]";
							return blockquote("$name revoked a vote on entryId: **{$entryid}** in topicId: **$topicid**\n\n{$entry[0]['description']} ", 'danger', $card_mode, ['headline' => '-1 Vote '. $curr_votes]);
						} else {
							$this->db->feedbackEntryVote($user, $param2);
							$num_votes = count($this->db->feedbackFetchEntryVotes($entryid));
							$curr_votes = "[**$num_votes votes registered**]";
							return blockquote("$name added a vote on entryId: **{$entryid}** in topicId: **$topicid**\n\n{$entry[0]['description']} ", 'success', $card_mode, ['headline' => '+1 Vote '. $curr_votes]);
						}		
					} else {
						return $entry_not_found;
					}
				}
			}
			return $this->db->adminGetFeatureUsage($id)['usage'];
			break;
				
				//FEATURE - WHOAMI
			case "whoami":
				$returnvalue = $access_pending = "";
				$contactinfo = $this->db->contactFetchContacts($user);
				$i=0;
				if (count($contactinfo)) {
					$headline = "{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}";
					$returnvalue .= "Group memberships:\n";
					$groups = $this->db->groupFetchGroups();
					foreach ($groups as $key => $value) {
						if ($this->db->groupCheckIfMember($contactinfo[0]['id'], $value['id'])) {
						    $desc = (!empty($value['description'])) ? "    - {$value['description']}\n":"";
							$returnvalue .=  "- **{$value['id']}** : {$value['groupname']}\n$desc";
							$i++;
						}
					}
					if ($i == 0) $access_pending = "**You are not part of any groups!**";
					$returnvalue .= "\n\nMember of **{$i}** groups. {$access_pending}";
					return cardProfileBox($headline, $returnvalue);
				}
				else {
					return blockquote("**{$user}** was not found in my database.", "danger", true);
				}
				break;
				
				//FEATURE - CHUCK
			case "chuck":
				return $this->generate->getJoke($id);
				break;
				
				//FEATURE - YOMOMMA
			case "yomomma":
				return $this->generate->getJoke($id);
				break;
				
				//FEATURE - JOKE
			case "joke":
				return $this->generate->getJoke($id);
				break;
				
				//FEATURE - WHOIS
			case "whois": //Does a lookup on Name or Email and returns the results
				if (count($feature_attributes)) {
					
					if(validateEmail($feature_attributes[0])) {
						
						$data = array(
								'recepientType' => 'email',
								'recepientValue' => $feature_attributes[0],
								'sender' => $botid);
					
					} else {
						
						$search_string = str_replace(" meta", "", implode(" ", $feature_attributes));
						$data = array(
								'recepientType' => 'displayName',
								'recepientValue' => $search_string,
								'sender' => $botid);
					
					}
					
					//$returnvalue = $this->generate->pretty($this->peopleGet($data), "spark", $botid);
					$meta = False;
					
					$returnvalue = $this->peopleGet($data);
					
					//DEBUG RESULT
					//return codeBlock(json_encode($returnvalue['items'], JSON_PRETTY_PRINT));
					if (in_array('meta', $feature_attributes)) {
						$meta = (count($returnvalue['items']) === 1) ? True : False;
					}
					
					return cardWhoisSearchResult($returnvalue['items'], $meta);
				}
				break;
				
				//FEATURE - ADMIN
			case "admin":
				if (count($feature_attributes)) {
					list($param1, $param2, $param3, $param4) = $feature_attributes;
					// ADMIN TASK QUEUE
					if ($param1 == "task") {
						if ($param2 == "queue"){
							if ($param3 == "lock") {
								return (queueLock("lock")) ? "Task queue locked temporarily (1 hour)":"Unable to lock queue";
							} elseif ($param3 == "unlock") {
								return (queueLock("unlock")) ? "Task queue unlocked successfully":"Unable to unlock queue";
							}
						}
					}
					// ADMIN MESSAGE
					elseif ($param1 == "message") {
						$report = blockquote("Message report", "info");
						$spaces = $contacts = $emails = [];
						$messageParsel = array();
						$a = 0;
						if ($param2 == "list") {
							if (issetor($param3)) {
								if (is_numeric($param3)) {
									$message = $this->db->messagesLoad($param3);
									if (count($message)){
										return "##".$message[0]['title']."\n".$message[0]['message'];
									}
									else return blockquote("Sorry that message does not exist", "warning");
								}
							}
							$response = blockquote("Stored messages", "info");
							$messages = $this->db->messagesLoad();
							if (count($messages)) {
								foreach ($messages as $key => $value) {
									$messagepart = substr(removeHTML($value['message']), 0, 50);
									$title = substr(removeHTML($value['title']), 0, 50);
									$response .= "\n- **{$value['id']}** : $title\n" . blockquote($messagepart, "info");
								}
								return $response;
							}
							else {
								return blockquote("No stored announcements was found, you can create announcements in the Webex Teams Bot Manager");
							}
						}
						elseif (in_array($param2, array('echo', 'announce'))) {
							if (issetor($param3)) {
								foreach (explode(',', $param3) as $k => $v) {
									if (validateEmail($v)) {
										$emails[] = $v;
										continue;
									}
									$groupinfo = $this->db->groupFetchGroups($v);
									if (count($groupinfo)) {
										$groupid = $groupinfo[0]['id'];
										if ($groupinfo[0]['botid']) {
											if ($groupinfo[0]['botid'] == $botid) {
												$spaces = array_merge($spaces, $this->db->groupGetSpaceMemberIdArray($groupid, $botid));
											}
											else {
												$report .= "\n- Group $v exists but is not owned by me.. skipping!";
												continue;
											}
										}
										$contacts = array_merge($contacts, $this->db->groupGetMemberIdArray($groupid));
									}
									else $report .= "\n- Group $v does not exist!";
								}
							}
							else return blockquote("You must specify at least one groupid or group-alias so I know where to send the message", "warning");
							
							$contacts = array_unique($contacts);
							$spaces = array_unique($spaces);
							if ($param2 == "echo") {
								$message = $this->db->quote(implode(' ', array_slice($feature_attributes, 3)));
							}
							else {
								if (is_numeric(issetor($param4))) {
									$messageinfo = $this->db->messagesLoad($param4);
									if (count($messageinfo)) {
										$title = (issetor($messageinfo[0]['title'])) ? "##".$messageinfo[0]['title']."  \n":"";
										$body = $messageinfo[0]['message'];
										$message = $title . $body;
									}
									else {
										return blockquote("This $param4 is not a valid message ID", "warning");
									}
								}
								else {
									return blockquote("You need to specify the message ID I am supposed to send, it has to be a numeric value.", "warning");
								}
							}
							
							if (trim(issetor($message))) {
								//Generate payloads for the requests
								if (count($contacts)) {
									foreach ($contacts as $key => $value) {
										$messageParsel[$a]['rec'] = $value;
										$messageParsel[$a]['type'] = "toPersonId";
										$messageParsel[$a]['message'] = $message;
										$messageParsel[$a]['files'] = "";
										$a++;
									}
								}
								if (count($spaces)) {
									foreach ($spaces as $key => $value) {
										$messageParsel[$a]['rec'] = $value;
										$messageParsel[$a]['type'] = "roomId";
										$messageParsel[$a]['message'] = $message;
										$messageParsel[$a]['files'] = "";
										$a++;
									}
								}
								if (count($emails)) {
									foreach ($emails as $key => $value) {
										$messageParsel[$a]['rec'] = $value;
										$messageParsel[$a]['type'] = "toPersonEmail";
										$messageParsel[$a]['message'] = $message;
										$messageParsel[$a]['files'] = "";
										$a++;
									}
								}
							}
							else {
								return blockquote("You cannot send a blank message, make sure you specify the attributes correctly and in the correct order", "warning");
							}
							
							if (count($messageParsel)) {
								$report .= $this->bulkSplitRequestPost("messages", $messageParsel, $botid, 1, 50, 150, 15, 100);
							}
							return $report;
						}
						else return $this->db->adminGetFeatureUsage($id)['usage'];
					}
					// ADMIN FEEDBACK
					elseif ($param1 == "feedback") {
						$allow_list = array('comments_allowed', 'votes_allowed', 'entry_create_allowed', 'entry_view_allowed', 'entry_delete_allowed');
						if($param2 == "create") {
							$topic = implode(' ', array_slice($feature_attributes, 2));
							if ($topic) {
								$data = array(
										'botid' => $botid,
										'title' => $topic,
										'comments_allowed' => 1,
										'votes_allowed' => 1,
										'entry_create_allowed' => 1,
										'entry_view_allowed' => 1,
										'entry_delete_allowed' => 1,
										'accessgroup' => 0
								);
								$created_id = $this->db->feedbackTopicAdd($data, true);
								return blockquote("Successfully created a new topic with topicId: <strong>$created_id</strong>", "success");
							} else return blockquote("You need to specifiy a topic title!", "warning");
						}
						elseif (in_array($param2,  $allow_list)) {
							if ($param3) {
								list($topicid, $value) = explode(':',$param3);
								if (in_array($value, array('1', '0'))) {
									if (is_numeric($topicid)) {
										$topic = $this->db->feedbackFetchTopics($botid, $topicid);
										if (count($topic)) {
											$this->db->feedbackTopicUpdate(array($param2 => $value), $topicid);
											return blockquote("<strong>$param2</strong> was set to <strong>$value</strong> in topicId: <strong>$topicid</strong>", "success");
										} return blockquote("Topic does not exist!", "warning");
									} return blockquote("Incorrect topicId", "warning");
								} else return blockquote("Value must be 0 or 1. <strong>topicId:value</strong>", "warning");
							} else return blockquote("Please provide topicId and allow value (0 or 1) <strong>topicId:value</strong>", "warning");
						}
						elseif ($param2 == "topic") {
							if (is_numeric($param3)) {
								$topic = $this->db->feedbackFetchTopics($botid, $param3);
								if (count($topic)) {
									$returnvalue = "";
									foreach ($topic[0] as $k => $v) {
										$returnvalue .= "<strong>$k</strong> : $v <br>";
									}
									return $returnvalue;
								} return blockquote("Topic does not exist!", "warning");
							} return blockquote("Incorrect topicId", "warning");
						}
					}
					// ADMIN MAINTENANCE
					elseif ($param1 == "maintenance") {
						if($param2 == "enable") {
							if ($this->db->adminCheckIfMaintenance()) {
								return "Maintenance mode is already enabled!";
							}
							else {
								$this->db->adminSetMaintenance(1);
								return "Maintenance mode is activated, no external tasks will be created!";
							}
						}
						// ADMIN MAINTENANCE DISABLE
						elseif($param2 == "disable") {
							if (!$this->db->adminCheckIfMaintenance()) {
								return "Maintenance mode is already disabled!";
							}
							else {
								$this->db->adminSetMaintenance(0);
								return "Maintenance mode is deactivated, external tasks will now be created!";
							}
						}
					}
					// ADMIN DELETE
					elseif ($param1 == "delete"){
						$emails = explode(",", $param2);
						$returnvalue = "**Delete report**";
						foreach($emails as $key => $value) {
							if(filter_var($value, FILTER_VALIDATE_EMAIL)) {
								$existing = $this->db->contactFetchContacts($value);
								if (count($existing)>0) {
									if ($value == $user){
										$returnvalue .= "\n- You cannot delete yourself! (<strong>$value</strong>)";
									}
									else {
										$this->db->contactRemove($existing[0]['id']);
										$returnvalue .= "\n- Successfully removed (<strong>$value</strong>)\n";
									}
								}
								else {
									$returnvalue .= "\n- The user does not exist or has already been deleted! (<strong>$value</strong>)\n";
								}
							}
							else{
								$returnvalue .= "\n- This email address was not valid! (<strong>$value</strong>)\n";
							}
						}
						return $returnvalue;
					}
					// ADMIN GROUP (Base command)
					elseif ($param1 == "group") {
						$returnvalue = $error_report ="";
						// ADMIN GROUP LIST (Lists the added groups)
						if ($param2 == "list") {
							$groups = $this->db->groupFetchGroups();
							$returnvalue = "**GROUPS**<hr>";
							foreach ($groups as $key => $value) {
								$members = $this->db->groupMembershipNumber($value['id']);
								$groupid = ($value['sub_id'] != "") ? $value['sub_id'] : $value['id'];
								$returnvalue .=  "\n- **$groupid**:{$value['groupname']} ($members members)\n>{$value['description']}";
							}
							return $returnvalue;
						}
						// ADMIN GROUP MENTION (Mentions a group of people in a space)
						elseif ($param2 == "mention") {
							if ($param3 != "") {
								if ($type != "group") {
									return "This feature only works in group spaces as I cannot mention people in a 1:1 space";
								}
								$groupinfo = $this->db->groupFetchGroups($param3);
								$text = "\n\n" . implode(' ', array_slice($feature_attributes, 3));
								if (count($groupinfo) > 0) {
									$headline = "<blockquote class='danger'><b>ATTENTION</b> : <b>" . $groupinfo[0]['groupname']. "</b></blockquote>\n";
									$returnvalue = "";
									$members = $this->db->groupGetMembers($groupinfo[0]['id']);
									if (count($members) > 0) {
										foreach ($members as $key => $value) {
											$user = $this->db->contactFetchContacts($value['contactid']);
											$returnvalue .= "<@personEmail:{$user[0]['emails']}|{$user[0]['firstName']}> ";
										}
										return "$headline $returnvalue $text";
									} else { return "This group has no member"; }
								} else { return "No such group"; }
							} else { return $this->db->adminGetFeatureUsage($id)['usage']; }
						}
						// ADMIN GROUP ADDTOSPACE (Adds a group of people to a space)
						elseif ($param2 == "addtospace") {
							$a = 0;
							$memberParsel = array();
							if ($type == "direct") return blockquote("This space is a 1:1 space, you cannot add people here", "warning");
							if ($param3 != "") {
								$groupinfo = $this->db->groupFetchGroups($param3);
								if (count($groupinfo)) {
									$members = $this->db->groupGetMembers($groupinfo[0]['id']);
									if (count($members)) {
										foreach ($members as $key => $member) {
											$memberParsel[$a]['roomtype'] = "roomId";
											$memberParsel[$a]['id'] = $roomid;
											$memberParsel[$a]['method'] = "personId";
											$memberParsel[$a]['person'] = $member['contactid'];
											$a++;
										}
										// Start request intervals
										$result = $this->bulkSplitRequestPost("memberships", $memberParsel, $botid, $botr=1);
										//Only report error if verbose is enabled
										if ($param4 == "-v") {
											return $result;
										} else {
											//Report successful if the above command is ran.
											return blockquote("Command was successfully executed!", "success");
										}
										// End request intervals
										
									} else return "**{$groupinfo[0]['groupname']}** has 0 members";
								} else return blockquote("No such group", "warning");
							} else return blockquote("Please provide a valid groupid", "warning");
						}
						//CONTINUE HERE
						// ADMIN GROUP MEMBERS (View members in group)
						elseif ($param2 == "members") {
							if ($param3 != "") {
								$groupinfo = $this->db->groupFetchGroups($param3);
								if (count($groupinfo) > 0) {
									$headline = "**MEMBERS OF {$groupinfo[0]['groupname']}** <hr>";
									$members = $this->db->groupGetMembers($groupinfo[0]['id']);
									if (count($members) > 0) {
										foreach ($members as $key => $member) {
											$memberinfo = $this->db->contactFetchContacts($member['contactid']);
											$returnvalue .= "\n- {$memberinfo[0]['firstName']} {$memberinfo[0]['lastName']} ({$memberinfo[0]['emails']})";
										}
										return $headline . $returnvalue;
									} else return "**{$groupinfo[0]['groupname']}** has 0 members";
								} else return "No such group";
							} else return "Please provide a valid groupid";
						}
						// ADMIN GROUP CREATE (Create group)
						elseif ($param2 == "create") {
							if ($param3 != "") {
								list($groupname, $subid) = explode(":", $param3);
								if ($groupname != "") {
									$groupname = $this->db->quote($groupname);
									if ($subid != "") {
										$subid = str_replace(" ", "", $subid);
										$subid = $this->db->quote($subid);
									}
									$this->db->groupAdd(array("groupname"=>$groupname,"sub_id"=>$subid, "botid" => "0"));
									return "Created group: $groupname";
								} else return "You need a valid groupname";
							} else return "Please provide the groupname and preferably a short identifier like this: **groupname**:**groupidentifier** (i.e. MY-GROUPNAME:g1)";
						}
						// ADMIN GROUP DELETE (Delete group)
						elseif ($param2 == "delete") {
							if ($param3 != "") {
								$groupid = $this->db->quote($param3);
								$groupinfo = $this->db->groupFetchGroups($param3);
								if (count($groupinfo) > 0) {
									$this->db->groupRemove($groupid);
									return "Group was deleted";
								} else return "This group does not exist!";
							} else return "You must provide a valid group ID";
						}
						elseif ($param2 == "add" or $param2 == "remove") {
							$returnvalue = blockquote("Execution {$param2} report", "info") . "\n";
							list($groupids, $email) = explode(":", $param3);
							$groups = explode(",", $groupids);
							$emails = explode(",", $email);
							foreach ($emails as $key => $value) {
								if(validateEmail($value) or $value == "this") {
									foreach ($groups as $key => $groupid) {
										$groupcheck = $this->db->groupFetchGroups($groupid);
										if (count($groupcheck)>0) {
											$groupid = issetor($groupcheck[0]['id']);
											$groupname = issetor($groupcheck[0]['groupname']);
											$sub_id = issetor($groupcheck[0]['sub_id']);
											$outputid = ($sub_id) ? $sub_id:$groupid;
											$isbotowner = ($botid != $groupcheck[0]['botid']);
											if ($value == "this") {
												$member = $this->db->groupCheckIfSpaceMember($roomid, $groupid, $botid);
												$roomdetails = $this->roomGetDetails($botid, $roomid);
												$roomname = $roomdetails['title'];
												if ($param2 == "remove") {
													if ($member) {
														$this->db->groupRemoveSpace($groupid, $roomid, $botid);
														$returnvalue .= "- **{$roomname}** was removed from **{$groupname}**\n";
														continue;
													} else {
														$returnvalue .= "- **{$roomname}** is not a member of **{$groupname}**\n";
														continue;
													}
												}
												elseif ($param2 == "add") {
													if ($isbotowner) {
														$returnvalue .= "- You can only add spaces to a group that I am the owner of (I do not own the **$groupname** group)\n";
														continue;
													}
													if ($roomdetails['type'] != "group") {
														$returnvalue .= "- You can only add group spaces to my group (this is a **{$roomdetails['type']}** space)\n";
														continue;
													}
													if ($member) {
														$returnvalue .= "- **{$outputid}:** **{$roomname}** is already member of **{$groupname}**\n";
													}
													else {
														$this->db->groupAddSpace(array('groupid' => $groupid, 'spaceid' => $roomid, 'botid' => $botid));
														$returnvalue .= "- **{$outputid}:** **{$roomname}** was added to **{$groupname}**\n";
													}
												}
											}
											else {
												$contactinfo = $this->db->contactFetchContacts($value);
												if (count($contactinfo)>0){
													$contactid = $contactinfo[0]['id'];
													$name = $this->db->contactGetName($contactid);
													$member = ($this->db->groupCheckIfMember($contactid, $groupid));
													if ($param2 == "remove") {
														if($member) {
															$this->db->groupRemoveContact($groupid, $contactid);
															$returnvalue .= "- **{$groupid}:** **{$name}** was removed from **{$groupname}**\n";
														}
														else {
															$returnvalue .= "- **{$name}** is not a member of **{$groupname}**\n";
														}
													}
													elseif ($param2 == "add") {
														if($member) {
															$returnvalue .= "- **{$outputid}:** {$value} is already member of **{$groupname}**\n";
														}
														else {
															$this->db->groupAddContact(array('groupid' => $groupid, 'contactid' => $contactid));
															$returnvalue .= "- Successfully added **{$name}** to **{$groupname}**\n";
														}
													}
												}
												else {
													$returnvalue .= " - **{$value}** does not exist in my database\n";
												}
											}
										}
										else {
											unset($groups[array_search($groupid, $groups)]);
											$returnvalue .= "- The group with id: **{$groupid}** does not exist in my database\n";
										}
									}
								}
								else {
									$returnvalue .= "- **{$value}** is not a valid email\n";
								}
							}
							return $returnvalue;
						}
					}
					// ADMIN JOINABLE
					elseif ($param1 == "joinable") {
						if ($this->db->adminCheckJoinableSpace($botid, $roomid)){
							$this->db->adminRemoveJoinableSpace($botid, $roomid);
							return "This space was removed from joinlist";
						}
						else{
							$roominfo = $this->roomGetDetails($botid, $roomid);
							$title = $roominfo['title'];
							if (empty($title)) {
								$title = "No title";
							}
							$data_array = array("spaceid" => $roomid,
									"spacetitle" => $title,
									"botid" => $botid);
							$this->db->adminAddJoinableSpace($data_array);
							return "This space was added to joinlist";
						}
					}
					// ADMIN USER
					elseif ($param1 == "user") {
					    
                        //Add user
					    if ($param2 == 'add') {
					        
					        $custom = array(
					            "loggedinuser"=>$user,
					            "botresponse"=>1,
					            "group"=>""
					        );
					        
					        $taskid = $this->db->generatetaskid();
					        $taskname = "Add contacts via bot";
					        $emails = explode(",", $param3);
					        $valid_emails = [];
					        
					        foreach ($emails as $key => $value) {
					            
					            if (validateEmail($value)) {
					                
					                $valid_emails[] = $value;
					                
					            } else {
					                
					                continue;
					                
					            }
					            
					        }
					        
					        $num_emails = count($valid_emails);
					        $valid_emails = array_unique($valid_emails);
					        
					        if ($num_emails > 50) {
					            
					            if ($this->db->taskQueueInsert($taskid, $taskname, 'addtolocal', $valid_emails, $botid, $custom)) {
					                
					                return $this->crd->msgCard("Task has been added the to queue, will attempt to add **$num_emails** user(s). I will notify you when the task is completed!", ['headline' => 'Please wait...']);
					            
					            } else {
					                
					                return $this->crd->msgCard("Task failed at query point", ['style'=>'error']);
					                
					            }
					            
					        } 
					        elseif (($num_emails <= 50) and ($num_emails > 0)) {
					            					
					                $userAddParsel = [];
					                
					                foreach ($valid_emails as $key => $email) {
					                    
					                    $userAddParsel[] = $email;
					                   
					                }
					                
					                if (count($userAddParsel) > 0) {
					                    // Start request interval
					                    $addResult = $this->bulkSplitRequestPost('addtolocaldirect', $userAddParsel, $botid, $botr=1);
					                    return $addResult;
					                    // End request interval
					                } else  {
					                    return "No valid e-mail addresses in your request";
					                }
					        }
					        else { 
					            return $this->crd->msgCard("No valid e-mails found, **admin user add email,email** no spaces between the commas. user@domain.com", ['style' => 'warning', 'headline' => 'You must supply one or more e-mail addresses']); 
					        }
					    }
					    					    
					    elseif ($param2 == "delete") {
					        
					        $emails = explode(",", $param3);
					     
					        $returnvalue = "**Delete report**";
					        $returnstyle = "success";
					        
					        foreach ($emails as $key => $value) {
					            
					            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
					            
					                $existing = $this->db->contactFetchContacts($value);
					                
					                if (count($existing)) {
					                    
					                    if ($value == $user) {
					                        
					                        $returnvalue .= "\n- You cannot delete yourself! (**$value**)";
					                        $returnstyle = "danger";
					                        
					                    } else {
					                        
					                        $this->db->contactRemove($existing[0]['id']);
					                        $returnvalue .= "\n- Successfully removed (**$value**)\n";
					                        
					                    }
					                    
					                } else {
					                    
					                    $returnvalue .= "\n- The user does not exist or has already been deleted! (**$value**)\n";
					                    $returnstyle = "warning";
					                    
					                }
					                
					            } else {
					                
					                $returnvalue .= "\n- This email address was not valid! (**$value**)\n";
					                $returnstyle = "warning";
					                
					            }
					            
					        }
					        
					        return blockquote($returnvalue, $returnstyle, $card_mode);
					        
					    }
					    
						// ADMIN USER LIST
						elseif ($param2 == "list") {
						    
							$contacts = $this->db->contactFetchContacts();
							$marker = 50;
							$a=0;
							$groups = $this->db->groupFetchGroups();
							$returnvalue = "**USERS**<hr>";
							
							foreach ($contacts as $key => $value) {
							    
								$groupbulk = "";
								$i = 0;
								
								foreach ($groups as $key1 => $value1) {
								    
									if ($this->db->groupCheckIfMember($value['id'],$value1['id'])) {
									    
										$groupbulk .= ":[**{$value1['id']}**]";
										$i++;
										
									}
									
								}
								
								if (!$i) { 
								    
								    $groupbulk = " - **Pending access**";
								
								}
								
								$returnvalue .=  "**{$value['firstName']} {$value['lastName']}**:**{$value['emails']}**{$groupbulk}<br>";
								$a++;
								if ($a == $marker){
									$query = array(	'recepientType' => 'roomId',
											'recepientValue' => $roomid,
											'sender' => $botid,
											'text' => $returnvalue);
									$this->messageSend($query);
									$marker = $a + 50;
									$returnvalue = "";
								}
							}
							return $returnvalue . "<br>Results: " . $a;
						}
						
						//ADMIN USER CHECK
						else if ($param2 == "check") {
						    
						    if (validateEmail($param3)) {
						        
						        $contactinfo = $this->db->contactFetchContacts($param3);
						        
						        if (count($contactinfo)) {
						            
						            $profile_card = $this->crd->cardNew("User profile");
						            $memberships = array();
						            
						            $groupMemberships = $this->db->contactGetGroupMemberships($contactinfo[0]['id']);
						            						            
						            foreach ($groupMemberships['groups'] as $key => $value) {
						                foreach ($value as $k => $v) {
    						                $memberships[] = array(
    						                    'title' => strval($k),
    						                    'value' => $v
    						                );
						                }
						            }
				     
						            $profile_body = $this->crd->cardItem('container', [
						                'items' => [
						                    $this->crd->cardItem('textblock', [
						                        'text' => "{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}",
						                        'color' => 'accent',
						                        'weight' => 'bolder',
						                        'size' => 'large'
						                    ]),
						                    $this->crd->cardItem('columnset', [
						                        'columns' => [
						                            $this->crd->cardItem('column', [
						                                'items' => [
						                                    $this->crd->cardItem('image', [
						                                        'url' => $contactinfo[0]['avatar'],
						                                        'style' => 'person',
						                                        'size' => 'medium'
						                                    ])
						                                ],
						                                'width' => 25
						                            ]),
						                            $this->crd->cardItem('column', [
						                                'items' => [
						                                    $this->crd->cardItem('factset', [
						                                        'facts' => [
						                                            [
						                                                'title' => 'isAdmin',
						                                                'value' => issetor($contactinfo[0]['type']) ? 'Yes': 'No'
						                                            ],
						                                            [
						                                                'title' => 'userId',
						                                                'value' => issetor($contactinfo[0]['id']) 
						                                            ],
						                                            [
						                                                'title' => 'email',
						                                                'value' => issetor($contactinfo[0]['emails']) 
						                                            ]
						                                        ]
						                                    ])
						                                ],
						                                'width' => 75
						                            ])
						                        ]
						                        
						                    ]),
						                    $this->crd->cardItem('textblock', [
						                        'text' => 'Group memberships:'
						                     ]),
						                     $this->crd->cardItem('factset', [
						                         'facts' => $memberships
						                     ]),
						                     $this->crd->cardItem('actionset', [
						                         'actions' => [
						                             $this->crd->cardItem('submit', [
						                                 'title' => 'Close',
						                                 'data' => array(
						                                     'wbm_cancel_card' => ''
						                                 ),
						                                 'style' => 'destructive'
						                             ])
						                         ]
						                     ])
						                ]
						            ]);
						            
						            $profile_card['body'][] = $profile_body;
						            
						            
						            if ($i == 0) $access_pending = "  **NOT PART OF ANY GROUPS!**";
						            
						            $returnvalue .= "Member of **{$i}** groups. {$access_pending}";
						            
						            return $this->crd->card_prepare($profile_card);
						            
						        } else {
						            
						            return $this->crd->msgCard("The user does not exist in the database", ['style' => 'warning']);
						        
						        }
						        
						    }
						    
						    else return "Usage: **admin check [user@domain.com]** - *Checks if a user exists in the database*";
						}
						// ADMIN USER BLOCK
						elseif ($param2 == "block") {
						    
						    // ADMIN BLOCK LIST
						    if ($param3 == "list") {
						        
						        $blocked_contacts = $this->db->adminGetBlockedContacts();
						        $returnvalue = "**Blocked contacts**<br><br>";
						        $i=0;
						        
						        foreach ($blocked_contacts as $key => $value) {
						            
						            $i++;
						            $returnvalue .= "{$i}: {$value['email']}<br>";
						            
						        }
						        
						        return ($i == 0) ? $this->crd->msgCard("No users are currently blocked") : $this->crd->msgCard($returnvalue);
						        
						    }
						    
						    elseif (validateEmail($param3)) {
						        
						        if (!$this->db->adminGetBlockedContacts($param3)) {
						            
						            if (($user == $param3)) {
						                
						                return $this->crd->msgCard("You cannot block yourself, that would be a mistake..", ['style' => 'warning']);
						                
						            }
						            
						            else {
						                
						                $this->db->adminBlockContact($param3);
						                return $this->crd->msgCard("{$param3} is now blocked", ['style' => 'success']);
						                
						            }
						            
						        } else {
						    
						            return $this->crd->msgCard("User is already blocked", ['style' => 'warning']);
						        
						        }
						    
						    } else {
						        
						        return $this->crd->msgCard("The email was not valid!", ['style' => 'warning']);
						    
						    }
						}
						// ADMIN USER UNBLOCK
						elseif ($param2 == "unblock") {
						    if(validateEmail($param3)) {
						        if ($this->db->adminGetBlockedContacts($param3)) {
						            $this->db->adminUnblockContact($param3);
						            return $this->crd->msgCard("{$param3} is now unblocked", ['style' => 'success', 'headline'=>'OK!']);
						        }
						        else return $this->crd->msgCard("Cannot unblock a user that is not blocked", ['style' => 'warning']);
						    }
						    else return $this->crd->msgCard("The email was not valid!", ['style' => 'warning']);
						}
						
						return $this->crd->featureAdminCard($param1);
						
				    }					
					// ADMIN QUEUE
					elseif ($param1 == "queue") {
						if($param2 == "report") {
							return "Number of tasks in the queue: **" . $this->db->adminCheckTaskQueue() . "**";
						}
						// ADMIN QUEUE PURGE
						elseif ($param2 == "purge"){
							$this->db->adminPurgeTaskQueue();
							$number = $this->db->adminCheckTaskQueue();
							return "Task queue was purged, there are now: **" . $number . "** tasks queued" ;
						}
						else return "Usage: **admin queue report** - *Checks how many tasks are in the queue currently*\nUsage: **admin queue purge** - *Deletes all pending tasks in the queue*";
					}
					// ADMIN SPACERESPONSE
					elseif ($param1 == "spaceresponse") {
						if($param2 == "status") {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							$returnvalue = "";
							if ($roominfo['type'] == "group") {
								$roomexcluded = $this->db->adminCheckGroupResponseAcl($botid, $roomid);
								$specialaccess = $this->db->adminCheckUserGroupResponseAcl($botid, $roomid);
								if ($roomexcluded){
									$returnvalue .= "Will I respond to all members of **this** space? **YES!**<br>";
								}
								else {
									$returnvalue .= "Will I respond to all members of **this** space? **NO!**<br>";
								}
								if (count($specialaccess)) {
									foreach ($specialaccess as $key => $value) {
										$contactinfo = $this->db->contactFetchContacts($value['contactid']);
										$returnvalue .= "I answer to **{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}** in **this** space<br>";
									}
								}
								else {
									$returnvalue .= "There are **no** single user access enabled for **this** space.";
								}
								return $returnvalue;
							}
							else {
								return "This space is not a group";
							}
						}
						// ADMIN SPACERESPONSE ENABLE
						elseif ($param2 == "enable") {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							if ($roominfo['type'] == "group") {
								if ($this->db->adminCheckGroupResponseAcl($botid, $roomid)) {
									return "I will already respond to everyone in **this** space, bah...";
								}
								else {
									$this->db->adminAddGroupResponseAcl($botid, $roomid, $roominfo['title']);
									if (($this->db->adminCheckGroupResponseAcl($botid, $roomid))) {
										return "I will now respond to everyone in **this** space!";
									}
									else {
										return "Something went wrong when enabling space response, not added";
									}
									
								}
							}
							else {
								return "This space is not a group!";
							}
						}
						// ADMIN SPACERESPONSE [EMAIL]
						elseif(validateEmail($param2)) {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							if ($roominfo['type'] == "group") {
								$contactinfo = $this->db->contactFetchContacts($param2);
								if (count($contactinfo) > 0) {
									if ($this->db->adminCheckUserGroupResponseAcl($botid, $roomid, $contactinfo[0]['id'])) {
										$this->db->adminRemoveUserGroupResponseAcl($botid, $roomid, $contactinfo[0]['id']);
										return "I will no longer answer to **{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}** in **this** space!";
									}
									else {
										$this->db->adminAddUserGroupResponseAcl($botid, $roomid, $contactinfo[0]['id']);
										return "I will now answer to **{$contactinfo[0]['firstName']} {$contactinfo[0]['lastName']}** in **this** space!";
									}
								} else return "The user was not found in my database, you need to add the user before granting special access.";
							} else return "This space is not a group!";
						}
						// ADMIN SPACERESPONSE DISABLE
						elseif ($param2 == "disable") {
							$roominfo = $this->roomGetDetails($botid, $roomid);
							if ($roominfo['type'] == "group") {
								if (!$this->db->adminCheckGroupResponseAcl($botid, $roomid)){
									return "I am already **NOT** responding to everyone in **this** space, comeon..";
								}
								else {
									$this->db->adminRemoveGroupResponseAcl($botid, $roomid);
									return "I will no longer respond to everyone in **this space**";
								}
							}
							else {
								return "This space is not a group!";
							}
						}
						else return "$usequote Usage:
						**admin spaceresponse enable** - *Enables all users in the group space to chat with the bot in that particular space*\n
						**admin spaceresponse disable** - *Prevent all users in the group space to chat with the bot in that particular space (default)\n
						**admin spaceresponse user@domain.com** - *Enables that particular user to chat with the bot in that particular space, issue the same command towards the same user will remove the privelege";
					}
					else return $this->db->adminGetFeatureUsage($id)['usage'];
				}
				else return $this->db->adminGetFeatureUsage($id)['usage'];
				break;
				//FEATURE - USAGE
			case "usage":
				$report = $this->db->adminReportLogs($botid);
				$query = array('recepientType' => 'roomId',
						'recepientValue' => $roomid,
						'sender' => $botid,
						'text' => $report);
				$this->messageSend($query);
				return "There are lies, damned lies and statistics. *(Mark Twain)*";
				break;
				//FEATURE - SPACE
			case "space":
				if (count($feature_attributes) == 1) {
					list($param1) = $feature_attributes;
				}
				elseif (count($feature_attributes) > 1) {
					list($param1, $param2) = $feature_attributes;
				}
				else {
					return $this->db->adminGetFeatureUsage($id)['usage'];
				}
				// SPACE LIST
				if ($param1 == "list"){
					$spacelist = $this->db->adminGetJoinableSpace($botid);
					$returnvalue = "**LIST OF JOINABLE SPACES**<br>(**ID**:SPACETITLE)\n";
					foreach ($spacelist as $key => $value) {
						$returnvalue .= "- **{$value['id']}**: {$value['spacetitle']}\n\n";
					}
					$returnvalue .= "**" . count($spacelist) . "** joinable spaces found. Type **space join [ID]**. To join more than one space, you can separate the ID's with comma (no spaces) i.e: **space join 1,2,3**.";
					return $returnvalue;
				}
				// SPACE JOIN
				elseif ($param1 == "join"){
					// SPACE JOIN [PARAM]
					if (isset($param2) and !empty($param2)){
						$returnvalue = "**SPACE JOIN REPORT**\n\n";
						$joinspaces = explode(",", $param2);
						foreach ($joinspaces as $key => $value){
							if ($this->db->adminCheckJoinableSpaceById($botid, $value)){
								$spaceinfo = $this->db->adminGetJoinableSpaceById($botid, $value)[0];
								$this->membershipCreate($botid, $spaceinfo['spaceid'], $user);
								$returnvalue .= "- Added you to {$spaceinfo['spacetitle']}\n";
							}
							else {
								if (empty($value)){
									$returnvalue .= "- ID input: **EMPTY** is not a joinable space (operation was terminated)! please separate id's with commas and do not include spaces i.e: **1,2,3**\n";
								}
								else {
									$returnvalue .= "- ID input: $value is not a joinable space\n";
								}
								
							}
						}
						return $returnvalue;
					}
					else {
						return "There was something wrong with your input! **space join [id]**, please try again.";
					}
				}
				// SPACE ADD
				elseif($param1 == "add") {
					if (isset($param2) and !empty($param2)) {
						$emails = explode(",",$param2);
						$emails = array_unique($emails);
						$a = 0;
						foreach ($emails as $key => $email) {
							if (validateEmail($email)) {
								$memberParsel[$a]['roomtype'] = "roomId";
								$memberParsel[$a]['id'] = $roomid;
								$memberParsel[$a]['method'] = "personEmail";
								$memberParsel[$a]['person'] = $email;
								$a++;
							}
						}
						if (count($memberParsel) > 0) {
							// Start request interval
							return $this->bulkSplitRequestPost("memberships", $memberParsel, $botid, $botr=1);
							// End request interval
						} else return "No valid e-mail addresses in your request";
					} else return "Please provide a CSV list of email addresses (only full e-mail addresses user@domain.com). Separate the emails with comma and no spaces!";
				}
				// SPACE DETAILS - PROVIDES DETAILS OF THE SPACE
				elseif($param1 == "details") {
				    
				    return codeBlock(json_encode($this->roomGetDetails($botid, $roomid), JSON_PRETTY_PRINT));				 
				}
				// SPACE GENCSV - GENERATES A CSV OF SPACE MEMBER EMAILS
				elseif($param1 == "gencsv") {
					$members = $this->membershipGet($roomid, $botid);
					$csv = array();
					$max = 100;
					$a = 1;
					$b = 1;
					foreach ($members['items'] as $key => $member) {
						$csv[] = $member['personEmail'];
						$a++;
						$b++;
						if ($b == $max) {
							$this->messageSend($this->messageBlob(implode(',',$csv), $user, $botid));
							$csv = array();
							$b = 1;
						}
					}
					if (count($csv)) {
						$this->messageSend($this->messageBlob(implode(',',$csv), $user, $botid));
					}
					return "Done, I have pinged you the result 1:1 in bulks of $max ($a total items)";
				}
				// SPACE KICK
				elseif($param1 == "kick") {
					if (isset($param2) and !empty($param2)) {
						$persontokick = $param2;
						if (!validateEmail($persontokick)) return "Please type in a valid e-mail";
						$membership_details = $this->membershipGet($roomid, $botid, $persontokick);
						$membership_id = $membership_details['items'][0]['id'];
						$this->membershipDelete($botid, $membership_id);
						return "removed";
					}
					else return "Who to kick? Usage: **space kick user@domain.com**";
				}
				// SPACE CREATE
				elseif($param1 == "create"){
					//Check is the room that the request came from is part of a team (fetch teamId)
					$space_details = $this->roomGetDetails($botid, $roomid);
					$teamid = (isset($space_details['teamId'])) ? $space_details['teamId']:"";
					$time = get_timestamp('UTC') . "UTC - ";
					$title = $time . "Created by " . $user;
					$newspace = $this->roomCreate($botid, $title, $teamid);
					$this->membershipCreate($botid, $newspace['id'], $user);
					
					//If there the parameter 2 exists we expect either a csv of emails or a title
					if (isset($param2) and !empty($param2)){
						$returnvalue = "**Space creation report:**\n\n";
						$error_report = "";
						$a = 0;
						$csv = $param2;
						$emails = explode(",",$csv);
						$emails = array_unique($emails);
						foreach ($emails as $key => $email) {
							if (validateEmail($email)) {
								$memberParsel[$a]['roomtype'] = "roomId";
								$memberParsel[$a]['id'] = $newspace['id'];
								$memberParsel[$a]['method'] = "personEmail";
								$memberParsel[$a]['person'] = $email;
								$a++;
							}
						}
						if (count($memberParsel) > 0) {
							// Start request interval
							return $this->bulkSplitRequestPost("memberships", $memberParsel, $botid, $botr=1);
							// End request interval
						} else {
							$title = $time . implode(array_slice($feature_attributes, 1), ' ');
							sleep(1);
							$this->roomUpdate($botid, $newspace['id'], $title);
						}
					}
					return "New space was created: " . $title;
				}
				elseif ($param1 == "archive" or $param1 == "delete") {
					$space_details = $this->roomGetDetails($botid, $roomid);
					$teamid = (isset($space_details['teamId'])) ? $space_details['teamId']:"";
					if (isset($param2) and !empty($param2)){
						//Force delete
						if ($param2 == "force") {
							//Check if owner
							if ($space_details['creatorId'] == $botid) {
								$this->roomDelete($botid, $roomid);
							} else return "I cannot delete a space by force if I have not created it";
						}
					}
					//If owner of the space, and the space is part of a team - archive it
					if ($space_details['creatorId'] == $botid) {
						if(!empty($teamid)) {
							$this->roomDelete($botid, $roomid);
						} else return "I cannot delete a space that is not part of a team";
					} else return "I cannot delete a space that is not created by me";
				}
				break;
			default:
				return false;
				break;
		}
	}
	
	public function wsIntegrationValidateActivationRequest($jwtarr) {
	    $url = 'https://xapi-a.wbx2.com/jwks';
	    $alg = "ES256";
	    
	    $validationStatus = array(
	        "valid" => False
	    );
	    	    
	    $validationCriteria = array(
	        'app_exists'       => False,
	        'kid_match'        => False,
	        'not_expired'      => False
	    );
	    
	    $jwt_parts = explode('.', $jwtarr['jwt']);
	    
	    $received_signature = $jwt_parts[2];
	    
	    $payload_json = base64_decode($jwt_parts[1]);
	    $headers_json = base64_decode($jwt_parts[0]);
	    
	    $payload = json_decode($payload_json, true);
	    $headers = json_decode($headers_json, true);
	    
	    $details = $this->db->wsIntegrationGet($payload['appId']);
	    
	    $expires = issetor($payload['expiryTime']);
	    $region = issetor($payload['region']);
	    $kid = issetor($headers['kid']);
	    	   	    
	    if (count($details)) {
	        $validationCriteria['app_exists'] = True;
	    }
	    	   	    
	    switch ($region) {
	        case 'us-west-2_r':
	            $url = 'https://xapi-r.wbx2.com/jwks';
	            break;
	        case 'us-east-2_a':
	            $url = 'https://xapi-a.wbx2.com/jwks';
	            break;
	        case 'eu-central-1_k':
	            $url = 'https://xapi-k.wbx2.com/jwks';
	            break;
	    }
	    
	    $keys = $this->data_get(array(), $this->build_spark_headers("NULL", "GET"), $url);
	    	    
	    foreach ($keys['keys'] as $k=>$v) {
	        if ($kid == $v['kid']) {
	            $validationCriteria['kid_match'] = True;
	            break;
	        }
	    }
	    	    
	    if (strtotime(get_timestamp()) > strtotime($expired)) {
	        $validationCriteria['not_expired'] = True;
	    }
	    
	    $test = array_filter($validationCriteria);
	    
	    if ($test == $validationCriteria) {
	        $validationStatus['valid'] = True;
	    }
	    
	    $validationStatus['checks'] = $validationCriteria;
	    	    
	    return $validationStatus;
	    
	}
	
	public function wsIntegrationGetAccessToken($payload, $url) { 
	    $options = $this->build_spark_headers("NULL", "POST", $payload, "integration");
	    
	    return $this->data_post($options, $url);
	}
	
	public function wsIntegrationGetAppManifest($id) {
	    $auth = $this->db->wsIntegrationGetValue($id, 'access_token');
	    $jwt = $this->db->wsIntegrationJwtInfo($id);
	    $appUrl = $jwt['payload']['appUrl'];
	    $options = $this->build_spark_headers($auth, "GET");
	    
	    return $this->data_get(array(), $options, $appUrl);
	}
	
	public function wsIntegrationGetWebexServerManifest($id) {
	    $auth = $this->db->wsIntegrationGetValue($id, 'access_token');
	    $jwt = $this->db->wsIntegrationJwtInfo($id);
	    $appUrl = $jwt['payload']['appUrl'];
	    $manifestUrl = str_replace("apps", "appManifests", $appUrl);
	    $options = $this->build_spark_headers($auth, "GET");
	    	    
	    return $this->data_get(array(), $options, $manifestUrl);
	}
	
	public function wsIntegrationActivateIntegration($payload, $appurl, $access_token) {
	    $options = $this->build_spark_headers($access_token, "PATCH", $payload);
	    
	    return $this->data_post($options, $appurl, False);
	}
	
	public function wsIntegrationGetUserInfo($appId) {
	    $url = $this->getApiUrl("people").'/me';
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    return $this->data_get(array(), $this->build_spark_headers($details['integration']['access_token'], "GET"), $url);
	}
	
	public function wsIntegrationCheckExpiredAndRefresh($appId) {
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    
	    if (isset($details['token']['at']['expired']) and $details['token']['at']['expired']) {
	        $this->wsIntegrationRefreshAccessToken($appId);
	    }
	    
	}
	
	public function wsIntegrationPatchIntegration($appId, $payload) {
	    $this->wsIntegrationCheckExpiredAndRefresh($appId);
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    
	    $appUrl = $details['jwt']['payload']['appUrl'];
	    
	    $options = $this->build_spark_headers($details['integration']['access_token'], "PATCH", $payload);
	    
	    return $this->data_post($options, $appUrl, False);
	}
	
	//DIGITAL SIGNAGE UPDATE
	
	public function wsIntegrationUpdateSignageConfig($appId, $config) {
	    $this->wsIntegrationCheckExpiredAndRefresh($appId);
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    
	    $signageUrl = issetor($details['jwt']['payload']['appUrl']).'/digitalSignage';
	    
	    if ($signageUrl) {
	        $payload = array(
	            "signageUrl"=>$config['signage_base_url']
	        );
	      
	        if (validateUrl($config['signage_content_url'])) {
	            $payload['crossLaunch'] = array(
	                "manageContent" => array("url" => $config['signage_content_url'])
	            );
	        }
	        
	        if (validateUrl($config['signage_assign_url'])) {
	            
	            $assignUrl = array("url" => $config['signage_assign_url']);
	            
	            $assign = array(
	                "assignContent" => $assignUrl
	            );
	            
	            if (!isset($payload['crossLaunch'])) {
	                $payload['crossLaunch'] = $assign; 
	            } else {
	                $payload['crossLaunch']['assignContent'] = $assignUrl;
	            }
	            
	        }
	       
	    }
	    	        	        
	    $options = $this->build_spark_headers($details['integration']['access_token'], "PUT", $payload);
	        
	    return $this->data_post($options, $signageUrl, False);
	    
	}
	
	public function wsIntegrationDeleteSignageConfig($appId) {
	    $this->wsIntegrationCheckExpiredAndRefresh($appId);
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    
	    $signageUrl = issetor($details['jwt']['payload']['appUrl']).'/digitalSignage';
	    
	    $options = $this->build_spark_headers($details['integration']['access_token'], "DELETE");
	    
	    return $this->data_post($options, $signageUrl, False);
	    
	}
	
	//DIGITAL SIGNAGE UPDATE\\
	
	//PERSISTENT WEB APP UPDATE
	
	public function wsIntegrationUpdatePWAConfig($appId, $config) {
	    $this->wsIntegrationCheckExpiredAndRefresh($appId);
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    
	    $signageUrl = issetor($details['jwt']['payload']['appUrl']).'/pwa';
	    	    
	    if ($signageUrl) {
	        $payload = array(
	            "pwaUrl"=>$config['pwa_base_url']
	        );
	        
	        
	        if (validateUrl($config['pwa_content_url'])) {
	            $payload['crossLaunch'] = array(
	                "manageContent" => array("url" => $config['pwa_content_url'])
	            );
	        }
	        
	        if (validateUrl($config['pwa_assign_url'])) {
	            
	            $assignUrl = array("url" => $config['pwa_assign_url']);
	            
	            $assign = array(
	                "assignContent" => $assignUrl
	            );
	            
	            if (!isset($payload['crossLaunch'])) {
	                $payload['crossLaunch'] = $assign;
	            } else {
	                $payload['crossLaunch']['assignContent'] = $assignUrl;
	            }
	        }
	        
	    }
	   	    
	    $options = $this->build_spark_headers($details['integration']['access_token'], "PUT", $payload);
	    
	    return $this->data_post($options, $signageUrl, False);
	    
	}
	
	public function wsIntegrationDeletePWAConfig($appId) {
	    $this->wsIntegrationCheckExpiredAndRefresh($appId);
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    
	    $signageUrl = issetor($details['jwt']['payload']['appUrl']).'/pwa';
	    
	    $options = $this->build_spark_headers($details['integration']['access_token'], "DELETE");
	    
	    return $this->data_post($options, $signageUrl, False);
	    
	}
	
	//PERSISTENT WEB APP UPDATE\\
	
	public function wsIntegrationRefreshAccessToken($appId) {
	    $status = array(
	        'true' => "Access Token was refreshed successfully",
	        'false' => "Access Token was not refreshed"
	    );
	    $status_code = "false";
	    $details = $this->db->wsIntegrationInfoAPI($appId);
	    $url = $this->getApiUrl("integrationtoken");
	    $payload = array(
	        "grant_type" => "refresh_token",
	        "client_id" => $details['integration']['client'],
	        "client_secret" => $details['integration']['secret'],
	        "refresh_token" => $details['integration']['refresh_token']
	    );
	    
	    $options = $this->build_spark_headers("NULL", "POST", $payload, "integration");
	    
	    $result = $this->data_post($options, $url);
	    
	    if (isset($result['access_token'])) {
	        $update = array(
	            'access_token' => $result['access_token'],
	            'refresh_token' => $result['refresh_token'],
	            'expires_in' => $result['expires_in'],
	            'refresh_token_expires_in' => $result['refresh_token_expires_in'],
	            'token_timestamp' => get_timestamp()
	        );
	        
	        if ($this->db->wsIntegrationUpdate($appId, $update)) {
	            $status_code = "true";
	        }
	        
	    }
	    
	    $this->db->wsIntegrationUpdate($appId, array('status'=>$status[$status_code]));
	    
	    return $result;
	    
	}	
	
	public function integrationAuth($client_id, $client_secret, $code, $redirect_url) {
		$url = $this->getApiUrl("integrationtoken");
		$data = array("grant_type" => "authorization_code",
				"client_id" => $client_id,
				"client_secret" => $client_secret,
				"code" => $code,
				"redirect_uri" => $redirect_url
		);
		$options = $this->build_spark_headers("NULL", "POST", $data, "integration");
		
		return $this->data_post($options, $url);
	}
	//Refresh the current accesstoken attached to the user.
	public function integrationRefresh($userid) {
		$tokendata = $this->db->integrationFetchToken($userid);
		
		$integrationsettings = $this->db->integrationFetchSettings();
		
		$url = $this->getApiUrl("integrationtoken");
		
		$data = array(
		    "grant_type" => "refresh_token",
			"client_id" => $integrationsettings['client_id'],
			"client_secret" => $integrationsettings['client_secret'],
			"refresh_token" => $tokendata['refresh_token']
		);
		
		$options = $this->build_spark_headers("NULL", "POST", $data, "integration");
		$result = $this->data_post($options, $url);
		$result = $this->db->integrationFilterTokenResult($result);
		
		if (issetor($result['access_token']) and issetor($result['refresh_token'])) {
		    
			if ($this->db->integrationUpdateToken($result, $userid)) {
			    
				$this->db->integrationDeleteRepresentation($userid);
				$this->integrationCreateBotRepresentation($userid);
				
				return true;
				
			}
			
		} 
		
		return false;
		
	}
	
	public function wsIntegrationCreateBotRepresentation($appId) {
	    global $config;
	    if ($this->db->wsIntegrationExists($appId)) {
	        $representation = $this->peopleGetMe($appId);
	        $representation['id'] = $appId;
	        $representation['type'] = "ws_integration_machine";
	        if (issetor($representation['avatar'])) {
	            $this->download_image($representation['avatar'], $config['base_dir'].'/public/images/bots/'.$appId.'.jfif');
	        }
	        $this->db->integrationDeleteRepresentation($appId);
	        $this->db->wizardAddBot($representation);
	    }
	}
	
	public function integrationCreateBotRepresentation($userid) {
		global $config;
		if ($this->db->integrationAccessTokenExists($userid)) {
			$tokendata = $this->db->integrationAPIGetTokenData($userid);
			$representation = $this->peopleGetMe($userid);
			$representation['id'] = $userid;
			if (issetor($representation['avatar'])) {
				$this->download_image($representation['avatar'], $config['base_dir'].'/public/images/bots/'.$userid.'.jfif');
			}
			$this->db->integrationDeleteRepresentation($userid);
			$this->db->wizardAddBot($representation);
		}
	}
	
	public function integrationGenAuthUrl($userid, $scopes='') {
		$settings = $this->db->integrationFetchSettings();
		$authUrl = $this->getApiUrl('integrationauth');
		$redirect_uri = urlencode($settings['redirect_url']);
		$scope = (!$scopes) ? rawurlencode($settings['default_scopes']):rawurlencode($scopes);
		
		$parameters = "client_id={$settings['client_id']}&response_type=code&redirect_uri=$redirect_uri&scope=$scope&state=$userid";
		
		return "{$authUrl}?{$parameters}";
	}
	
	public function data_post($options, $api_url, $json=True) {
		$curl = curl_init();
		$options[CURLOPT_URL] = $api_url;
		curl_setopt_array($curl, $options);
		$result = curl_exec($curl);
		
		
		
		if($this->db->debugIsEnabled() and $json == True) {
			$this->db->debugLog(array(
					'type'=>'data_post',
					'request'=>json_encode($options),
					'response'=>$result,
					'description'=>'function_data_post_log'
			));
		}
		if ($json) {
		    return json_decode($result, true);
		} else {
		    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		    return array(
		        'result'=>$result,
		        'http_code'=>$httpcode
		    ); 
		}
	}
	
	public function data_multi_post($data) {
		//array(array('url'=>'', 'method'=>'', 'type'=>'', 'auth'=>'', 'post'=>array()))
		$curl = array();
		$result = array();
		$payload = array();
		$execution = null;
		$mh = curl_multi_init();
		
		foreach ($data as $id => $d) {
			$curl[$id] = curl_init();
			$options = $this->build_spark_headers($d['auth'], $d['method'], $d['post'], $d['type']);
			$payload[$id] = $d['post'];
			$options[CURLOPT_URL] = $d['url'];
			$options[CURLINFO_HEADER_OUT] = true;
			curl_setopt_array($curl[$id], $options);
			curl_multi_add_handle($mh, $curl[$id]);
		}
		
		do {
			curl_multi_exec($mh, $execution);
		} while($execution > 0);
		
		foreach($curl as $id => $c) {
			$result[$id]['response'] = json_decode(curl_multi_getcontent($c));
			$result[$id]['http_code'] = curl_getinfo($c, CURLINFO_HTTP_CODE);
			$result[$id]['outgoing_info'] = $payload[$id];
			curl_multi_remove_handle($mh, $c);
		}
		
		curl_multi_close($mh);
		return $result;
	}
	
	public function data_multi_get($data) {
		//array(array('url'=>'', 'method'=>'', 'type'=>'', 'auth'=>'', 'post'=>array()))
		$curl = array();
		$result = array();
		$request = array();
		$execution = null;
		$mh = curl_multi_init();
		
		foreach ($data as $id => $d) {
			$curl[$id] = curl_init();
			$options = $this->build_spark_headers($d['auth'], $d['method']);
			$payload[$id] = $d['get'];
			$options[CURLOPT_URL] = $d['url'].'?'.http_build_query($d['get']);
			$options[CURLINFO_HEADER_OUT] = true;
			curl_setopt_array($curl[$id], $options);
			curl_multi_add_handle($mh, $curl[$id]);
		}
		
		do {
			curl_multi_exec($mh, $execution);
		} while($execution > 0);
		
		foreach($curl as $id => $c) {
			$result[$id]['response'] = json_decode(curl_multi_getcontent($c),true);
			$result[$id]['http_code'] = curl_getinfo($c, CURLINFO_HTTP_CODE);
			$result[$id]['outgoing_info'] = $payload[$id];
			curl_multi_remove_handle($mh, $c);
		}
		curl_multi_close($mh);
		return $result;
	}
	//Check for errors in a multi_post_request and return result
	public function check_multi_error($result) {
		$failed = array();
		$error_html = "";
		foreach ($result as $key => $r) {
			if ($r['http_code'] == '200') {
				//skip
			} else {
				$failed[$key]['http_code'] = $r['http_code'];
				$failed[$key]['response'] = json_encode($r['response']);
				$failed[$key]['outgoing_info'] = $r['outgoing_info'];
			}
		}
		$total_count = count($result);
		$error_count = count($failed);
		$success_count = $total_count - $error_count;
		$success_code = ($success_count < $total_count or $total_count == 0) ? ($success_count == 0) ? "alert" : "warning" : "success";
		if ($error_count > 0) {
			$error_html = "<b>Errors:</b><br><table><tr><td><b/>Response id<td><b/>Response code<td><b/>Server response<td><b/>My payload";
			foreach ($failed as $key => $value) {
				$my_payload = json_encode($value['outgoing_info']);
				$error_html .= "<tr><td>$key<td>{$value['http_code']}<td>{$value['response']}<td>{$my_payload}";
			}
			$error_html .= "</table>";
		}
		$report = array(
				'total' => $total_count,
				'errors' => $error_count,
				'success' => $success_count,
				'success_code' => $success_code,
				'error_report' => $failed,
				'error_html' => $error_html
		);
		return $report;
	}
	
	public function data_get($data, $options, $api_url){
		$curl = curl_init();
		$options[CURLOPT_URL] = $api_url . '?' . http_build_query($data);
		curl_setopt_array($curl, $options);	
		$result = json_decode(curl_exec($curl),true);
		if($this->db->debugIsEnabled()) {
			$this->db->debugLog(array(
					'type'=>'data_get',
					'request'=>json_encode($options),
					'response'=>json_encode($result),
					'description'=>'function_data_get_log'
			));
		}
		
		return $result;
	}
}

class OutputEngine {
	
	protected $db;
	
	public function __construct() {
		$this->db = new dB();
	}
	public function prettyPrint($data, $type=''){
		$returnvalue = "<hr><table class='rounded'>";
		if ($type == '') {
			foreach ($data as $key=>$value){
				$returnvalue .= (($key == 'emails') or ($key == 'mentionedPeople')) ? "<tr><td><b>$key:</b><td>$value[0]":"<tr><td><b>$key:</b><td>$value";
			}
		} else if ($type == 'messages') {
			foreach ($data as $key=>$value){
				if ($key == 'toPersonId') {
					$name = $this->db->contactGetName($value);
					$returnvalue .= "<tr><td><b>$key:</b><td>$name";
				}
			}
		}
		
		$returnvalue .= "</table>";
		return $returnvalue;
	}
	//Function for the message feature to display a short message with the remaining messages
	public function shortMessageOutput($name, $remaining, $type){
		$returnvalue = "<hr><table>";
		$returnvalue .= "<tr><td><b>Sent message to $name!</b><td> Remaining $type messages: $remaining";
		$returnvalue .= "</table>";
		return $returnvalue;
	}
	public function responseGenResponses($botid) {
		global $link_confirm;
		global $neg_color;
		$responses = $this->db->responseFetchResponses($botid);
		$returnvalue = "<table width='100%' id='tasks' class='table table-bordered table-striped'><tr><td><b>KEYWORDS</b><td><b>RESPONSES</b><td><b>FILE URL</b><td><b>TASK</b><td><b>FEATURE</b><td><b>PROTECTED</b><tdcolspan=2><b>MODIFY</b>";
		foreach ($responses as $key => $value) {
			if ($value['accessgroup'] != 0) {
				$group = $this->db->groupFetchGroups($value['accessgroup']);
				$group_title = (issetor($group[0]['groupname'])) ? $group[0]['groupname']:"404";
				$location = "index.php?id=groups&viewgroup={$value['accessgroup']}";
			}
			else {
				$group_title = "Everyone";
				$location = "#";
			}
			$is_feature = onoff($value['is_feature'],"Feature","Not a Feature");
			$is_task = onoff($value['is_task'],"Task","Not a Task");
			$response = substr($value['response'],0,20) . '...';
			$response = str_replace("<", "", str_replace(">", "", $response));
			$accessgroup = ($group_title == "404") ? warning("This response is protected but the accessgroup does NOT EXIST! Please update the access group for this response") : "<a title='{$group_title}' href='{$location}'>".onoff($value['accessgroup'],"Restricted to {$group_title}","Unrestricted")."</a>";
			$returnvalue .= "<tr><td> <b>{$value['keyword']}</b><td>$response<td>{$value['file_url']}<td>$is_task<td>$is_feature<td>{$accessgroup}<td width='31'><a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=response&edit={$value['id']}'>".actionButton("edit", "Edit")."</a> <td> <a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=response&delete={$value['id']}' {$link_confirm}>".actionButton("delete", "Delete")."</a>";
		}
		$returnvalue .= "</table>";
		
		return $returnvalue;
	}
	public function teamGenSpaces($botid, $data) {
		global $neg_color;
		$returnvalue = "";
		foreach ($data['items'] as $key => $space) {
			$returnvalue .= "<a href='index.php?id=spaces&botid=$botid&viewspace={$space['id']}&spacetype=group'>{$space['title']}</a>";
		}
		return $returnvalue;
	}
	public function roomGenJoinable($botid) {
		global $link_confirm;
		$spaces = $this->db->adminGetJoinableSpace($botid);
		$returnvalue = "<table class='rounded'><tr><td bgcolor='737CA1'><b>Joinable spaces</b><td bgcolor='737CA1'><b>Remove</b><tr>";
		foreach ($spaces as $key => $value) {
			$returnvalue .= "<tr><td>{$value['spacetitle']}<td><a href='index.php?id=administration&sub=spaceoptions&botid=$botid&removespace={$value['spaceid']}'>Remove</a>";
		}
		$returnvalue .= "</table>";
		
		return $returnvalue;
	}
	public function roomGenCheckbox($data, $name, $botid) {
		$options = "";
		foreach ($data['items'] as $key => $value){
			//$options .= "<input type='checkbox' name='{$name}' value='{$value['id']}'> <a href='index.php?id=spaces&botid=$botid&viewspace={$value['id']}&spacetype=group'>{$value['title']}</a><br>";
			$options .= "<option value='{$value['id']}'>{$value['title']}</option>";
		}
		return $options;
	}
	public function roomGenLinks($data, $botid, $type='') {
		$spaces = "";
		$teamid = "";
		$i = 1;
		
		if(!empty($data['items']))
		{
			$spaces = "<table id='spacelist' class='table table-bordered table-striped' width='100%'>
			<thead>
			<tr>
			<th>Created</th>
			<th>Space name</th>
			<th>Type</th>
			</tr>
			</thead>
			<tbody>";
			foreach ($data['items'] as $key => $value){
				if ($type != "") {
					$value['type'] = $type;
					$teamid = "&teamid={$value['teamId']}";
				}
				$spacet = (issetor($value['teamId'])) ? "teamspace":$value['type']; 
				$spaces .= "<tr><td>{$value['created']}<td><a href='index.php?id=spaces&botid=$botid&viewspace={$value['id']}$teamid&spacetype={$value['type']}' title='Click to see details'>{$value['title']}</a><td>{$spacet}";
				$i++;
			}
			$spaces .= "</tbody></table>";
		}
		else
		{
			echo 'No direct chats or groups available for this bot';
		}
		
		return $spaces;
	}
	public function roomGenMembership($botid, $data, $spacetype, $teamid="", $mod="") {
		global $neg_color;
		global $infocolor;
		$mailcsv = $personcsv = "";
		$remove = "";
		$returnvalue = "
							<table class='table' width='100%'>";
		foreach ($data['items'] as $key => $member) {
			//Check if moderator
			$ismod = ($member['isModerator']) ? true:false;
			//Set title on modlight
			$modtitleTrue = "(isModerator:True) - Click to Demote Moderator";
			$modtitleFalse = "(isModerator:False) - Click to Promote Moderator";
			//Set modlight
			$modlight = "Promote to moderator";
			if ($ismod) {
				$modlight = "Remove moderator role";
			}
			//Gather emails for CSV
			$mailcsv .= $member['personEmail'].', ';
			$personcsv .=  $member['personId'].', ';
			//If team, set teamid
			$teamid_link = ($teamid != "") ? "teamid=$teamid&":"";
			if($spacetype == "direct") {
				$modlink = $remove = "";
			}
			else {
				$modlink = ($ismod) ? 	"<a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&unsetmod={$member['id']}' title='$modtitleTrue' class='btn btn-success'>$modlight</a>" :
				"<a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&setmod={$member['id']}' title='$modtitleFalse' class='btn btn-warning'>$modlight</a>";
				$remove = "<a href='index.php?id=spaces&botid=$botid&viewspace={$member['roomId']}&{$teamid_link}spacetype=$spacetype&membershipdelete={$member['id']}' title='Remove membership' class='btn btn-danger'>Remove from space</a>";
			} 
			
			$returnvalue .= "<tr>
			<td>
			<b><font color='$infocolor'>{$member['personDisplayName']}</font></b><br>({$member['personEmail']})
			<td>
			<ul>
			$remove $modlink";
		}
		$returnvalue .= "<tr>
		<td>
		CSV member emails:
		<td>
		$mailcsv
        <tr>
        <td>CSV PersonId:
        <td>
        $personcsv
		</table>";
		return $returnvalue;
	}
	public function teamGenLinks($data, $botid) {
		$teams = "<table id='spacelist' class='table table-bordered table-striped' width='100%'>
			<thead>
			<tr>
			<th>Created</th>
			<th>Space name</th>
			<th>Type</th>
			</tr>
			</thead>
			<tbody>";
		$i = 1;
		if(!empty($data['items']))
		{
			foreach ($data['items'] as $key => $value){
				print_r($value);
				$spacet = "team";
				$teams .= "<tr><td>{$value['created']}<td><a href='index.php?id=spaces&teamspaces&botid=$botid&teamid={$value['id']}&spacetype=teamspaces' title='{$value['name']}'>{$value['name']}</a><td>{$spacet}";
				$i++;
			}
			$teams .= "</tbody></table>";
		}
		else
		{
			echo 'No Team Spaces available for this bot';
		}
		return $teams;
	}
	public function contactGenProfile($contactid, $type=''){
		global $link_confirm;
		global $neg_color;
		$mainbot = $this->db->botGetMainInfo();
		$botid = $mainbot['id'];
		$contact = $this->db->contactFetchContacts($contactid);
		$groups = $this->db->groupFetchGroups();
		$name = strtoupper($this->db->contactGetName($contactid));
		$groups_member = $groups_not_member = $memberships = "";
		if ($type == 'listmemberships') {
			foreach ($groups as $key => $value) {
				if ($this->db->groupCheckIfMember($contact[0]['id'], $value['id'])) {
					$groups_member .= $this->groupLinks($format="groupremove", $value['id'], $contact[0]['id']);
				}
				else {
					$groups_not_member .= $this->groupLinks($format="groupadd", $value['id'],$contact[0]['id']);
				}
			}
			$memberships = "<tr> <td> Member of groups: <td> <b>{$groups_member}</b> <hr>
			<tr> <td> Available groups: <td> {$groups_not_member} </td> </tr>";
		}
		$image = (!empty($contact[0]['avatar'])) ? $contact[0]['avatar'] : "images/static/noimagefound.jpeg";
		$groupresponses = $this->db->contactGetGroupResponseAcl($contactid);
		
		$html = "
		<div class='row'>
			<div class='col-md-6'>
				<!-- Widget: user widget style 2 -->
				<div class='card card-widget widget-user-2'>
					<!-- Add the bg color to the header using any of the bg-* classes -->
						<div class='widget-user-header wbm-ciscoblue'>
							<div class='widget-user-image'>
								<img class='img-circle elevation-2' src={$image} alt='User Avatar' style='background-color: #FFFFFF'>
							</div>
							<!-- /.widget-user-image -->
							<h3 class='widget-user-username'>{$contact[0]['firstName']} {$contact[0]['lastName']}</h3>
							<h5 class='widget-user-desc'>{$contact[0]['emails']}</h5>
						</div>
						<div class='card-footer p-0 bg-white'>
							<table class='table'>
							<tr>
								<th>Webex Teams ID:</th>
								<td>{$contact[0]['id']}</td>
							</tr>
							<tr>
								<th>Member of groups:</th>
								<td>{$groups_member}</td>
							</tr>
							<tr>
								<th>Available groups:</th>
								<td>{$groups_not_member}</td>
							</tr>
							<tr>
								<th>Group response access:</th>
								<td>{$groupresponses}</td>
							</tr>
						</table>
						</div>
					</div>
          <!-- /.widget-user -->
		</div>
		";
		
		return $html;
	}
	public function contactGenUserList(){
		global $link_confirm;
		$contact = $this->db->contactFetchContacts();
		$num_contacts = count($contact);
		$html = "<form action='index.php?id=contacts' onsubmit='loading(\"process\", \"Please wait while updating...\")' method='post' enctype='multipart/form-data'>
		<table width='100%' id='tasks' class='table table-bordered table-striped'><thead><tr><th rowspan='2'><input type='checkbox' onClick=\"toggle(this,'userlist[]')\">Select all<br> (per page)</th><th colspan='4'><input type='submit' name='bulk_update' class='btn btn-md btn-primary' value='Update'><input type='submit' class='btn btn-md btn-danger pull-right' name='bulk_delete' value='Delete'></tr><tr><th>Entry</th><th>Full Name</th><th>Email</th></tr></thead><tbody>";
		$i = 1;
		foreach ($contact as $key => $value) {
			$headline = strtoupper($value['firstName'] . " " . $value['lastName']);
			$name = (trim($headline) == "") ? $value['emails']:$headline;
			$image = (!empty($value['avatar'])) ? $value['avatar'] : "images/static/noimagefound.jpeg";
			$html .= "<tr><td><input type='checkbox' name='userlist[]' value='{$value['id']}'></td><td><b>{$i}.</b></td>
			<td> <a class='linkblock' href='index.php?id=contacts&contactid={$value['id']}'><b>{$name}</b></a></td>
			<td> {$value['emails']} </td>
			";
			$i++;
		}
		$html .= "</tbody></table></form>";
		return $html;
	}
	public function messageConversation($data, $botid) {
		global $link_confirm;
		global $neg_color;
		global $pos_color;
		$botinfo = $this->db->botFetchBots($botid);
		$botmail = $botinfo[0]['emails'];
		$botname = $botinfo[0]['displayName'];
		$returnvalue = "";
		
		$number_of_items = (isset($data['items'])) ? count($data['items']) : 0;
		if ($number_of_items > 0) {
			for ($i=0; $i<$number_of_items; $i++) {
				$created = $data['items'][$i]['created'];
				if ($data['items'][$i]['personEmail'] == $botmail) {
					$returnvalue .= "<div class='card card-outline card-success'>
							   		<div class='card-header'>
										<img src='{$botinfo[0]['avatar']}' class='img-circle' height='20' width='20'> $botname $created
									</div>
									<div class='card-body'>
										{$data['items'][$i]['text']}
									</div>
									<a href='index.php?id=spaces&botid={$botid}&messagedelete={$data['items'][$i]['id']}&viewspace={$data['items'][$i]['roomId']}&spacetype=group' {$link_confirm} class='btn btn-danger pull-right'>Delete message</a>
							  </div>";
				}
				else {
					$name = $this->db->contactGetNameLink($data['items'][$i]['personEmail'], $avatar=True);
					$returnvalue .= "<div class='card card-outline card-primary'>
										<div class='card-header'>
											$name $created
										</div>
										<div class='card-body'>
										{$data['items'][$i]['text']}
										</div>
									</div>
										
									";
					
				}
			}
		}
		$returnvalue .= "Number of messages found: " . $number_of_items;
		return $returnvalue;
	}
	public function userSearch($data) {
		global $gradrul;
		if (isset($data['items']) and count($data['items'])) {
			$returnvalue = "
				<hr>
					<div id='input'>
						<table width='100%'>
							<tr>
								<td align='center'>Avatar<td align='center'>Display name<td>Options";
			foreach ($data['items'] as $key => $value) {
				$image = (issetor($value['avatar'])) ? $value['avatar'] : "images/static/noimagefound.jpeg";
				$contact_link = (count($this->db->contactFetchContacts($value['id']))) ?
				"<input name='update_contact' class='btn btn-md btn-primary' value='Update user' type='submit'> <input name='delete_contact' class='btn btn-md btn-danger' value='Delete' type='submit'>" :
				"<input name='add_contact' class='btn btn-md btn-primary' value='Add user' type='submit'>";
				$returnvalue .= "
				<tr>
				<td align='center'>
				<a href='{$value['avatar']}'><img class='img-circle img-thumbnail' src='$image' width='50' height='50'></a>
				<td align='center'>
				<b>{$value['displayName']}</b> ({$value['emails'][0]})
				<td><form id='searchresults' method='post' action='#' enctype='multipart/form-data'>
				<div id='mininav'>
				$contact_link
				<input type='hidden' name='contactid' value='{$value['id']}'>
				</div></form>";
			}
			$returnvalue .= "</table></div>";
			return $returnvalue;
		} else return "<hr>No results";
		
	}

	public function pretty($data, $type="", $botid="") {
		global $link_confirm;
		global $neg_color;
		global $pos_color;
		
		$returnvalue = "";
		
		$number_of_items = count($data['items']);
		$memberlist = array('0'=>'');
		if ($number_of_items > 0) {
			for ($i=0; $i<$number_of_items; $i++) {
				switch ($type) {
					//Profile is used to pretty print json from the people API in Spark
					case 'profile':
						$image = (issetor($data['items'][$i]['avatar'])) ? $data['items'][$i]['avatar'] :
						"images/static/noimagefound.jpeg";
						$contact_link = (count($this->db->contactFetchContacts($data['items'][$i]['id']))) ? "<b><a class='linkblock linkblock-border' href='index.php?id=contacts&botid=$botid&update_contact={$data['items'][$i]['emails'][0]}'>Update user</a><a class='linkblock linkblock-cancel' href='index.php?id=contacts&botid=$botid&delete_contact={$data['items'][$i]['id']}' {$link_confirm}>Delete user</a>" :
						"<a class='linkblock linkblock-border' href='index.php?id=contacts&botid=$botid&add_contact={$data['items'][$i]['emails'][0]}'>Add user</a>";
						$returnvalue .= "
						<div id='subnav' class='wide-content'> <table>
						<tr> <td class='tdStyle'><a href='{$data['items'][$i]['avatar']}'><img class='img-circle' src='$image' width='50' height='50'></a> <td class='tdStyle-nolist'>
						<b>{$data['items'][$i]['displayName']}</b>
						<tr>
						<td>Options:
						<td>
						<div id='mininav'>
						$contact_link
						</div>";
						break;
					case 'hooks':
						$webhookid = $data['items'][$i]['id'];
						$sync = $this->db->adminCheckWebhookExists($webhookid, $botid);
						
						$resyncmsg = $resync = "";
						
						if (!$sync) {
							$resync = "<input type='submit' value='Re-sync webhook' class='btn btn-md btn-warning' name='resync'>";
							
						}
						$exists = onoff($sync);
						$disable = ($sync) ? "":"disabled";
						$accessgroup = $this->db->webhookGetAccessGroup($webhookid);
						$groupoptions = $this->groupOptions($accessgroup);
						$resyncmsg = ($sync) ? "This webhook is in sync!":"This webhook is not in sync with the database and triggers will not work. Please re-sync";
						
						$returnvalue .= "
						<form action='#$webhookid' id='$webhookid' method='post' enctype='multipart/form-data'>
							<div class='row' style='padding-top: 1%;'>
								<div class='col-md-1'>
									<input type='submit' $disable value='Set accessgroup' class='btn btn-md btn-primary' name='webhook_setaccess'>
									<input type='hidden' value='$webhookid' name='webhookid'>
								</div>
								<div class='col-md-3'>
									<select $disable name='whgroup' class='form-control'>
										<option value='0'>No access group selected</option>
										$groupoptions
									</select>
								</div>
							</div>
							<div class='row' style='padding-top: 1%;'>
								<div class='col-md-1'>
									<input type='submit' value='Delete webhook' class='btn btn-md btn-danger' name='delete_webhook' {$link_confirm} class='cancel'>
								</div>
								<div class='col-md-3'>
									 $resync
								</div>
							</div>
						</form>
						<br>
						Database sync:  $exists $resyncmsg
						<br>
						";
						break;
					case 'member':
						$returnvalue .= "
						<div id='subnav'>
						<table class='rounded'><tr><td class='tdStyle'>Options: <td> <a href='index.php?id=spaces&botid={$botid}&membershipdelete={$data['items'][$i]['id']}&viewspace={$data['items'][$i]['roomId']}&spacetype=group' {$link_confirm}>Delete membership</a>
						";
						break;
					case 'listmsg':
						$returnvalue .= "
						<div id='subnav'>
						<table class='rounded'><tr><td class='tdStyle'>Options: <td> <a href='index.php?id=spaces&botid={$botid}&messagedelete={$data['items'][$i]['id']}&viewspace={$data['items'][$i]['roomId']}&spacetype=group' {$link_confirm}>Delete message</a>
						";
						break;
					case 'spark':
						$a = $i+1;
						$returnvalue .= "**Lookup result number: {$a}**\n\n";
						break;
						//List rooms in pretty form, including a differentiator between groups and direct.
					case 'rooms':
						$returnvalue .= "
								<div id='subnav'>
									<table class='rounded'><tr><td class='tdStyle'>Options: <td>";
						//If the room is a group, give option to leave room, enable group response for all users or specific user
						if($data['items'][$i]['type'] == "group") {
							$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&delete={$data['items'][$i]['id']}' {$link_confirm}>Leave Room</a> - ";
							$response_changer = ($this->db->adminCheckGroupResponseAcl($botid, $data['items'][$i]['id'])) ? "disable" : "enable";
							$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&group_response={$response_changer}&spaceId={$data['items'][$i]['id']}'>{$response_changer} group response</a>";
							if ($response_changer == "enable") {
								$returnvalue .= "<form method='post' action='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&add_special_access={$data['items'][$i]['id']}' enctype='multipart/form-data'>
								<input type='text' placeholder='email' required='' name='user'> <input type='submit' name='add_access' value='Grant group response'>
								</form>";
							}
						}
						elseif($data['items'][$i]['type'] == "direct")
						{
							$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=rooms&list_messages={$data['items'][$i]['id']}'>Get messages</a> - ";
						}
						$returnvalue .= "<a href='index.php?id=bots&sub=profile&botid={$botid}&apiq=messages&recepient={$data['items'][$i]['id']}&recepientType=roomId'>Send message</a>";
						break;
					default:
						$returnvalue .= "
									<table class='smallform'>
									";
						break;
				}
				foreach($data['items'][$i] as $key => $value) {
					if ($type == "spark")  {
						$expanson = "-";
						if ($key == 'emails') {
							if ($value[0] == "mohm@cisco.com") {
								$returnvalue .= "**My beloved almighty creator <3**\n\n";
							}
							$returnvalue .= "{$expanson} **E-mail**: {$value[0]}\n";
						}
						elseif ($key == 'displayName') {
							$returnvalue .= "{$expanson} **Name**: {$value}\n";
						}
						elseif ($key == 'status') {
							$returnvalue .= "{$expanson} **Status**: {$value}\n";
						}
						elseif ($key == 'lastActivity') {
							$returnvalue .= "{$expanson} **Last seen active**: {$value}\n";
						}
						elseif ($key == 'avatar') {
							$returnvalue .= "{$expanson} **Image Avatar**: [Avatar]({$value})\n";
						}
						elseif ($key == 'orgId') {
							$returnvalue .= "{$expanson} **OrgId**: {$value}\n";
						}
						elseif ($key == 'type') {
							$returnvalue .= "{$expanson} **User type**: {$value}\n";
						}
					}
					elseif ($type == "member"){
						if ($key == 'personEmail' or $key == 'personDisplayName'){
							$returnvalue .= "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value}</li> ";
							if ($key == 'personEmail') {
								$memberlist[0] .= $value . ",<br>";
							}
						}
						
					}
					elseif ($type == "listmsg"){
						if ($key == 'text' or $key == 'created' or $key == 'personEmail'){
							$returnvalue .= "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value}</li> ";
						}
						elseif ($key == 'files'){
							$returnvalue .= "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value[0]}</li>";
						}
					}
					else {
						if ($key == 'phoneNumbers') continue;
						$returnvalue .= (($key == 'emails') or ($key == 'mentionedPeople')) ? "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value[0]}</li>" :
						"<div class='col-md-3'>{$key}</div><div class='col-md-9'>{$value}</div>";
						//$returnvalue .= (($key == 'emails') or ($key == 'mentionedPeople')) ? "<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>: {$value[0]}</li>" :
						//"<tr><td class='tdStyle'><li><b>{$key}</b><td class='tdStyle-nolist'><li>21: {$value}</li>";
					}
				}
				$returnvalue .= ($type != "spark") ? "</table>" : "<hr>\n\n";
			}
		}
		else {
			$returnvalue = "Number of results: " . $number_of_items;
		}
		if ((strlen($returnvalue) > 7000) and $type == "spark") {
			$returnvalue = "Too many results to be displayed! Please try again with a more narrow search. Search for name or email.";
		}
		elseif ($number_of_items > 0) {
			$returnvalue .= "Displaying <b>{$i}</b> entries!<br>";
			if ($type == "member") {
				$returnvalue .= "<br><br>";
				$returnvalue .= $memberlist[0];
			}
		}
		return $returnvalue;
	}
	public function feedbackGenTopics($botid, $format="") {
		$topics = $this->db->feedbackFetchTopics($botid);
		$html = "";
		foreach ($topics as $key => $value) {
			$html .= "<a href='index.php?id=feedback&botid=$botid&topic={$value['id']}' class='linkblock'>{$value['title']}</a><br>";
		}
		return $html;
	}
	//Generate topic entries
	public function feedbackGenTopicEntries($botid, $topicid, $format="") {
		$entries = $this->db->feedbackFetchTopicEntries($topicid);
		$html = "<table width='100%' class='table table-bordered table-striped'><tr><td><input type='checkbox' onClick=\"toggle(this, 'entries[]')\"> <b/>Select all<td><b/>ID<td><b/>Text<td><b/>Votes<td><b/>Comments<td><b/>Created by";
		foreach ($entries as $key => $value) {
			$votes_num = count($this->db->feedbackFetchEntryVotes($value['id']));
			$comments_num = count($this->db->feedbackFetchEntryComments($value['id']));
			$desc = (strlen($value['description'])>50) ? substr($value['description'], 0, 50) . "..." : $value['description'];
			$html .= "<tr><td><input type='checkbox' name='entries[]' value='{$value['id']}'><td><b>{$value['id']}</b><td><a href='index.php?id=feedback&botid={$botid}&topic={$topicid}&entry={$value['id']}#entry' class='linkblock'>$desc</a><td>$votes_num<td>$comments_num<td>{$value['created_by']}";
		}
		return $html;
	}
	//Generate topic entry comments
	public function feedbackGenEntryComments($entryid, $format="") {
		$html = "";
		$comments = $this->db->feedbackFetchEntryComments($entryid);
		if (count($comments)) {
			$html = "
				<form method='post' id='comment' action='".formUrl($_GET)."#comment' enctype='multipart/form-data'>
					<div class='form-group'>
						<input type='checkbox' onClick=\"toggle(this, 'comments_selected[]')\"> <b>Select all</b></input>
						<input type='submit' class='btn btn-danger float-right' name='comments_delete' value='Delete selected comments'>
					</div>
				";
			foreach ($comments as $key => $value) {
				$name = $this->db->contactGetNameLink($value['email'], $avatar=True);
				$comment = $value['comment'];
				$html .= "
					<div class='card card-outline card-success'>
						<div class='card-header'>
							<input type='checkbox' name='comments_selected[]' value='{$value['id']}'> $name - {$value['created']}
						</div>
						<div class='card-body'>
							$comment
						</div>
					</div>
				";
			}
			$html .= "</form>";
		}
		return $html;
	}
	//Generate topic entry votes
	public function feedbackGenEntryVotes($entryid, $format="") {
		global $com_color1, $com_color2;
		$votes = $this->db->feedbackFetchEntryVotes($entryid);
		$html = "";
		if (count($votes) > 0) {
			$html .= "
					<form action='".formUrl($_GET)."#del_v' id='del_v' method='post' enctype='multipart/form-data'>
					<div class='form-group'>
						<input type='checkbox' onClick=\"toggle(this, 'votes_selected[]')\"> <b>Select all</b> 
						<input type='submit' class='btn btn-danger float-right' name='votes_delete' value='Delete selected votes'>
					</div>";
			foreach ($votes as $key => $value) {
				$contactinfo = $this->db->contactFetchContacts($value['email']);
				$n = $this->db->contactGetName($value['email']);
				$name = $this->db->contactGetNameLink($value['email'], $avatar=True);
				$html .= "
					<div class='card card-outline card-success'>
						<div class='card-header'>
						<input type='checkbox' name='votes_selected[]' value='{$value['id']}'>$name voted {$value['voted']}<i class='fa fa-check float-right'></i> 
						</div>
					</div>";
			}
		}
		return $html;
	}
	public function groupOptions($groupid="") {
		$html = "";
		$groupinfo = $this->db->groupFetchGroups();
		foreach ($groupinfo as $key => $value) {
			$number = $this->db->groupMembershipNumber($value['id']);
			$selected = ($groupid == $value['id']) ? "selected":"";
			$html .= "<option $selected value='{$value['id']}'>{$value['groupname']}({$number})</option>";
		}
		return $html;
	}
	public function cardOptions($botid, $selected="") {
		$cards = $this->db->cardFetchBotCards($botid);
		$returnvalue = "";
		foreach ($cards as $key => $value) {
			$returnvalue .= ($selected == $value['id']) ? "<option value='{$value['id']}' selected>{$value['title']}</option>":"<option value='{$value['id']}'>{$value['title']}</option>";
		}
		return $returnvalue;
	}
	public function groupLinks($format="", $groupid="", $contactid="", $botid="") {
		global $pos_color, $neg_color, $infocolor, $warningcolor;
		
		$html = "";
		$groupinfo = $this->db->groupFetchGroups($groupid);
		switch ($format) {
			case 'nestedgroups':
				//Generates a list of links with a checkbox adapted for the nested groups, the name of the array is groups[]
				$groupinfo = $this->db->groupFetchGroups();
				$membergroups = $this->db->groupGetGroupMembers($groupid);
				foreach ($groupinfo as $key => $groupvalue) {
					$actual_members = $this->db->groupUserMembershipNumber($groupvalue['id']);
					$total_members = $this->db->groupMembershipNumber($groupvalue['id']);
					$virtual_members = $total_members - $actual_members;
					
					$act = colorize_value($infocolor, $actual_members, "Actual members to be linked");
					$tot = colorize_value($infocolor, $total_members, "Total members");
					$virt = colorize_value($infocolor, ($virtual_members >= 0) ? $virtual_members:"0", "Members from other groups");
					
					if ($groupvalue['id'] == $groupid) continue;
					if ($actual_members == 0) continue;
					$checked = "";
					foreach ($membergroups as $key => $v) {
						if ($v['nestedid'] == $groupvalue['id']) {
							$checked = "checked";
						}
					}
					$number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']));
					$html .= ($groupvalue['botid'] == "0") ? "<input type='checkbox' {$checked} name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$act})<br>":"";
				}
				break;
			case 'optionlinks':
				//Generates a list of links with a checkbox, the name of the array is groups[]
				$bots = $this->db->botFetchBots();
				foreach ($groupinfo as $key => $groupvalue) {
					$number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']));
					//$html .= ($groupvalue['botid'] == "0") ? "<input type='checkbox' name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$number})<br>":"";
					$html .= ($groupvalue['botid'] == "0") ? "<option value='{$groupvalue['id']}'>{$groupvalue['groupname']} ({$number})</option>":"";
				}
				if ($botid != "" and count($this->db->groupGetGroupOwner($botid))>0) {
					$num_groups = colorize_value($infocolor, count($groupinfo = $this->db->groupGetGroupOwner($botid)));
					$botname = $this->db->botFetchBots($botid)[0]['displayName'];
					$html .= "<br><b>{$botname}'s groups:</b><br>";
					foreach ($groupinfo as $key => $groupvalue) {
						$user_number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']), "Number of users");
						$space_number = colorize_value($pos_color, $this->db->groupSpaceMembershipNumber($groupvalue['id'], $botid), "Number of spaces");
						//$html .= "<input type='checkbox' name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$user_number}|{$space_number})<br>";
						$html .= "<option value='{$groupvalue['id']}'>{$groupvalue['groupname']} ({$user_number}|{$space_number})</option>";
					}
				}
				elseif ($botid == "all") {
					foreach ($bots as $key => $botvalue) {
						if (count($groupinfo = $this->db->groupGetGroupOwner($botvalue['id']))>0) {
							$html .= "<br><b>{$botvalue['displayName']} ($num_groups):</b><br>";
							foreach ($groupinfo as $key => $groupvalue) {
								$number = colorize_value($infocolor, $this->db->groupMembershipNumber($groupvalue['id']));
								//$html .= "<input type='checkbox' name='groups[]' value='{$groupvalue['id']}'> <a href='index.php?id=groups&viewgroup={$groupvalue['id']}'>{$groupvalue['groupname']}</a> ({$number})<br>";
								$html .= "<option value='{$groupvalue['id']}'>{$groupvalue['groupname']} ({$number})</option>";
							}
						}
					}
				}
				break;
			case 'linksnumbers':
				//Generates a list of group links with the amount of members. It sorts out the link list based on the owner of the group.
				$bots = $this->db->botFetchBots();
				$html .= "<table class='table'><tr><td><b/>Public groups<td><b/>Members<td><b/>Default<td><b/>Subscribable";
				foreach ($groupinfo as $key => $groupvalue) {
					$tot_mem_num = $this->db->groupMembershipNumber($groupvalue['id']);
					$actual_mem_num = $this->db->groupUserMembershipNumber($groupvalue['id']);
					$linked_mem_num = ($actual_mem_num > 0) ? $tot_mem_num - $actual_mem_num : $tot_mem_num;
					
					$actual_mem_output = colorize_value($infocolor, "<b>$actual_mem_num</b>", "Real members");
					$tot_mem_output = colorize_value($warningcolor, "<b>$tot_mem_num</b>", "Total members of this group");
					$linked_mem_output = colorize_value($neg_color, "<b>$linked_mem_num</b>", "Virtual members");
					
					$def_group = ($groupvalue['default_group']) ? onoff(true,"Default group"):"";
					$sub_group = ($groupvalue['subscribable']) ? onoff(true,"Subscribable group"):"";
					
					$html .= ($groupvalue['botid'] == "0") ? "<tr><td><a class='linkblock' href='index.php?id=groups&viewgroup={$groupvalue['id']}' title='{$groupvalue['description']}'>{$groupvalue['groupname']}</a><td>{$actual_mem_output} | {$linked_mem_output} | {$tot_mem_output} <td> $def_group<td> $sub_group\n":"";
				}
				foreach ($bots as $key => $botvalue) {
					if (count($groupinfo = $this->db->groupGetGroupOwner($botvalue['id']))) {
						$html .= "<tr><td><b>{$botvalue['displayName']}</b><td><td><td><td>";
						$groupinfo = multiSort($groupinfo, 'groupname');
						foreach ($groupinfo as $key => $groupvalue) {
							$tot_mem_num = $this->db->groupMembershipNumber($groupvalue['id']);
							$actual_mem_num = $this->db->groupUserMembershipNumber($groupvalue['id']);
							$linked_mem_num = ($actual_mem_num > 0) ? $tot_mem_num - $actual_mem_num : $tot_mem_num;
							$space_mem_num = $this->db->groupSpaceMembershipNumber($groupvalue['id'], $botvalue['id']);
							
							$actual_mem_output = colorize_value($infocolor, "<b>$actual_mem_num</b>", "Real members");
							$tot_mem_output = colorize_value($warningcolor, "<b>$tot_mem_num</b>", "Total members of this group");
							$linked_mem_output = colorize_value($neg_color, "<b>$linked_mem_num</b>", "Virtual members");
							$space_mem_output = colorize_value($pos_color, "<b>$space_mem_num</b>", "Spaces part of this group");
							
							$def_group = ($groupvalue['default_group']) ? onoff(true,"Default group"):"";
							$sub_group = ($groupvalue['subscribable']) ? onoff(true,"Subscribable group"):"";
							
							$html .= "<tr><td><a class='linkblock' href='index.php?id=groups&viewgroup={$groupvalue['id']}&botid={$botvalue['id']}' title='{$groupvalue['description']}'>{$groupvalue['groupname']}</a> <td>{$actual_mem_output} | {$linked_mem_output} | {$tot_mem_output} | {$space_mem_output}<td> $def_group <td> $sub_group";
						}
					}
				}
				$html .= "</table>";
				break;
			case 'options':
				foreach ($groupinfo as $key => $value) {
					$number = $this->db->groupMembershipNumber($value['id']);
					$selected = ($groupid == $value['id']) ? "selected":"";
					$html .= "<option $selected value='{$value['id']}'>{$value['groupname']}({$number})</option>";
				}
				break;
			case "groupadd":
				foreach ($groupinfo as $key => $value) {
					$html .= "<a href='index.php?id=contacts&add&group={$value['id']}&contactid={$contactid}' style='color: $neg_color;' title='Click to add user to this group'>{$value['groupname']}</a></font><br>";
				}
				break;
			case "groupremove":
				$linked = "";
				$title = "Click to remove from group";
				foreach ($groupinfo as $key => $value) {
					$pc = $pos_color;
					if (!$this->db->groupCheckIfActualMember($contactid, $groupid)) {
						$linked_group_info = $this->db->groupCheckContactGroupLink($groupid, $contactid);
						$pc = $infocolor;
						$linked = " - Linked via {$linked_group_info[0]['groupname']}";
						$title = "Cannot remove from group (must remove from host group or unlink)";
					}
					$html .= "<a href='index.php?id=contacts&remove&group={$value['id']}&contactid={$contactid}' style='color: $pc;' title='{$title}'>{$value['groupname']}</a>$linked<br>";
				}
				break;
			default:
				return $groupinfo;
				break;
		}
		return $html;
	}
	
	public function wsIntegrationGenSelect($name, $userId) {
	    
	    $userlink = $this->db->wsIntegrationGetUserLink($userId);
	    $wsintegrations = $this->db->wsIntegrationGet();
	    $appId = "";
	    
	    if (count($userlink)) {
	        $appId = $userlink[0]['appid'];
	    }
	    
	    $returnvalue = "<select name='{$name}' class='form-control'><option value=''>No integration selected</option>";
	    
	    if (isset($wsintegrations) and count($wsintegrations)) {
    	    foreach ($wsintegrations as $key => $value) {
    	        $returnvalue .= ($value['id'] == $appId) ? "<option value='{$value['id']}' selected>{$value['name']}</option>" : "<option value='{$value['id']}'>{$value['name']}</option>"; 
    	    }
	    }
	    
	    $returnvalue .= "</select>";
	    
	    return $returnvalue;
	    
	}
	
	public function integrationGenScopeSelect($name='default_scopes[]') {
	    
		$scopes = $this->db->integrationFetchScopes();
		$integrationSettings = $this->db->integrationFetchSettings();
		$scopeselect = "<select class='select2' multiple='multiple' name='$name' style='width: 100%;' required>";
		
		$selectedscopes = explode(' ', issetor($integrationSettings['default_scopes']));
		
		foreach ($scopes as $key => $value) {
		    
			$scopeselect .= (in_array($value['scope'], $selectedscopes)) ? "<option value='{$value['scope']}' selected>{$value['scope']}</option>" : "<option value='{$value['scope']}'>{$value['scope']}</option>";
		
		}
		
		$scopeselect .= "</select>";
		
		return $scopeselect;
	}
	
	public function integrationGenAuth($userid) { 
	    
		$authorization = $this->db->integrationFetchToken($userid);
		$time = strtotime($authorization['timestamp']);
		$expires_in = $authorization['expires_in'];
		$refresh_token_expires_in = $authorization['refresh_token_expires_in'];
		//echo $time;
		$curtime = time();
		$access_token_future = ($time+$expires_in);
		$refresh_token_future = ($time+$refresh_token_expires_in);
		
		$timeleft_at = $access_token_future-$curtime;	
		$expiration_date_at = date("Y-m-d h:s", $access_token_future); 
		$days_left_at = round((($timeleft_at/24)/60)/60);
		
		$timeleft_rt = $refresh_token_future-$curtime;
		$expiration_date_rt = date("Y-m-d h:s", $refresh_token_future);
		$days_left_rt = round((($timeleft_rt/24)/60)/60);
		 
		
		$returnvalue = "<div class='row' style='padding-top: 1%'>
						  <div class='col-md-3'>
							Access token
						  </div>
						  <div class='col-md-9'>
							{$authorization['access_token']}
						  </div>
					    </div>
						<div class='row' style='padding-top: 1%'>
						  <div class='col-md-3'>
							Expires in
						  </div>
						  <div class='col-md-9'>
							{$expiration_date_at} ({$days_left_at} Days)
						  </div>
					    </div>
						<div class='row' style='padding-top: 1%'>
						  <div class='col-md-3'>
							Refresh token expires in
						  </div>
						  <div class='col-md-9'>
							{$expiration_date_rt} ({$days_left_rt} Days)
						  </div>
					    </div>";
		return $returnvalue;					
	}
	public function logsPrint($botid='', $limit='50'){
		$logs = $this->db->logsGet($botid, $limit);
		$num = count($logs);
		$returnvalue = "<table class='rounded'><tr><td>Total results: <b>{$num}</b></table><br><table class='rounded'><tr><td><b>User</b><td><b>Bot</b><td class='tdStyle'><b>Payload</b><td><b>Date</b><tr>";
		foreach ($logs as $key) {
			$botinfo = $this->db->botFetchBots($key['botid'])[0];
			$returnvalue .= "<td class='tdStyle'>{$key['user']}<td><a href='index.php?".http_build_query(array_merge($_GET, array("botfilter"=>$botinfo['id'])))."'>{$botinfo['emails']}</a><td class='wrap'>{$key['command']}<td>{$key['date']}<tr>";
		}
		$returnvalue .= "<td colspan='4'>Total results: <b>{$num}</b></table>";
		return $returnvalue;
	}
	public function getJoke($type){
		if ($type == "chuck"){
			$url = "https://api.chucknorris.io/jokes/random";
			$returnvalue = 'value';
		}
		elseif ($type == "yomomma"){
			$url = "http://api.yomomma.info";
			$returnvalue = 'joke';
		}
		elseif ($type == "joke"){
			$url = "https://icanhazdadjoke.com/";
			$returnvalue = 'joke';
		}
		$options = array('http' => array(
				'header' => 'Accept: application/json',
				'method' => 'GET'),);
		$result = json_decode(file_get_contents($url, false, stream_context_create($options)), true);
		return $result[$returnvalue];
	}
	public function botGenProfile($data) {
		$headline = strtoupper($data['displayName']);
		$main = onoff($data['main']);
		$html = "<div id='input'><h1><a href='index.php?id=bots'>MY BOTS</a> > {$headline}</h1><hr class='gradient'><table>
		<tr> <td rowspan='5'> <img class='img-circle' src='{$data['avatar']}' width='150' height='150'>
		<tr> <td> Name: <td> {$data['displayName']}</a>
		<tr> <td> Email: <td> {$data['emails']}
		<tr> <td> Is primary: <td> $main
		</td></tr></table></div>";
		return $html;
	}
	public function botGenDropdown($name, $botid='') {
		$bots = $this->db->botFetchBots();
		$returnvalue = "<select name='$name' class='form-control select2'><option value='0'>Bot owner (None)</option>";
		foreach ($bots as $key => $value) {
			$returnvalue .= ($botid == $value['id']) ? "<option value='{$value['id']}' selected>{$value['displayName']}</option>":"<option value='{$value['id']}'>{$value['displayName']}</option>";
		}
		$returnvalue .= "</select>";
		return $returnvalue;
	}	
	public function botGenSelector($type, $botid="", $integration="") {
		$bots = ($integration) ? $this->db->botFetchBots("", $integration):$this->db->botFetchBots();
		$returnvalue = false;
		global $spark;
		foreach ($bots as $key => $value) {
			if ($botid == $value['id']) {
				echo "<a class='linkblock linkblock-selected' href='index.php?id=$type&botid={$value['id']}'><img class='img-circle img-thumbnail' title='{$value['displayName']} (SELECTED)' height='40' width='40' src='images/bots/{$value['id']}.jfif'></a> ";
				$returnvalue = $value;
			}
			else {
				echo "<a class='linkblock' href='index.php?id=$type&botid={$value['id']}'><img class='img-circle img-thumbnail' title='{$value['displayName']}' height='35' width='35' src='images/bots/{$value['id']}.jfif'></a>";
			}
		}
		
		return $returnvalue;
	}
}

?>