<?php 
// Webex Teams content identifier
$cardmanifest = array(
		"contentType" => "application/vnd.microsoft.card.adaptive",
		"content" => array()
);
// Device UI Adaptive card
$cardDeck = array(
//CLOUD XAPI DEVICE CARD UI (DEVICE FEATURE)
	'deviceKeyParam' => '{
    "type": "AdaptiveCard",
    "body": [
        {
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "items": [
                        {
                            "type": "TextBlock",
                            "weight": "Bolder",
                            "text": "Placeholder",
                            "horizontalAlignment": "Left",
                            "wrap": true,
                            "color": "Light",
                            "size": "Large",
                            "spacing": "Small"
                        }
                    ],
                    "width": "stretch"
                }
            ]
        },
        {
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "width": 35,
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "Device Name:",
                            "color": "Light",
                            "spacing": "Medium"
                        },
                        {
                            "type": "TextBlock",
                            "text": "Product:",
                            "weight": "Lighter",
                            "color": "Light",
                            "spacing": "Small"
                        },
                        {
                            "type": "TextBlock",
                            "text": "Software:",
                            "weight": "Lighter",
                            "color": "Light",
                            "spacing": "Small"
                        },
                        {
                            "type": "TextBlock",
                            "text": "Upgrade Channel:",
                            "weight": "Lighter",
                            "color": "Light",
                            "spacing": "Small"
                        }
                    ]
                },
                {
                    "type": "Column",
                    "width": 65,
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "color": "Medium"
                        },
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "color": "Light",
                            "weight": "Lighter",
                            "spacing": "Small"
                        },
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "weight": "Lighter",
                            "color": "Light",
                            "spacing": "Small"
                        },
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "weight": "Lighter",
                            "color": "Light",
                            "spacing": "Small"
                        }
                    ]
                }
            ],
            "spacing": "Padding",
            "horizontalAlignment": "Center"
        },
        {
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "width": "stretch",
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "Method:"
                        }
                    ]
                },
                {
                    "type": "Column",
                    "width": "stretch",
                    "items": [
                        {
                            "type": "Input.ChoiceSet",
                            "placeholder": "Cloud xAPI",
                            "choices": [
                                {
                                    "title": "Status",
                                    "value": "status"
                                },
                                {
                                    "title": "Command",
                                    "value": "command"
                                },
                                {
                                    "title": "Config",
                                    "value": "config"
                                }
                            ],
                            "value": "status",
                            "id": "xapiType"
                        }
                    ]
                }
            ]
        },
        {
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "width": "stretch",
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "Action (xConfig):"
                        }
                    ]
                },
                {
                    "type": "Column",
                    "width": "stretch",
                    "items": [
                        {
                            "type": "Input.ChoiceSet",
                            "placeholder": "placeholder",
                            "choices": [
                                {
                                    "title": "Get",
                                    "value": "get"
                                },
                                {
                                    "title": "Set",
                                    "value": "set"
                                },
                                {
                                    "title": "Set default",
                                    "value": "default"
                                }
                            ],
                            "value": "get",
                            "id": "configOption"
                        }
                    ]
                }
            ]
        },
        {
            "type": "TextBlock",
            "text": "Key path (required): ",
            "size": "Small",
            "color": "Dark"
        },
        {
            "type": "Input.Text",
            "placeholder": "placeholder",
            "id": "key"
        },
        {
            "type": "TextBlock",
            "text": "Attributes (xCommand) or Value (xConfig Set):",
            "size": "Small",
            "color": "Dark"
        },
        {
            "type": "Input.Text",
            "placeholder": "placeholder",
            "id": "value",
            "isMultiline": true
        },
        {
            "type": "TextBlock",
            "text": "Multiline body (xCommand):",
            "size": "Small",
            "color": "Dark"
        },
        {
            "type": "Input.Text",
            "placeholder": "placeholder",
            "isMultiline": true,
            "id": "body"
        },
        {
            "type": "ActionSet",
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": "placeholder",
                    "data": {
                        "deviceid": "",
                        "wbm_command": "device"
                    },
                    "id": "device",
                    "style": "positive"
                }
            ],
            "horizontalAlignment": "Left",
            "spacing": "None"
        },
        {
            "type": "TextBlock",
            "text": "[Click here](https://developer.webex.com/docs/api/guides/xapi) for more information about the cloud device xAPI"
        }
    ],
    "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
    "version": "1.2"
}',
//SUBSCRIBE FEATURE UI FORM
    'subscribeForm' => '{
        "type": "AdaptiveCard",
        "version": "1.0",
        "body": [
            {
                "type": "TextBlock",
                "text": "Subscriptions",
                "size": "ExtraLarge"
            },
            {
                "type": "TextBlock",
                "text": "Select items and click update to make changes.",
                "size": "Default"
            },
            {
                "type": "Container",
                "separator": true,
                "items": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": "stretch",
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "text": "Active subscriptions",
                                        "weight": "Bolder"
                                    }
                                ]
                            }
                        ],
                        "style": "Accent"
                    }
                ]
            },
            {
                "type": "Container",
                "separator": true,
                "items": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": "stretch",
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "text": "Available subscriptions",
                                        "weight": "Bolder"
                                    }
                                ]
                            }
                        ],
                        "style": "Good"
                    }
                ]
            },
            {
                "type": "ActionSet",
                "actions": [
                    {
                        "type": "Action.Submit",
                        "title": "Update subscriptions",
                        "data": {
                            "wbm_command": "subscribe",
                            "wbm_delete_card":""
                        }
                    }
                ]
            }
        ],
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json"
}', 
'integrationUi' => '{
    "type": "AdaptiveCard",
    "body": [
        {
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "Integration",
                            "size": "ExtraLarge",
                            "color": "Accent"
                        }
                    ],
                    "width": "stretch"
                }
            ]
        },
        {
            "type": "TextBlock",
            "text": "Authorize a Webex user. This user can use the place and device commands if it has the correct scopes. Create new workspaces and generate activation codes and utilize the cloud xAPI.",
            "wrap": true
        },
        {
            "type": "TextBlock",
            "text": "Integration actions",
            "weight": "Bolder"
        },
        {
            "type": "ActionSet",
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": "Authorize new user",
                    "data": {
                        "wbm_command": "integration authorize card"
                    }
                },
                {
                    "type": "Action.Submit",
                    "title": "Help",
                    "data": {
                        "wbm_command": "integration"
                    }
                }
            ],
            "spacing": "Small"
        },
        {
            "type": "ActionSet",
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": "Show default scopes",
                    "data": {
                        "wbm_command": "integration defaults"
                    }
                },
                {
                    "type": "Action.Submit",
                    "title": "Show current authorization",
                    "data": {
                        "wbm_command": "integration show"
                    }
                }
            ]
        },
        {
            "type": "TextBlock",
            "text": "Authorization will use the default scopes set for the integration, view the integration help file to find out how to authorize a custom set of scopes. ",
            "wrap": true
        },
        {
            "type": "ActionSet",
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": "Delete current authorization",
                    "style": "destructive",
                    "data": {
                        "wbm_command": "integration delete"
                    }
                }
            ]
        },
        {
            "type": "TextBlock",
            "text": "Delete your current authorization. "
        }
    ],
    "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
    "version": "1.2"
}',
	'integrationAuthorizationCard' => '{
    "type": "AdaptiveCard",
    "version": "1.0",
    "body": [
        {
            "type": "TextBlock",
            "text": "Authorize user",
            "size": "ExtraLarge",
            "color": "Accent"
        },
        {
            "type": "ActionSet",
            "actions": [
                {
                    "type": "Action.OpenUrl",
                    "title": "Click here to begin authorization",
                    "url": ""
                }
            ],
            "spacing": "Small"
        },
        {
            "type": "TextBlock",
            "text": "placeholder",
            "weight": "Bolder",
            "wrap": true
        },
        {
            "type": "TextBlock",
            "text": "You will be forwared to the Webex Login portal. Log in with the user you want to authorize. When the scopes are accepted, you will be forwarded to the WBM authorization handler. Follow the instructions there. You may have to log out from any \"logged in\" Webex user in your open browser session. ",
            "wrap": true
        }
    ],
    "$schema": "http://adaptivecards.io/schemas/adaptive-card.json"
}',
//WHOIS FEATURE	UI 	
	'whoisSearchResult' => '{
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "items": [
						{
                            "type": "TextBlock",
                            "text": "1",
                            "weight": "Bolder",
                            "color": "Default"
                        }
                    ],
                    "width": "auto",
                    "spacing": "None",
                    "verticalContentAlignment": "Center"
                },
                {
                    "type": "Column",
                    "width": "stretch",
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "weight": "Bolder",
                            "color": "Default"
                        },
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "spacing": "Small",
                            "size": "Small",
                            "weight": "Lighter",
                            "color": "Light",
                            "horizontalAlignment": "Left"
                        }
                    ],
                    "verticalContentAlignment": "Center",
                    "style": "Accent",
                    "spacing": "Small"
                }
            ],
            "separator": true,
			"selectAction": {
                "type": "Action.Submit",
                "data": ""
            }
        }',
	
	'whoisProfile' => '{
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "items": [
                        {
                            "type": "Image",
                            "altText": "",
                            "url": "placeholder",
                            "width": "100px",
                            "style": "Person",
                            "selectAction": {
                                "type": "Action.OpenUrl",
                                "url": "placeholder"
                            }
                        }
                    ],
                    "width": "auto",
                    "spacing": "None",
                    "verticalContentAlignment": "Center",
                    "style": "emphasis"
                },
                {
                    "type": "Column",
                    "width": "stretch",
                    "items": [
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "size": "Large",
                            "weight": "Bolder",
                            "color": "Accent"
                        },
                        {
                            "type": "TextBlock",
                            "text": "placeholder",
                            "spacing": "None"
                        },
						{
                            "type": "ActionSet",
                            "actions": [
                                {
                                    "type": "Action.Submit",
                                    "title": "Show Webex Userdata",
                                    "data": ""
                                }
                            ],
                            "horizontalAlignment": "Left"
                        }
                    ],
                    "verticalContentAlignment": "Center",
                    "spacing": "Small",
                    "style": "emphasis"
                }
            ],
            "separator": true
        }',
    
    'choiceSet' => '{
        "type": "Input.ChoiceSet",
        "placeholder": "placeholder",
        "choices": [    
        ],
        "style": "expanded",
        "isMultiSelect": true,
        "id": "placeholder",
        "wrap": true
}',
    
    'emptyCard' => '{
        "type": "AdaptiveCard",
        "version": "1.0",
        "body": [
        ],
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json"
}',
    
    'columnSet' => '{
        "type": "ColumnSet",
        "columns": [
        ]
}',
    
    'column' => '{
        "type": "Column",
        "width": "stretch",
        "items": [                                
        ],
        "verticalContentAlignment": "Top"
}',
    
    'textBlock' => '{
       "type": "TextBlock",
       "text": "Success",
       "wrap": true
}'
    
);

?>