<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
include('session.php');
$windowid = (isset($_GET['id'])) ? $_GET['id'] : "bots";
if ($windowid != "login" and !verify()) header("Location: index.php?id=login");

include_once "includes/functions.inc.php";
$db_local = new Db();
$generate = new OutputEngine();
$spark = new SparkEngine();
$ldap = new LDAP();

// SQL server connection information
$sql_details = array(
    'user' => $config['username'],
    'pass' => $config['password'],
    'db'   => $config['dbname'],
    'host' => $config['dbhost']
);

//Get Data Processing Type
$type = $_GET['type'];


 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

if($type == 'table')
{
	require( 'ssp.class.php' );

	// DB table to use
$table = $_GET['table'];
 
// Table's primary key
$primaryKey = $_GET['primarykey'];


	if($table == 'contacts')
	{
		$columns = 
		array(
			array(
					'db' 		=> 'id',
					'dt'		=> 0,
					'formatter' => function( $d, $row ) {
						return '<input type="checkbox" name="userlist[]" value="'. $d .'">';
					}
				),
			array(
	        		'db'        => 'avatar',
					'dt'        => 1,
					'formatter' => function( $d, $row ) {
	    			if(empty($d)) { $d = 'images/static/noimagefound.jpeg';} 
	    				return '<img class="img-circle" src="'.$d.'" width="30" height="30">';
	    			}
	    		),
			array(
					'db'		=> 'firstName',
					'dt'		=> 2
				),
			array(
					'db'		=> 'lastName',
					'dt'		=> 3
				),
			array(
					'db'		=> 'emails',
					'dt'		=> 4
				),
			array(
					'db'        => 'id',
					'dt'        => 5,
					'formatter' => function( $d, $row ) {
	    			return '<a href="index.php?id=contacts&contactid='.$d.'" title="View Contact"><i class="fas fa-external-link-alt"></i></a>';
	        		}
	    		)
			);
		echo json_encode(
	    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
	}elseif($table == 'cards')
	{
		$columns =
		array(
				array(
						'db'		=> 'id',
						'dt'		=> 0
				),
				array(
						'db'		=> 'title',
						'dt'		=> 1
				),
				array(
						'db'        => 'botid',
						'dt'        => 2,
						'formatter' => function( $d, $row ) {
						return '<a href="index.php?id=cards&botid='.$d.'&card='. $row['id'] .'" title="View Card"><i class="fas fa-external-link-alt"></i></a>';
						}
						)
				);
		echo json_encode(
				SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, "botid = '{$_GET['botid']}'" )
				);
	}elseif($table == 'card_data')
	{
		$columns =
		array(
				array(
						'db'		=> 'person_id',
						'dt'		=> 0,
						'formatter' => function( $d, $row ) {
						return '<input type="checkbox" name="datalist[]" value="'. $d .'">';
						}
				),
				array(
						'db'		=> 'person_email',
						'dt'		=> 1,
						'formatter' => function ($d, $row) {
							global $db_local;
							return $db_local->contactGetName($row['person_email']);
						}
				),
				array(
						'db'        => 'input_data',
						'dt'        => 2,
						'formatter' => function( $d, $row ) {
						return "<pre>" . json_encode(json_decode($d, true), JSON_PRETTY_PRINT) . "</pre>";
						}
						)
				);
		echo json_encode(
				SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, "card_id = '{$_GET['card']}'" )
				);
	}elseif($table == 'workspace_integrations')
	{
	    $columns =
	    array(
	        array(
	            'db'		=> 'id'
	        ),
	        array(
	            'db'		=> 'name',
	            'dt'		=> 0,
	            'formatter' => function( $d, $row ) {
	            return '<a href="index.php?id=wsintegrations&int='.$row['id'].'" title="View integration">'.$d.'</a>';
	            }
	        ),
	        array(
	            'db'        => 'health_status',
	            'dt'        => 1,
	            'formatter' => function( $d, $row ) {
	                               return statusBoolPill(($row['health_status']), "OK", "Not OK"); 
	                           }
	        ),
	        array(
	                'db'        => 'activation_status',
	                'dt'        => 2,
	                'formatter' => function( $d, $row ) {
	                                  return statusBoolPill(($row['activation_status']), "Activated", "Not activated");
	                               }
	        )
	   );
	    echo json_encode(
	        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	        );
	}elseif($table == 'feedback_topic')
	{
		$columns = 
		array(
			array(
					'db'		=> 'id',
					'dt'		=> 0
				),
			array(
					'db'		=> 'title',
					'dt'		=> 1
				),
			array(
					'db'        => 'botid',
					'dt'        => 2,
					'formatter' => function( $d, $row ) {
					return '<a href="index.php?id=feedback&botid='.$d.'&topic='. $row['id'] .'" title="View Topic"><i class="fas fa-external-link-alt"></i></a>';
	        		}
	    		)
		);
		echo json_encode(
	    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, "botid = '{$_GET['botid']}'" )
		);
	}elseif($table == 'activity_log')
	{
		$columns = 
		array(
			array(
					'db'		=> 'date',
					'dt'		=> 0
				),
			array(
					'db'		=> 'user',
					'dt'		=> 1
				),
			array(
					'db'		=> 'botid',
					'dt'		=> 2,
					'formatter' => function( $d, $row ) {
						global $db_local;
						$bot = $db_local->botFetchBots($d);
	    			return $bot[0]['emails'];
	        		}
				),
			array(
					'db'		=> 'command',
					'dt'		=> 3
				)
		);
		echo json_encode(
	    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
	} elseif($table == 'debug_log')
	{
	    $columns =
	    array(
	        array(
	            'db'		=> 'timestamp',
	            'dt'		=> 0
	        ),
	        array(
	            'db'		=> 'user',
	            'dt'		=> 1
	        ),
	        array(
	            'db'		=> 'bot',
	            'dt'		=> 2
	        ),
	        array(
	            'db'		=> 'request',
	            'dt'		=> 3,
	            'formatter' => function( $d, $row ) {
	            return "<pre>" . $d . "</pre>";
	            }
	        ),
	        array(
	            'db'		=> 'response',
	            'dt'		=> 4,
	            'formatter' => function( $d, $row ) {
	            return "<pre>" . json_encode(json_decode($d, true), JSON_PRETTY_PRINT) . "</pre>";
	            }
	        ),
	        array(
	            'db'		=> 'type',
	            'dt'		=> 5
	        ),
	        array(
	            'db'		=> 'description',
	            'dt'		=> 6
	        )
	    );
	    echo json_encode(
	        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	        );
	}
}


 