<?php 
//Verification
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
//Declarations
$executor = $form = $isbot_member_output = "";
$rooms_selected = $contacts_selected = array();
$spacetype_const = (issetor($_GET['spacetype'])) ? $db_local->quote($_GET['spacetype']):"";
$spaceid = (issetor($_GET['viewspace'])) ? $db_local->quote($_GET['viewspace']):"";
$teamid = (issetor($_GET['teamid'])) ? $db_local->quote($_GET['teamid']):"";
$context = (isset($_GET['team'])) ? true:false;
$botid = (issetor($_GET['botid'])) ? $db_local->quote($_GET['botid']):"";
$botinfo = (!empty($botid)) ? $db_local->botFetchBots($botid):"";
$disable = ($botid) ? "":"disabled";
$botid_link = ($botid) ? "&botid=" . $botid : "";
$teamspace_create_link = ($teamid != "") ? "&teamid=$teamid&spacetype=teamspaces&":"";

//Delete membership
if (isset($_GET['membershipdelete'])){
	$spark->membershipDelete($botid, $db_local->quote($_GET['membershipdelete']));
}
//Delete message
if (isset($_GET['messagedelete'])){
	$spark->messageDelete($botid, $db_local->quote($_GET['messagedelete']));
}
//Delete space
if (isset($_GET['spacedelete'])){
	$spark->roomDelete($botid, $db_local->quote($_GET['spacedelete']));
}
//Switch joinable flag
if (isset($_GET['setjoinable'])){
	$spaceinfo = $spark->roomGetDetails($botid, $spaceid);
	$title = $spaceinfo['title'];
	$data_array = array("spaceid" => $spaceid,
			"spacetitle" => $title,
			"botid" => $botid);
	$db_local->adminAddJoinableSpace($data_array);
}
elseif (isset($_GET['setunjoinable'])){
	$db_local->adminRemoveJoinableSpace($botid, $spaceid);
}
//Switch moderator flag
if (isset($_GET['setmod']) and !empty($botid)) {
	$spark->membershipSetMod($_GET['setmod'], $botid, "true");
}
elseif (isset($_GET['unsetmod']) and !empty($botid)) {
	$spark->membershipSetMod($_GET['unsetmod'], $botid, "false");
}
//Switch group response flag
if (isset($_GET['setgroupresponse'])) {
	$db_local->adminAddGroupResponseAcl($botid, $spaceid);
}
elseif (isset($_GET['unsetgroupresponse'])) {
	$db_local->adminRemoveGroupResponseAcl($botid, $spaceid);
}
//Create space or add users
$creator = isset($_POST['creator']);
$addtoexisting = isset($_POST['addpersontospace']);
if ($addtoexisting or $creator) {
	$a = 0;
	$memberParsel = array();
	if ($creator) {
		$c_title = $_POST['c_spacetitle'];
		$c_type = $_POST['c_spacetype'];
		$c_group = $_POST['c_group'];
		$c_csv = str_replace(" ", "", $_POST['c_mailcsv']);	
		
		if (empty($c_title)) $c_title = 'No name';
		
		if ($c_type == 'team') {
			$newSpaceId = $spark->teamCreate($botid,$c_title);
		} 
		elseif ($c_type == 'space') {
			$newSpaceId = $spark->roomCreate($botid,$c_title)['id'];
		} else {
			$newSpaceId = $spark->roomCreate($botid,$c_title,$c_type)['id'];
			$c_type = "Teamspace";
		}
	} elseif($addtoexisting) {
		$newSpaceId = $spaceid;
		$c_group = issetor($_POST['groups']);
		$c_type = "";
		$c_csv = str_replace(" ", "", $_POST['personemail']);
	}
	if ($c_type != "team") {
		if (!empty($c_group)) {
			$members = $db_local->groupGetMembers($c_group);
			foreach ($members as $key => $member){
				$memberParsel[$a]['roomtype'] = "roomId";
				$memberParsel[$a]['id'] = $newSpaceId;
				$memberParsel[$a]['method'] = "personId";
				$memberParsel[$a]['person'] = $member['contactid'];
				$a++;
			}
		}
		if (!empty($c_csv)) {
			$addlist = explode(",", $c_csv);
			foreach ($addlist as $key => $usertoadd) {
				if (validateEmail($usertoadd)) {
					$memberParsel[$a]['roomtype'] = "roomId";
					$memberParsel[$a]['id'] = $newSpaceId;
					$memberParsel[$a]['method'] = "personEmail";
					$memberParsel[$a]['person'] = $usertoadd;
					$a++;
				}
			}
		}
		//Start request intervals	
		echo $spark->bulkSplitRequestPost("memberships", $memberParsel, $botid);
		//End request intervals
	} elseif ($c_type == "team") echo feedbackMsg("The team was created, adding people directly when creating a team is not supported. To add many people to a team, please select the teams general space and add members from there (That will make the users member of the team automatically). This is a limitation in the team membership API.", "", "warning");
}
if (isset($_GET['joinbot'])) {
	$addlist = $db_local->quote($_GET['joinbot']);
	$spark->membershipCreate($botid, $db_local->quote($spaceid),$addlist);
}
//Set title
if (isset($_POST['settitle'])){
	$spark->roomUpdate($botid, $db_local->quote($_GET['viewspace']),$_POST['spacetitle']);
}
if ($teamid != "") {
	$teaminfo = $spark->teamGetDetails($botid, $teamid);
}

//Page tooltips
$tooltips = array(
		'spaceTitle'=>tooltip('Space title','Type in the title of the space or team that you will create.'),
		'spaceType'=>tooltip('Space type','Create a normal space or a team. Unfortunately, adding people to a team at the time of creation is not currently possible, please add people after the team is created. By selecting a team in the right pane you will get an option to create a space inside the selected team.'),
		'addFromGroup'=>tooltip('Add from group','After the space has been created, I will add all the people part of the group you have selected to the new space.'),
		'bulkAdd'=>tooltip('Bulk add','Type in e-mail addresses in a CSV format to add additional people. This field can be used together with the group add field, as both fields are considered.'),
		'titleChange'=>tooltip('Space title','Type in a new title and press "Change title" to change the space title.'),
		'addFromGroup1'=>tooltip('Add from group','Select the group of people you want to add to this space and press "Add users to space"'),
		'bulkAdd1'=>tooltip('Bulk add','Type in e-mail addresses in a CSV format to add people to this space. This field will not be used together with the group add field.'),
		'actions'=>tooltip('Space actions','When a space is joinable, a user can query the bot for a list of spaces and tell the bot to add them to the space (space control feature must be enabled). When a space has "Space response" enabled, the bot will override the webhook restriction if any and let all people chat with the bot in that specific space (domain restrictions still apply). Deleting a space will kick all users from the space and delete it permanently (be careful with this).'),
		'spaceFilter'=>tooltip('Filter spaces','Click one of the filters to filter the spaces in order to display only direct conversations, group spaces, teams and teamspaces'),
		'spaceList'=>tooltip('Space list','Select a space or direct conversation to see the space details. In direct conversations you may chat directly with the user.'),
		'spaceLocked'=>tooltip('Locked space','In a locked space there are one or more moderators present'),
		'botModerate'=>tooltip('Moderate space','A bot may moderate a space if there are no moderators or if the bot is set as moderator, in spaces that is part of a team that is not the general space, you cannot set moderator. Moderation includes removing/adding users, changing the space title etc.'),
		'team'=>tooltip('Is the space part of a team?','This check is to tell you if this space is actually part of a team.'),
		'botTeam'=>tooltip('Is the bot part of the team?','If the space is part of a team, this checks tells you if the bot is part of the same team.'),
		'botInSpace'=>tooltip('Is the bot in this space?','When you check a team space, this check will verify if the bot is in the space. If the bot is not in the space you will get an option to join the space (only available if the bot is part of the team).'),
		'messages'=>tooltip('Messages','In direct conversations you can chat with the person and see messages here. In a group conversations you will see communication of which the bot is mentioned in only, you may also delete the bot messages here.'),
		
);

$space_create_placeholder = ($teamid == "") ? "Space Name":"Team Space Name"; 
?>
<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Spaces & Teams</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6">
				<!-- general form elements -->
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Select a bot to manage spaces & teams</h3>
					</div>
					<div class="card-body">
					<?php 			
						$selected = $generate->botGenSelector('spaces', issetor($botid));
						
						if ($selected) {
							$executor = "<b>{$selected['displayName']}</b>";
							$executor_avatar = "<img class='rounded' title='{$selected['displayName']} (SELECTED)' height='35' width='35' src='{$selected['avatar']}'>";
						}
						
						$disable = (empty($executor)) ? "disabled" : "";
					?>
					</div>
				</div>
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Select space filter</h3>
					</div>
					<div class="card-body">
					<b><?php echo $tooltips['spaceFilter']; ?> Select one to filter spaces:</b><br>
					<?php  
					if(isset($_GET['botid'])) {
						$botid=$_GET['botid'];
						$group_class = $group_class = $direct_class = $all_class = $teamspaces_class = $teams_class = ""; 
						$spacetype = ($spacetype_const) ? $spacetype_const : "group";
						$selectedvalue = "btn-primary";
						switch($spacetype){
							case "group":
								$group_class = $selectedvalue;
								break;
							case "direct":
								$direct_class = $selectedvalue;
								break;
							case "all":
								$all_class = $selectedvalue;
								break;
							case "teamspaces":
								$teamspaces_class = $selectedvalue;
								break;
							case "teams":
								$teamspaces_class = $selectedvalue;
								break;
						}
						//class='btn btn-danger pull-right'
						echo "<a class='btn $all_class' href='index.php?id=spaces&botid=$botid&spacetype=all'>All types</a>
							  <a class='btn $direct_class' href='index.php?id=spaces&botid=$botid&spacetype=direct'>Direct conversations</a>
							  <a class='btn $group_class' href='index.php?id=spaces&botid=$botid&spacetype=group'>Group spaces</a>
							  <a class='btn $teamspaces_class' href='index.php?id=spaces&team&botid=$botid&spacetype=teams'>Teams / Teamspaces</a>"; 
					}
					else {
						echo "Select a bot to show filters!<br>";
					}
					$filtered_spaces = "<br><br>{$tooltips['spaceList']} ";
					
					if ($context) $filtered_spaces .= "<b>Select team to see team spaces:</b>"; 
					elseif ($spacetype_const == "teamspaces") $filtered_spaces .= "<b>Select a team space to view details:</b>"; 
					else $filtered_spaces .= "<b>Select a space to view details:</b>";
					echo "$filtered_spaces <br>";
					if(!empty($botid)) {
					$type = "";
					$spacetype = ($spacetype_const) ? $spacetype_const : "group";
					$maxresults = "1000";
					switch($spacetype){
						case "group":
							$query = array("sender"=>$botid,"max"=>$maxresults,"type"=>"group");		
							break;
						case "direct":
							$query = array("sender"=>$botid,"max"=>$maxresults,"type"=>"direct");
							break;
						case "all":
							$query = array("sender"=>$botid,"max"=>$maxresults, "type"=>"");
							break;
						case "teamspaces":
							$query = array("sender"=>$botid,"max"=>$maxresults,"teamId"=>$teamid,"type"=>"group");
							$type = $spacetype; 
							break;
						case "teams":
							$query = $spark->teamGet($botid, $maxresults);
							break;
						default:
							$query = array("sender"=>$botid,"max"=>$maxresults,"type"=>"group");
							break;
					}
					echo ($spacetype == 'teams') ? $generate->teamGenLinks($query, $botid) : $generate->roomGenLinks($spark->roomGet($query), $botid, $type);		
				}
				else {
					echo "Select a bot to view spaces!<br>";
				}
				$contacts = $db_local->contactFetchContacts();
				$number_of_contacts = count($contacts);
					echo "
					</b>
					</div>
					</div>
				</div>
			";
			if(empty($botid)) { echo '</div>';}
		if(!empty($botid) and empty($spaceid)) {
			$c_groupoptions = $generate->groupLinks('options');
			$teamspace_selector = ($teamid and $teaminfo) ? "<input type='radio' value='$teamid' name='c_spacetype'> Team space in {$teaminfo['name']}<br>": "";
			echo "
			</div><div class='col-lg-6'>
				<div class='card card-primary card-outline'>
					<div class='card-header'>
						<h3 class='card-title'>Create a space or team</h3>
					</div>
					<div class='card-body'>
							<form method='post' action='#' enctype='multipart/form-data'>
								<div class='form-group'>
									{$tooltips['spaceTitle']} <label for='c_spacetitle' class='control-label'>Space title:</label>
										<input type='text' name='c_spacetitle' id='c_spacetitle' placeholder='Name of the space or team' class='form-control'>
								</div>
								<div class='form-group'>
									{$tooltips['spaceType']} <label for='c_spacetype' class='control-label'>Space type:</label><br>
										<input type='radio' value='space' id='c_spacetype' checked name='c_spacetype'> Normal space<br>
										$teamspace_selector
										<input type='radio' value='team' id='c_spacetype' name='c_spacetype'> Team
								</div>
								<div class='form-group'>
									{$tooltips['addFromGroup']} <label for='c_group' class='control-label'>Add people from group:</label>
										<select name='c_group' id='c_group' class='form-control'><option value=''>Select group to add to space</option>$c_groupoptions</select>
								</div>
								<div class='form-group'>
									{$tooltips['bulkAdd']} <label for='c_group' class='control-label'>Bulk CSV add:</label>
										<textarea name='c_mailcsv' id='c_mailcsv' placeholder='E-mail CSV input: user@example.com,user@example.com,etc..' class='form-control'></textarea>
								</div>
						</div>
						<div class='card-footer'>
							<input type='submit' name='creator' value='Create space' class='btn btn-md btn-primary' > 
						</div>
						</form>
					</div>
				</div>
</div>
			";
		}
?>
	
<?php 

if (!empty($spaceid)){
	$disable = "";
	$spaceinfo = $spark->roomGetDetails($botid, $spaceid);
	if (!isset($spaceinfo['title'])) {
		redirect("index.php?id=spaces&botid=$botid");
	}
	$spacetype = (issetor($spacetype_const) == "teamspaces") ? "group":$spacetype_const;
	$membershipinfo = $spark->membershipGet($spaceid, $botid);
	$team_link = ($teamid != "") ? "teamid=$teamid&":""; 
	$baselink = "index.php?id=spaces&botid=$botid&viewspace=$spaceid&{$team_link}spacetype=$spacetype_const"; 
	$joinable = ($db_local->adminCheckJoinableSpace($botid, $spaceid)) ? "<a class='btn btn-success' href='$baselink&setunjoinable'>Make unjoinable</a>":"<a class='btn btn-success' href='$baselink&setjoinable'>Make joinable</a>";
	$groupresponse = ($db_local->adminCheckGroupResponseAcl($botid, $spaceid)) ? "<a class='btn btn-success' href='$baselink&unsetgroupresponse'>Disable space response</a>":"<a class='btn btn-success' href='$baselink&setgroupresponse'>Enable space response</a>";
	$groupoptions = $generate->groupLinks("options");
	$isteam = (isset($spaceinfo['teamId']));
	$isteam_light = onoff($isteam);
	$islocked = issetor($spaceinfo['isLocked']);
	$islocked_light = onoff($islocked);
	$isbot_space = $spark->membershipGet($spaceid, $botid, $botinfo[0]['emails']);
	$isbot_space_test = (!empty($isbot_space['items'])) ? true:false;
	if ($isbot_space_test) {
		$messageinfo = $spark->messageFetchMessages($botid, $spaceid, $spacetype);
		$mod = ($isbot_space['items'][0]['isModerator'] or !$islocked) ? true:false;
		$isbot_moderator_light = onoff($mod); 
	} else {
		$isbot_moderator_light = onoff(false);
		$mod = false;
	}
	if ($isteam) {
		$spacedelete = "<a class='btn btn-warning' href='index.php?id=spaces&botid=$botid&{$team_link}spacetype=$spacetype_const&spacedelete=$spaceid' $link_confirm style='color: $neg_color;'>Archive space</a>";
		$team_membership = $spark->roomGet(array("sender"=>$botid,"max"=>$maxresults,"teamId"=>$spaceinfo['teamId'],"type"=>"group"));
		$isbot_member = (isset($team_membership['items']) and count($team_membership['items'])) ? true:false;
		if ($isbot_member and $isbot_space_test) $messageinfo = $spark->messageFetchMessages($botid, $spaceid, $spacetype);
		$isbot_member_output = "<div class='col-md-3'>{$tooltips['botTeam']} Bot is part of the team:</div>";
		$isbot_member_output .= "<div class='col-md-3'>" . onoff($isbot_member) . "</div>";
		$isbot_space_output = "<div class='col-md-3'>{$tooltips['botInSpace']} Bot is in the space:</div>";
		$isbot_space_output .= ($isbot_space_test) ?  "<div class='col-md-3'>" . onoff(true) . "</div>":"<br><div class='col-md-3'><a href='$baselink&joinbot={$botinfo[0]['emails']}' class='btn btn-warning'>Bot is not in this team space, click here to join!</a></div>";
		if (!$mod) {
			$spacedelete = "";
			$joinable = "";
			$groupresponse = "No options available";
			$disable = "disabled";
		}	
	} else {
		$spacedelete = "<a class='btn btn-danger' href='index.php?id=spaces&botid=$botid&{$team_link}spacetype=$spacetype_const&spacedelete=$spaceid' $link_confirm>Delete space</a>";
		if ($spacetype_const == "direct" or !$mod) {
			$spacedelete = "";
			$joinable = "";
			$groupresponse = "No opions available";
			$disable = "disabled";
			$mod = false;
		}
		$isbot_space_output = "";
		$isbot_member = "";
	}
	$convoWidget = "
    <script src='https://code.s4d.io/widget-space/production/bundle.js'></script>
	<link rel='stylesheet' href='https://code.s4d.io/widget-space/production/main.css'>
    <div id='my-webexteams-widget' style='width: 100%; height: 500px;'></div>
	<script>
	  var widgetEl = document.getElementById('my-webexteams-widget');
	  // Init a new widget
	  ciscospark.widget(widgetEl).spaceWidget({
		accessToken: '". $spark->authGet($botid) ."',
		destinationId: '$spaceid',
			destinationType: 'spaceId'
	  });
	</script>";
	
	echo "
		<!-- left column -->
		
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>Space settings and members</h3>
			</div>
			<div class='card-body'>
				<div class='row'>
					<div class='col-md-3'>
						{$tooltips['spaceLocked']} Space is locked:
					</div>
					<div class='col-md-3'>
						$islocked_light
					</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>
						{$tooltips['botModerate']} Bot can moderate space:
					</div>
					<div class='col-md-3'>
						$isbot_moderator_light
					</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>
						{$tooltips['team']} Space is part of team:
					</div>
					<div class='col-md-3'>
						$isteam_light
					</div>
				</div>
				<div class='row'>
						$isbot_member_output
				</div>
				<div class='row'>
						$isbot_space_output
				</div>
			
				" . $generate->roomGenMembership($botid, $membershipinfo, $spacetype_const, $teamid, $mod) . "
			</div>
		</div>
		</div>

		<div class='col-lg-6'>
			<div class='card card-primary card-outline'>
				<div class='card-header'>
					<h3 class='card-title'>{$tooltips['actions']} Space actions</h3>
					<div class='card-tools'>
						$joinable
						$groupresponse	
						$spacedelete
					</div>
				</div>
				<div class='card-body'>
				{$tooltips['titleChange']} <label for='spacetitle' class='control-label'>Space title:</label><br>	
				<form method='post' action='$baselink' enctype='multipart/form-data'>
				<div class='row'>
					<div class='col-md-9'>
						<input type='text' $disable name='spacetitle' id='spacetitle' value='{$spaceinfo['title']}' class='form-control'>			
					</div>	
					<div class='col-md-3'>
						<input type='submit' class='btn btn-primary btn-block' $disable name='settitle' value='Change title'>
					</div>
				</div>
				</form>	
				{$tooltips['addFromGroup1']} <label for='groups' class='control-label'>Add people to space from group:</label><br>
				<form method='post' action='$baselink' enctype='multipart/form-data'>
					<div class='row'>
						<div class='col-md-9'>
							<select name='groups' id='groups' class='form-control' $disable>
								<option value='' selected>Add groups of people</option>
								$groupoptions
							</select>
						</div>
						<div class='col-md-3'>
							<input type='submit' class='btn btn-primary btn-block' value='Add users to space' $disable name='addpersontospace'>
						</div>
					</div>
				</form>
				{$tooltips['bulkAdd1']} <label for='groups' class='control-label'>Add people manually:</label><br>
				<form method='post' action='$baselink' enctype='multipart/form-data'>
					<div class='row'>
						<div class='col-md-9'>
							<textarea $disable class='form-control' placeholder='CSV format: email,email,email..' name='personemail'></textarea> 
						</div>
						<div class='col-md-3'>
							<input type='submit' class='btn btn-primary btn-block' value='Add users to space' $disable name='addpersontospace'>
						</div>
					</div>	
				</form>
			</div>
		</div>

		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>{$tooltips['messages']} Messages</h3>
			</div>
			<div class='card-body'>";
			$messageinfo = $spark->messageFetchMessages($botid, $spaceid, $spacetype);
			if ($spacetype_const == 'direct') {
				echo (count($messageinfo)) ? $generate->messageConversation($messageinfo, $botid):feedbackMsg("Sorry!", "could not fetch messages", "warning");
			} else {
				echo (($isteam and $isbot_space_test) or (!$isteam)) ? $generate->messageConversation($messageinfo, $botid):feedbackMsg("Sorry!", "could not fetch messages", "warning");
			}	
			echo "
			</div>
		</div>

	</div>
</div>	
";

}
?>

</div>
</div>