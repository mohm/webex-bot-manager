<?php if ($windowid != "login" and !verify()) header("Location: login.php"); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Devices</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
	<div class="container-fluid">

<?php
$botid = issetor($_GET['botid']);
$deviceid = issetor($_GET['deviceid']);

$result = $commandkey = "";

if (empty($botid)) {
	$disable = "disabled";
}

$filter = (isset($_POST['status'])) ? strtolower(str_replace(" ", ".", issetor($_POST['filter']))):'';

if (isset($_POST['command'])) {
	$commandkey = strtolower(str_replace(" ", ".", issetor($_POST['commandkey'])));
	$args = issetor($_POST['args']);
} else {
	
}
?>
<br>
 
		<div class="row">
			<div class="col-lg-3">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Select Bot to see controlled devices</h3>
					</div>
					<div class="card-body">
						<?php $generate->botGenSelector('devices', issetor($botid), $_SESSION['userid']); ?>
					</div>
				</div>
			</div>
		</div>	
		<?php $disable = (empty($executor)) ? "disabled" : ""; ?>

		<?php
			if ($botid) {
				$botname = $db_local->botGetName($botid);
				echo "
				<div class='row'>
					<div class='col-lg-12'>
						<div class='card card-primary card-outline'>
							<div class='card-header'>
								<h3 class='card-title'>Devices controlled by <b>{$botname}</b></h3>
							</div>
							<div class='card-body'>";
						
								$devices = $spark->deviceList(issetor($botid));
								
									if (count($devices['items']) > 0) {
										echo "<table width='100%' id='devices' class='table table-bordered table-striped'>
												<thead>
													<tr>
									 					<th>Display Name</th>
									 					<th>Product</th>
									 					<th>Device IP</th>
									 					<th>Software</th>
									 					<th>Connection Status</th>
									 					<th>Tags</th>
									 					<th>Actions</th>
												</thead>
												<tbody>";
												foreach ($devices['items'] as $key => $value) {
											$selected = (issetor($_GET['deviceid']) == $value['id']) ? "linkblock-selected":"";
											$connectionstatus = $value['connectionStatus'];
											$devid = $value['id'];
											echo "<tr>
													<td>{$value['displayName']}</td>
													<td>{$value['product']}</td>
													<td>{$value['ip']}</td>
													<td>{$value['software']}</td>
													<td>{$value['connectionStatus']}</td>
													<td>";
													if(!empty($value['tags']))
													{
														foreach($value['tags'] as $key => $value)
														{
															echo "<font color='blue'><i class='fas fa-tag'></i></font> " . $value . "<br>";
														}
													}
												echo "</td>
												<td><a href='index.php?id=devices&botid=$botid&deviceid={$devid}' title='Enable device'><i class='fas fa-external-link-alt'></i></a></td>";
										}
										echo "</tbody>
											<tfoot>
									            <tr>
									            	<th>Display Name</th>
								 					<th>Product</th>
								 					<th>Device IP</th>
								 					<th>Software</th>
								 					<th>Connection Status</th>
								 					<th>Tags</th>
								 					<th>Actions</th>
									            </tr>
									        </tfoot>
										</table>";
									 }
									else {
										echo 'No devices controlled by this bot, please give access to a bot via Control Hub';
									}
							echo '</div>
								</div>
							</div>
						</div>';
							}
						?>

				<?php
					if(!empty($_GET['deviceid']) and !empty($_GET['botid']) ) {
							if ($filter) {
								$status = $spark->deviceStatus($botid, $deviceid, $filter);
							}
							if ($commandkey) {
								$result = $spark->deviceCommand($botid, $deviceid, $commandkey, $args);
							}
							
						$deviceid = $_GET['deviceid'];
						foreach ($devices['items'] as $key => $value) {
							if($deviceid == $value['id'])
							{
								$DeviceName = $devices['items'][$key]['displayName'];
							}
						}
						
						echo '
						<div class="row">
							<div class="col-lg-12">
								<div class="card card-primary card-outline">
									<div class="card-header">
										<h3 class="card-title">'. $DeviceName .'</h3>
									</div>
									<div class="card-body">
									        <div class="row">
												<div class="col-lg-4">
													<form id="command" method="post" action="#command" enctype="multipart/form-data">
											        	<h5>Device commands</h5>
											        		<div class="row mb-2 mb-md-1">
											            		<input type="text" name="commandkey" class="form-control form-control-sm" required value="'.issetor($commandkey).'" placeholder="presentation.start">
											            	</div>
											            	<div class="row mb-2 mb-md-1">
											            		<textarea cols="40" rows="4" name="args" class="form-control form-control-sm" placeholder="ConnectorId:[1,2]">'.issetor($args).'</textarea>
										            		</div>
										            		<div class="row mb-2 mb-md-1">
											           			<input type="submit" name="command" class="btn btn-sm btn-primary" value="Send command">
										           			</div>    
													</form>';
													if (isset($result)) {
														print_r($result);
													}
											echo '</div>
												<div class="col-lg-2">
												</div>
										        <div class="col-lg-4">
										        	<form id="status" method="post" action="#status" enctype="multipart/form-data">
										            	<h5>Device status</h5>
										            	<div class="row mb-2 mb-md-1">
										            		<input type="text" name="filter" class="form-control form-control-sm" placeholder="systemunit.*" value="'. issetor($filter) .'">
									            		</div>
									            		<div class="row mb-2 mb-md-1">
										            		<input type="submit" name="status" class="btn btn-sm btn-primary" value="Get status">
									            		</div>
									            	</form>';
									            	if (isset($status['result'])) {
														echo statusdigger($status['result']);
													} else {
														echo "No results found on status filter, please check that the filter is correct. Wildcards are allowed, for example: Audio Input *, audio.input.*, audio.*.connectors.*. If the command is incomplete you must always end the filter with a .*";
													}
											echo '</div>
									        </div>
										</div>
									</div>
								</div>
							</div>';
					}
				?>
				
	</div>
</div>


