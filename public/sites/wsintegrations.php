<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 

$integration = issetor($_GET['int']);
$botid = issetor($_GET['botid']);
$activated = 0;

$disabled_on_activated = "";

$home = 'index.php?id=wsintegrations';

if (isset($_POST['download_manifest'])) {
    redirect("/api/handlers/ws_integration_manifest.php?appId={$_POST['wsintid']}");
}

//PAGE OPERATIONS
if (isset($_POST['wsint_add'])) {
	$id = get_guid();
	$name = $db_local->quote($_POST['name']);
	
	$data = array(
	    'id'=>$id,
	    'name'=>$name,
	    'description'=>"Change me!"
	);
			
	$db_local->wsIntegrationAdd($data);
	
	redirect($home."&int=$id");
	
}

if (isset($_POST['wsint_refresh'])) {
    if (!$integration) {
        redirect($home);
    }
    
    $spark->wsIntegrationRefreshAccessToken($integration);
}

if (isset($_POST['wsint_test'])) {
    $connectionTest = array(
        'status'=>''
    );
    $connection = false;
    
    if (!$integration) {
        redirect($home);
    }
    
     $result = $spark->wsIntegrationGetAppManifest($integration);
          
     if ($result['id'] == $integration) {
         $connectionTest['status'] = "Connection to {$result['customer']['name']} was successful";
         $connection = true;
     } else {
        $connectionTest['status'] =  "Connection with Control Hub integration failed";
     }
     
     echo alerts(($connection) ? "success":"error", ($connection) ? "Success":"Failed", $connectionTest['status']);
     
     $db_local->wsIntegrationUpdate($integration, $connectionTest);
}

if (isset($_POST['wsint_update'])) {
    if (!$integration) {
        redirect($home);
    }
        
    $_POST['data_collection'] = issetor($_POST['data_collection'], "0");
    $_POST['webhook_logging'] = issetor($_POST['webhook_logging'], "0");
    $_POST['actions_logging'] = issetor($_POST['actions_logging'], "0");
    
    $data = $db_local->quoteArray($_POST);
        
    $db_local->wsIntegrationUpdate($_POST['wsintid'], $data, 'webform');
    
}


if (isset($_POST['wsint_delete'])) {
    $spark->wsIntegrationPatchIntegration($_POST['wsintid'], array("provisioningState"=>"error"));
    $db_local->wsIntegrationDelete($_POST['wsintid']);
    redirect($home);
}

if (isset($_POST['ws_webhooks_clear'])) {
    
    $db_local->wsIntegrationWebhookLogClear($_POST['wsintid']);
     
}

if (isset($_POST['ws_devices_clear'])) {
    
    $db_local->wsIntegrationDeviceCollectorClear($_POST['wsintid']);
    
}

if (isset($_POST['ws_actions_clear'])) {
    
    $db_local->wsIntegrationActionLogClear($_POST['wsintid']);
    
}

if (isset($_POST['patchSignageConfig'])) {
    unset($_POST['patchSignageConfig']);
    $res = $spark->wsIntegrationUpdateSignageConfig($integration, $_POST);
    echo alerts(($res['http_code'] == 204) ? "success":"error");
}

if (isset($_POST['deleteSignageConfig'])) {
    $res = $spark->wsIntegrationDeleteSignageConfig($integration);
    echo alerts(($res['http_code'] == 204) ? "success":"error");
}

if (isset($_POST['patchPWAConfig'])) {
    unset($_POST['patchPWAConfig']);
    $res = $spark->wsIntegrationUpdatePWAConfig($integration, $_POST);
    echo alerts(($res['http_code'] == 204) ? "success":"error");
}

if (isset($_POST['deletePWAConfig'])) {
    $res = $spark->wsIntegrationDeletePWAConfig($integration);
    echo alerts(($res['http_code'] == 204) ? "success":"error");
}

if (isset($_POST['queueSetup'])) {
    unset($_POST['queueSetup']);
        
    $queue = array(
        "queue" => array(
            "state" => issetor($_POST['queueState'], 'enabled')
        )
    );
        
    if ($integration) {
        $res = $spark->wsIntegrationPatchIntegration($integration, $queue);
        echo alerts(($res['http_code'] == 200) ? "success":"error");
    }
        
}

// UPDATE MANIFEST REQUEST

if (isset($_POST['updateManifestRequest'])) {
    
    $commands = $events = $status = $scopes = [];
    
    unset($_POST['updateManifestRequest']);
    settype($POST['manifestVersion'], 'integer');
    
    $commands_arr = json_decode(issetor($_POST['commands'], []), true);
    $status_arr = json_decode(issetor($_POST['status'], []), true);
    $events_arr = json_decode(issetor($_POST['events'], []), true);
    $scopes_arr = json_decode(issetor($_POST['apiAccess'], []), true);
        
    $availableFeatures = array(
      'digital_signage' => array(
          'scope'=> 'spark-admin:devices_digital_signage',
          'access'=> 'required'
      ),
      'persistent_web_app' => array(
          'scope' => 'spark-admin:devices_pwa',
          'access'=> 'required'
      )
    );
    
    
    
    if (isset($_POST['features'])) {
        foreach ($availableFeatures as $feature => $scope) {
            if (in_array($feature, $_POST['features'])) {
                if (!in_array($scope['scope'], array_column($scopes_arr, 'scope'))) {
                    array_push($scopes_arr, $scope);
                } 
            }
            
        }
    }
    
    $provisioningType = (issetor($_POST['provisioning'])) ? $_POST['provisioning'] : "https";
    
    if ($provisioningType == "https") {
        $prov = array(
            "type" => "https",
            "url" => getHostUrl()."/api/handlers/ws_integration_activate.php"
        );
    } else {
        $prov = array(
            "type" => "manual"
        );
    }
        
    $_POST['apiAccess'] = $scopes_arr;
    $_POST['xapiAccess'] = array(
      "commands" => $commands_arr,
      "status" => $status_arr,
      "events" => $events_arr
    );
    if (!count($commands_arr)) {
        unset($_POST['xapiAccess']['commands']);
    }
    if (!count($status_arr)) {
        unset($_POST['xapiAccess']['status']);
    }
    if (!count($events_arr)) {
        unset($_POST['xapiAccess']['events']);
    }
    
    $_POST['provisioning'] = $prov;
     
    unset($_POST['commands']);
    unset($_POST['status']);
    unset($_POST['events']);
    
    //var_dump($_POST);

        
    $base = array(
        "actionsUrl" => getHostUrl()."/api/handlers/ws_integration_action.php",
        "updateRequest" => array(
            "newManifest" => $_POST
        )
    );
    
   
    
    echo "<pre>".json_encode($base, JSON_PRETTY_PRINT)."</pre>";
    
    if ($integration) {
        $res = $spark->wsIntegrationPatchIntegration($integration, $base);
        echo alerts(($res['http_code'] == 200) ? "success":"error", "Manifest update request", codeBlock(json_encode($res)));        
    }
    
    
}

// UPDATE MANIFEST REQUEST \\

if (isset($_POST['patchManifest'])) {
    unset($_POST['patchManifest']);
    
    $secret = sha1(generatePassword());
        
    $webhook = array(
        "webhook" => (!issetor($_POST['webhook'])) ? array("type" => "none") : array(
            "targetUrl" => $_POST['webhookUrl'],
            "secret" => $secret,
            "disabled" => False
        )
    );
    
    if ($_POST['webhook'] == 'disabled') {
        $webhook = array(
            "webhook" => array(
                "targetUrl" => $_POST['webhookUrl'],
                "secret" => $secret,
                "disabled" => True 
            )
        );
    }
    
    $base = array(
        "provisioningState" => "completed", #required
        "actionsUrl" => (!issetor($_POST['action'])) ? null : $_POST['action'] #optional       
    );
    
    if ($integration) {
        
        $base['webhook'] = $webhook['webhook']; 
        
        $res = $spark->wsIntegrationPatchIntegration($integration, $base);
      
        echo alerts(($res['http_code'] == 200) ? "success":"error");
        
        if (isset($webhook['webhook']['secret'])) {
            echo $db_local->wsIntegrationUpdate($integration, array("whs" => $secret));
        }
    }
}

if (isset($_POST['wsint_close'])) {
 
    redirect($home);

}



if ($integration) {
		$actionname = "wsint_update"; 
		$submitname = "Apply changes";
		$edit = "Update integration (appId: <b>$integration</b>)";
		$details = $db_local->wsIntegrationGet($db_local->quote($integration));
		
		
		
		
		
		
		if (count($details)) {
		  
		    $details = $details[0];
		
		} else { 
		   
		    redirect("index.php?id=wsintegrations"); 
		
		}
		
		$activated = $details['activation_status'];
		
		$disabled_on_activated = ($activated) ? "disabled":"";
		
		$groupoptions = $generate->groupOptions($details['accessgroup']);
		
		//var_dump($details);
		
		$created_by = "<input type='hidden' value='{$_SESSION['email']}' name='created_by'>";
} else {
		
    $actionname = "wsint_add";
	$submitname = "Add";
	$edit = "Create a workspace integration";
	$groupinfo = array("0"=>array("id"=>"", "groupname"=>""));
	$form = "";
	
}

//Page tooltips
$tooltips = array(
        'oauth'=>tooltip('Oauth','When you upload the integration manifest to Webex Control Hub you will receive an Oauth client/secret pair. It is required to insert the oauth details before activation can begin. After activation, you cannot update the oauth pair anymore.'),
        'intName'=>tooltip('Integration name','The name of your integration is set by you when you create one, but after activation it will reflect the name from the manifest'),
		'intCreate'=>tooltip('Workspace Integrations','Workspace integrations allow you to integrate a machine account with an organization, giving it fine grained access to different Webex APIs.'),
		'intCreated'=>tooltip('Created workspace integrations','Here is a list of your existing workspace integrations. Click on one to see the bigger picture.'),
        'featureSignage'=>tooltip('Digital Signage','If you enable digital signage you can set a signage URL that can be provisioned to devices in the organization. Note that WBM is only managing the Workspace Integration and not hosting the signage service. Since WBM is managing the Workspace Integration you may apply the signage configuration URLs from WBM. Note that you cannot enable Digital Signage after the workspace integration has been created, you must choose before adding the integration.'),
        'signageUrl'=>tooltip('Digital Signage URL','Set the signage URL to your signage service (third party service). You can use placeholders according to the workspace integration guide. This field is required for updating the digital signage config'),
        'signageAssignUrl'=>tooltip('Assign Content URL','Set the URL to where the integrator can assign content to devices (third party service) - optional'),
        'signageManageUrl'=>tooltip('Manage Content URL','Set the URL to where the integrator can manage content (third party service) - optional'),
        'pwaUrl'=>tooltip('Persistent Web App URL','Set the Persistent Web App URL to your service (third party service). You can use placeholders according to the workspace integration guide. This field is required for updating the pwa config'),
        'pwaAssignUrl'=>tooltip('Assign Content URL','Set the URL to where the integrator can assign content to devices (third party service) - optional'),
        'pwaManageUrl'=>tooltip('Manage Content URL','Set the URL to where the integrator can manage content (third party service) - optional'),
		'accessGroup'=>tooltip('Access Group','When mounting integrations via bots, you have to be part of the selected access group to use the integration. This is only valid if the "Workspace integration" feature is enabled on a bot.'),
		'settings'=>tooltip('Settings','Here you can enable logging and storing of webhooks and or devices. Note that storing actions and webhooks may take a lot of space in the database and should only be used for verification of receiving webhooks. By enabling collecting devices, WBM will still receive webhooks and create a device when a webhook is received, it will always replace the latest status on the device with the new that was received. Information can be accessed and used from WBM via your own app.'),
		'status'=>tooltip('Integration status','Here is an overview of status messages and other actions.'),
        'appManifest'=>tooltip('App manifest','This document reflects what is currently active on the Webex side'),
        'serverManifest'=>tooltip('Webex server manifest','This is the current manifest that is uploaded to the webex side.'),
        'upgradeManifest'=>tooltip('Upgrade manifest','If you need to update scopes or other details in the manifest, you can edit the manifest here and send an update request to the server. This requires an admin in the organization to approve the update before the changes takes effect. Approvals only apply to access changes, not metadata like descriptions etc.'),
        'queue'=>tooltip('Queue','By enabling the queue you will get a URL that you can use for long polling. Useful for apps that do not have a public webhook receiver. Cannot be enabled at the same time as webhooks, please disable either one before enabling the other. '),
        'signageUpdate'=>tooltip('Update signage config','If you have configured a Digital Signage URL to your integration above, you can apply the configurations by pressing the Update Digital Signage config button. The URLs are stored locally until applied. You can delete the webex side configuration by pressing Delete Digital Signage config, this will not delete the locally stored URLs and can be applied again later.'),
        'webhooks'=>tooltip('Webhooks','You can disable or enable webhooks on the Webex side. As long as this is enabled, Webex will continue to send webhooks to your configured URL even though WBM is not collecting it. If you do not need to collect data from devices, you should consider disabling webhooks to reduce traffic. You can use queue as an alternative but cannot be used at the same time.'),
		'manifest'=>tooltip('Integration Manifest','You need a manifest to create an integration. Click the download button and you will get a pre-generated manifest for this integration. Remember that integrations are unique so you cannot put one integration to many organizations. You have to create more, and you can with WBM. You should modify the downloaded manifest to suite your needs, but keep in mind that some WBM bot features may not work if you remove the pre-defined access scopes and you may not have access to all the commands and configurations. After activation, you must use the Manifest controls and features to update the manifest on the server side.'),
		'activation'=>tooltip('Activation','When we have received an activation request from Control Hub the integration will be active'),
		'accesstoken'=>tooltip('Access Token','This token is used to invoke Webex APIs using the scopes granted via the integration manifest. You can test the connection to the integration on the Webex side by pressing "Test". If there is an error, then the integration may have been deleted or deactivated. (WBM should detect this automatically)'),
		'refreshtoken'=>tooltip('Refresh Token','Used to refresh the access token that has a short lifespan. Do not worry, WBM refreshes the token automatically when needed, but you can also do it manually by clicking the "Refresh" button.'),
		'lastStatus'=>tooltip('Last status','Contains a string for what have just happened. For example if you do a health check from Control Hub, it will say here. Or if the integration was just activated etc.'),
		'webhookscollected'=>tooltip('Webhooks collected','Gives you the number of webhooks stored in the database for this integration. Storing webhooks should only be used to track events as they happen, WBM does not use this data for anything other than linking them to an integration and a specific device'),
		'actionscollected'=>tooltip('Actions collected','Gives you the number of action requests stored in the database. Used for logging purposes and linking a specific action to an integration.'),
		'devicescollected'=>tooltip('Devices collected','This is what is actually makes the data useful. WBM is receiving webhooks all the time from the integration. When WBM receives a webhook from a device that is not on record, it will automatically get the device details and link it with the current status that it received, this is updated everytime WBM receives a webhook. The data collected can be used by other apps to monitor certain devices or create an overview. You do not require to store webhooks to use this as it is a separate collector. You app requires to authenticate with your WBM API token to get access to the data. Click View API and copy the link. '),
		'org'=>tooltip('Organization','The name of the organization that activated the workspace integration')
);

?>
<script language="JavaScript">
function toggleMember(source, name) {
	  checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggleNonMember(source, name) {
	checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	} 
	
</script>
<!-- Content Header (Page header) -->
<div class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1 class="m-0 text-dark">Workspace integrations</h1>
         </div><!-- /.col -->
      </div><!-- /.row -->
   </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
   <div class="container-fluid">
<div class='row'>
         <div class='col-lg-6'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'><?php echo $tooltips['intName']; ?> <?php echo $edit; ?></h3>
               </div>
               <div class='card-body'>
					<form name='groups' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>		
					<div class='form-group'>
					</div>
					<?php 
					
					
					echo "
					<div class='form-group'>
						<label>{$tooltips['intName']} Integration name</label>
						<br>
						<input type='text' class='form-control' placeholder='Integration name' $disabled_on_activated required name='name' value='".issetor($details['name'])."'>";
  					
  								
  				
					echo "</div>
					<div class='box-footer'>";
					
					
				        
					
												
						if ($integration) {
						    
						    
						    $spark->wsIntegrationCheckExpiredAndRefresh($integration);
						    					    						    
						    $jwt = issetor($details['activation_jwt']);
						    
						    $wsinttest = $db_local->wsIntegrationInfoAPI($integration);
						    //echo json_encode($wsinttest, JSON_PRETTY_PRINT);
						    
						    
						    if ($jwt) {
						      //$jwt_info = jwtDecode(json_decode($details['activation_jwt'], true), 1);
						    }
						    
							?>
							<label><?php echo $tooltips['oauth'];?> Oauth client</label>
							<input type='text' name='client' placeholder='Client ID' class='form-control' <?php echo $disabled_on_activated; ?> value='<?php echo issetor($details['client']); ?>'><br>
							<label><?php echo $tooltips['oauth'];?> Oauth secret</label>
							<input type='text' name='secret' placeholder='Client Secret' class='form-control' <?php echo $disabled_on_activated; ?> value='<?php echo issetor($details['secret']); ?>'><br>
							
							<label><?php echo $tooltips['accessGroup'];?> Accessgroup</label>
							<select class='form-control' name='accessgroup'>
							<option value='0'>No access group selected</option>
							<?php  
							echo $generate->groupOptions(issetor($details['accessgroup'])); 
							?></select><br>
														
							<label><?php echo $tooltips['settings'];?> Settings</label>
							<div class="custom-control custom-switch" >
  								<input type="checkbox" class="custom-control-input" id="data_collection" name="data_collection" <?php echo ($details['data_collection']) ? "checked":""; ?> value='1'>
  								<label class="custom-control-label" for="data_collection">Collect devices from webhooks for this app</label><br>
							</div><br>
							<div class="custom-control custom-switch">
  								<input type="checkbox" class="custom-control-input" id="webhook_logging" name="webhook_logging" <?php echo ($details['webhook_logging']) ? "checked":""; ?> value='1'>
  								<label class="custom-control-label" for="webhook_logging">Log incoming webhooks for this app</label><br>
							</div><br>
							<div class="custom-control custom-switch">
  								<input type="checkbox" class="custom-control-input" id="actions_logging" name="actions_logging" <?php echo ($details['actions_logging']) ? "checked":""; ?> value='1'>
  								<label class="custom-control-label" for="actions_logging">Log incoming actions for this app</label><br>
							</div><br>
							
							
							<label><?php echo $tooltips['status'];?> Integration status</label>
							
							<table class='table'>
    							<tr>
    								<td style="vertical-align: middle;">
    									<label><?php echo $tooltips['manifest'];?> Integration Manifest</label>
    										<td>	
    											
    											<td>
    												<?php echo ($activated) ? "Already activated, use the edit button below":"<input type='button' style='margin-left: 2px' class='btn btn-primary' onClick='parent.location=\"/api/handlers/ws_integration_manifest.php?appId={$integration}\"' value='Download example for upload to Control Hub' formtarget='_blank'> "; ?>
    							<tr>
    								<td>
    									<label><?php echo $tooltips['activation'];?> Activation</label>
    										<td>
    											<?php echo statusBoolPill($details['activation_status'], 'Activated', 'Not activated');?>
    											<td>	
    												
    							
    							<tr>
    								<td>
    									<label><?php echo $tooltips['accesstoken'];?> Access token</label>
    										<td>
    										<?php 
        											
        											if (!empty($details['access_token'])) {
        											    
        											     echo statusBoolPill(($wsinttest['token']['at']['expired']) ? False:True, "Valid for {$wsinttest['token']['at']['days_left']} days", "Expired {$wsinttest['token']['at']['days_left']} days");
        											} else {
        											    echo statusBoolPill(false, "", "Access token missing"); 
        											}
        											?>
        										<td>
        											<?php echo (!empty($details['refresh_token'])) ? "<input type='submit' class='btn btn-primary float-left' style='margin-right: 2px' name='wsint_test' value='Test'>":""; ?>
        											
    							<tr>
    								<td>
    									<label><?php echo $tooltips['refreshtoken'];?> Refresh token</label>
    										<td>
    											<?php 
    											if (!empty($details['access_token'])) {
    											    echo statusBoolPill(($wsinttest['token']['rt']['expired']) ? False:True, "Valid for {$wsinttest['token']['rt']['days_left']} days", "Expired {$wsinttest['token']['at']['days_left']} days");
    											} else {
    											
    											    echo statusBoolPill(false, "", "Refresh token missing"); 
    											}
    											
    											?>
    											<td>
    												<?php echo (!empty($details['refresh_token'])) ? "<input type='submit' class='btn btn-primary float-left' style='margin-right: 2px' name='wsint_refresh' value='Refresh'>":""; ?>
    							<tr>			
    								<td>
    									<label><?php echo $tooltips['lastStatus'];?> Last status</label>
    										<td>
    											<?php echo issetor($details['status'], 'No status recorded');?>
    												<td>
    							<tr>
    								<td>
    									<label><?php echo $tooltips['org'];?> Integrated with organization</label>
    										<td>
    											<?php echo issetor($details['organization'], 'Unknown');?>
    												<td>
    							<tr>
    								<td>
    									<label><?php echo $tooltips['webhookscollected'];?> Webhooks collected</label>
    										<td>
    											<?php 
    											
    											$whcount = count($db_local->wsIntegrationWebhookLogGet($integration));
    											
    											echo $whcount;
    											
    											?>
    												<td>
    													<?php if ($whcount) {
    													    echo "<a href='/api/v1/ws.php?appId=$integration&wh' class='btn btn-primary float-left' style='margin-right: 2px' target='_blank'>View API</a>";
    													    echo "<input type='submit' class='btn btn-danger float-right' style='margin-right: 2px' $link_confirm name='ws_webhooks_clear' value='Clear'>";
    													}?>
    												
    							<tr>
    								<td>
    									<label><?php echo $tooltips['actionscollected'];?> Actions collected</label>
    									<td>
    										<?php 
    											
    											$actioncount = count($db_local->wsIntegrationActionLogGet($integration));
    											
    											echo $actioncount;
    											
    											?>
    												<td>
    												<?php if ($actioncount) {
    													    echo "<a href='/api/v1/ws.php?appId=$integration&actions' class='btn btn-primary float-left' style='margin-right: 2px' target='_blank'>View API</a>";
    													    echo "<input type='submit' class='btn btn-danger float-right' style='margin-right: 2px' $link_confirm name='ws_actions_clear' value='Clear'>";
    													}?>
    												
    							<tr>
    								<td>
    									<label><?php echo $tooltips['devicescollected'];?> Devices collected</label>
    										<td>
    											<?php 
    											
    											$devcount = count($db_local->wsIntegrationDeviceCollectorGetDevices($integration));
    											
    											echo $devcount;
    											
    											?>
    												<td>
    												<?php if ($devcount) {
    													    echo "<a href='/api/v1/ws.php?appId=$integration&devices' class='btn btn-primary float-left' style='margin-right: 2px' target='_blank'>View API</a>";
    													    echo "<input type='submit' class='btn btn-danger float-right' style='margin-right: 2px' $link_confirm name='ws_devices_clear' value='Clear'>";
    													}?>
							
								<tr>
									<td style="vertical-align: middle;">
										Actions
											<td>
											
                                           	<td>
							</table>					

							<?php 
						}
						
						echo "<input type='submit' name='$actionname' class='btn btn-primary' value='{$submitname}' />";
						?>
						
						
						
						<?php 
						
						if ($integration) {
						    $spark->wsIntegrationCreateBotRepresentation($integration);
                            echo "<input type='hidden' name='wsintid' value='$integration'>";
                            echo "<a href='/api/v1/ws.php?appId=$integration&all' class='btn btn-primary' target='_blank'>View API</a>"; 
                            echo "<input type='submit' class='btn btn-danger float-right' style='margin-left: 2px' $link_confirm name='wsint_delete' value='Delete integration'>";
                            echo "<input type='submit' class='btn btn-danger float-right' name='wsint_close' value='Close'>";
						}?>
				</div>
				</form>
			</div>
		</div>
	</div>	
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'><?php echo $tooltips['intCreated']; ?> Workspace integrations</h3>
			</div>
			<div class='card-body'>
				<form name='dataentries' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>
				<table width='100%' id='wsint' class='table table-bordered table-striped'>
					<thead>
						<tr>
							<th width='60%'>Integration</th>
							<th>Health check</th>
							<th>Activation</th>
						</tr>
					</thead>
				</table>
				</form>
				
			</div>
		</div>
	</div>
	<?php if ($integration) { 
	    if (issetor($details['activation_status'])) {
	       	        
	        $result = $spark->wsIntegrationGetAppManifest($integration);
	        $wxSrvManifest = $spark->wsIntegrationGetWebexServerManifest($integration);
	        	        
	        $host = getHostUrl();
	    
	?>
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>Webex Server Side Controls</h3>
			</div>
			
			<div class='card-body'>
				<div class='row'>
				<div class='col-md-3'>
					<div class='card card-primary card-outline'>
						<div class='card-header'>
							<h3 class='card-title'>Webhook Setup</h3>
				
						</div>
        				<div class='card-body'>
        				<form method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>	
        				
        				<label><?php echo $tooltips['webhooks'];?> Webhooks</label><br>
        					<input type='hidden' name='action' value='<?php echo "{$host}/api/handlers/ws_integration_action.php"; ?>'>
        					<input type='hidden' name='webhookUrl' value='<?php echo "{$host}/api/handlers/ws_integration_webhook.php"; ?>'>
        					<input type='radio' name='webhook' value='enabled' <?php echo (isset($result['webhook']) and $result['webhook']['disabled']) ? "":"checked"; ?>> Enabled<br>
            				<input type='radio' name='webhook' value='disabled' <?php echo (isset($result['webhook']) and $result['webhook']['disabled']) ? "checked":""; ?>> Disabled<br>
            				<input type='radio' name='webhook' value='' <?php echo (isset($result['webhook'])) ? "":"checked"; ?>> Deactivated<br><br>
            			    <?php echo "<input type='submit' class='btn btn-primary' name='patchManifest' value='Apply'>";?> <br>
            		    </form>
            		   </div>
        			</div>
        		</div>
        		
			    <div class='col-md-3'>
			    <div class='card card-primary card-outline'>
						<div class='card-header'>
							<h3 class='card-title'>Queue for Polling</h3>
				
						</div>
						<div class='card-body'>
        			    <form method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>	
        				
        				<label><?php echo $tooltips['queue'];?> Queue</label><br>
        					<input type='radio' name='queueState' value='enabled' <?php echo (isset($result['queue']['state']) and $result['queue']['state'] == 'enabled') ? "checked":""; ?>> Enabled<br>
            				<input type='radio' name='queueState' value='disabled' <?php echo (!isset($result['queue']['state']) or $result['queue']['state'] == 'disabled') ? "checked":""; ?>> Disabled<br>
            				<input type='radio' name='queueState' value='remove' <?php echo (!isset($result['queue']['state'])) ? "checked":""; ?>> Deactivated<br><br>
        					
            			    <?php echo "<input type='submit' class='btn btn-primary' name='queueSetup' value='Apply'>";?> 
        			    </form>
        			    </div></div>
        		</div>
        		
        		<div class='col-md-5'>
			    <div class='card card-primary card-outline'>
						<div class='card-header'>
							<h3 class='card-title'>Manifest controls and features</h3>
				
						</div>
						<div class='card-body'>
        			    	
        				
        				<label><?php echo $tooltips['upgradeManifest'];?> Upgrade manifest </label><br>
        				
        					<!-- Digital Signage configuration -->
							<div class="modal fade" id="digital-signage">
                    			<div class="modal-dialog modal-md">
                    			  <div class="modal-content">
                    			    <div class="modal-header">
                    			      <h4 class="modal-title">Digital signage</h4>
                    			      <button type="button" class="close" aria-label="Close">
                    			        <span aria-hidden="true">&times;</span>
                    			      </button>
                    			    </div>
                    			    <div class="modal-body">
                    			      <form name='digitalSignage' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>
                    						<?php 
                    						$digitalSignageUrl = issetor(explode('?_',issetor($result['digitalSignageConfiguration']['signageUrl']))[0]);
                      								    echo "<label>{$tooltips['signageUrl']} Digital signage URL</label>
                      								    <br>
                      								    <input type='text' class='form-control' placeholder='Digital Signage URL' name='signage_base_url' value='".$digitalSignageUrl."'><br>
                                                        <label>{$tooltips['signageAssignUrl']} Assign Content URL</label>
                      								    <br>
                                                        <input type='text' class='form-control' placeholder='Assign Content URL' name='signage_assign_url' value='".issetor($result['digitalSignageConfiguration']['crossLaunch']['assignContent']['url'])."'><br>
                                                        <label>{$tooltips['signageManageUrl']} Manage Content URL</label>
                      								    <br>
                                                        <input type='text' class='form-control' placeholder='Manage Content URL' name='signage_content_url' value='".issetor($result['digitalSignageConfiguration']['crossLaunch']['manageContent']['url'])."'>";
                      								    
                      						
                      					?>
           
                    			    </div>
                    			    <div class="modal-footer justify-content-between">
                    			      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    			      <input type='submit' name='patchSignageConfig' class="btn btn-primary" value='Update signage config'><input type='submit' name='deleteSignageConfig' class="btn btn-danger" value='Delete signage config'>
                    			      </form>
                    			    </div>
                    			  </div>
                    			  <!-- /.modal-content -->
                    			</div>
                    			<!-- /.modal-dialog -->
                    			</div>
                    			<!-- / Digital Signage Configuration -->
                    			
                        		<!-- PWA configuration -->
    							<div class="modal fade" id="pwa-config">
                        			<div class="modal-dialog modal-md">
                        			  <div class="modal-content">
                        			    <div class="modal-header">
                        			      <h4 class="modal-title">Persistent Web Apps</h4>
                        			      <button type="button" class="close" aria-label="Close">
                        			        <span aria-hidden="true">&times;</span>
                        			      </button>
                        			    </div>
                        			    <div class="modal-body">
                        			      <form name='pwaConfig' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>
                        						<?php 
                        						$persistentWebAppUrl = issetor(explode('?_',issetor($result['persistentWebAppConfiguration']['pwaUrl']))[0]);
                          								    echo "<label>{$tooltips['pwaUrl']} Web App URL</label>
                          								    <br>
                          								    <input type='text' class='form-control' placeholder='Web App URL' name='pwa_base_url' value='".issetor($persistentWebAppUrl)."'><br>
                                                            <label>{$tooltips['pwaAssignUrl']} Assign Content URL</label>
                          								    <br>
                                                            <input type='text' class='form-control' placeholder='Assign Content URL' name='pwa_assign_url' value='".issetor($result['persistentWebAppConfiguration']['crossLaunch']['assignContent']['url'])."'><br>
                                                            <label>{$tooltips['pwaManageUrl']} Manage Content URL</label>
                          								    <br>
                                                            <input type='text' class='form-control' placeholder='Manage Content URL' name='pwa_content_url' value='".issetor($result['persistentWebAppConfiguration']['crossLaunch']['manageContent']['url'])."'>";
                          								    
	   
                          					?>
                          					
                        			    </div>
                        			    <div class="modal-footer justify-content-between">
                        			      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        			      <input type='submit' name='patchPWAConfig' class="btn btn-primary" value='Update PWA config'><input type='submit' name='deletePWAConfig' class="btn btn-danger" value='Delete PWA config'>
                        			      </form>
                        			    </div>
                        			  </div>
                        			  <!-- /.modal-content -->
                        			</div>
                        			<!-- /.modal-dialog -->
                        			</div>
                        			<!-- / PWA Configuration -->
							
        				
        					<!-- Manifest upgrade -->
    							<div class="modal fade" id="manifest-config">
                        			<div class="modal-dialog modal-lg">
                        			  <div class="modal-content">
                        			    <div class="modal-header">
                        			      <h4 class="modal-title">Workspace Integration Manifest</h4>
                        			      <button type="button" class="close" aria-label="Close">
                        			        <span aria-hidden="true">&times;</span>
                        			      </button>
                        			    </div>
                        			    <div class="modal-body">
                        			      <form name='pwaConfig' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>
                        						<?php 
                        						$checkSignage = (isset($result['features']) and in_array('digital_signage', $result['features'])) ? "checked":"";
                        						$checkPwa = (isset($result['features']) and in_array('persistent_web_app', $result['features'])) ? "checked":"";
                        					    $httpsSelected = ($wxSrvManifest['provisioning']['type'] == "https") ? "selected":"";
                        					    $manualSelected = ($wxSrvManifest['provisioning']['type'] == "manual") ? "selected":"";
                          								    
                        						            $newManifestVersion = $result['manifestVersion'] + 1;
                        						            
                          								    echo "
                                                                <label>appId (r)</label>
                                                                <input type='text' class='form-control' placeholder='id' name='id' readonly value='".issetor($wxSrvManifest['id'])."'><br>
                                                                <label>manifestVersion (r) - next version:</label>
                                                                <input type='text' class='form-control' placeholder='Version' name='manifestVersion' readonly value='".issetor($newManifestVersion)."'><br>
                                                                <label>apiAccess (r)</label>
                                                                <textarea rows='7' name='apiAccess' class='form-control' placeholder='apiAccess' required>".json_encode($wxSrvManifest['apiAccess'], JSON_PRETTY_PRINT)."</textarea><br>
                                                                <label>displayName (r)</label>
                                                                <input type='text' class='form-control' placeholder='scopes' name='displayName' required value='".issetor($wxSrvManifest['displayName'])."'><br>
                                                                <label>vendor (r)</label>
                                                                <input type='text' class='form-control' placeholder='vendor' name='vendor' required value='".issetor($wxSrvManifest['vendor'])."'><br>
                                                                <label>email (r)</label>
                                                                <input type='text' class='form-control' placeholder='email' name='email' required value='".issetor($wxSrvManifest['email'])."'><br>
                                                                <label>description (r)</label>
                                                                <input type='text' class='form-control' placeholder='description' name='description' required value='".issetor($wxSrvManifest['description'])."'><br>
                                                                <label>availability (r)</label>
                                                                <input type='text' class='form-control' placeholder='availability' name='availability' value='".issetor($wxSrvManifest['availability'])."'><br>
                                                                <label>tocUrl (o)</label>
                                                                <input type='text' class='form-control' placeholder='tocUrl' name='tocUrl' value='".issetor($wxSrvManifest['tocUrl'])."'><br>
                                                                <label>descriptionUrl (o)</label>
                                                                <input type='text' class='form-control' placeholder='descriptionUrl' name='descriptionUrl' value='".issetor($wxSrvManifest['descriptionUrl'])."'><br>
                                                                <label>xapiAccess (Commands, Status, Events)</label><br>
                                                                Commands: <textarea class='form-control' rows='7' placeholder='commands' name='commands'>".json_encode($wxSrvManifest['xapiAccess']['commands'], JSON_PRETTY_PRINT)."</textarea><br>
                                                                Status: <textarea class='form-control' rows='7' placeholder='status' name='status'>".json_encode($wxSrvManifest['xapiAccess']['status'], JSON_PRETTY_PRINT)."</textarea><br>
                                                                Events: <textarea class='form-control' rows='7' placeholder='events' name='events'>".json_encode($wxSrvManifest['xapiAccess']['events'], JSON_PRETTY_PRINT)."</textarea><br>
                                                                <label>Provisioning</label>
                                                                <select name='provisioning' class='form-control'>
                                                                    <option value='https' $httpsSelected>HTTPS</option>
                                                                    <option value='manual' $manualSelected>Manual</option>
                                                                </select> 
                                                                
                                                                <br>
                                                                <label>Features</label><br>
                                                                <input type='checkbox' class='' $checkSignage name='features[]' value='digital_signage'> Digital Signage <br>
                                                                <input type='checkbox' class='' $checkPwa name='features[]' value='persistent_web_app'> Persistent Web App 
                                                    
                                                            ";
                          								    
                          					?>
                          					
                        			    </div>
                        			    <div class="modal-footer justify-content-between">
                        			      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        			      <input type='submit' name='updateManifestRequest' class="btn btn-primary" value='Send update to Webex'><input type='submit' name='' class="btn btn-primary" value='Download manifest'>
                        			      </form>
                        			    </div>
                        			  </div>
                        			  <!-- /.modal-content -->
                        			</div>
                        			<!-- /.modal-dialog -->
                        			</div>
                        			<!-- / PWA Manifest upgrade -->
        					
            			   <button type="button" class="btn btn-md btn-primary btn-block" data-toggle="modal" data-target="#manifest-config">
                  				Edit manifest
                			</button>
                			
                			<?php if (isset($result['features']) and in_array('digital_signage', $result['features'])) { ?>
                			
                			<button type="button" class="btn btn-md btn-primary btn-block" data-toggle="modal" data-target="#digital-signage">
                  				Digital Signage Config
                			</button>
                			
                			<?php } if (isset($result['features']) and in_array('persistent_web_app', $result['features'])) { ?>
                			
                			<button type="button" class="btn btn-md btn-primary btn-block" data-toggle="modal" data-target="#pwa-config">
                  				Persistent Web App Config
                			</button><br>
                			
                			<?php }?>
        			    </form>
        			    </div></div>
        		</div>
        	</div>
			    
			   	<label><?php echo $tooltips['appManifest'];?> Current App Manifest <?php echo statusBoolPill((!isset($result['updateRequest'])), 'Manifest is approved and in sync', 'Update pending for new manifest version')?></label><br>
			   	<?php 
			   	if (isset($result['id']) and $result['id'] == $integration) {

			         echo "<pre>".json_encode($result, JSON_PRETTY_PRINT)."</pre>";
			         
			   	} else {
			   	    
			   	     echo "<pre>UNABLE TO GET WEBEX APP MANIFEST - THE APP MAY HAVE BEEN DELETED FROM WEBEX</pre>";
			   	     
			   	}
			     ?>
			     
			     <br>
			     
			     <label><?php echo $tooltips['serverManifest'];?> Current Active Server Manifest</label><br>
			   	<?php 
			   	if (isset($wxSrvManifest['id']) and $wxSrvManifest['id'] == $integration) {

			         echo "<pre>".json_encode($wxSrvManifest, JSON_PRETTY_PRINT)."</pre>";
			         
			         if (($wxSrvManifest['displayName'] != $details['name']) or ($wxSrvManifest['description'] != $details['description']))  {
			             $update_array = array('name' => $wxSrvManifest['displayName'], 'description'=>$wxSrvManifest['description']);
			             $db_local->wsIntegrationUpdate($integration, $update_array);
			             redirect($home."&int=$integration");
			         }
			         
			   	} else {
			   	    
			   	     echo "<pre>UNABLE TO GET WEBEX SERVER MANIFEST - THE APP MAY HAVE BEEN DELETED FROM WEBEX</pre>";
			   	     
			   	}
			   	
			     ?>
			   
			</div>
		</div>
	</div>
	<?php } }?>
</div>
</div>
</div>
