<?php if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); ?>
<?php 

if (isset($_POST['clear_debug'])) {
    $db_local->debugClear();
}

if (isset($_POST['debug_mode'])) {
    ($db_local->debugIsEnabled()) ? $db_local->debugDisable() : $db_local->debugEnable();
}

if ($db_local->debugIsEnabled()) {
    $debug_state = "Disable"; 
} else {
    $debug_state = "Enable";
}

?>
<!-- Content Header (Page header) -->
 <style>
   table {border-collapse:collapse; table-layout:fixed; width:310px;}
   table td {border:solid 1px #fab; width:100px; word-wrap:break-word;}
   </style>
<div class='content-header'>
	<div class='container-fluid'>
		<div class='row mb-2'>
			<div class='col-sm-6'>
				<h1 class='m-0 text-dark'>Debug</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class='content'>
	<div class='container-fluid'>

		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h3 class="card-title">Debug logs</h3><br>
						<form action="#" method="post" enctype='multipart/form-data'>
						<input type="submit" value="<?php echo $debug_state; ?>" name="debug_mode" class="btn btn-primary"> <button class='btn btn-primary' onClick="window.location.reload();">Refresh</button> <input type="submit" value="Clear logs" name="clear_debug" class="btn btn-danger">
						</form>
					</div>
					<div class="card-body table-responsive">
						<table width='100%' id='debug_log' class='table table-bordered table-striped'>
							<thead>
								<tr>
									<th>Date</th>
									<th>User</th>
									<th>Bot/Integration</th>
									<th>Request</th>
									<th>Response</th>
									<th>Type</th>
									<th>Description</th>
								</tr>
							</thead>
							<tfoot>
					            <tr>
					            	<th>Date</th>
					                <th>User</th>
									<th>Bot/Integration</th>
									<th>Request</th>
									<th>Response</th>
									<th>Type</th>
									<th>Description</th>
					            </tr>
					        </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>