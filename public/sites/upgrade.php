<?php 
if ($windowid != "login" and !verify()) header("Location: login.php"); 
?><section class="content-header">
	<h1><?php echo issetor($feedback); ?>
		Upgrade
	</h1>
</section>
<section class="content">
<?php



function upgradeQuery($version)
{
	//SQL STATEMENTS FOR EACH VERSION

	/* EXAMPLE ARRAY FOR UPGRADE QUERIES
	$upgrade_queries["<VERSION NUMBER>"][] = "-- Create example table"; //This is a comment to display on screen
	$upgrade_queries["<VERSION NUMBER>"][] = "CREATE TABLE example ( //SQL query
		`id` INT AUTO_INCREMENT,
		`example1` VARCHAR(255) NOT NULL,
		`example2` VARCHAR(255) NOT NULL,
		PRIMARY KEY (id)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["<VERSION NUMBER>"][] = "UPDATE `site_settings` SET `value`='<VERSION NUMBER>' WHERE `settings` = 'dbversion'"; //MANDATORY query to update dbversion in site_settings table
	*/

	/*
	DB Version 2 update. 
	Adding tables for AD Groups and Mapping AD groups to local groups for nightly tasks
	Adding queue tasks and queue subtasks for new queuing system.
	*/
	$upgrade_queries["2"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 2";
	$upgrade_queries["2"][] = "-- Create ad_groups table";
	$upgrade_queries["2"][] = "CREATE TABLE `ad_groups` (
	  `id` int(100) NOT NULL AUTO_INCREMENT,
	  `group_name` varchar(255) NOT NULL,
	  PRIMARY KEY (id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create ad_group_mapping table";
	$upgrade_queries["2"][] = "CREATE TABLE `ad_group_mapping` (
	  `id` int(100) NOT NULL AUTO_INCREMENT,
	  `ad_group_id` int(100) NOT NULL,
	  `local_group_id` int(100) NOT NULL,
	  PRIMARY KEY (id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create queue_task table";
	$upgrade_queries["2"][] = "CREATE TABLE `queue_task` (
	  `task_id` varchar(100) NOT NULL,
	  `task_type` varchar(50) NOT NULL,
	  `task_name` varchar(35) NOT NULL,
	  `task_submitter` varchar(35) NOT NULL,
	  `submit_timestamp` int(10) NOT NULL,
	  `clear` varchar(3) NOT NULL,
	  PRIMARY KEY (task_id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create queue_subtasks table";
	$upgrade_queries["2"][] = "CREATE TABLE `queue_subtasks` (
	  `subtask_id` varchar(100) NOT NULL,
	  `task_id` varchar(100) NOT NULL,
	  `bot_id` varchar(255) NOT NULL,
	  `subtask_type` varchar(25) NOT NULL,
	  `task_json` mediumtext NOT NULL,
	  `group_id` varchar(25) NOT NULL,
	  `task_code` varchar(3) NOT NULL,
	  `task_results` varchar(255) NOT NULL,
	  PRIMARY KEY (subtask_id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Create webex_response_codes table";
	$upgrade_queries["2"][] = "CREATE TABLE `webex_response_codes` (
	  `code` varchar(3) NOT NULL,
	  `status` varchar(50) NOT NULL,
	  `label` varchar(255) NOT NULL,
	  `description` varchar(500) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["2"][] = "-- Inserting Codes into webex_response_codes table";
	$upgrade_queries["2"][] = "INSERT INTO `webex_response_codes` (`code`, `status`, `label`, `description`) VALUES
	('200', 'OK', '<span class=\"label label-success\">Success</span>', 'Successful request with body content.'),
	('204', 'No Content', '<span class=\"label label-success\">No Content</span>', 'Successful request without body content.'),
	('400', 'Bad Request', '<span class=\"label label-warning\">Bad Request</span>', 'The request was invalid or cannot be otherwise served. An accompanying error message will explain further.'),
	('401', 'Unauthorized', '<span class=\"label label-warning\">Unauthorized</span>', 'Authentication credentials were missing or incorrect.'),
	('403', 'Forbidden', '<span class=\"label label-warning\">Forbidden</span>', 'The request is understood, but it has been refused or access is not allowed.'),
	('404', 'Not Found', '<span class=\"label label-warning\">Not Found</span>', 'The URI requested is invalid or the resource requested, such as a user, does not exist. Also returned when the requested format is not supported by the requested method.'),
	('405', 'Method Not Allowed', '<span class=\"label label-warning\">Method Not Allowed</span>', 'The request was made to a resource using an HTTP request method that is not supported.'),
	('409', 'Conflict', '<span class=\"label label-warning\">Conflict</span>', 'The request could not be processed because it conflicts with some established rule of the system. For example, a person may not be added to a room more than once.'),
	('415', 'Unsupported Media Type', '<span class=\"label label-warning\">Unsupported Media Type</span>', 'The request was made to a resource without specifying a media type or used a media type that is not supported.'),
	('429', 'Too Many Requests', '<span class=\"label label-warning\">Too Many Requests</span>', 'Too many requests have been sent in a given amount of time and the request has been rate limited. A Retry-After header should be present that specifies how many seconds you need to wait before a successful request can be made.'),
	('500', 'Internal Server Error', '<span class=\"label label-danger\">Internal Server Error</span>', 'Something went wrong on the server'),
	('502', 'Bad Gateway', '<span class=\"label label-danger\">Bad Gateway</span>', 'The server received an invalid response from an upstream server while processing the request. Try again later.'),
	('503', 'Service Unavailable', '<span class=\"label label-danger\">Service Unavailable</span>', 'Server is overloaded with requests. Try again later.');";
	$upgrade_queries["2"][] = "UPDATE `site_settings` SET `value`='2' WHERE `settings` = 'dbversion'";
	/*
	 DB Version 3 update
	 Added system message feature
	 
	 */
	$upgrade_queries["3"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 3";
	$upgrade_queries["3"][] = "ALTER TABLE `generic_feedback` ADD PRIMARY KEY(`id`);";
	$upgrade_queries["3"][] = "-- Alter table, insert warning mode";
	$upgrade_queries["3"][] = "ALTER TABLE `service_status` ADD `warning_mode` INT(1) NOT NULL AFTER `maintenance_mode`;";
	$upgrade_queries["3"][] = "-- Create warning message and default";
	$upgrade_queries["3"][] = "INSERT INTO `generic_feedback` (`id`, `message`, `default_message`) VALUES ('warning', '**Warning:** We are currently experiencing network issues so you may experience problems with communication.', '**Warning:** We are currently experiencing network issues so you may experience problems with communication.');";
	$upgrade_queries["3"][] = "ALTER TABLE `queue_task` CHANGE `submit_timestamp` `submit_timestamp` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';";
	$upgrade_queries["3"][] = "UPDATE `site_settings` SET `value`='3' WHERE `settings` = 'dbversion';";
	/*
	 DB Version 4 update
	 Added botid and botresponse to task so the task knows which bot was used to send the request and also to notify a user when a task was completed.
	 The user can add a task to add contacts via a bot. This will show as "Add contacts via bot" as task name. 
	 */
	$upgrade_queries["4"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 4";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_task` CHANGE `submit_timestamp` `submit_timestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_task` ADD `botresponse` INT(1) NOT NULL AFTER `task_submitter`;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_task` ADD `botid` VARCHAR(150) NOT NULL AFTER `botresponse`;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_subtasks` CHANGE `task_results` `task_results` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_subtasks` ADD `payload` TEXT NOT NULL AFTER `task_code`;";
	$upgrade_queries["4"][] = "ALTER TABLE `queue_subtasks` ADD `response` TEXT NOT NULL AFTER `payload`;";
	$upgrade_queries["4"][] = "UPDATE `site_settings` SET `value`='4' WHERE `settings` = 'dbversion'";
	/*
	 DB Version 5 update
	 Added integration table for setting up an integration with WBM. 
	 Added audit log table for future use
	 */
	$upgrade_queries["500"][] = "-- EXECUTING QUERY TABLE FOR WBM DB VERSION 5.0.0";
	$upgrade_queries["500"][] = "CREATE TABLE `integration` ( 
  `id` VARCHAR(255) NOT NULL ,
  `client_id` VARCHAR(255) NOT NULL , 
  `client_secret` VARCHAR(255) NOT NULL , 
  `redirect_url` VARCHAR(255) NOT NULL , 
  `default_scopes` TEXT NOT NULL , 
  `name` VARCHAR(100) NOT NULL , 
  PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["500"][] = "INSERT INTO `integration` (`id`, `client_id`, `client_secret`, `redirect_url`, `default_scopes`, `name`) VALUES
('1', '', '', '', '', '');";
	$upgrade_queries["500"][] = "CREATE TABLE `debug_log` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `request` text NOT NULL,
  `response` text NOT NULL,
  `bot` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text NOT NULL DEFAULT '',
  `customfield1` text NOT NULL DEFAULT '',
  `customfield2` text NOT NULL DEFAULT '',
  `customfield3` text NOT NULL DEFAULT '',
  `customfield4` text NOT NULL DEFAULT '',
  `customfield5` text NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["500"][] = "ALTER TABLE `debug_log`
  ADD PRIMARY KEY (`id`);";
	$upgrade_queries["500"][] = "ALTER TABLE `debug_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
	$upgrade_queries["500"][] = "CREATE TABLE `integration_tokens` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `expires_in` int(255) NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `refresh_token_expires_in` int(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["500"][] = "ALTER TABLE `integration_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`userid`);";
	$upgrade_queries["500"][] = "ALTER TABLE `integration_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
	$upgrade_queries["500"][] = "CREATE TABLE `integration_scopes` (
  `scope` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
	$upgrade_queries["500"][] = "ALTER TABLE `integration_scopes`
  ADD PRIMARY KEY (`scope`);";
	$upgrade_queries["500"][] = "INSERT INTO `integration_scopes` (`scope`, `description`) VALUES
('audit:events_read', 'Access to the audit log for an organization'),
('identity:placeonetimepassword_create', 'Access to a one time password to a place to create an activation code'),
('spark-admin:call_qualities_read', 'Access to read organization\'s call qualities'),
('spark-admin:devices_read', 'See details for any device in your organization'),
('spark-admin:devices_write', 'Delete any device in your organization'),
('spark-admin:hybrid_clusters_read', 'Access to read hybrid clusters for your organization'),
('spark-admin:hybrid_connectors_read', 'Access to read hybrid connectors for your organization'),
('spark-admin:licenses_read', 'Access to read licenses available in your user\'s organizations'),
('spark-admin:organizations_read', 'Access to read your user\'s organizations'),
('spark-admin:people_read', 'Access to read your user\'s company directory'),
('spark-admin:people_write', 'Access to write to your user\'s company directory'),
('spark-admin:places_read', 'See details for any places and place service in your organization'),
('spark-admin:places_write', 'Create, update and delete any place and place service in your organization'),
('spark-admin:resource_groups_read', 'Access to read your organization\'s resource groups'),
('spark-admin:resource_group_memberships_read', 'Access to read your organization\'s resource group memberships'),
('spark-admin:resource_group_memberships_write', 'Access to update your organization\'s resource group memberships'),
('spark-admin:roles_read', 'Access to read roles available in your user\'s organization'),
('spark-compliance:events_read', 'Access to read events in your user\'s organization'),
('spark-compliance:memberships_read', 'Access to read memberships in your user\'s organization'),
('spark-compliance:memberships_write', 'Access to create/update/delete memberships in your user\'s organization'),
('spark-compliance:messages_read', 'Access to read messages in your user\'s organization'),
('spark-compliance:messages_write', 'Post and delete messages in all spaces in your user\'s organization'),
('spark-compliance:rooms_read', 'Access to read rooms in your user\'s organization'),
('spark-compliance:teams_read', 'Access to read teams in your user\'s organization'),
('spark-compliance:team_memberships_read', 'Access to read team memberships in your user\'s organization'),
('spark-compliance:team_memberships_write', 'Access to update team memberships in your user\'s organization'),
('spark:all', 'Full access to your Webex Teams account'),
('spark:devices_read', 'See details for your devices'),
('spark:devices_write', 'Modify and delete your devices'),
('spark:memberships_read', 'List people in the rooms you are in'),
('spark:memberships_write', 'Invite people to rooms on your behalf'),
('spark:messages_read', 'Read the content of rooms that you are in'),
('spark:messages_write', 'Post and delete messages on your behalf'),
('spark:people_read', 'Read your users\' company directory'),
('spark:places_read', 'See details for places and place services you manage'),
('spark:places_write', 'Create, modify and delete places and place services you manage'),
('spark:rooms_read', 'List the titles of rooms that you are in'),
('spark:rooms_write', 'Manage rooms on your behalf'),
('spark:teams_read', 'List the teams your user\'s a member of'),
('spark:teams_write', 'Create teams on your users\' behalf'),
('spark:team_memberships_read', 'List the people in the teams your user belongs to'),
('spark:team_memberships_write', 'Add people to teams on your users\' behalf'),
('spark:xapi_commands', 'Execute all commands on RoomOS-enabled devices.'),
('spark:xapi_statuses', 'Retrieve all information from RoomOS-enabled devices.');";
	
	$upgrade_queries["500"][] = "-- Re-inserting updated webex_response_codes table";
	$upgrade_queries["500"][] = "DELETE FROM `webex_response_codes`";
	$upgrade_queries["500"][] = "INSERT INTO `webex_response_codes` (`code`, `status`, `label`, `description`) VALUES
	('100', 'Continue', '<span class=\"label label-warning\">Continue</span>', 'Server could not confirm that the request was successful.'),
	('200', 'OK', '<span class=\"label label-success\">Success</span>', 'Successful request with body content.'),
	('204', 'No Content', '<span class=\"label label-success\">No Content</span>', 'Successful request without body content.'),
	('400', 'Bad Request', '<span class=\"label label-warning\">Bad Request</span>', 'The request was invalid or cannot be otherwise served. An accompanying error message will explain further.'),
	('401', 'Unauthorized', '<span class=\"label label-warning\">Unauthorized</span>', 'Authentication credentials were missing or incorrect.'),
	('403', 'Forbidden', '<span class=\"label label-warning\">Forbidden</span>', 'The request is understood, but it has been refused or access is not allowed.'),
	('404', 'Not Found', '<span class=\"label label-warning\">Not Found</span>', 'The URI requested is invalid or the resource requested, such as a user, does not exist. Also returned when the requested format is not supported by the requested method.'),
	('405', 'Method Not Allowed', '<span class=\"label label-warning\">Method Not Allowed</span>', 'The request was made to a resource using an HTTP request method that is not supported.'),
	('409', 'Conflict', '<span class=\"label label-warning\">Conflict</span>', 'The request could not be processed because it conflicts with some established rule of the system. For example, a person may not be added to a room more than once.'),
	('415', 'Unsupported Media Type', '<span class=\"label label-warning\">Unsupported Media Type</span>', 'The request was made to a resource without specifying a media type or used a media type that is not supported.'),
	('429', 'Too Many Requests', '<span class=\"label label-warning\">Too Many Requests</span>', 'Too many requests have been sent in a given amount of time and the request has been rate limited. A Retry-After header should be present that specifies how many seconds you need to wait before a successful request can be made.'),
	('500', 'Internal Server Error', '<span class=\"label label-danger\">Internal Server Error</span>', 'Something went wrong on the server'),
	('502', 'Bad Gateway', '<span class=\"label label-danger\">Bad Gateway</span>', 'The server received an invalid response from an upstream server while processing the request. Try again later.'),
	('503', 'Service Unavailable', '<span class=\"label label-danger\">Service Unavailable</span>', 'Server is overloaded with requests. Try again later.');";
	$upgrade_queries["500"][] = "ALTER TABLE `webex_response_codes` ADD PRIMARY KEY(`code`);";
	$upgrade_queries["500"][] = "ALTER TABLE `service_status` ADD `debug_mode` INT(1) NOT NULL AFTER `maintenance_message`;";
	$upgrade_queries["500"][] = "UPDATE `site_settings` SET `value`='500' WHERE `settings` = 'dbversion';";
	//DB version 501
	$upgrade_queries["501"][] = "INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES ('16', 'Integration controls', 'integration', 'Allow authorizing with integration using the bot. ', '##INTEGRATION COMMANDS\r\n---\r\n\r\n<blockquote class=primary>Authorize</blockquote>\r\n\r\n- **integration authorize** [**scope** **scope**..] \r\n - Generates an authorization URL for your user 1:1. If no scopes are issued the system default ones will be used.\r\n- **integration refresh** \r\n - Refreshes your currently attached access token\r\n\r\n<blockquote class=primary>View</blockquote>\r\n\r\n- **integration show**\r\n - Displays your current integration settings including the system default scopes. \r\n\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **integration delete**\r\n - Removes your current integration authorization')";
	$upgrade_queries["501"][] = "UPDATE `site_settings` SET `value`='501' WHERE `settings` = 'dbversion';";
	//DB version 502
	$upgrade_queries["502"][] = "INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES ('17', 'Place controls', 'place', 'Enables control over places, requires autorized integration token to use. The authorized user needs to be a site admin. \r\n\r\nScopes: spark-admin:places_read, spark-admin:places_write and identity:placeonetimepassword_create\r\n\r\n', '##PLACES COMMANDS\r\n---\r\n\r\nThis feature may require you to authrorize a user with the following scopes: spark-admin:places_read, spark-admin:places_write. In order to get an activation code you need the identity:placeonetimepassword_create\r\n\r\n<blockquote class=primary>Get details</blockquote>\r\n\r\n- **place list** [**displayName**]\r\n     - List places, search for displayName (optional). \r\n- **place show** [**placeId**] \r\n     - List details for the place, requires placeId.\r\n\r\n<blockquote class=primary>Create</blockquote>\r\n\r\n- **place create** [**displayName**] \r\n     - Creates a new place with the specified displayName\r\n- **place update** [**placeId**] [**newDisplayName**] \r\n     - Updates an existing place with the specified displayName\r\n- **place activate** [**displayName**]\r\n     - Creates a new place using the displayName and returns an activation code for a Cisco Webex Room Device\r\n- **place code** [**placeId**]\r\n     - Activates an existing place and returns an activation code for a Cisco Webex Room Device\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **place delete** [**placeId**]\r\n     - Deletes a place')";
	$upgrade_queries["502"][] = "DELETE FROM `features` WHERE `id` = '15'";
	$upgrade_queries["502"][] = "DELETE FROM `features` WHERE `id` = '16'";
	$upgrade_queries["502"][] = "INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES ('16', 'Integration controls', 'integration', 'Allow authorizing with integration using the bot. ', '##INTEGRATION COMMANDS\r\n---\r\n\r\nWhen authorizing new users you might have to log out of your currently logged in user in your browser.\r\n\r\n<blockquote class=primary>Authorize</blockquote>\r\n\r\n- **integration authorize** [**scope** **scope**..] \r\n     - *Generates an authorization URL for your user, login with any Webex Teams user to attach the token to your current user. If no scopes are issued the system default ones will be used.*\r\n- **integration refresh** \r\n     - *Refreshes your currently attached access token*\r\n\r\n<blockquote class=primary>View</blockquote>\r\n\r\n- **integration show**\r\n     - *Displays your current integration settings including the system default scopes.*\r\n- **integration scopes**\r\n     - *Displays the available scopes with description that can be used when generating custom authrorization URLs*\r\n- **integration defaults** \r\n     - *Displays the system default scopes*\r\n\r\n\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **integration delete**\r\n     - *Removes your current integration authorization*')";
	$upgrade_queries["502"][] = "INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES ('15', 'Devices ', 'device', 'Enable device control', '##DEVICE COMMANDS\r\n---\r\n\r\n<blockquote class=primary>View devices</blockquote>\r\n\r\n- **device list** [**displayName**] or JSON (see example)\r\n     - *List the available devices controlled by this bot or integration (including deviceId)* \r\n\r\n<blockquote class=primary>Status, configurations and commands</blockquote>\r\n- **device status** [**deviceId**] [**statusKey**] \r\n     - *Get status from the device on a statuskey*\r\n- **device command** [**deviceId**] [**commandKey**] [**{\"attribute\":\"value\"}**]*\r\n     - *Send a command to a device with a command key*\r\n- **device config get** [**deviceId**] [**configKey**] [**-v**]*\r\n     - *Fetch configuration details from device*\r\n     - *The flag -v will display fewer nodes but more info*\r\n- **device config set** [**deviceId**] [**configKey**] [**value**]\r\n     - *Modifies a configuration key on the specified device*\r\n\r\nExample usage:\r\n\r\nThe **deviceId** is acquired using **device list**\r\n\r\n- **device list My system**\r\n     - *Searches for a device named My system and lists the deviceId if found*\r\n- **device list {\"product\":\"SX10\",\"start\":\"100\"}**\r\n     - *Returns a list of devices that matches the product and starts the list from device number 100*\r\n- **device show [deviceId]**\r\n     - *Lists the details of a device, most of the detail keys can be used when searching for devices using the device list command with JSON*\r\n\r\n- **device status [deviceId] audio.volume** \r\n     - *Returns the current volume on the device*\r\n- **device command [decviceId] audio.volume.set {\"Level\": 60}** \r\n     - *Set the volume on the device*\r\n- **device config get [deviceId] SerialPort.Mode**\r\n     - *Gets the configuration details of xConfiguration SerialPort Mode*\r\n- **device config set [deviceId] SerialPort.Mode Off**\r\n     - *Sets the configuration node xConfiguration SerialPort Mode to Off*\r\n\r\n')";
	$upgrade_queries["502"][] = "UPDATE `site_settings` SET `value`='502' WHERE `settings` = 'dbversion';";
	//DB version 503
	$upgrade_queries["503"][] = "ALTER TABLE `tasks` ADD `files` TEXT NOT NULL AFTER `roomid`, ADD `parentMessageId` TEXT NOT NULL AFTER `files`;";
	$upgrade_queries["503"][] = "UPDATE `site_settings` SET `value`='503' WHERE `settings` = 'dbversion';";
	//DB version 510
	$upgrade_queries["510"][] = "ALTER TABLE `saved_messages` ADD `card_attachment` INT(11) NOT NULL AFTER `files`;";
	$upgrade_queries["510"][] = "CREATE TABLE `card_data` (
  `card_id` varchar(255) NOT NULL,
  `person_id` varchar(255) NOT NULL,
  `botid` varchar(255) NOT NULL,
  `person_email` varchar(255) NOT NULL,
  `input_data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["510"][] = "CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `card_body` text NOT NULL,
  `reply` text NOT NULL,
  `accessgroup` int(25) NOT NULL,
  `input_active` int(1) NOT NULL,
  `delete_active` int(1) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `botid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["510"][] = "ALTER TABLE `card_data` ADD UNIQUE( `card_id`, `person_id`);";
	$upgrade_queries["510"][] ="ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);";
	$upgrade_queries["510"][] ="ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
	$upgrade_queries["510"][] = "UPDATE `site_settings` SET `value`='510' WHERE `settings` = 'dbversion';";
	//DB version 511
	$upgrade_queries["511"][] = "ALTER TABLE `cards` ADD `echo_active` INT(1) NOT NULL AFTER `delete_active`, ADD `update_active` INT(1) NOT NULL AFTER `echo_active`;";
	$upgrade_queries["511"][] = "UPDATE `site_settings` SET `value`='511' WHERE `settings` = 'dbversion';";
	//DB version 512
	$upgrade_queries["512"][] = "ALTER TABLE `response` ADD `card_id` INT(11) NOT NULL AFTER `file_url`;";
	$upgrade_queries["512"][] = "ALTER TABLE `cards` ADD `datacode` VARCHAR(255) NOT NULL AFTER `botid`;";
	$upgrade_queries["512"][] = "UPDATE `site_settings` SET `value`='512' WHERE `settings` = 'dbversion';";
	//DB version 513
	$upgrade_queries["513"][] = "ALTER TABLE `cards` ADD `redirect_reply` INT(1) NOT NULL AFTER `update_active`;";
	$upgrade_queries["513"][] = "ALTER TABLE `card_data` ADD UNIQUE( `card_id`, `person_id`);";
	$upgrade_queries["513"][] = "UPDATE `features` SET `usage` = '##SPACE CONTROLS\n---\n\n<blockquote class=primary>Space commands</blockquote>\n\n- **space create** \n    - *Creates a new space with you and the bot with a autogenerated title*\n- **space create [title text ..]** \n    - *Creates a new space with you and the bot. The space will be named with UTC timestamp and the specified title*\n- **space create [email,email..]** \n    - *Creates a new space and adds the users specified with CSV at the same time*\n    - *If a new space is created in a team space, the space will be created in the team.*\n- **space add [email,email..]** \n    - *Adds users to the current space*\n- **space kick [email]** \n    - Removes a user from a space, note that if you write only \"space kick\" it will remove you. \n- **space gencsv** \n    - Generates a list of emails based on who is in the space the command is issued. The list is CSV format and will be given to you 1:1 in bulks of 100 emails. If there are too many people > 1000 in the space I might not be able to provide the full list. \n- **space list** \n    - Lists spaces marked as **joinable** for this bot (shows spaceid)\n- **space join [spaceid]**\n    - I will invite you to the space \n\n\n' WHERE `features`.`id` = 2;";
	$upgrade_queries["513"][] = "UPDATE `features` SET `usage` = '##PLACES COMMANDS\n---\n\nThis feature may require you to authrorize a user with the following scopes: spark-admin:places_read, spark-admin:places_write. In order to get an activation code you need the identity:placeonetimepassword_create\n\n<blockquote class=primary>Get details</blockquote>\n\n- **place list** [**displayName**]\n    - List places, search for displayName (optional). \n- **place show** [**placeId**] \n    - List details for the place, requires placeId.\n\n<blockquote class=primary>Create</blockquote>\n\n- **place create** [**displayName**] \n    - Creates a new place with the specified displayName\n- **place update** [**placeId**] [**newDisplayName**] \n    - Updates an existing place with the specified displayName\n- **place activate** [**displayName**]\n    - Creates a new place using the displayName and returns an activation code for a Cisco Webex Room Device\n- **place code** [**placeId**]\n    - Activates an existing place and returns an activation code for a Cisco Webex Room Device\n\n<blockquote class=primary>Remove</blockquote>\n\n- **place delete** [**placeId**]\n    - Deletes a place' WHERE `features`.`id` = 17;";
	$upgrade_queries["513"][] = "UPDATE `features` SET `usage` = '##INTEGRATION COMMANDS\r\n---\r\n\r\nIntegration allow you to connect any Webex Teams user to your bot user. Since device and place management is the primary features you get from integration, it is recommended to authorize a device admin or org admin to perform device activation.  \r\n\r\nWhen authorizing new users you might have to log out of your currently logged in user in your browser.\r\n\r\n<blockquote class=primary>Authorize</blockquote>\r\n\r\n- **integration authorize** [**scope** **scope**..] \r\n     - *Generates an authorization URL for your user, login with any Webex Teams user to attach the token to your current user. If no scopes are issued the system default ones will be used.*\r\n- **integration refresh** \r\n     - *Refreshes your currently attached access token*\r\n\r\n<blockquote class=primary>View</blockquote>\r\n\r\n- **integration show**\r\n     - *Displays your current integration settings including the system default scopes.*\r\n- **integration scopes**\r\n     - *Displays the available scopes with description that can be used when generating custom authrorization URLs*\r\n- **integration defaults** \r\n     - *Displays the system default scopes*\r\n\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **integration delete**\r\n     - *Removes your current integration authorization*' WHERE `features`.`id` = 16;";
	$upgrade_queries["513"][] = "UPDATE `features` SET `usage` = '##DEVICE COMMANDS\r\n---\r\n\r\nWith the device commands you can find your device based on your current integration user.If no integration user is configured to your account the bot will use its own user token to look for devices it has been granted control for. Configuration require admin scopes.  \r\n\r\n<blockquote class=primary>View devices</blockquote>\r\n\r\n- **device list** [**displayName**] or JSON (see example)\r\n     - *List the available devices controlled by this bot or integration (including deviceId)* \r\n\r\n<blockquote class=primary>Status, configurations and commands</blockquote>\r\n\r\n- **device ui** [**deviceId**]\r\n    - *Returns an adaptive card that is locked to the deviceId you type in. Makes it easier to get status, send commands and configs to the device.*\r\n- **device status** [**deviceId**] [**statusKey**] \r\n     - *Get status from the device on a statuskey*\r\n- **device command** [**deviceId**] [**commandKey**] [**{\"attribute\":\"value\"}**]*\r\n     - *Send a command to a device with a command key*\r\n- **device config get** [**deviceId**] [**configKey**] [**-v**]*\r\n     - *Fetch configuration details from device*\r\n     - *The flag -v will display fewer nodes but more info*\r\n- **device config set** [**deviceId**] [**configKey**] [**value**]\r\n     - *Modifies a configuration key on the specified device*\r\n\r\nExample usage:\r\n\r\nThe **deviceId** is acquired using **device list**\r\n\r\n- **device list My system**\r\n     - *Searches for a device named My system and lists the deviceId if found*\r\n- **device list {\"product\":\"SX10\",\"start\":\"100\"}**\r\n     - *Returns a list of devices that matches the product and starts the list from device number 100*\r\n- **device show [deviceId]**\r\n     - *Lists the details of a device, most of the detail keys can be used when searching for devices using the device list command with JSON*\r\n\r\n- **device status [deviceId] audio.volume** \r\n     - *Returns the current volume on the device*\r\n- **device command [decviceId] audio.volume.set {\"Level\": 60}** \r\n     - *Set the volume on the device*\r\n- **device config get [deviceId] SerialPort.Mode**\r\n     - *Gets the configuration details of xConfiguration SerialPort Mode*\r\n- **device config set [deviceId] SerialPort.Mode Off**\r\n     - *Sets the configuration node xConfiguration SerialPort Mode to Off*\r\n\r\n' WHERE `features`.`id` = 15;";
	$upgrade_queries["513"][] = "UPDATE `site_settings` SET `value`='513' WHERE `settings` = 'dbversion';";
	//DB version 514
	$upgrade_queries["514"][] = "ALTER TABLE `tasks` ADD `card_data` TEXT NOT NULL AFTER `files`";
	$upgrade_queries["514"][] = "UPDATE `site_settings` SET `value`='514' WHERE `settings` = 'dbversion';";
	//DB version 515
	$upgrade_queries["515"][] = "DELETE FROM `integration_scopes` WHERE scope IN ('spark-admin:places_read', 'spark-admin:places_write', 'spark:places_read', 'spark:places_write');";
	$upgrade_queries["515"][] = "INSERT INTO `integration_scopes` (`scope`, `description`) VALUES
            ('spark-admin:workspaces_read', 'See details for workspaces'),
            ('spark-admin:workspaces_write', 'Create, modify and delete workspaces')";
	$upgrade_queries["515"][] = "ALTER TABLE `group_groups` ADD UNIQUE( `groupid`, `nestedid`);";
	$upgrade_queries["515"][] = "UPDATE `integration` SET `default_scopes`='' WHERE `id` = '1';"; //Remove the default scopes because the scopes has been updated
 	$upgrade_queries["515"][] = "UPDATE `site_settings` SET `value`='515' WHERE `settings` = 'dbversion';";
 	//DB version 516
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## ADMINISTRATOR COMMANDS\r\n---\r\n\r\n<blockquote class=primary>System</blockquote>\r\n\r\n- **admin maintenance** [**enable**/**disable**] \r\n    - Enables or disables maintenance mode, can be enabled to prevent new external tasks to be created in case of modifications or downtime. \r\n\r\n<blockquote class=primary>Message</blockquote>\r\n\r\n- **admin message list [messageid]** \r\n    - Get a list of stored messages with message id. You can supply a message id to the list command to preview a message.\r\n- **admin message announce [groupid] [messageid]**\r\n    - Announce a stored message specified groups. You can issue multiple groups in csv (no spaces)\r\n- **admin message echo [groupid],[email] [free text here..]**\r\n    - Sends an echo of your message to the specified groups or emails. You can issue a combo of multiple groups and emails in csv (no spaces)\r\n\r\n<blockquote class=primary>Feedback</blockquote>\r\n\r\n- **admin feedback create [Topic text..]** \r\n    - Creates a new feedback topic for this bot\r\n- **admin feedback allow_statement [topicId:0|1]**\r\n    - Turn a rule off or on.\r\n- **admin feedback topic [topicId]**\r\n    - View topic details and rules\r\n\r\n<blockquote class=primary>User management</blockquote>\r\n\r\n- **admin user list** \r\n    - Lists all the users added to the database\r\n- **admin check** [**email_address**] \r\n    - Checks if a user exists in the database and any memberships\r\n- **admin add** [**email_address**],[**email_address**]..\r\n    - Adds one or more new users to the database (if not exists)\r\n- **admin delete** [**email_address**],[**email_address**].. \r\n    - Deletes one or more users from the database\r\n- **admin block** [**email_address**] \r\n    - Blocks all my communication with this user regardless \r\n- **admin unblock** [**email_address**] \r\n    - Unblocks user \r\n\r\n<blockquote class=primary>Group and membership management</blockquote>\r\n\r\n- **admin group list** \r\n    - Lists available groups\r\n- **admin group members [groupid]**\r\n    - Lists all members of a group\r\n- **admin group create [groupname:groupid]**\r\n    - Creates a new group. The groupname and the groupid cannot contain spaces and must be separated with colon only if you create a groupid. Groupid is optional but it makes it easier to add users to the group, else the ID will be an incremental number.\r\n- **admin group delete [groupid]**\r\n- **admin group mention [groupid]** \r\n    - Creates a mention tag for all members of the group and will be mentioned by the bot in the current space.\r\n- **admin group addtospace [groupid]**\r\n    - Adds all the members of the group to the current space\r\n- **admin group add** [**groupid:email_addess**]  \r\n    - Adds a user or a group space to a group, you can separate the groupid and or emails with comma to add multiple users to multiple groups (groupid,groupid,groupid:email_address,email_address). To add a space you have to type **this** instead of an e-mail: [**groupid:this**] the space must be a group space and the group must be owned by the bot you are talking to. \r\n- **admin group remove** [**groupid:email_addess**] - Removes a user from a group, you can separate the groupid and or e-mails with comma to remove multiple groups (XX,XX,XX:email_address,email_address)\r\n\r\n<blockquote class=primary>Space controls (admin commands)</blockquote>\r\n\r\n- **admin spaceresponse status** \r\n    - Checks the response status in the current space (command must be issued in the space you wish to see the status)\r\n- **admin spaceresponse enable|disable** \r\n    - Enables or disables group response for any members in the space (command must be issued in the space you wish to enable)\r\n- **admin spaceresponse [email_address]** \r\n    - Enables spaceresponse for that particular user in the particular space (command must be issued in the space you wish to enable)\r\n- **admin joinable** \r\n    - Make a group space joinable with the **space join** command (must be issued inside the space you want to make joinable)\r\n\r\n<blockquote class=primary>Task handler</blockquote>\r\n\r\n- **admin queue report** \r\n    - Checks number of tasks in the queue\r\n- **admin queue purge** \r\n    - Purge task queue\r\n' WHERE `features`.`id` = 1;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## SPACE CONTROLS\r\n---\r\n\r\n<blockquote class=primary>Space commands</blockquote>\r\n\r\n- **space create** \r\n    - *Creates a new space with you and the bot with a autogenerated title*\r\n- **space create [title text ..]** \r\n    - *Creates a new space with you and the bot. The space will be named with UTC timestamp and the specified title*\r\n- **space create [email,email..]** \r\n    - *Creates a new space and adds the users specified with CSV at the same time*\r\n    - *If a new space is created in a team space, the space will be created in the team.*\r\n- **space add [email,email..]** \r\n    - *Adds users to the current space*\r\n- **space kick [email]** \r\n    - Removes a user from a space, note that if you write only \"space kick\" it will remove you. \r\n- **space gencsv** \r\n    - Generates a list of emails based on who is in the space the command is issued. The list is CSV format and will be given to you 1:1 in bulks of 100 emails. If there are too many people > 1000 in the space I might not be able to provide the full list. \r\n- **space list** \r\n    - Lists spaces marked as **joinable** for this bot (shows spaceid)\r\n- **space join [spaceid]**\r\n    - I will invite you to the space \r\n\r\n\r\n' WHERE `features`.`id` = 2;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## JOKE\r\n---\r\n\r\n<blockquote class=primary>JOKE - ENTERTAINMENT</blockquote>\r\n\r\n- **joke** - *The bot will tell a random friendly joke*' WHERE `features`.`id` = 3;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## YOMOMMA\r\n---\r\n\r\n<blockquote class=primary>YOMOMMA - ENTERTAINMENT</blockquote>\r\n\r\n- **yomomma** - *The bot will tell a random yomomma joke*' WHERE `features`.`id` = 4;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## CHUCK\r\n---\r\n\r\n<blockquote class=primary>CHUCK NORRIS - ENTERTAINMENT</blockquote>\r\n\r\n- **chuck** - *The bot will tell a random Chuck Norris joke*' WHERE `features`.`id` = 5;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## REMOVE MESSAGE\r\n---\r\n\r\n<blockquote class=primary>REMOVE LAST MESSAGES</blockquote>\r\n\r\n- **remove last** - *The bot will remove its latest reply in the space*' WHERE `features`.`id` = 6;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## SERVICE\r\n---\r\n\r\n<blockquote class=primary>SERVICE REPORT</blockquote>\r\n\r\n- **service** - *The bot will let you know if the external task service is still alive*' WHERE `features`.`id` = 7;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## USAGE\r\n---\r\n\r\n<blockquote class=primary>USAGE REPORTS</blockquote>\r\n\r\n- **usage** - *The bot will go through the activity logs and give an estimate of how many times a command has been issues and what command is used most frequently*' WHERE `features`.`id` = 8;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## WHO AM I\r\n---\r\n\r\n<blockquote class=primary>WHOAMI - SELF CHECK</blockquote>\r\n\r\n- **whoami** - *The bot will check what groups a user is part of in the bot management system and provide the list of memberships to the user*' WHERE `features`.`id` = 9;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## WHO IS\r\n---\r\n\r\n<blockquote class=primary>WHO IS PERSON</blockquote>\r\n\r\n- **whoid** [**email|name**] - *The bot will look up the user in spark and provide som trivial information about the user*' WHERE `features`.`id` = 10;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## REQUEST\r\n---\r\n\r\n<blockquote class=primary>Request participation</blockquote>\r\n\r\n- **request** - *The bot will add the user that issues this command to the database and to the pre-configured default groups. Makes administration of users easier*' WHERE `features`.`id` = 11;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## SUBSCRIPTION GROUPS\r\n---\r\n\r\n<blockquote class=primary>Usage:</blockquote>\r\n\r\n- **subscribe** ' WHERE `features`.`id` = 12;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## RESIGN AS A USER\r\n---\r\n\r\n<blockquote class=primary>Usage:</blockquote>\r\n\r\n- **resign** - *Deletes the issuer as a user (if exists).* ' WHERE `features`.`id` = 13;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## FEEDBACK COMMANDS\r\n---\r\n\r\n<blockquote class=primary>Introduction</blockquote>\r\n\r\nThis feature opens up for user input. An admin can create one or more topics for the bot. \r\n\r\nEach topic has a set of rules that can allow a user to view and create entries of which other users can vote and comment on.\r\n\r\nEach topic may have a different set of rules, for example a topic can be private (only accessible to a specific group of people) or certain features may be turned off, like commenting or voting. I will notify you if such a rule applies. Only admins can create topics and setup rules. \r\n\r\n<blockquote class=primary>Topic commands</blockquote>\r\n\r\nNote: **[placeholders]** are typed without the **[]** \r\n\r\n- **feedback topics** \r\n    - View all available topics and the correlating topicId.\r\n- **feedback topic [topicId]** \r\n    - View all entries in the specified topicId. This also includes an overview of how many comments and votes per entry.\r\n- **feedback topic [topicId] add [entry text ..]**\r\n    - Create a new entry in the specified topicId\r\n\r\n<blockquote class=primary>Entry commands</blockquote>\r\n\r\n- **feedback entry [entryId]** \r\n    - View the full details of a specified entry including votes and comments. \r\n- **feedback entry [entryId] delete**\r\n    - Deletes the specified entryId if you have created it. Deleting an entry will also delete all votes and comments on the specified entry. This command will not provide a confirmation prompt and cannot be undone.  \r\n\r\n<blockquote class=primary>Votes and Comments</blockquote>\r\n\r\n- **feedback vote [entryId]** \r\n    - Place your vote on the specified entryId. You can revoke the vote by placing the same command a second time.   \r\n- **feedback comment [entryId] [comment text ...]**\r\n    - Place a comment on the specified entryId. \r\n- **feedback comment delete [commentId]**\r\n    - Deletes a comment that you have created using the commentId. The commentId can be found in the comment by viewing the entry details.   \r\n\r\n\r\n' WHERE `features`.`id` = 14;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## DEVICE COMMANDS\r\n---\r\n\r\nWith the device commands you can find your device based on your current integration user.If no integration user is configured to your account the bot will use its own user token to look for devices it has been granted control for. Configuration require admin scopes.  \r\n\r\n<blockquote class=primary>View devices</blockquote>\r\n\r\n- **device list** [**displayName**] or JSON (see example)\r\n     - *List the available devices controlled by this bot or integration (including deviceId)* \r\n\r\n<blockquote class=primary>Status, configurations and commands</blockquote>\r\n\r\n- **device ui** [**deviceId**]\r\n    - *Returns an adaptive card that is locked to the deviceId you type in. Makes it easier to get status, send commands and configs to the device.*\r\n- **device status** [**deviceId**] [**statusKey**] \r\n     - *Get status from the device on a statuskey*\r\n- **device command** [**deviceId**] [**commandKey**] [**{\"attribute\":\"value\"}**]*\r\n     - *Send a command to a device with a command key*\r\n- **device config get** [**deviceId**] [**configKey**] [**-v**]*\r\n     - *Fetch configuration details from device*\r\n     - *The flag -v will display fewer nodes but more info*\r\n- **device config set** [**deviceId**] [**configKey**] [**value**]\r\n     - *Modifies a configuration key on the specified device*\r\n\r\nExample usage:\r\n\r\nThe **deviceId** is acquired using **device list**\r\n\r\n- **device list My system**\r\n     - *Searches for a device named My system and lists the deviceId if found*\r\n- **device list {\"product\":\"SX10\",\"start\":\"100\"}**\r\n     - *Returns a list of devices that matches the product and starts the list from device number 100*\r\n- **device show [deviceId]**\r\n     - *Lists the details of a device, most of the detail keys can be used when searching for devices using the device list command with JSON*\r\n\r\n- **device status [deviceId] audio.volume** \r\n     - *Returns the current volume on the device*\r\n- **device command [decviceId] audio.volume.set {\"Level\": 60}** \r\n     - *Set the volume on the device*\r\n- **device config get [deviceId] SerialPort.Mode**\r\n     - *Gets the configuration details of xConfiguration SerialPort Mode*\r\n- **device config set [deviceId] SerialPort.Mode Off**\r\n     - *Sets the configuration node xConfiguration SerialPort Mode to Off*\r\n\r\n' WHERE `features`.`id` = 15;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## INTEGRATION COMMANDS\r\n---\r\n\r\nIntegration allow you to connect any Webex Teams user to your bot user. Since device and place management is the primary features you get from integration, it is recommended to authorize a device admin or org admin to perform device activation.  \r\n\r\nWhen authorizing new users you might have to log out of your currently logged in user in your browser.\r\n\r\n<blockquote class=primary>Authorize</blockquote>\r\n\r\n- **integration authorize** [**scope** **scope**..] \r\n     - *Generates an authorization URL for your user, login with any Webex Teams user to attach the token to your current user. If no scopes are issued the system default ones will be used.*\r\n- **integration refresh** \r\n     - *Refreshes your currently attached access token*\r\n\r\n<blockquote class=primary>View</blockquote>\r\n\r\n- **integration show**\r\n     - *Displays your current integration settings including the system default scopes.*\r\n- **integration scopes**\r\n     - *Displays the available scopes with description that can be used when generating custom authrorization URLs*\r\n- **integration defaults** \r\n     - *Displays the system default scopes*\r\n\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **integration delete**\r\n     - *Removes your current integration authorization*' WHERE `features`.`id` = 16;";
 	$upgrade_queries["516"][] = "UPDATE `features` SET `usage` = '## PLACES COMMANDS\r\n---\r\n\r\nThis feature may require you to authrorize a user with the following scopes: spark-admin:places_read, spark-admin:places_write. In order to get an activation code you need the identity:placeonetimepassword_create\r\n\r\n<blockquote class=primary>Get details</blockquote>\r\n\r\n- **place list** [**displayName**]\r\n    - List places, search for displayName (optional). \r\n- **place show** [**placeId**] \r\n    - List details for the place, requires placeId.\r\n\r\n<blockquote class=primary>Create</blockquote>\r\n\r\n- **place create** [**displayName**] \r\n    - Creates a new place with the specified displayName\r\n- **place update** [**placeId**] [**newDisplayName**] \r\n    - Updates an existing place with the specified displayName\r\n- **place activate** [**displayName**]\r\n    - Creates a new place using the displayName and returns an activation code for a Cisco Webex Room Device\r\n- **place code** [**placeId**]\r\n    - Activates an existing place and returns an activation code for a Cisco Webex Room Device\r\n<blockquote class=primary>Remove</blockquote>\r\n\r\n- **place delete** [**placeId**]\r\n    - Deletes a place' WHERE `features`.`id` = 17;";
 	$upgrade_queries["516"][] = "UPDATE `site_settings` SET `value`='516' WHERE `settings` = 'dbversion';";
 	//DB version 517
 	$upgrade_queries['517'][] = "ALTER TABLE `tasks` ADD `user_details` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `user`;";
 	$upgrade_queries["517"][] = "UPDATE `site_settings` SET `value`='517' WHERE `settings` = 'dbversion';";
 	//DB version 600 - Workspace integrations + bug fixes and future values
 	$upgrade_queries["600"][] = "INSERT INTO `features` (`id`, `title`, `keyword`, `description`, `usage`) VALUES
(18, 'Workspace integration control', 'ws', 'Control the workspace integrations. Use the bot to switch between integrations and check status. Enable certain features on the workspace integration you have currently mounted. ', '## WORKSPACE INTEGRATIONS\r\n---\r\n\r\n<blockquote class=primary>Workspace integration commands</blockquote>\r\n\r\n- **ws** \r\n    - *Display this list of commands*\r\n- **ws list**\r\n    - *List activated workspace integrations (with ID) that are available for you to mount.*\r\n- **ws show**\r\n    - *Display limited information about the workspace integration currently attached to your account*\r\n- **ws show [id]**\r\n    - *Display limited information about the workspace integration specified by ID. The specified workspace integration do not have to be mounted to your account*\r\n- **ws mount [id]**\r\n    - *Mount an activated workspace integration to your account*\r\n- **ws unmount** \r\n    - *Remove the mounted workspace integration from your account* \r\n- **ws refresh**\r\n    - *The access token will automatically be refreshed when running commands that require the workspace integration access token. This command refreshes the token manually.* \r\n- **ws monitor [deviceId]**\r\n    - *Will send you a message when a webhook is received from this device. Just a notification. You may monitor more than one device.* \r\n\r\nUse the commands **place** and **device** when a workspace integration is mounted to your account.\r\n\r\n\r\n\r\n\r\n ');";
	$upgrade_queries["600"][] = "ALTER TABLE `bots` ADD `card_mode` INT(1) NOT NULL DEFAULT '0' AFTER `defres`;";
	$upgrade_queries["600"][] = "ALTER TABLE `bots` ADD `same_org_response` INT(1) NOT NULL DEFAULT '0' AFTER `card_mode`;";
	$upgrade_queries["600"][] = "CREATE TABLE `ws_webhook_log` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `appId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (id),
  KEY `appId` (`appId`,`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["600"][] = "CREATE TABLE `ws_integration_user` (
  `userid` varchar(255) NOT NULL,
  `appid` varchar(255) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["600"][] = "ALTER TABLE `ws_integration_user`
  ADD UNIQUE KEY `userid` (`userid`,`appid`);";
	$upgrade_queries["600"][] = "CREATE TABLE `ws_device_monitoring` (
  `appId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `notification_groups` text NOT NULL DEFAULT '',
  `botid` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["600"][] = "ALTER TABLE `ws_device_monitoring`
  ADD UNIQUE KEY `appId` (`appId`,`deviceId`,`userid`);";
	$upgrade_queries["600"][] = "CREATE TABLE `ws_device_collector` (
  `appId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `deviceInfo` text NOT NULL DEFAULT '',
  `lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latestStatus` text NOT NULL DEFAULT '',
  `lastEvent` text NOT NULL DEFAULT '',
  PRIMARY KEY (`appId`,`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	
	$upgrade_queries["600"][] = "CREATE TABLE `ws_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appId` varchar(255) NOT NULL,
  `jti` varchar(255) NOT NULL DEFAULT '',
  `action` text NOT NULL DEFAULT '',
  `iat` int(11) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["600"][] = "CREATE TABLE `workspace_integrations` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `client` varchar(255) NOT NULL DEFAULT '',
  `secret` varchar(255) NOT NULL DEFAULT '',
  `refresh_token` varchar(255) NOT NULL DEFAULT '',
  `access_token` varchar(255) NOT NULL DEFAULT '',
  `expires_in` varchar(255) NOT NULL DEFAULT '',
  `refresh_token_expires_in` varchar(255) NOT NULL DEFAULT '',
  `token_timestamp` timestamp NULL DEFAULT NULL,
  `scopes` text NOT NULL DEFAULT '',
  `accessgroup` int(11) NOT NULL DEFAULT '0',
  `activation_jwt` text NOT NULL DEFAULT '',
  `organization` text NOT NULL DEFAULT '',
  `status` text NOT NULL DEFAULT '',
  `error` text NOT NULL DEFAULT '',
  `whs` varchar(255) NOT NULL DEFAULT '',
  `health_timestamp` timestamp NULL DEFAULT NULL,
  `health_status` int(11) NOT NULL DEFAULT '0',
  `activation_status` int(11) NOT NULL DEFAULT '0',
  `webhook_logging` int(11) NOT NULL DEFAULT '0',
  `actions_logging` int(11) NOT NULL DEFAULT '0',
  `data_collection` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$upgrade_queries["600"][] = "ALTER TABLE `debug_log` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '', CHANGE `customfield1` `customfield1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '', CHANGE `customfield2` `customfield2` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '', CHANGE `customfield3` `customfield3` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '', CHANGE `customfield4` `customfield4` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '', CHANGE `customfield5` `customfield5` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';";
	$upgrade_queries["600"][] = "UPDATE `site_settings` SET `value`='600' WHERE `settings` = 'dbversion';";
	//DBVERSION 601
	$upgrade_queries["601"][] = "ALTER TABLE `workspace_integrations` ADD `feature_signage_enabled` INT NOT NULL DEFAULT '0' AFTER `data_collection`;";
	$upgrade_queries["601"][] = "ALTER TABLE `workspace_integrations` ADD `signage_base_url` TEXT NOT NULL DEFAULT '' AFTER `feature_signage_enabled`;";
	$upgrade_queries["601"][] = "ALTER TABLE `workspace_integrations` ADD `signage_content_url` TEXT NOT NULL DEFAULT '' AFTER `signage_base_url`, ADD `signage_assign_url` TEXT NOT NULL DEFAULT '' AFTER `signage_content_url`;";
	$upgrade_queries["601"][] = "UPDATE `site_settings` SET `value`='601' WHERE `settings` = 'dbversion';";
	//DBVERSION 602
	$upgrade_queries["602"][] = "ALTER TABLE `feedback_topic` ADD `public` INT(1) NOT NULL DEFAULT '0' AFTER `accessgroup`, ADD `publickey` VARCHAR(255) NOT NULL DEFAULT '' AFTER `public`;";
	$upgrade_queries["602"][] = "ALTER TABLE `workspace_integrations` ADD `feature_pwa_enabled` INT(11) NOT NULL DEFAULT '0' AFTER `signage_assign_url`, ADD `pwa_base_url` TEXT NOT NULL DEFAULT '' AFTER `feature_pwa_enabled`, ADD `pwa_content_url` TEXT NOT NULL DEFAULT '' AFTER `pwa_base_url`, ADD `pwa_assign_url` TEXT NOT NULL DEFAULT '' AFTER `pwa_content_url`;";
	$upgrade_queries["602"][] = "UPDATE `site_settings` SET `value`='602' WHERE `settings` = 'dbversion';";
	//DBVERSION 603
	$upgrade_queries["603"][] = "ALTER TABLE `ws_action_log` ADD `full` TEXT NOT NULL DEFAULT '' AFTER `iat`;";
	$upgrade_queries["603"][] = "ALTER TABLE `workspace_integrations` ADD `description` TEXT NULL DEFAULT '' AFTER `name`;";
	$upgrade_queries["603"][] = "UPDATE `site_settings` SET `value`='603' WHERE `settings` = 'dbversion';";
	//DBVERSION 604
	$upgrade_queries["604"][] = "ALTER TABLE `bot_webhook` ADD `access_key` VARCHAR(255) NOT NULL DEFAULT '' AFTER `groupid`;"; 
	$upgrade_queries["604"][] = "ALTER TABLE `tasks` ADD `webhook` VARCHAR(255) NOT NULL DEFAULT '' AFTER `parentMessageId`, ADD `message` VARCHAR(255) NOT NULL DEFAULT '' AFTER `webhook`, ADD `signature` VARCHAR(255) NOT NULL DEFAULT '' AFTER `message`;";
	$upgrade_queries["604"][] = "ALTER TABLE `bots` ADD `botsecret` VARCHAR(255) NOT NULL DEFAULT '' AFTER `same_org_response`;";
	$upgrade_queries["604"][] = "ALTER TABLE `workspace_integrations` CHANGE `access_token` `access_token` VARCHAR(5000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';";
	$upgrade_queries["604"][] = "UPDATE `site_settings` SET `value`='604' WHERE `settings` = 'dbversion';";
	if (isset($upgrade_queries[$version]))
		return $upgrade_queries[$version];
	return false;
	
}

if(!empty($_GET['function'])){ $upgradeFunction = $_GET['function']; } else { $upgradeFunction = ''; }
if(!empty($_GET['currentdbver'])){ $currentDBver = $_GET['currentdbver']; } else { $currentDBver = ''; }
if(!empty($_GET['newdbver'])){ $newDBver = $_GET['newdbver']; } else { $newDBver = ''; }


if($upgradeFunction == 'upgradedb')
{
	$upgradedb = upgradedb($db_local,$currentDBver,$newDBver);
	
	echo '
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">DB Upgrade Status</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body"  style="height: 250px; overflow: auto;">
					<table class="table">
						<tr>
							<th width="75%">ID</th>
							<th>Status</th>
						</tr>';
						
						foreach ($upgradedb as $version => $queryobj) {
							foreach ($queryobj as $resultkey => $resultobj ) {
									echo'<tr>
										 <td style="word-wrap: break-word">'. $resultobj[0] .'</td>';
									if($resultobj[1] == '1')
									{
										echo '<td>SQL successful</td>';
									}
									else
									{
										echo '<td>' . $resultobj[1] . '</td>';
									}
									echo'
								</tr>';
								
							}
									
							
						}
				echo '</table>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-primary btn-block" onclick="location.href = \'index.php?id=upgrade\';" >CLOSE</button>
						</div>
					</div>
				</div>
			</div>';
	$currconfig = readconfigfile();
	writeconfigfile($currconfig);
}
elseif($upgradeFunction == 'loadsitesettings')
{
	$loadsitesettings = $db_local->insertsitesettings();

	if(!empty($loadsitesettings))
	{
		if($loadsitesettings[0] === true)
		{
			redirect("index.php?id=upgrade&function=successful-insertsitesettings");			
		}
	}
}

function upgradedb($db_local,$currentDBversion,$NewDBVersion) 
{
	$upgradedb = array();
	$version = $currentDBversion + 1;
	while ($version <= $NewDBVersion)
	{
		$upgrade_queries = upgradeQuery($version);
		if ($upgrade_queries) {
			foreach($upgrade_queries as $key => $value) {
				$upgradedb[$version][$key][] = $value;
				$upgradedb[$version][$key][] = $db_local->query($value);
			}
		}
		$version++;
	}
	return $upgradedb;
}

?>

<div class="row">
	<div class="col-md-3">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">WBM Version</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class='row'>
					<div class='col-md-10'>
						WBM Database Version
					</div>
					<div class='col-md-2'>
						<?php echo $db_local->checkdbversion()[0]['value']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>