<?php 
if ($windowid != "login" and !verify()) header("Location: index.php?id=login"); 
$r_y = $e_y = $o_y = $i_y = $d_y = "checked";
$r_n = $e_n = $o_n = $i_n = $d_n = "";
$card = issetor($_GET['card']);
$botid = issetor($_GET['botid']);

$note = (!$botid) ? " - Select a bot":"";

//PAGE OPERATIONS
if (isset($_POST['card_add']) or isset($_POST['card_update'])) {
	$card_verification = json_decode($_POST['card_body'], true);
	if ($card_verification) {
		$_POST['card_body'] = $db_local->quote(json_encode($card_verification, JSON_PRETTY_PRINT));
		if (isset($_POST['card_add'])) {
			$_POST['reply'] = $db_local->quote($_POST['reply']);
			if ($db_local->cardAdd($_POST)) {
				echo alerts("success", "Success", "Card successfully added!");
			}
		} else {
			$_POST['reply'] = $db_local->quote($_POST['reply']);
			if ($db_local->cardUpdate($_POST, $_POST['cardid'])) {
				echo alerts("success", "Success", "Card successfully updated!");
			}
		}
	} else {
		echo alerts("warning", "Warning", "Invalid JSON!");
	}
}

if (issetor($_POST['card_delete'])) {
	$db_local->cardDelete($_POST['cardid']);
	redirect("index.php?id=cards&botid={$botid}");
}

if (issetor($_POST['data_clear_entries'])) {
	if (isset($_POST['datalist']) and count($_POST['datalist']) and $card) {
		foreach ($_POST['datalist'] as $key => $value) {
			$db_local->cardDeleteDataEntry($card, $value);
		}
	}
}

if (issetor($_POST['data_clear_all']) and $card) {
	$db_local->cardDeleteData($card);
}

if (isset($_POST['test_card'])) {
	$spark->cardTestCardMessage($card, $_SESSION['userid']);
}

if (issetor($_POST['cancel_card'])) {
	redirect("index.php?id=cards&botid={$botid}");
}

if ($card) {
		$actionname = "card_update"; 
		$submitname = "Update card";
		$edit = "Update card (cardId: <b>$card</b>)";
		$cardinfo = $db_local->cardFetchCards($db_local->quote($card));
		$form = (issetor($_GET['card'])) ? "" : "&topic={$cardinfo[0]['id']}";
		list($i_y, $i_n) = ($cardinfo[0]['input_active']) ? array("checked", ""):array("","checked");
		list($d_y, $d_n) = ($cardinfo[0]['delete_active']) ? array("checked", ""):array("","checked");
		list($e_y, $e_n) = ($cardinfo[0]['echo_active']) ? array("checked", ""):array("","checked");
		list($o_y, $o_n) = ($cardinfo[0]['update_active']) ? array("checked", ""):array("","checked");
		list($r_y, $r_n) = ($cardinfo[0]['redirect_reply']) ? array("checked", ""):array("","checked");
		
		$created_by = "<input type='hidden' value='{$_SESSION['email']}' name='created_by'>";
}
else {
		$actionname = "card_add";
		$submitname = "Create card";
		$edit = "Create card";
		$groupinfo = array("0"=>array("id"=>"", "groupname"=>""));
		$form = "";
}

//Page tooltips
$tooltips = array(
        'cards'=>tooltip('Cards','Create a card form and collect data from users, card flows are not supported. Users may submit one data entry per card. If you want to re-use the same card, please duplicate the card to create a new one. You can also send informational card without options for submitting data. In order to collect data, it is required for the bot to have a Card Attachment webhook!'),
        'selectBot'=>tooltip('Select a bot','Created cards will belong to the bot you select.'),
		'topics'=>tooltip('Topics','Here are all the topics that has been created for this bot. Only bot-admins or site admins can create topics.'),
		'cardTitle'=>tooltip('Card title','The title of your card'),
		'cardRedirect'=>tooltip('Redirect reply','If you post an input form into a group space, you may want the bot to send the feedback to the user 1:1 instead of in the group space. If this is disabled, the feedback will be sent to the same space where the form is submitted. This setting will redirect any feedback the card submission may produce if enabled to a direct conversation with the user that submitted the form: Reply, data echo, access permission and error messages'),
		'cardBody'=>tooltip('Card body','Please paste the adaptive card JSON. Use the adaptive card editor found on https://developer.webex.com to create the design.'),
		'cardInput'=>tooltip('Card input','If enabled, the bot manager will collect incoming data from users that submit authorized data from this card.'),
		'cardReply'=>tooltip('Card reply','If enabled, the bot will send the specified text back to the same space (depends on the re-direct setting) the submission came from when the data is successfully collected.'),
		'cardDelete'=>tooltip('Delete card on submit','When the user submits the card and the data is successfully collected, the card will be deleted so the user cannot submit anymore.'),
		'cardSettings'=>tooltip('Card settings','Configure the card behavior.'),
		'cardOverwrite'=>tooltip('Overwrite data','Multiple submissions from the same user will overwrite the previous submission. This can be useful if the card is posted in a group space and when the card is not deleted after submission. If set to off, only one submission will be accepted. '),
		'cardEcho'=>tooltip('Echo submission','The bot will echo the submission data into the same space where it was submitted (depending on the re-direct setting). If the the space is a group space, the person that submitted the data will be mentioned in the echo.'),
		'cardAccess'=>tooltip('Card access group','Only people part of the selected accessgroup can submit data from this card!'),
		'entries'=>tooltip('Entries','Entries are sub-data to a topic, it can either be open so users can add entires or locked so that users can only vote on pre-defined entries in a topic for example. Users may only delete their own entries.'),
		'comment'=>tooltip('Commenting','Users may comment on entries in a topic, this could also work as a voting system to force users to write feedback on a certain entry instead of just a +1. Users may only delete their own comments'),
		'vote'=>tooltip('Voting','Voting is a simple +1 for each people that has voted on a entry. If the topic is "What do you think about the seminar?" with entries "A: Good", "B: OK", "C: Bad", a user can vote for the entries. A user can vote for more than one entry, but only place one vote per entry. Users may also revoke their vote on an entry.')
);

$card_id = ($card) ? "<input type='hidden' value='{$card}' name='topicid'>":"";
$group_options = $generate->groupOptions(issetor($cardinfo[0]['accessgroup']));
?>
<script language="JavaScript">
function toggleMember(source, name) {
	  checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggleNonMember(source, name) {
	checkboxes = document.getElementsByName(name);
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	} 
</script>
<!-- Content Header (Page header) -->
<div class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1 class="m-0 text-dark">Cards</h1>
         </div><!-- /.col -->
      </div><!-- /.row -->
   </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
   <div class="container-fluid">
<div class='row'>
         <div class='col-lg-6'>
            <div class='card card-primary card-outline'>
               <div class='card-header'>
                  <h3 class='card-title'><?php echo $tooltips['cards']; ?> <?php echo $edit; ?></h3>
               </div>
               <div class='card-body'>
					<form name='groups' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>		
					<div class='form-group'>
						<label for='BotSelection'><?php echo $tooltips['selectBot']; ?> Select A Bot</label>
						<br>
						<?php $generate->botGenSelector('cards', issetor($botid)); ?>
					</div>
					<?php 
					if($botid)
					{
					echo "
					<div class='form-group'>
						<label>{$tooltips['cardTitle']} Card title</label>
						<br>
						<input type='text' class='form-control' placeholder='Card title' required name='title' value='".issetor($cardinfo[0]['title'])."'>
						<input type='hidden' value='{$botid}' name='botid'>
						<input type='hidden' value='{$_SESSION['userid']}' name='userid'>
					</div>
					<div class='form-group'>
						<label>{$tooltips['cardBody']} Card body</label>
						<br>
						<textarea class='form-control' name='card_body' required placeholder='Card JSON'>".issetor($cardinfo[0]['card_body'])."</textarea>
					</div>
					<div class='form-group'>
						<label>{$tooltips['cardSettings']} Card settings</label>
						<br>
						<table class='table'>
							<tr>
								<th>{$tooltips['cardInput']} Collect card data on user submit:</th>
								<td><label>On</label> <input type='radio' $i_y name='input_active' value='1'>
								<label>Off</label> <input type='radio' $i_n name='input_active' value='0'></td>
							</tr>
							<tr>
								<th>{$tooltips['cardDelete']} Delete card on user submit:</th>
								<td><label>On</label> <input type='radio' $d_y name='delete_active' value='1'>
								<label>Off</label> <input type='radio' $d_n name='delete_active' value='0'></td>
							</tr>
							<tr>
								<th>{$tooltips['cardOverwrite']} Overwrite on multiple submissions:</th>
								<td><label>On</label> <input type='radio' $o_y name='update_active' value='1'>
								<label>Off</label> <input type='radio' $o_n name='update_active' value='0'></td>
							</tr>
							<tr>
								<th>{$tooltips['cardEcho']} Echo submissions:</th>
								<td><label>On</label> <input type='radio' $e_y name='echo_active' value='1'>
								<label>Off</label> <input type='radio' $e_n name='echo_active' value='0'></td>
							</tr>
							<tr>
								<th>{$tooltips['cardReply']} Reply on user submit:</th>
								<td><input type='text' name='reply' placeholder='Your submission has been recorded, thank you!' class='form-control' value=\"".issetor($cardinfo[0]['reply'])."\"'></td>
							</tr>
							<tr>
								<th>{$tooltips['cardRedirect']} Re-direct bot replies to 1:1:</th>
								<td><label>On</label> <input type='radio' $r_y name='redirect_reply' value='1'>
								<label>Off</label> <input type='radio' $r_n name='redirect_reply' value='0'></td>
							</tr>
							<tr>
								<th>{$tooltips['cardAccess']} Card accessgroup:</th>
								<td><select name='accessgroup' class='form-control'>
										<option value=''>Select access group</option>
											$group_options
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class='box-footer'>
						<input type='submit' name='$actionname' class='btn btn-primary' value='{$submitname}' />";
						
						if ($card) 
						{
							echo " <input type='submit' name='test_card' class='btn btn-primary' value='Send me a test' />";
							echo "<input type='hidden' name='cardid' value='$card'>";
						    echo "<input type='submit' class='btn btn-danger float-right' style='margin-left: 2px' $link_confirm name='card_delete' value='Delete card'>";
							echo "<input type='submit' class='btn btn-danger float-right' name='cancel_card' value='Cancel'>";
						}
					echo "
					</div>
					";
					}?>
				</form>
			</div>
		</div>
	</div>	
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'><?php echo $tooltips['topics']; ?> Cards</h3>
			</div>
			<div class='card-body'>
				<?php
				if($botid)
					{
				?>
				<table width='100%' id='cards' class='table table-bordered table-striped'>
					<thead>
						<tr>
							<th width='10%'>ID</th>
							<th width='60%'>Card</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<?php
if ($card) {
	//$entries = $generate->feedbackGenTopicEntries($botid, $topic);
	?>
<div class='row'>
	<div class='col-lg-12'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>Card data</h3>
			</div>
			<div class='card-body'>
			<form name='dataentries' method='post' action='<?php echo formUrl($_GET); ?>' enctype='multipart/form-data'>
				<table width='100%' id='card_data' class='table table-bordered table-striped'>
					<thead>
						<tr>
							<th width='10%'>Select entry</th>
							<th width='60%'>Collected from</th>
							<th>Collected data</th>
						</tr>
					</thead>
				</table>
						
					<div class='form-group'>
						<input type='submit' class='btn btn-danger' <?php echo $link_confirm; ?> name='data_clear_entries' value='Delete selected entries'> 
						<input type='submit' class='btn btn-danger' <?php echo $link_confirm; ?> name='data_clear_all' value='Delete all data for this card'>
					</div>
			</div>
		</div>
	</div>
	
	</div>
	<?php
		}
		
	?>

</div>
</div>