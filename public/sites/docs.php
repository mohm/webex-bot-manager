<?php if ($windowid != "login" and !verify()) header("Location: index.php?id=login");

function toc($d) {
    $tocfile = __FILE__;
    
    $file = fopen($tocfile,"r");
    
    $html_string = fread($file, filesize($tocfile)); 
    fclose($file);
    
    $pattern = '/<h[2-'.$d.']*[^>]*>.*?<\/h[2-'.$d.']>/';
    $whocares = preg_match_all($pattern,$html_string,$winners);
    
    $heads = implode("\n",$winners[0]);
    $heads = str_replace('<a name="','<a href="#',$heads);
    $heads = str_replace('</a>','',$heads);
    $heads = preg_replace('/<h([1-'.$d.'])>/','<li class="toc$1">',$heads);
    $heads = preg_replace('/<\/h[1-'.$d.']>/','</a></li>',$heads);
    
    $contents = '<div class="card card-outline">
					
					<div class="card-body">
						<div class="doc-main">
						
						<h1>Table of contents</h1><br> '.$heads.'

						</div>
					</div>
				</div>';
    echo $contents;
}

?>

<!-- /.content-header -->
<div class='content'>
	<div class='container-fluid'>
		<div class="row">
			<div class="col-lg-12">
				<div class="card card-primary card-outline">
					
					<div class="card-body">
						<div class='doc-main'>
						<h1>Webex Bot Manager - User guide</h1>
						
						Webex Bot Manager is a simple yet advanced tool to get started with your bots. 
						
						</div>
					</div>
				</div>
				<?php echo toc(5);?>
				<div class="card card-outline">
					
					<div class="card-body">
						<div class='doc-main'>
						
						<h2><a name="introduction"></a>Introduction</h2>

						<p>
						Webex Bot Manager (WBM) was designed to make it simple to bring a bot to life and wraps around most of the Webex Teams API. 
						A number of features are already built in so you don´t have to do the basic programming, but at the same time opens up for 
						you to program your own features.
						</p>
						
						
						</div>
					</div>
				</div>
				
				<div class="card card-outline">
					
					<div class="card-body">
						<div class='doc-main'>
						
						<h2><a name="gettingstarted"></a>Getting started</h2>

						<p>
						Right now you have probably just installed WBM and have a ton of questions on what this thing can do. Where to start..?
						</p>
						
						<p>
						WBM requires you to add one user (you) and one bot. If you want to talk to your bot and get a response, your bot must have an active webhook created by WBM 
						and WBM must be deployed on a public server reachable by the Webex Teams Webhook service. If you installed WBM on a server that is not public you will still be able 
						to broadcast messages, manage users, spaces, devices and teams but you will not be able to communicate with the bot and use the bot communication features. 
						</p>
						
						<h3><a name="botoverview"></a>My bots</h3>
						
						<p>
						If you click on the link <a href='index.php?id=bots'>My Bots</a> you will see an overview of your added bots. Here you will see if the bot has an active domain restriction policy. 
						This is important since you might not want your bot to respond to people outside your organizations domain. You will also see how many responses a bot currently have, how many webhooks it has, 
						if it has an access token and if the bot is currently the primary bot. 
						</p>
						
						<h4><a name="primary"></a>The primary bot</h4>
						
						<p>
						Your primary bot just have a few extra functions compared to the other bots. The primary bot access token is used by WBM to look up users and send out forgotten passwords to users. 
						Other bots are just idle until you select them for particular tasks, for example sending a message. 
						</p>
						
						<h4><a name="primary"></a>Adding more bots</h4>
						
						<p>
						Add as many bots you want, WBM will keep track of them and allow you to select the bot for the activity you choose. Type in the access token of the bot you want to add and click "Add bot". New bots will not have webhooks or responses by default so it must be configured. 
						</p>
						
						<h4><a name="configure"></a>Configure your bots</h4>
						
						<p>
						In <a href='index.php?id=bots'>My Bots</a> you can click on a bot to configure it individually. It is from here you create webhooks, responses or tasks and setup different restriction levels. 
						On the bot profile page you will see the bot details, botId, accesss token, domains it is restricted to, default response and if the bot is your primary bot.
						</p>
						
						<h5><a name="botid"></a>Bot ID</h5>
						
						<p>
						The bot ID is used to identify the bot in WBM and must be used in some casess when integrating your apps to WBM. It is listed here so you can easily find it. 
						</p>
						
						<h5><a name="accesstoken"></a>Access token</h5>
						
						<p>
						The access token is Webex Teams representation of the bot. It is used to get details about the bot and when performing actions on behalf of the bot. When you are on the bot profile, it is that bot 
						access token it is using to verify its validity. The access token must bee valid and belong to the bot you have added for the token to be confirmed valid. WBM stores the access token so you don´t have to remember it. 
						</p>
						
						<h5><a name="domainrestriction"></a>Domain restriction</h5>
						
						<p>
						Select which domains the bot will respond to. Let´s say the bot has a domain restriction to example.com. This bot will only look at messages that comes from a user with @example.com while all other domains will be discarded right away. 
						You can add more domains in the WBM settings and select multiple domains you want the bot to reply to. If no domain is selected, the bot will reply to any domain. But fear not, you can apply restrictions on many levels. See command restrictions, webhook restrictions and space restrictions.  
						</p>
						
						<h5><a name="defaultresponse"></a>Default response</h5>
						
						<p>
						The default response is the response the bot will send back to the user if it could not find the matching keyword. Usually you want to hint the user about a command they can use to see which commands the bot has to offer, for example <b>help</b>. 
						A good default response can be: "Unknown command, please type <b>help</b> to get an overview of my commands.". If nothing is filled in here, the bot will be silent if it did not understand the command.    
						</p>
						
						<h5><a name="purge"></a>Purge a bot</h5>
						
						<p>
						Purging a bot from WBM will delete all bot data (responses, reestrictions, webhook links etc) and this cannot be undone. It will not delete the registered webhooks in Webex Teams, but any webhooks coming in later will be igonored because the webhook must be in sync with WBM. See Webhooks.  
						</p>
						
						
						
						
						</div>
					</div>
				</div>
						
						<table class='rounded'>
							<tr>
								<td><b>Options</b><td><b>Summary</b>
							<tr>
								<td>ME<td>This page will display the bot details from the Webex Teams API in a list. 
							<tr>
								<td>ACTIVTITY<td>Select which activity you want to do with the bot, send messages, manage devices or teams and spaces on behalf of the current bot. 
							<tr>
								<td>RESPONSES<td>Define which commands the bot should respond to and what the response should be. You can also configure a response to trigger an external task. See the API guide for more about external tasks.
							<tr>
								<td>WEBHOOKS<td>Create webhooks for the bot, either custom or use the pre-defined (recommended). You can create a 1:1 filter webhook or a group filter webhook. See the webhooks section for more information. 
							<tr>
								<td>SETTINGS<td>The settings page is where you configure the bot basics. Here you can update the bot details (if you change its avatar or update the displayname etc.), update the access token, enable domain restriction, set the default response
												or promote the bot to primary. If the bot is already primary, you will see a checkmark. From this page you can also delete the bot.  
						</table>

						<h3>Webhooks</h3>

						<p>Two webhooks are automatically generated for the first bot that is added through the setup wizard (you could also choose to not enable this by default). These two webhooks are pre-built as a 1:1 filter webhook and a group filter webhook. </p>

						<table class='rounded'>
							<tr>
								<td><b>Type</b><td><b>Description</b>
							<tr>
								<td>1:1 filter<td>We receive an event when someone is talking to the bot privately. 
							<tr>
								<td>Group filter<td>We recieve an event when someone is talking to the bot in a space with many people. 
						</table>

						<p>The Webex Bot Manager do not currently support other webhooks than messages created but it does allow you to create and keep track of custom webhooks that may go to other web services as well. You also get the oppertunity to easily delete a bot webhook.</p>

						<p>The pre-created webhooks are called "direct" and "group" for a reason. The webhook that has the name "group" has a a built in access restriction, which means you can control who can talk to the bot in a group space. If you create your own group webhook to the internal API and give it a different name than group,
						it will bypass the built in restriction and the bot will reply in groups to everyone (domain restriction still apply, please see the domain restriction section). </p>

						<p><b>Three levels of access for the group webhook</b></p>

						<table class='rounded'>
							<tr>
								<td><b>Type</b><td><b>Description</b>
							<tr>
								<td>Individual access<td>You can grant a single (or several) person(s) access to talk to the bot in a group space. You can grant individual access through the bot (admin controls) if enabled, or through "spaces & teams". <br><br>
														 Bot command: <b>admin spaceresponse email@domain.com</b>
														     
							<tr>
								<td>Space access<td>We recieve an event when someone is talking to the bot in a space with many people. 
							<tr>
								<td>Global access<td>We recieve an event when someone is talking to the bot in a space with many people. 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


<?php 

//Cards supported flags: 
// wbm_cancel_card, wbm_command, wbm_delete_card

?>
