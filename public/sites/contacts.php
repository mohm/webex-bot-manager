<?php 
if ($windowid != "login" and !verify()) header("Location: login.php"); 
			
set_time_limit(6000);
$mainbot = $db_local->botGetMainInfo();
$botid = $mainbot['id'];
$contactid = issetor($_GET['contactid']);

//Page tooltips
$tooltips = array(
		'addUsers'=>tooltip('Adding users','Search for Name (within your own org) or e-mail address to find users in Webex Teams. If you have a CSV list of e-mails you can bulk import users. All data is fetched from Webex Teams and stored in WBM as virtual references so you can send messages to them. Users are not notified when added as a regular contact.'),
		'bulkImport'=>tooltip('Bulk import','Type in full e-mail addresses in Comma Separated Values (CSV) format. I.e. user@example.com,user@example.com.. etc. (Note that e-mails will only work for users within the same organization as the bot) and click "Import".'),
		'importedUsers'=>tooltip('Imported users','Users that are added to WBM. Use the checkboxes and select one or many users and press update to update user details and avatars from Webex Teams. By pressing delete, the selected user references will be removed from WBM along with all their group memberships.'),
		'addMembership'=>tooltip('Add to group','When imported, users will get added to all the relevant default groups owned by the Primary bot and default public groups. Here you can select an additional group the imported users should be added to. Note that you can use this to add users to groups even if they already exist as an imported user.'),
		'userSettings'=>tooltip('User settings','All users that are added to WBM are stored as contact references and are not users of WBM by default. Here you can set a user password or autogenerate passwords. When a password is set the user gains site admin privileges and can log in to WBM.'),
		'siteAdmin'=>tooltip('Site admin','Indicates if this user can log into this instance of WBM as site admin.'),
        'linkWSIntegration'=>tooltip('Link workspace integration','Links a workspace integration to the user. This works the same way as integrations but this overrides the authorized user (if any). When you use the bot features for devices and places, the bot will use the linked workspace integration token on behalf of the user speaking to the bot. Unlink to use the authorized user instead, it is simple. Only one integration can be in use per user at a time.'),
        'manualPassword'=>tooltip('Set password manually','Makes this user site admin or changes the password. The user is not notified via the Primary bot if the password is set or changed using the manual method.'),
		'generateRandom'=>tooltip('Generate password','Makes this user site admin. By generating a password, the Primary bot will instantly message this user 1:1 with the generated password and a link to the WBM instance in Webex Teams, the user is advised to change the password after login.'),
		'apiToken'=>tooltip('API token','The API token is needed if you want to use the task or the notification API. This is for authenticating external integrations with WBM. For more information, read the API guide.'),
		'removeAdmin'=>tooltip('User settings','Removes the login privelileges for this user, the user will just become a normal contact reference again. This does not affect the users access to the admin feature for a given bot (if any).'),	
		'groupResponse'=>tooltip('Group response','Using the bot space feature, you can grant certain users access to speak with the bot in group spaces, if this user has singular access to speak to a bot in a space, the space will be listed here. You can also remove the access. '),
		'MailerbulkImport'=>tooltip('Mailer/AD Import','Mailer Bulk Import allows you to add an AD group/email mailer into WBM. It will create a group and import all the users recursively.'),
		'MailerRecursive'=>tooltip('Recursive Search','Some Mailers/AD groups are nested. This option allows you to either search a top level search or add users recursively'),
);

//Page operations 
if (issetor($contactid)) {
	if(isset($_POST['authorize_new'])) {
		$authurl = $spark->integrationGenAuthUrl($contactid, implode(' ', $_POST['scopes']));
		redirect($authurl);
	}
}


if(isset($_POST['add_contact'])) {
	$user_request = array('recepientValue' => $_POST['contactid'], 'recepientType' => 'id', 'sender' => $botid);
	$user_info = $spark->peopleGet($user_request);
	if (isset($user_info['items'][0]['id'])) {
		if(!array_key_exists('avatar', $user_info['items'][0])) { $user_info['items'][0]['avatar'] = ''; }
		$db_local->contactAdd($user_info, $botid);
		redirect("index.php?id=contacts&contactid={$user_info['items'][0]['id']}&feedback=success-UserAdd");
	} else {
	    redirect("index.php?id=contacts&feedback=warning-UserNotFound");
	}
}

if(isset($_POST['wslinkuser'])) {
   
    if (issetor($_POST['contactid'])) {
        
        if (isset($_POST['wsintegrationselect'])) {
            
            var_dump($_POST);
                
            $db_local->wsIntegrationUpdateLinkUser($_POST['wsintegrationselect'], $_POST['contactid']);
                
        } 
        
    }
}

if (isset($_POST['bulk_add'])) {
	//Adds 50 users at a time (pace control)
	$users = $_POST['users_csv'];
	$group = $_POST['groups'];
	$csv = preg_replace("/\r|\n|\s+/", "", $users);
	$csv = explode(",", $csv);
	$taskid = $db_local->generatetaskid();
	//insertintotaskqueue($taskid, $taskname, $type, $parsel, $botid, $sentby) {
	$custom = array('group'=>$group, 'loggedinuser'=>$_SESSION['login_user']);
	$result = $db_local->taskQueueInsert($taskid, 'Add Contacts', 'addtolocal', $csv, $botid, $custom);
	if($result == 1)
	{
		echo "<script>	Swal.fire({
		title: 'Success!',
		html: 'Contact Bulk Add <br> Request sent to queue for processing',
		type: 'success',
		confirmButtonText: 'Close',
		})
		</script>";
	}
}
if (isset($_POST['retry_addtolocal'])) {
	$a = 0;
	$group = $_POST['groups'];
	//Start request intervals
	echo $spark->bulkSplitRequestPost("addtolocal", $_POST['personids'], $botid, 0, 50, 150, 10, 150, $group);
	//End request intervals
}
if (isset($_POST['mailer_add'])) {
	//Getting Mailer and removing @example.com for the searches to AD searches to work properly.
	if(strpos($_POST['mailer_search'], '@'))
	{
	$mailer = substr($_POST['mailer_search'], 0, strpos($_POST['mailer_search'], "@"));
	}
	else
	{
		$mailer = $_POST['mailer_search'];
	}
	$group = $_POST['groups'];
	
	$connection = $ldap->connect($config['ADserver'],$config['ADport']);

	// Bind with LDAP instance
	$ldap->bind($connection,$config['ADuser'],$config['ADpass']);

	// Search with a wildcard
	$GroupSearch = $ldap->search($connection,$config['BaseDN'],'CN=' . $mailer);
	
	if($GroupSearch['count'] == 0)
	{
		echo "<script>	Swal.fire({
		title: 'Mailer/AD Group Not Found!',
		html: '',
		type: 'error',
		confirmButtonText: 'Close',
		})
		</script>";
	}
	else
	{
		$GroupSearchDN = $GroupSearch[0]['distinguishedname'][0];
		$taskid = $db_local->generatetaskid();
		$custom = array('group'=>$group, 'loggedinuser'=>$_SESSION['login_user']);
		$result = $db_local->taskQueueInsert($taskid, 'Add Mailer/AD Group', 'adgroup', $mailer, $botid, $custom);
		if($result == 1)
		{
			echo "<script>	Swal.fire({
			title: 'Success!',
			html: 'Mailer/AD Group Found <br> Sent to queue for processing',
			type: 'success',
			confirmButtonText: 'Close',
			})
			</script>";
		}
	}
	
}
						
if (isset($_POST['delete_updatelocal'])) {
	if (count($_POST['personids']) > 0) {
		foreach ($_POST['personids'] as $key => $value) {
			$db_local->contactRemove($value);
		}
	}
}
if (isset($_POST['bulk_delete']) or isset($_POST['bulk_update'])) {
	$context = (isset($_POST['bulk_delete'])) ? "delete":"update";
	if (isset($_POST['userlist'])) {
		$data = $_POST['userlist'];
		if (isset($_POST['bulk_delete'])) {
			foreach ($data as $key => $value) {
				$db_local->contactRemove($value);
				if ($value == $_SESSION['userid']) {
					session_destroy();
					redirect('index.php?id=login');
				}
			}
		} else {
			$taskid = $db_local->generatetaskid();
			$custom = $_SESSION['login_user'];
			$result = $db_local->taskQueueInsert($taskid, 'Contact Update', 'updatelocal', $data, $botid, $custom);
			if($result == 1)
			{
				echo "<script>	Swal.fire({
				title: 'Success!',
				html: 'Contact Update request sent to queue for processing',
				type: 'success',
				confirmButtonText: 'Close',
				})
				</script>";
			}
		}
	}
	else {
		echo feedbackMsg("No users selected","please select users to $context them","warning");
	}
}
if(isset($_POST['update_contact'])) {
	$updateid = $db_local->quote($_POST['contactid']);
	$user_request = array('recepientValue' => $updateid, 'recepientType' => 'id', 'sender' => $botid);
	$user_info = $spark -> peopleGet($user_request);
	if (count($user_info['items'])) {
		$db_local->contactUpdate($user_info);
		echo alerts("success", "Success", "User details was updated");
	} else {
		echo alerts("error");
	}
}
if (isset($_GET['add']) and isset($_GET['group']) and isset($_GET['contactid'])) {
		$db_local -> groupAddContact(array('groupid' => $db_local->quote($_GET['group']), 
										   'contactid' => $db_local->quote($_GET['contactid'])));
}
if (isset($_GET['remove']) and isset($_GET['group']) and isset($_GET['contactid'])) {
		$db_local -> groupRemoveContact($db_local->quote($_GET['group']), $db_local->quote($_GET['contactid'])); //Removes a group membership
}
if (isset($_GET['special_access_remove'])) {
	$db_local -> adminRemoveUserGroupResponseAcl($db_local->quote($_GET['botid']), 
											   $db_local->quote($_GET['spaceid']), 
											   $db_local->quote($_GET['special_access_remove']));
}
if(isset($_POST['delete_contact'])) {
	if ($_POST['contactid'] == $_SESSION['userid']) {
		echo alerts("error", "Oh no!", "you cannot delete yourself!");
	} else {
		$db_local -> contactRemove($db_local->quote($_POST['contactid']));
		redirect('index.php?id=contacts');
	}	
}
$userprofilename = "Users";
if (isset($_GET['contactid'])) {
	$userprofilename = "<a href='index.php?id=contacts'><i class='fa fa-angle-left'></i> Users</a> ";
}

echo "<!-- Content Header (Page header) -->
<div class='content-header'>
	<div class='container-fluid'>
		<div class='row mb-2'>
			<div class='col-sm-6'>
				<h1 class='m-0 text-dark'>Users</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
 <div class='content'>
      <div class='container-fluid'>";

if (isset($_GET['contactid'])) {
	$userid = $db_local->quote($_GET['contactid']);
	
	if (isset($_POST['genpass'])) {
		$password = generatePassword();
		$hashpw = password_hash($password, PASSWORD_DEFAULT);
		$host = getHostUrl();
		$text = "Here is your new login passord to the [Webex Bot Manager]($host), please change the password when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
		$db_local->userUpdateSettings('update',$hashpw, $userid, '1');
		$spark->messageSend($spark->messageBlob($text, $userid, $botid));
	}
	if (isset($_POST['gentoken'])) {
		$token = hash('md5', generatePassword());
		$db_local -> userUpdateSettings('token',$token,$userid);
	}
	if (isset($_POST['setpass'])) {
		$password = $db_local -> quote($_POST['password']);
		$conf_password = $db_local -> quote($_POST['pwconfirm']);
		if (!empty($password)) {
			$message = feedbackMsg("Success!", "New password set", "success");
			if ($password == $conf_password) { 
				$hashpw = password_hash($password, PASSWORD_DEFAULT);
				$db_local -> userUpdateSettings('update', $hashpw, $userid, '1');
				echo alerts('success', 'Success!', 'Password has been updated', "index.php?id=contacts&contactid=$userid");
			} 
			else {
				echo alerts('warning', 'There was a problem!', 'Passwords did not match!', "index.php?id=contacts&contactid=$userid");
			}
		}else {
			echo alerts('error', 'There was an error', 'You cannot save a blank password!', "index.php?id=contacts&contactid=$userid");
		}
	}
	if (isset($_POST['removelogin'])) {
		$db_local -> userUpdateSettings('remove','',$userid);
	}

	echo $generate->contactGenProfile($userid, 'listmemberships');
	
	$contact = $db_local->contactFetchContacts($userid);

	$valid = onoff($db_local->adminCheckIsLoginUser($userid),"Yes","No", "User is a site admin", "User is not a site admin");
	$userinfo = $db_local->contactFetchContacts($userid);
	$message = issetor($message);
	if (issetor($userinfo[0]['token'])) {
		$token = $userinfo[0]['token'];
		$tokenval = "success";
	} else {
		$token = "No token found";
		$tokenval = "danger";
	}
	
?>
	<div class='col-lg-6'>
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'><?php echo $tooltips['userSettings']; ?> User settings</h3>
			</div>
			<div class='card-body'>
				<form method='post' id='usersettings' action='#usersettings' enctype='multipart/form-data'>
					<div class='row'>
						<div class='col-md-3'>
							<?php echo $tooltips['siteAdmin']; ?> Site admin:
						</div>
						<div class='col-md-6'>
						<?php echo $valid; ?>
						</div>
						<div class='col-md-3'>
			
						</div>
					</div>
					<div class='row' style='padding-top: 1%'>
						<div class='col-md-3'>
							<?php $tooltips['manualPassword']; ?> Set/change admin password (manual):
						</div>
						<div class='col-md-6'>
							<input type='password' class='form-control' placeholder='New password' name='password'>
							<input type='password' class='form-control' name='pwconfirm' placeholder='Confirm password'>
							<input type='hidden' name='contactid' value='<?php echo issetor($userid); ?>'>
						</div>
						<div class='col-md-3'>
							<input type='submit' value='Set password' class='btn btn-sm btn-primary btn-block' name='setpass'>
						</div>
					</div>
					<div class='row' style='padding-top: 1%'>
						<div class='col-md-3'>
							<?php echo $tooltips['generateRandom']; ?> Generate Random Password:
						</div>
						<div class='col-md-6'>
							(Messages User of New Password)
						</div>
						<div class='col-md-3'>
							<input type='submit' name='genpass' class='btn btn-sm btn-primary btn-block' value='Generate'>
						</div>
					</div>
					<div class='row' style='padding-top: 1%'>
						<div class='col-md-3'>
							<?php echo $tooltips['apiToken']; ?> API Token (API Access):
						</div>
						<div class='col-md-6'>
							<strong><?php echo $token ?></strong>
						</div>
						<div class='col-md-3'>
							<input type='submit' name='gentoken' class='btn btn-sm btn-primary btn-block' value='Generate new token'>
						</div>
					</div>
					<div class='row' style='padding-top: 1%'>
						<div class='col-md-3'>
						 <?php echo $tooltips['linkWSIntegration']; ?> Workspace integration
						</div>
						<div class='col-md-6'>
							<?php echo $generate->wsIntegrationGenSelect('wsintegrationselect', issetor($userid));?>
						</div>
						<div class='col-md-3'>
							<input type='submit' class='btn btn-sm btn-primary btn-block' name='wslinkuser' value='Link integration'>
						</div>
					</div>
					<div class='row' style='padding-top: 1%'>
						<div class='col-md-3'>
						 <?php echo $tooltips['removeAdmin']; ?> Remove site admin role:
						</div>
						<div class='col-md-6'>
							(does not delete user)
						</div>
						<div class='col-md-3'>
							<input type='submit' class='btn btn-sm btn-danger btn-block' name='removelogin' value='Remove admin role'>
						</div>
					</div>
					
				</form>
				<br>
				<div align='left' style='padding-top: 2%'>
					<form action='#' method='post' enctype='multipart/form-data'>
						<input type='submit' class='btn btn-md btn-primary' name='update_contact' value='Update user details'>
						<input type='submit' <?php echo $link_confirm ?> class='btn btn-md btn-danger pull-right' name='delete_contact' value='Delete user'>
						<input type='hidden' name='contactid' value='<?php echo issetor($userid); ?>'>
					</form>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<?php 
		if (isset($_POST['deletetoken'])) {
			$db_local->integrationDeleteToken($userid);
		}
		if ($db_local->integrationCheckConfiguration()) {
			
			if ($db_local->integrationAccessTokenExists($userid)) {
				if (isset($_POST['refresh_token'])) {
					if ($spark->integrationRefresh($userid)) {
						alerts("success", "Success", "Token was refreshed!");
					} 
					else echo alerts("error");
				}
				
				$tokendata = $db_local->integrationAPIGetTokenData($userid);
				$myauthorization = $generate->integrationGenAuth($userid);
				$refresh = (!$tokendata['refresh_token']['expired']) ? "<button name='refresh_token' class='btn btn-primary'>Refresh Access Token</button>":"<font color='red'>Expired</font>";
				$deletetoken = "<button name='deletetoken' class='btn btn-danger' $link_confirm>Delete authorization</button>";
				
				$userdata = $db_local->botFetchBots($userid)[0];
				$useroutput = "<div class='row' style='padding-top: 1%'><div class='col-md-3'>
							Authorized user
						</div>
						<div class='col-md-3'>
							<img src='/images/bots/{$userdata['id']}.jfif' width='20' height='20' class='img-circle'> {$userdata['displayName']}
						</div>
							<div class='col-md-3'>
							{$userdata['emails']}
						</div>
							<div class='col-md-3'>
							
						</div></div>";
				
				$tokenoptions = "<div class='row' style='padding-top: 1%'><div class='col-md-3'>
				Authorization options
				</div>
				<div class='col-md-3'>
				$refresh
				</div>
				<div class='col-md-3'>
				$deletetoken
				</div>
				</div>";
			}
			$scopeselect = $generate->integrationGenScopeSelect('scopes[]');
		?>
		
		<div class='card card-primary card-outline'>
			<div class='card-header'>
				<h3 class='card-title'>Integration authorized user details</h3>
			</div>
			<div class='card-body'>
				<form method='post' id='integration' action='#integration' enctype='multipart/form-data'>
					<div class='row' style='padding-top: 1%'>
						<div class='col-md-3 mb-1'>
							Authorize with scopes
						</div>
						<form method='post' id='authuser' action='#authuser' enctype='multipart/form-data'>
						<div class='col-md-3 mb-1'>
							<button class='btn btn-primary' name='authorize_new'>Authorize new user</a>
						</div>
						<div class='col-md-6 mb-1'>
							<?php echo issetor($scopeselect);?>
							
						</div>
						</form>
					</div>
									
					<?php echo issetor($useroutput); ?>
					
					<?php echo issetor($myauthorization); ?>
					<form method='post' id='integration' action='#integration' enctype='multipart/form-data'>
					<?php echo issetor($tokenoptions); ?>
					</form>
				</form>
				<br>
			</div>
			<!-- /.box -->
		</div>
		<?php }?>
	</div>
</div>
</div>
</div>
<?php
}
else {
	$groupoptions = $generate->groupOptions();
	?>

	<div class="row">
		<div class="col-lg-6">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title"><?php echo $tooltips['addUsers'];?> Add users</h3>
				</div>
				<div class="card-body">
					<form method='post' onsubmit='loading("process", "Searching...")' action='#' enctype='multipart/form-data'> 	
						<div class="main">
  							<div class="form-group has-feedback">
								<div class="input-group mb-3">
									<input name='search_contact' width='100%' class="form-control" placeholder="Search for Name, ID or E-mail in Webex Teams">
									<div class="input-group-append">
									<span class="input-group-text"><i class="fa fa-search"></i></span>
									<input type='hidden' name='search' value='search'>
									</div>
								</div>
 							</div> 
						</div>
					</form>	
					<?php 
						if(isset($_POST['search'])) {
						    if (validateEmail($_POST['search_contact'])) {
						        $recepient_type = "email"; 
						    } else {
						        $recepient_type = (strpos( $_POST['search_contact'], "Y2") === 0) ? "id" : "displayName";
						    }
						    
							$contact_data = array('recepientValue' => $_POST['search_contact'], 'recepientType' => $recepient_type, 'sender' => $botid);
							
							$contact_result = $spark->peopleGet($contact_data);
							
							//if (!isset($contact_result['items'][0]['id']) and $recepient_type == "email") {
							// $roomdata = $spark->peopleGetCrossOrg($botid, array($_POST['search_contact']));
							// var_dump($roomdata);
							//}

							echo $generate->userSearch($contact_result);
							
						}
					?>
					<?php
						$ldapenabled = $db_local->checkifldapisenabled();
						if($ldapenabled == 1)
						{

					?>
					<hr>
					<form method='post' onsubmit='loading("process", "Searching...")' action='#' enctype='multipart/form-data'> 	
						<div class="main">
  							<div class="form-group has-feedback">
    							<span class="form-control-feedback"></span>
								<label for="mailer_search"><?php echo $tooltips['MailerbulkImport'];?> Bulk import users from Mailer/AD Group:</label>
    							<input type="text" name='mailer_search' class="form-control" placeholder="Add users from a mailer/AD group" required>
								<br>
								<label for="groups"><?php echo $tooltips['addMembership'];?> Add bulk imported users to group:</label>
								<select name='groups' class='form-control'>
									<option value='' selected>Make member of..</option>
								<?php echo $groupoptions; ?>
							</select>
 							</div> 
						</div>	
							<input type='submit' class='btn btn-primary btn-block' id='mailer_add' value='Import Users' name='mailer_add'>
					</form>	
					<?php
					}
					?>
					<hr>
					<form method='post' action='#' onsubmit='loading("process", "Please wait while importing users...")' enctype='multipart/form-data'>
						<label for="users_csv"><?php echo $tooltips['bulkImport'];?> Bulk import users from CSV:</label>
						<textarea name='users_csv' class='form-control' required placeholder='E-mails / UserIDs (CSV)' rows='3' cols='60' required></textarea>
						<br>
						<label for="users_csv"><?php echo $tooltips['addMembership'];?> Add bulk imported users to group:</label>
						<select name='groups' class='form-control'>
							<option value='' selected>Make member of..</option>
							<?php echo $groupoptions; ?>
						</select>
						<br>
						<input type='submit' class='btn btn-primary btn-block' id='import_users' value='Import users' name='bulk_add'>
					</form>
				</div>
            </div>
		</div>
		<div class="col-lg-6">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title"><?php echo $tooltips['importedUsers'];?> Imported users</h3>
				</div>
				<div class="card-body overflow-auto">
					<form id="frm-contacts" action="index.php?id=contacts" method="POST">
						<table width='100%' id='contacts' class='table table-bordered table-striped'>
							<thead>
								<tr>
					                <th rowspan="2">Select All<br><input name="select_all" value="1" id="contacts-select-all" type="checkbox" /></th>
					                <th colspan="5"><input type='submit' name='bulk_update' class='btn btn-md btn-primary' value='Update'>
					                <input type='submit' <?php echo $link_confirm ?> class='btn btn-md btn-danger float-right' name='bulk_delete' value='Delete'></th>
            					</tr>
								<tr>
									<th>Avatar</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>


</div>
</div>
<?php
}
?>

