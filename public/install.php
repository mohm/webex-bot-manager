<?php

error_reporting(1);
@ini_set('display_errors', 1);

$phpver = phpversion();
$phpversupported = 7; //7 and above
$mysqlsupportedversion = 5; //5 and above
$required_php_modules = array("curl", "mysqli", "pdo_mysql");
$required_modules_passed = true;
$required_permissions = true;

$rootdoc = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
$subrootdoc = str_replace("/public", "", $rootdoc);

function issetornot(&$var, $default = "") {
	return isset($var) ? $var : $default;
}

function redir($url) {
	if (!headers_sent()) {
		header('Location: '.$url);
		exit;
	}
	else {
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$url.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
		echo '</noscript>'; exit;
	}
}

function blockq($input, $class) {
	switch ($class) {
		case "success":
			$head = "<strong>Success!</strong>";
			break;
		case "warning":
			$head = "<strong>Attention!</strong>";
			break;
		case "danger":
			$head = "<strong>Error!</strong>";
			break;
		default:
			$head = "<strong>Info!</strong>";
			$class = "primary";
			break;
	}
	return "<div class='alert alert-$class'>
	<strong>$head</strong> $input
	</div>";
}

function test_sql_conn($dbhost, $dbuser, $dbpass, $dbname='') {
	$conn = new mysqli($dbhost, $dbuser, $dbpass);
	if ($conn->connect_errno) {
		return $conn->connect_errno;
	}
	if ($dbname) {
		$tables = $conn->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '$dbname'");
		$list = array();
		while($row = $tables->fetch_assoc()) {
			$list[] = $row['TABLE_NAME'];
		}
		if (count($list)) {
			return false;
		}
	}
	return true;
}

if ($phpver >= $phpversupported)
{
	$phpverinfo = blockq('Passed', "success");
}
else
{
	$phpverinfo =  blockq('Your php version is not supported - Currently PHP is ' . $phpver, "danger");
}

$extensions = get_loaded_extensions();
$moduleinfo = "";

foreach ($required_php_modules as $k => $v) {
	if (issetornot(array_search($v, $extensions))) {
		$moduleinfo .= blockq("$v Passed", "success");
	} else {
		$moduleinfo .= blockq("$v Failed, please install", "danger");
		$required_modules_passed = false;
	}
}

$configFile = $subrootdoc.'/config_ini.php';

if (isset($_GET['sub'])) {
	$includeitems = array('primarybot', 'createadmin');
	if (in_array($_GET['sub'], $includeitems)) {
		include 'includes/functions.inc.php';
		$db_local = new Db();
		$generate = new OutputEngine();
		$spark = new SparkEngine();
		$tooltip = array(
				"access" => tooltip("Access Token", "The Bot Access Token can be generated from your profile in https://developer.webex.com", $icon="fa-info-circle", $placement='left'),
				"direct" => tooltip("Direct Webhook", "Automatically generates a webhook for 1:1 conversation", $icon="fa-info-circle", $placement='left'),
				"group" => tooltip("Group Webhook", "Automatically generates a webhook for group spaces", $icon="fa-info-circle", $placement='left'),
				"feature" => tooltip("Features", "Webex Bot Manager comes with a default set of bot features, checking this will enable them all", $icon="fa-info-circle", $placement='left'),
		);
	}
	
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="webexteams.ico">
  <title>Webex Bot Manager | <?php echo ucfirst($_GET['id']); ?></title>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- pace-progress -->
  <link rel="stylesheet" href="plugins/pace-progress/themes/black/pace-theme-flat-top.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Editor -->
  <link rel="stylesheet" href="plugins/editormd/css/editormd.css" />

  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <link rel="stylesheet" href="dist/wbm.css">
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<script language="JavaScript">
function toggle(source, name) {
    checkboxes = document.getElementsByName(name);
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }
function loading(id, loadingtext)
{
  ele = document.getElementById(id);
  ele.style.display="block"
  ele.innerHTML='<i class="fa fa-spinner fa-spin"></i> ' + loadingtext;
}

</script>
</head>
<body class="hold-transition login-page">
<div class="install-box">
  <div class="login-logo">
    <a href="index.php"><img  src="images/static/webexteams.png" height="100px" width="90px">
    	<br><b>Webex </b>Bot Manager</a>
  </div>
  <!-- /.login-logo -->
	<div class="card card-primary card-outline">
		<div class="card-header">
			<h3 class="card-title">Webex Bot Manager - Installation</h3>
			<div class="install-card-tools">
				<?php 
							if(empty($_GET['sub']))
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
										Step 1/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'db')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
										Step 2/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'proxy')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
										Step 3/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'primarybot')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 67%;">
										Step 4/6 
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'createadmin')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 83%;">
										Step 5/6
									</div>
								</div>';
							}
							elseif ($_GET['sub'] == 'finished')
							{
								echo '
								<div class="progress" style="height: 20px;">
									<div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
										Step 6/6
									</div>
								</div>';
							}
						?>
						</div>
						<!-- /.card-tools -->
					</div>
					<!-- /.card-header -->
					<div class="card-body">
					<?php 
						if(empty($_GET['sub']))
						{
							echo '
								<div class="row">
									<div class="col-md-5">
									   PHP Required version (>= '.$phpversupported.')
									</div>
									<div class="col-md-6">
									   ' . $phpverinfo . '
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
									   PHP Required modules
									</div>
									<div class="col-md-6">
									   ' . $moduleinfo  . '
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
									Folder permission check
									</div>
									<div class="col-md-6">';
									
                           if (is_writable($rootdoc)) {
                              echo blockq('<br>Houston, we have write permission!', "success");
                              echo (is_writable($rootdoc."/images/bots")) ? "": blockq("Cannot write to bot avatar folder <strong>/images/bots</strong>", "danger");
                              echo (is_writable($subrootdoc."/temp")) ? "": blockq("Cannot write to temp lock folder {$subrootdoc}./temp", "danger");
                           } else {
                           	  
                              echo blockq("<br>Root directory is: $rootdoc", 'warning');
                              echo blockq('<br>Houston, we have a problem! The root directory is not writable!', 'danger');
                           }
                           echo '</div>
								</div>
							</div>
							<!-- /.card-body -->
							<div class="card-footer">
								';
								if ($phpver >= $phpversupported && $required_modules_passed && is_writable($rootdoc))
								{
									echo '<a class="btn btn-primary pull-right" href="install.php?sub=db">Next</a>';
								}
							echo '</div>';
					}
					elseif ($_GET['sub'] == 'db')
					{			
					    if (file_exists($subrootdoc."/config_ini.php")) {
					    	redir("install.php?sub=primarybot");
					    }
					    //check here if config exists and if db_connect is fine if yes, redirect else show config windowd
					    $dbname = issetornot($_POST['dbname']);
						$dbuser = issetornot($_POST['dbuser']);
						$dbhost = issetornot($_POST['dbhost']);
						
						echo '<b>This process will create all the tables needed for Webex Bot Manager</b><br>';
						echo '<br><b>Please enter your database credentials</b><br>';
						echo '
						<form name="dbcreds" method="post" action="" enctype="multipart/form-data">
							<div class="row">
                        <div class="col-md-3">
                        Database Name:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="text" name="dbname" value="'.$dbname.'" class="form-control form-control-sm" placeholder="dabase name" required/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                        Database Username:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="username" name="dbuser" value="'.$dbuser.'" class="form-control form-control-sm" placeholder="username" required/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                        Database Password:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="password" name="dbpass" class="form-control form-control-sm" placeholder="password" required/>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                        Database Host:
                        </div>
                        <div class="col-md-5 mb-1">
                        <input type="text" name="dbhost" value="'.$dbhost.'" class="form-control form-control-sm" placeholder="localhost" value="localhost" required/>
                        </div>
                     </div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer">';
							if(isset($_POST['dbload']))
                     {
                        $button = '<br><button type="submit" name="dbload" class="btn btn-primary">Re-try operation</button>';
                         $conn = @new mysqli($_POST['dbhost'], $_POST['dbuser'], $_POST['dbpass']);
                         if ($conn->connect_errno) {
                         	 echo blockq("Failed to connect to MySQL<br>$conn->connect_error", "danger");
                             echo $button;
                             die();
                         }
                         else {
                         	echo blockq("Connection successful!", "success");
                             $mysqlversion = mysqli_get_server_info($conn);
                             $mysqlmajorversion = explode('.', $mysqlversion);
                             
                             if (!((int) $mysqlmajorversion[0] >= $mysqlsupportedversion)) {
                             	echo blockq("Unsupported MySQL version $mysqlmajorversion (>=$mysqlsupportedversion)", "danger");
                             	die();
                             }
                             
                             $dbname = $_POST['dbname'];
                             $config_data = '<?php
								$config = array("dbhost"=>"'.$_POST['dbhost'].'",
										"dbname"=>"'.$_POST['dbname'].'",
										"username"=>"'.$_POST['dbuser'].'",
										"password"=>"'.$_POST['dbpass'].'",
										"proxyurl"=>"",
										"proxyuserpass"=>"",
										"base_dir"=>"'.$subrootdoc.'",
										"ADserver"=>"",
										"ADport"=>"",
										"BaseDN"=>"",
										"AdminGroup"=>"",
										"ADuser"=>"",
										"ADpass"=>"");
								?>';
                             
                             try {
                                 $db = $conn->select_db($dbname);
                             } catch (Exception $e) {
                                 $db = false;
                             }
                             
                             if (!$db) {
                                 echo blockq("$dbname did not exist, trying to create the database..</font>", "warning");
                                 if ($conn->query('CREATE DATABASE IF NOT EXISTS `' . $dbname . '` CHARACTER SET utf8 COLLATE utf8_general_ci') === TRUE) {
                                     echo blockq("Created database $dbname", "success"); 
                                     $db = $conn->select_db($dbname);
                                     if (!$db) {
                                         echo blockq("Something went wrong selecting the database after creation: $dbname", "danger");
                                         echo $button;
                                         die();
                                     }
                                 } else {
                                     echo blockq("Failed to create database $dbname <br>".$conn->error."", "danger");
                                     echo $button;
                                     die();
                                 }
                             } else {
                                 echo blockq("$dbname exists, checking tables..", "success");
                                 $tables = $conn->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = '$dbname'");
                                 $list = array();
                                 while($row = $tables->fetch_assoc()) {
                                     $list[] = $row['TABLE_NAME'];
                                 }        
                                 if (count($list)) {
                                     echo blockq("Database was not empty!<br>Unable to proceed... $dbname has (".count($list).") tables. Use an empty database or create a new before you re-try.", "danger");
                      				//Add function to use existing database -> skip to creation of config_ini.php -> login.php
                                     echo $button;
                                     die();
                                 } else {
                                     echo blockq("$dbname was empty, creating tables now..", "success");
                                 }
                             }
                         }
                        //Credit to https://dev.to/erhankilic/how-to-import-sql-file-with-php--1jbc
                       
                         $configFile = $subrootdoc.'/config_ini.php'; 
                         $config_ini = fopen($configFile, "w") or die(blockq("Unable to create config.ini file. Please check apache has write permissions to the directory.", "danger"));
                        
                        fwrite($config_ini, $config_data);
                        //Name of the SQL file
                        $sqlTemplate = $subrootdoc.'/easybot-template.sql';
                        echo
                        // Temporary variable, used to store current query
                        $templine = '';
                        // Read in entire file
                        $lines = file($sqlTemplate);
                        // Loop through each line
                        foreach ($lines as $line) {
                        // Skip it if it's a comment
                           if (substr($line, 0, 2) == '--' || $line == '')
                              continue;

                        // Add this line to the current segment
                           $templine .= $line;
                        // If it has a semicolon at the end, it's the end of the query
                           if (substr(trim($line), -1, 1) == ';') {
                              // Perform the query
                              $conn->query($templine) or print('Error performing query \'<strong>' . $templine . '\': <br /><br />');
                              // Reset temp variable to empty
                              $templine = '';
                           }
                        }
                        echo blockq("Tables imported successfully to $dbname", "success");
                        echo '<a class="btn btn-primary float-right" href="install.php?sub=proxy">Proceed to the next step</a>';
                     }
                     else
                     {
                        echo '<button type="submit" name="dbload" class="btn btn-primary">Connect to DB and load tables</button>';
                     }
                  echo '</form>
                  </div>
                  ';
               }
				elseif ($_GET['sub'] == 'proxy')
					{
						echo '<b>Click the button to test internet connectivity</b><br>';
						echo '<br>If your network requires a proxy to reach the Internet, please fill in the below settings before clicking the button!<br>';
						echo '
						<form name="proxyserver" method="post" action="" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-2">
									Proxy Host:
									</div>
									<div class="col-md-5 mb-1">
									<input type="text" name="proxyhost" class="form-control form-control-sm"/>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
									Proxy Port:
									</div>
									<div class="col-md-5 mb-1">
									<input type="text" name="proxyport" class="form-control form-control-sm"/>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
									Proxy Username:
									</div>
									<div class="col-md-5 mb-1">
									<input type="username" name="proxyuser" class="form-control form-control-sm" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
									Proxy Password:
									</div>
									<div class="col-md-5 mb-1">
									<input type="password" name="proxypass" class="form-control form-control-sm" />
									</div>
								</div>
							</div>
							<div class="card-footer">';
							function checkinternet( $proxyurl, $proxycreds)
							{
								$url = 'https://webexapis.com';
								$ch = curl_init($url);
								curl_setopt($ch, CURLOPT_NOBODY, true);
								curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
								curl_setopt($ch, CURLOPT_PROXY, $proxyurl);
								curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxycreds);
								curl_exec($ch);
								$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								curl_close($ch);
								if (401==$retcode) {
									echo blockq('Houston, we have internet connectivity!', 'success');
									echo '<a class="btn btn-primary float-right" href="install.php?sub=primarybot">Next</a>';
									require $subrootdoc . '/config_ini.php';
									$configFile = $subrootdoc.'/config_ini.php'; 
									$config_ini = fopen($configFile, "w") or die("Unable to create config.ini file. Please check apache has write permissions to the directory.");
									$config_data = '<?php 
										$config = array("dbhost"=>"'.$config['dbhost'].'", 
											"dbname"=>"'.$config['dbname'].'",
											"username"=>"'.$config['username'].'",
											"password"=>"'.$config['password'].'",
											"proxyurl"=>"'.$proxyurl.'",
											"proxyuserpass"=>"'.$proxycreds.'",
											"base_dir"=>"'.$config['base_dir'].'",
											"ADserver"=>"",
											"ADport"=>"",
											"BaseDN"=>"",
											"AdminGroup"=>"",
											"ADuser"=>"",
											"ADpass"=>"");
										?>';
				
								fwrite($config_ini, $config_data) or die("Unable");
									
								} 
								else 
								{
									echo blockq('Unable to reach webex.com<br>Webex Bot Manager requires Internet access to work!'. $retcode, 'danger');
									echo '<button type="submit" name="proxy" class="btn btn-danger">Try Again</button>';
								}
							}
							if(isset($_POST['proxy']))
							{
								if(!empty($_POST['proxyhost']) && !empty($_POST['proxyport']))
								{
									$proxyurl = $_POST['proxyhost'] . ":" . $_POST['proxyport'];
								}
								if(!empty($_POST['proxyuser']) && !empty($_POST['proxyuser']))
								{
									$proxycreds = $_POST['proxyuser'] . ":" . $_POST['proxypass'];
								}
								checkinternet($proxyurl, $proxycreds);
							}
							else
							{
								echo '<button type="submit" name="proxy" class="btn btn-primary">Check Internet connection</button>';
							}
							echo '
							</div>
						</form>';
					}
					elseif ($_GET['sub'] == 'primarybot')
					{
						if ($db_local->botGetMainInfo()) {
							redirect("install.php?sub=createadmin");
						}
						
						echo '
						<b>Primary Bot Setup!</b>
						<br>The primary bot is the bot that makes all the API requests on behalf of the Webex Bot Manager and is required to operate WBM.
						<br><br>
						<form name="primarybot" method="post" action="#" enctype="multipart/form-data">
						
							<div class="row">
								<div class="col-md-5">
								'.$tooltip['access'].' Enter a valid Bot Access Token:
								</div>
								<div class="col-md-6">
								<input type="text" class="form-control form-control-sm" value="'.issetornot($_POST['accesstoken']).'" name="accesstoken"> 
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-5">
								'.$tooltip['direct'].' Setup direct conversation webhook 
								</div>
								<div class="col-md-3">
								<input type="checkbox" name="webhook_direct" value="1">
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
								'.$tooltip['group'].' Setup group conversation webhook
								</div>
								<div class="col-md-3">
								<input type="checkbox" name="webhook_group" value="1"> 
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
								'.$tooltip['feature'].' Enable all features
								</div>
								<div class="col-md-3">
								<input type="checkbox" name="bot_features" value="1"> 
								</div>
							</div>
							<br>Note that Webhooks and Features can be enabled later as well. 
						</div>
						
						<div class="card-footer">
						
						';
						if (isset($_POST['accesstoken'])) {
							$token = trim($_POST['accesstoken']);
							$botAdd = $spark->wizardAddBot($token, $primary=True);
							$bot = $db_local->botGetMainInfo();
							if ($botid = $bot['id']) {
								if (isset($_POST['webhook_direct'])) {
									$spark->webhookCreateQuick($botid, 'direct');
								}
								if (isset($_POST['webhook_group'])) {
									$spark->webhookCreateQuick($botid, 'group');
								}
								if (isset($_POST['bot_features'])) {
									$features = $db_local->adminGetFeatures();
									if (count($features)) {
										foreach ($features as $key => $value) {
											$db_local->adminSetFeature($value['id'], $botid);
										}
									}
								}								
								echo msgList("success-BotAdd");
								echo feedbackMsg("Primary bot added!", $botAdd, "info");
								echo '<a class="btn btn-primary float-right" href="install.php?sub=createadmin">Next</a>';
							}
							else {
								echo msgList("alert-BotAddAccess");
								echo '<input type="submit" class="btn btn-md btn-primary" value="Re-try">';
							}

						}
						else
						{
							echo '<input type="submit" class="btn btn-md btn-primary" value="Add primary bot">';
						}
						echo '</div>
						</form>';
					}
					elseif ($_GET['sub'] == 'createadmin')
					{
						if ($db_local->adminCheckSiteAdminExists()) {
							redir("login.php");
						}
						echo '
						<b>The Webex Teams user e-mail you type in here will be granted power over Webex Bot Manager. Make your choice..</b><br>
						<br>
						<form method="post" action="" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-3">
								Teams Email Address
								</div>
								<div class="col-md-5">
									<input type="email" size="100" value="'.issetornot($_POST['useremail']).'" class="form-control" name="useremail" required>
								</div>
							</div>
							</div>
							<div class="card-footer">
							 
						';
						$primaryBotInfo = $db_local->botGetMainInfo();
						if (empty($primaryBotInfo)) {
							$primaryFound = false;
						}
						else {
							$primaryFound = true;
							$botid = $primaryBotInfo['id'];
							if (empty($primaryBotInfo['access'])) {
								$tokenExists = false;
							}
							else {
								$tokenExists = true;
								$me = $spark->peopleGetMe($botid);
								$tokenValid = ($me['id'] == $botid) ? true:false;
							}
						}

						if(isset($_POST['useremail'])) {
							if (validateEmail($useremail = $db_local->quote($_POST['useremail']))) {
								$payload = array("sender"=>$botid, "recepientType"=>"email", "recepientValue"=>$useremail);
								$userDetails = $spark->peopleGet($payload);
								if (count($userDetails['items']) == 1) {
									$db_local->contactAdd($userDetails, $botid);
									if (!empty($userdata = $db_local->contactFetchContacts($userDetails['items'][0]['id']))) {
										$userid = $userdata[0]['id'];
										$password = generatePassword();
										$token = hash('md5', generatePassword());
										$hashpw = password_hash($password, PASSWORD_DEFAULT);
										$host = getHostUrl();
										$text = "Here is your new login passord to the [Webex Bot Manager]($host), please change the password in the user settings when you login.\n<blockquote class='success'>Password: <b>" . $password . "</b></blockquote>\n";
										list($foo, $domain) = explode('@', $userdata[0]['emails']);
										$domainid = $db_local->acceptedDomainAdd(array('domain'=>$domain));
										if ($domainid){
											$db_local->botAddAllowedDomain($domainid, $botid);
										}
										$features = $db_local->responseFetchResponses($botid);
										if (count($features)) {
											$feature_list = blockquote("Here are my currently enabled features, type the keyword to get a help list", "success");
											foreach ($features as $fk => $feature) {
												$feature_list .= "\n- " . $feature['keyword'];
											}
										}
										$db_local->userUpdateSettings('update',$hashpw,$userid,'1');
										$db_local->userUpdateSettings('token',$token,$userid,'1');
										if ($spark->messageSend($spark->messageBlob($text, $userid, $botid))) {
											echo msgList("success-Query");
											$spark->messageSend($spark->messageBlob($feature_list, $userid, $botid));
											sleep(2);
											$spark->messageSend($spark->messageBlob("You can login and start to administer the responses. Remember, you might want to disable some of the commands or put them behind an accessgroup to avoid misuse.", $userid, $botid));
										}
										else {
											echo msgList("success-Generic");
										}
										redir("install.php?sub=finished");
										echo '<a class="btn btn-primary pull-right" href="install.php?sub=finished">Finish</a>';
									}
									else {
										echo msgList("warning-AddFailed");
									}
								}
								else {
									echo msgList("warning-UserNotFound");
								}
							}
							else {
								echo msgList("warning-Email");
							}
						}
						echo '<input type="submit" class="btn btn-md btn-primary" value="Create site admin">
						</form>';
						echo '</div>';
					}
					elseif ($_GET['sub'] == 'finished')
					{
						echo '<div align="center">';
						echo '<b>Behold, master of bots.. total domination has been granted. Use thy power wisely.</b><br><br>';
						echo '<a href="index.php" class="btn btn-lg btn-primary">Log in</a>';
						echo '</div>';
					}
			?>
		</div>
	</div>
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script>
$('[data-toggle="popover"]').popover({
    container: 'body'
    })
</script>


</body>
</html>
