<?php 
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-type: application/json');

$db = new Db();
$spark = new SparkEngine();
$auth_header = $_SERVER['HTTP_AUTHORIZATION'];

$output = array('Error'=>'Empty');

list($type, $token) = explode(" ", $auth_header);

if ($db->adminCheckIfValidAPIToken($token) or $_SESSION['status'] == "logged") {
    
    if (issetor($_GET['appId']) and $db->wsIntegrationExists($_GET['appId'])) {
        
        $appId = $db->quote($_GET['appId']);
        
        if (isset($_GET['refresh'])) {
            $spark->wsIntegrationRefreshAccessToken($appId);
        }
        
        $spark->wsIntegrationCheckExpiredAndRefresh($appId);
        
        $output = $db->wsIntegrationInfoAPI($appId);
        
        if (!isset($_GET['all'])) {
            $output = $db->wsIntegrationInfoAPISensor($output);
        }
        
        if (isset($_GET['wh']) and !isset($_GET['devices'])) {
            if (!issetor($_GET['wh'])) {
                
                $output['webhooks'] = $db->wsIntegrationWebhookLogGet($appId); 
                
            } 
            else {
                
                $deviceId = $db->quote($_GET['wh']);
                
                $output['webhooks'] = $db->wsIntegrationWebhookLogGet($appId, $deviceId); 
                
            }
            
        } else if (isset($_GET['devices'])) {
            
            if (issetor($_GET['devices'])) {
                
                $deviceId = $db->quote($_GET['devices']);
                                                
                $output['devices'] = $db->wsIntegrationDeviceCollectorGetDevicesAPI($appId, $deviceId);
                
                if (isset($_GET['wh'])) {
                    $output['devices']['items'][0]['webhooks'] = $db->wsIntegrationWebhookLogGet($appId, $deviceId); 
                }
            
            } else {
                
                $output['devices'] = $db->wsIntegrationDeviceCollectorGetDevicesAPI($appId);
            
            }
            
        } else if (isset($_GET['actions'])) {
            
            $output['actions'] = $db->wsIntegrationActionLogGet($appId);
            
        }
        
    } else {
        
        $integrations = $db->wsIntegrationGet();
        
        if (count($integrations)) {
            
            $output = array();
            
            foreach ($integrations as $k => $v) {
                
                $output['items'][] = $db->wsIntegrationInfoAPI($v['id']);
                
            }
            
        } else {
            
            $output = array(
                
                "Error" => "No workspace integrations found"
                
            );
            
        }
        
    }
	
} 
else {
	header("Status: 401 Unauthorized");
	$output = array('Error'=>'401 Unauthorized - incorrect token');	
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>