<?php 
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-type: application/json');

$db = new Db();
$spark = new SparkEngine();
$auth_header = $_SERVER['HTTP_AUTHORIZATION'];

list($type, $token) = explode(" ", $auth_header);

if ($db->adminCheckIfValidAPIToken($token) or $_SESSION['status'] == "logged") {
	
	$output = ($_GET['id']) ? $db->contactDetailsAPI($_GET['id']) : $db->contactDetailsAPI();
	
} 

else {
    
	header("Status: 401 Unauthorized");
	
	$output = array('Error'=>'401 Unauthorized - incorrect token');	
	
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>