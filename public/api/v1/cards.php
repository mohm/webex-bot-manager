<?php 
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-type: application/json');

$db = new Db();
$spark = new SparkEngine();
$auth_header = $_SERVER['HTTP_AUTHORIZATION'];
$output = array('Error'=>'Empty');

list($type, $token) = explode(" ", $auth_header);

if ($db->adminCheckIfValidAPIToken($token) or $_SESSION['status'] == "logged") {
	//Handle incoming data
	$incoming = file_get_contents("php://input");
	if (isset($_GET['id'])) {
	    
		$output = $db->cardAPIGetData($_GET['id']);
	
	} else {
	    
		$output = $db->cardAPIGetCards();
		
	}
} 
else {
	header("Status: 401 Unauthorized");
	$output = array('Error'=>'401 Unauthorized - incorrect token');	
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>