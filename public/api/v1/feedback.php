<?php 
session_start();

include '../../includes/functions.inc.php';
header('Content-type: application/json');

$db = new Db();
$spark = new SparkEngine();
$auth_header = $_SERVER['HTTP_AUTHORIZATION'];
$output = array('Error'=>'Empty');

list($type, $token) = explode(" ", $auth_header);

if ($db->adminCheckIfValidAPIToken($token) or $_SESSION['status'] == "logged") {
	//Handle incoming data
	$incoming = file_get_contents("php://input");
	
	$botId = issetor($_GET['botId']);
	$topicId = issetor($_GET['topicId']);
	$entryId = issetor($_GET['entryId']);
	$commentId = issetor($_GET['commentId']);
	
	$output = $db->feedbackAPI($botId, $topicId, $entryId, $commentId);
	
	
} 
else {
	header("Status: 401 Unauthorized");
	$output = array('Error'=>'401 Unauthorized - incorrect token');	
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>