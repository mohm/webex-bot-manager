<?php 
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-Type: application/json');

$wx = new SparkEngine();
$db = new Db();

if ($_SESSION['status'] == "logged") {
    $appId = issetor($_GET['appId']);
    
    if ($appId) {
        $details = $db->wsIntegrationGet($appId);
        
        
        if (!count($details)) {
           die("404 - Integration not found");  
        }
        
        $details = $details[0];
        
        header('Content-disposition: attachment; filename='.$details['id'].'.json');
        header('Content-type: application/json');
        $manifest = array(
            "id"                =>  $details['id'],
            "manifestVersion"   =>  1,
            "displayName"       =>  $details['name'],
            "vendor"            =>  "Webex Bot Manager",
            "email"             =>  $_SESSION['email'],
            "description"       =>  $details['description'],
            "descriptionUrl"    =>  "",
            "tocUrl"            =>  "",
            "availability"      =>  "org_private",
            "apiAccess"         =>  array(
                array(
                    "scope"=> "spark-admin:devices_read",
                    "access"=> "required",
                    "role"=> "id_readonly_admin"
                ),
                array(
                    "scope"=> "spark-admin:workspaces_read",
                    "access"=> "required",
                    "role"=> "id_readonly_admin"
                ),
                array(
                    "scope"=> "spark-admin:workspaces_write",
                    "access"=> "required",
                    "role"=> "id_full_admin"
                ),
                array(
                    "scope"=> "identity:placeonetimepassword_create",
                    "access"=> "required",
                    "role"=> "id_device_admin"
                ),
                array(
                    "scope"=> "spark-admin:devices_write",
                    "access"=> "required",
                    "role"=> "id_full_admin"
                ),
                array(
                    "scope"=> "spark:xapi_statuses",
                    "access"=> "required"
                ),
                array(
                    "scope"=> "spark:xapi_commands",
                    "access"=> "required"
                ),
                array(
                    "scope"=> "spark:all",
                    "access"=> "required"
                )
            ),
            "xapiAccess" => array(
                "status" => array(
                    array(
                        "path" => "*",
                        "access" => "required"
                    )
                ),
                "commands" => array(
                    array(
                        "path" => "*",
                        "access" => "required"
                    )
                ),
                "events" => array(
                    array(
                        "path" => "*",
                        "access" => "required"
                    )
                )
            ),
            "provisioning" => array(
                "type"=>"https",
                "url"=>getHostUrl()."/api/handlers/ws_integration_activate.php"
            )
           
        );
        
        $features = array();
        
        if (issetor($details['feature_signage_enabled'])) {
            $manifest['apiAccess'][] = array("scope"=>"spark-admin:devices_digital_signage", "access"=>"required");
            $features[] = "digital_signage"; 
        }
        if (issetor($details['feature_pwa_enabled'])) {
            $manifest['apiAccess'][] = array("scope"=>"spark-admin:devices_pwa", "access"=>"required");
            $features[] = "persistent_web_app";
        }
        
        if (count($features)) {
            $manifest['features'] = $features;
        }
        
        echo json_encode($manifest, JSON_PRETTY_PRINT);
    }   
    
}

?>