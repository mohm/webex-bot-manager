<?php 
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-Type: application/json');

$incoming = file_get_contents("php://input");
$dec_incoming = json_decode($incoming, true);
$hmac_body = issetor($_SERVER['HTTP_X_SPARK_SIGNATURE']);

if (!$hmac_body) {
    
    header("Status: 403");
    die();
    
}

$wx = new SparkEngine();
$db = new Db();

if (issetor($dec_incoming['appId'])) {
    
    $appId = $db->quote($dec_incoming['appId']);
    
    if ($db->wsIntegrationExists($appId)) {
    
        if (issetor($dec_incoming['type']) == 'healthCheck') {
            header("Status: 200");
            die();
        }
       
        if (issetor($dec_incoming['deviceId'])) {
           
            $deviceId = $db->quote($dec_incoming['deviceId']);
        
            $wsi = $db->wsIntegrationInfoAPI($appId);
            $secret = $wsi['integration']['whs'];
            
            $signature = hash_hmac('sha1', $incoming, $secret);
            
            if ($signature == $hmac_body) {
                
                if ($wsi['token'] and isset($wsi['token']['at']) and $wsi['token']['at']['expired']) {
                    $wx->wsIntegrationRefreshAccessToken($appId);
                }
                
                if ($wsi['integration']['webhook_logging']) {
                    $data = array(
                        "deviceId" => $deviceId,
                        "appId" => $appId,
                        "data" => json_encode($dec_incoming)
                    );
                    
                    $db->wsIntegrationWebhookLog($data);
                    
                }
                
                if ($wsi['integration']['data_collection']) {
                    $device = $db->wsIntegrationDeviceCollectorGetDevices($appId, $deviceId);
                    $deviceInfo = $wx->deviceGetDetails($appId, $deviceId);
                    
                    if (count($device)) {
                        $device = $device[0];
                        $latestStatus = array();
                        
                        if (!empty($device['latestStatus']) and $device['latestStatus'] != "null") {
                            
                            $latestStatus = json_decode($device['latestStatus'], true);
                            
                        } else {
                            
                            $latestStatus = array();
                            
                        }
                        
                        if ($dec_incoming['type'] == "status" and isset($dec_incoming['changes']['updated']) and count($dec_incoming['changes']['updated'])) {
                            $deviceUpdate = array(
                                'latestStatus' => json_encode(array_merge($latestStatus, $dec_incoming['changes']['updated']))
                            );
                            if(isset($deviceInfo['id'])) {
                                $deviceUpdate['deviceInfo'] = json_encode($deviceInfo);
                            }
                            
                            $db->wsIntegrationDeviceCollectorUpdateDevice($appId, $deviceId, $deviceUpdate);
                            
                        } else if ($dec_incoming['type'] == "events") {
                            
                            $deviceUpdate['lastEvent'] = json_encode($dec_incoming['events']);
                            
                            $db->wsIntegrationDeviceCollectorUpdateDevice($appId, $deviceId, $deviceUpdate);
                            
                        }
                        else {
                            
                            die();
                            
                        }
                        

                    } else {
                        
                        $deviceInsert = array(
                            'appId' => $appId,
                            'deviceId' => $deviceId
                        );
                        
                        
                        $deviceInfo = $wx->deviceGetDetails($appId, $deviceId);
                        
                        if (issetor($deviceInfo['id']) == $deviceId) {
                            $deviceInsert['deviceInfo'] = json_encode($deviceInfo);
                        }
                        
                        if ($dec_incoming['type'] == "status" and isset($dec_incoming['changes']['updated']) and count($dec_incoming['changes']['updated'])) {
                            $deviceInsert['latestStatus'] = json_encode($dec_incoming['changes']['updated']);
                        }
                        
                        $db->wsIntegrationDeviceCollectorAddDevice($deviceInsert);
                        
                        //if ($dec_incoming['type'] == "events") {
                        //    $deviceInsert['lastEvent'] = json_encode($dec_incoming['changes']['updated']);
                        //}
                        
                    }
                } 
                
                $bot_monitoring = $db->wsIntegrationGetDeviceMonitoring($appId, $deviceId);
                
                if (count($bot_monitoring)) {
                    $returnvalue = array(
                        "deviceInfo" => "N/A",
                        "webhook" => $dec_incoming
                    );
                    //BOT NOTIFICATIONS AND GROUP NOTIFICATIONS
                    if (isset($deviceInfo['id'])) {
                        $returnvalue['deviceInfo'] = $deviceInfo; 
                    }
                    
                    $receiver = ($bot_monitoring[0]['notification_groups']) ? true:false;
                    
                    $parsel = array(
                        "sender" => $appId,
                        "receiver_type" => ($receiver) ? "roomId":"toPersonId",
                        "receiver" => ($receiver) ? $bot_monitoring[0]['notification_groups']:$bot_monitoring[0]['userid'],
                        "text" => codeBlock(json_encode($returnvalue, JSON_PRETTY_PRINT))
                    );
                                   
                    $wx->messageSendCustom($parsel);
                                        
                }
                
            } else {
                
                error_header("403", "Forbidden");
                die();
                
            }
            
        } else {
            
            error_header("403", "Forbidden");
            die();
            
        }
        
    } else {

        error_header("410", "Gone");
        die();
        
    }
    
} else {
    
    error_header("500", "Error");
    die();
    
}

header("Status: 200");

?>