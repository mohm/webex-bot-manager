<?php
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-Type: application/json');

if (!in_array($_SERVER['REQUEST_METHOD'], array("POST"))) {
    die(header("Status: 500"));
}

$incoming = file_get_contents("php://input");
$dec_incoming = json_decode($incoming,true) or die(error_header("500", "Missing content"));

$wx = new SparkEngine();
$db = new Db();

$validation = $wx->wsIntegrationValidateActivationRequest($dec_incoming);

//$myfile = fopen("activations.txt", "a+") or die("Unable to open file!");

//fwrite($myfile, $dec_incoming['jwt']);
//fwrite($myfile, "\n".json_encode($validation, true));

$decoded_arr = jwtDecode($dec_incoming, 1);

#print_r($decoded_arr);

$appId = issetor($decoded_arr['appId']);

//Fetch the local integration based on the incoming appId from JWT
$localDetails = $db->wsIntegrationGet($db->quote($appId));

//Check if the integration exists in the database else produce error
if (!count($localDetails)) {
    error_header('404', "Integration do not exist");
    die();
}

if (!$validation['valid']) {
    $db->wsIntegrationUpdate($appId, array("status"=>"Activation received but the JWT failed the validation \n" . json_encode($validation, JSON_PRETTY_PRINT) ));
    die();
}

$wsint = $localDetails[0];

$oauth_client = $wsint['client'];
$oauth_secret = $wsint['secret'];

$oauth_url = $decoded_arr['oauthUrl'];
$appUrl = $decoded_arr['appUrl'];
$refreshToken = $decoded_arr['refreshToken'];

$access_token_payload = array(
    "grant_type" => "refresh_token",
    "client_id" => $oauth_client,
    "client_secret" => $oauth_secret,
    "refresh_token" => $refreshToken
);

#var_dump($access_token_payload);

$decoded = json_encode($decoded_arr);

//fwrite($myfile, "\n\n".$decoded);

//fwrite($myfile, "\n\n".json_encode($token_response, JSON_PRETTY_PRINT));

$host = getHostUrl();
$sign_secret = sha1(generatePassword());

$activation = array(
    "provisioningState" => "completed", #required
    "actionsUrl" => "{$host}/api/handlers/ws_integration_action.php", #optional
    "webhook" => array( #Optional
        "targetUrl" => "{$host}/api/handlers/ws_integration_webhook.php",
        "secret" => $sign_secret
    ),
    "customer" => array( #optional
        "id" => "ohmnet",
        "name" => $decoded_arr['orgName']
    ) 
);

$token_response = $wx->wsIntegrationGetAccessToken($access_token_payload, $oauth_url);

if (!isset($token_response['access_token'])) {
    error_header('400', "Could not generate access token!");
    die();
}

$access_token = $token_response['access_token'];
$activation_r = $wx->wsIntegrationActivateIntegration($activation, $appUrl, $access_token);

//fwrite($myfile, "\n".$activation_r);

//fclose($myfile);

$bearer = $db->integrationFilterTokenResult($token_response);

$data = array(
    'activation_jwt' => $incoming,
    'activation_status' => 1,
    'token_timestamp' => get_timestamp(),
    'status' => "Activation received from {$decoded_arr['orgName']}",
    'whs' => $sign_secret,
    'organization' => $decoded_arr['orgName']
);

$update_array = array_merge($bearer, $data); 

$db->wsIntegrationUpdate($appId, $update_array);

header("Status: 200");
$test = array(
    'redirectURL'=>'https://ohm-net.com'
);

echo json_encode($test);

?>