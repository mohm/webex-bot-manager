<?php

include '../../includes/functions.inc.php';
//Start classes
$db_local = new Db();
$spark = new SparkEngine();
$keyword = false;
$query = array();


//Incoming data from webhook (Teams)
$raw_webhook_data = file_get_contents("php://input");
$webhook_data_array = json_decode($raw_webhook_data,true);

//Find owner of the webhook or abort
$botid = $db_local->botGetWebhookOwner($webhook_data_array['id']);
if (!$botid) die();

//If webhook is listed with an owner and the owner does not exist, abort
$botinfo = $db_local->botFetchBots($botid);
if (!$botinfo) die();

//Get the auth token from the owner or abort
$auth = $spark->authGet($botid);
if (!$auth) die();

$access = "granted";
$botmail = $botinfo[0]['emails'];

//Get data ID from incoming webex hook
$incoming_data_id = $webhook_data_array["data"]["id"];

if ($incoming_data_id) {
	
	//Get the actual data from the card submission
	$response = $spark->cardGetData($botid, $incoming_data_id);
	
	if (!$response) die();
	
	//Assign variables based on the incoming message
	$personId = $response['personId'];
	$roomId = $response['roomId'];
	$messageId = $response['messageId'];
	$recepientType = 'roomId';
	$personEmail = $db_local->contactGetEmail($personId);
	$redirect = false;
	$redirectMetaData = "";
	$replyNoParent = false;
	$name = $db_local->contactGetName($personEmail);
	$name = (validateEmail($name)) ? "" : " ($name)";
	
	if (!validateEmail($personEmail)) {
	    
		$personDetails = $spark->peopleGetDetails($botid, $personId);
	    
	    if (isset($personDetails['emails'])) {
	       
	    	$personEmail = $personDetails['emails'][0];
	    
	    } else {
	    	
	    	$spark->messageSendEvent("**{$personId}$name** submitted a card\n- Payload\n\n" . $input_data_string . "\n- Submission access: **denied - user e-mail could not be aquired!**\n- Bot: **{$botmail}**");
	    	die();
	    
	    }
	
	}
		
	$roomType = "direct";
	$messageDetails = $spark->messageGetDetails($botid, $messageId);
	
	//Get the room type of the incoming card data
	if (isset($messageDetails['roomType'])) {
	   
		$roomType = $messageDetails['roomType'];
	
	}
	
	$parentId = (isset($messageDetails['parentId'])) ? $messageDetails['parentId'] : $messageId; 
	
	//Assign input data from the card to $input_data
	$input_data = $response['inputs'];
	$input_data_string = codeBlock(json_encode($input_data, JSON_PRETTY_PRINT));
	#$input_data_string = str_replace('=', ' : ', http_build_query($input_data, null, "\n    - "));
		
	//If the incoming data is from the bot, then abort
	if ($personEmail == $botmail) die();
	
	//Check if there is a restriction on the bot
	if ($db_local->adminCheckBotRestriction($botid)) {
		
		//If there is a restriction, check if the e-mail domain is valid
		
		if (!$db_local->adminCheckIfValidDomain($personEmail, $botid)) {
			
			$spark->messageSendEvent("**{$personEmail}$name** submitted a card\n- Payload\n\n" . $input_data_string . "\n- Submission access: **denied - domain violation (Domain restriction policy)!**\n- Bot: **{$botmail}**");
			die();
		
		}
	
	}
	//Check if the user is blocked
	if ($db_local->adminGetBlockedContacts($personEmail)) {
	    
		$spark->messageSendEvent("**{$personEmail}$name** submitted a card:\n- Payload\n\n" . $input_data_string . "\n- Submission access: **denied - user blocked (Blocked user policy)!**\n- Bot: **{$botmail}**");
		die();
	
	}
	
	//Webhook accessgroup checker (check if person has access to use webhook)
	$webhookAccessgroup = $db_local->webhookGetAccessGroup($webhook_data_array['id']);
	//0 is no restriction on the webhook
	if($webhookAccessgroup != 0) {
		
		if (!$db_local->groupCheckIfMember($personId, $webhookAccessgroup)) {
			
			//If the user does not have access, check for group space exceptions
			if ($db_local->adminCheckGroupResponseAcl($botid,$roomId) or (count($db_local->adminCheckUserGroupResponseAcl($botid,$roomId,$personId))) or $db_local->groupCheckIfSpaceMember($roomId, $webhookAccessgroup, $botid)) {
				
				$roomType = "group";
			
			} else {
				
				$spark->messageSendEvent("**{$personEmail}$name** submitted a card:\n- Payload\n\n" . $input_data_string . "\n- Submission access: **denied - webhook restriction (Webhook restriction policy) - Cannot provide input**\n- Bot: **{$botmail}**");
				die();
			
			}
		
		}
	
	}
	
	//Card access verification - Check if the user can submit data with this card
	if (issetor($input_data['cardId'])) {
		
		$cardinfo = $db_local->cardFetchCards($input_data['cardId']);
		
		if ($cardinfo) {
			
			if ($cardinfo[0]['accessgroup']) {
				
				$contactinfo = $db_local->contactFetchContacts($personEmail);
				
				if ($db_local->groupCheckIfMember($contactinfo[0]['id'], $cardinfo[0]['accessgroup']) or $db_local->groupCheckIfSpaceMember($roomId, $cardinfo[0]['accessgroup'], $botid)) {
					
					//Do nothing
					
				} else {
					
					$spark->messageSend($spark->messageBlob("Sorry, I could not find $personEmail as part of the response access group for this submission", $personId, $botid, "toPersonId", 'direct'));
					$spark->messageSendEvent("**{$personEmail}$name** submitted a keyword card:\n- Payload\n\n" . $input_data_string . "\n- Submission access: **Denied, user not part of card configured access group (Card submission policy)**\n- Bot: **{$botmail}**");
					die();
					
				}
				
			}
		}
	}
	
	//Override: wbm_cancel_card flag will delete the card before executing anything
	if (isset($input_data['wbm_cancel_card']) and $access == "granted") {
	    
	    $spark->messageDelete($botid, $messageId);
	    die();
	    
	}
	
	if (isset($input_data['wbm_command'])) {
		
		$payload_attributes = explode(' ', $input_data['wbm_command']);
		$keyword = $db_local->quote(strtolower(array_shift($payload_attributes)));
	
	} else if (isset($input_data['wbm_id_command']) and isset($input_data[$input_data['wbm_id_command']])) {
	    
	    $payload_attributes = explode(' ', $input_data[$input_data['wbm_id_command']]);
	    
	    $keyword = $db_local->quote(strtolower(array_shift($payload_attributes)));
	    
	}
	
	if ($keyword and $access == "granted") {
		
		$redirect = false;
		$reply = $db_local->responseFetchResponse($botid, $keyword);

		if (issetor($input_data['cardId'])) {
			
			if ($cardinfo) {
				
				$redirect = $cardinfo[0]['redirect_reply'];
			
			}
			
		}
		if (isset($input_data['wbm_redirect_reply'])) {
		    
		    $redirect = true;
		    $redirectMetaData = $input_data['wbm_redirect_reply']; //Future use
		    
		}
		
		if (isset($input_data['wbm_reply_no_parent'])) {
		    
		    $replyNoParent = true;
		    
		}
		
		//Response to the incoming message
		if ($reply) {
			
			//Command access verification - check if the user is allowed to execute the command
			if ($reply['accessgroup']) {
				
				$contactinfo = $db_local->contactFetchContacts($personEmail);
				
				if ($db_local->groupCheckIfMember($contactinfo[0]['id'], $reply['accessgroup']) or $db_local->groupCheckIfSpaceMember($roomId, $reply['accessgroup'], $botid)) {
					
					//Do nothing, continue
				
				} else {
					
					//Deny access and reply with access denied
					$spark->messageSend($spark->messageBlob("Sorry, I could not find $personEmail as part of the response access group for this submission.", $personId, $botid, "toPersonId", 'direct'));
					$spark->messageSendEvent("**{$personEmail}$name** submitted a keyword card:\n- Payload\n\n" . $input_data_string . "\n- Submission access: **denied - command restriction (Command restriction policy)**\n- Bot: **{$botmail}**");
					die();
					
				}
			}
									
			//Check if the keyword is considered a feature
			if ($access == "granted" and $reply['is_feature']) {
				
				//Hardcoded WBM features supporting cards
				$info = array($botid, $personEmail, $personId, $roomId, $roomType, $botmail);			
				
				//Hard coded feature card for devices cloud xAPI - using card from cards.inc.php - specify order of the custom input data to feature execute.
				if ($keyword == "device") {
					
					list($xapiType, $deviceId, $key, $getset) = array($input_data['xapiType'], $input_data['deviceid'], $input_data['key'], $input_data['configOption']);
						
					if (in_array($xapiType, array("command", "status", "config"))) {
							
						switch ($xapiType) {
							
							case "command":
								$payload_attributes = array($xapiType, $deviceId, $key, $input_data['value'], $input_data['body']);
								break;
							
							case "status":
								$payload_attributes = array($xapiType, $deviceId, $key);
								break;
							
							case "config":
								$payload_attributes = array($xapiType, $getset, $deviceId, $key, $input_data['value']);
								break;
						}
								
					
					} else {
						
						$reply['response'] = blockquote("Unable to identify xAPI type", "warning");
					
					}
						
				}
				else if ($keyword == "subscribe") {
				    
				    $a = $b = array();
				    
				    if (isset($input_data['subscribe']) and !empty($input_data['subscribe'])) {
				        
				        $a = explode(',', $input_data['subscribe']);
				        
				    }
				    
				    if (isset($input_data['unsubscribe']) and !empty($input_data['unsubscribe'])) {
				        
				        $b = explode(',', $input_data['unsubscribe']);
				        
				    }
				    
				    $payload_attributes = array(implode(',', array_merge($a, $b)));
				
				}
				else if ($keyword == "admin") {
				    if (isset($input_data['emails'])) {
				        $payload_attributes[] = str_replace(' ', '', $input_data['emails']);
				    }
				}
				else if ($keyword == "place") {
				    if (isset($input_data['input'])) {
				        $payload_attributes[] = $input_data['input'];
				    }
				}
				else if ($keyword == "feedback") {
				    if (isset($input_data['feedback_new_entry'])) {
				        $payload_attributes[] = $input_data['feedback_new_entry'];
				    }
				    else if (isset($input_data['feedback_new_comment'])) {
				        $payload_attributes[] = $input_data['feedback_new_comment'];
				    }
				}
					
				$result = $spark->featureExecute($keyword, $payload_attributes, $info);
					
				//Check if the feature result returns a card (attachments)
				if ($result) {
				
					if (isset($result['markdown']) and isset($result['attachments'])) {
					
						$reply['response'] = $result['markdown'];
						$reply['attachments'] = $result['attachments'];
				
					} elseif (is_array($result)) {
					    
					    $reply['attachments'] = $result;
					    $reply['response'] = "Card Render: Unable to render card";
					    
					} else {
					
						$reply['response'] = $result;
				
					}
					
				} else {
					
					$reply['response'] = "Failed to execute feature";
				
				}
			
				
			} else if ($access == "granted" and $reply['is_task']) {
			
				if(!$db_local->adminCheckIfMaintenance()) {
					if (!$db_local->adminCheckServiceStatus()){
						$files_csv = $payload = "";
						//foreach ($card_data as $key => $value) {
						//	$value = str_replace('"', '\"', $value);
						//}
						$card_data = json_encode($input_data);
						
						$card_data = $db_local->quote($card_data);
						
						$user_details = $db_local->contactDetailsAPI($personEmail);
						
						if (count($user_details['items'])) {
						    $user_details = json_encode($user_details['items'][0]);
						} else {
						    $user_details = "";
						}
						
						if (!$db_local->taskAddNewTask($keyword, $payload_attributes, array(
								$personEmail,
						        $user_details,
								($redirect) ? "direct" : $roomType, 
								($redirect) ? $personEmail : $roomId, 
								$botmail, 
								$payload, 
								$files_csv, 
								($redirect and $roomType == "group") ? "" : $parentId, 
								$card_data))) {
							$reply['response'] = "The request failed a sanity check and was purged, please check your input and try again.";
						}
					}
					else {
						$reply['response'] = feedbackMsg("Task service status",$db_local->adminCheckServiceStatus('report'), "warning", 1);
					}
				}
				else {
					$reply['response'] = feedbackMsg("Maintenance mode is enabled!",$db_local->adminGetMaintenanceMessage(), "warning", 1);
				}
			
			} else if ($access == "granted" and !$reply['is_task'] and !$reply['is_feature']) {
				
				//A response has been found bound to the keyword, now check if the response has a new card (flow)
				if ($reply['card_id']) {
					
					if (count($cardDetails = $db_local->cardFetchCards($reply['card_id']))) {
						
						$reply['attachments'] = $spark->cardPrepareCard($cardDetails[0]['id'], json_decode($cardDetails[0]['card_body'], true));
						
					}
					
				}
			
			}	
			
			//Add the card payload to the response if exists
			if (!empty($reply['attachments']) and $access = "granted") {
			    
			    $query['attachments'] = array($reply['attachments']);
			    
			} else {
			    
			    //File attachments does not work when a card is attached
			    if(!empty($reply['file_url']) and $access == "granted") {
			        
			        $query['files'] = array($reply['file_url']);
			        
			    }
			    
			}
			
			//Override: wbm_delete_card flag will delete the card after executing the command to support custom flows
			if (isset($input_data['wbm_delete_card']) and $access == "granted") {
				
				$spark->messageDelete($botid, $messageId);
				
			}
			
		} else {
			
			//The response was not found
			$reply['response'] = blockquote("Sorry, the feature key is disabled!", "warning");
		
		}
	
	} 
	
	if (issetor($input_data['cardId'])) {
		
		if ($cardinfo) {
		
			$redirect = $cardinfo[0]['redirect_reply'];
				
			if ($access == "granted") {
				
				if ($cardinfo[0]['reply']) {
					$userfeedback = $cardinfo[0]['reply'];
				}
										
				if ($cardinfo[0]['input_active']) {
					unset($input_data['cardId']);
						
					if (!count($input_data)) {
						$input_data['empty'] = "No input data found with the submission";
					}
					
					$cardDataBlock = array(
							"card_id" => $cardinfo[0]['id'],
							"person_id" => $personId,
							"botid" => $botid,
							"person_email" => $personEmail,
							"input_data" => $db_local->quote(json_encode($input_data))
					);	
					$usersubmission = $db_local->cardFetchCardData($cardinfo[0]['id'], $personId);
					
					if ($cardinfo[0]['update_active']) {
						
						if (count($usersubmission)) {
							
							if ($db_local->cardDataCollectUpdate($cardDataBlock)) {
								
								//No action
							
							} else {
								
								$reply['response'] = "Unexpected error when updating your previous submission";
							
							}
						} 
							
					} 
					
					if ($dbmsg = $db_local->cardDataCollectAdd($cardDataBlock)) {
						
						if (explode(' ', trim($dbmsg))[0] == "Duplicate" and !$cardinfo[0]['update_active'] and ($cardinfo[0]['reply'] and $cardinfo[0]['input_active'])) {
							
							$userfeedback = "Only one submission is allowed! Your initial data submission was left intact.";							
						
						} 
							
						if (explode(' ', trim($dbmsg))[0] == "Duplicate" and $cardinfo[0]['update_active'] and ($cardinfo[0]['reply'] and $cardinfo[0]['input_active'])) {
							
							$userfeedback = "Your submission was updated!";
							
						}
					
					} else {
						
						$reply['response'] = "Unexpected error when collecting the input data";
					
					}
						
					if ($cardinfo[0]['echo_active']) {
							
						$mentioned = $db_local->contactGetName($personEmail);
							
						if (validateEmail($mentioned)) {
								
							$personDetails = $spark->peopleGetDetails($botid, $personId);
							$mentioned = (isset($personDetails['displayName'])) ? $personDetails['displayName'] : $personEmail; 
							
						}
							
						$mention = ($roomType == "group" and !$redirect) ? "<@personEmail:$personEmail|$mentioned>" : "You";
						$submitted = json_encode($input_data, JSON_PRETTY_PRINT);
						$text = "$mention submitted the following data : \n\n $submitted";
						
						if ($redirect) {
							
							$spark->messageSend($spark->messageBlob($text, $personId, $botid, "toPersonId", 'direct'), ($roomType == "group") ? "" : $parentId);
							
						} else {
							
							$spark->messageSend($spark->messageBlob($text, $roomId, $botid, "roomId", $roomType), $parentId);
						
						}
						
					}
							
				} else {
					
					//$spark->messageSend($spark->messageBlob("The form you submitted is currently not active, no data was collected.", $personId, $botid, "toPersonId", 'direct'));
				
				}
						
				if ($cardinfo[0]['delete_active']) {
						
					$spark->messageDelete($botid, $messageId);
					$parentId = ($parentId == $messageId) ? "" : $parentId;
				
				}
				
				//Overrides card delete if this input field is set
				if (isset($input_data['wbm_delete_card'])) {
					
					$spark->messageDelete($botid, $messageId);
					$parentId = ($parentId == $messageId) ? "" : $parentId;
					
				}
				
				if (!empty($userfeedback)) {
					
					if ($redirect) {
						
						$spark->messageSend($spark->messageBlob($userfeedback, $personId, $botid, "toPersonId", 'direct'), ($roomType == "group") ? "" : $parentId);
						
					} else {
						
						$spark->messageSend($spark->messageBlob($userfeedback, $roomId, $botid, "roomId", $roomType), $parentId);
						
					}
					
				}
		
			}
			
		} else {
				
			$access = "Granted, but was unable to find the card in the database (deleted?), the submission was ignored and card deleted from user";
			$reply['response'] = "The data collector for this form does no longer exist. Deleting form to avoid future confusion, sorry.";
			$spark->messageDelete($botid, $messageId);
			
		}
		
	} 	
	
	
	//Build the spark response array
	$query['recepientType'] = ($redirect) ? 'toPersonId' : 'roomId';
	$query['recepientValue'] = ($redirect) ? $personId : $roomId;
	$query['sender'] = $botid;
	$query['text'] = $reply['response'];
	$query['type'] = ($redirect) ? 'direct' : $roomType;
	
	if (($redirect and ($roomType == "group")) or $replyNoParent) {
	    $parentId = "";
	}
			
	$test = (!empty($reply['response'])) ? $spark->messageSend($query, $parentId):false;
	$spark->messageSendEvent("**{$personEmail}$name** <strong>submitted a card</strong>\n- Payload\n\n" . $input_data_string . "\n- Submission access: **$access**\n- Bot: **{$botmail}**");
	$qs = $db_local -> constructInsertValues("activity_log", array('botid'=>$botid,'command'=>$input_data_string,'user'=>$personEmail));
	$db_local -> query($qs);

}


?>