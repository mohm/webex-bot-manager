<?php

include '../../includes/functions.inc.php';
$db = new Db();
$auth_header = issetor($_SERVER['HTTP_AUTHORIZATION']);
$error = false;

if (!empty($auth_header)) {
    
    list($type, $token) = explode(' ', $auth_header);
    
    if ($db->adminCheckIfValidAPIToken($token)) {
        
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            
            if (isset($_GET['fetch'])) {
                
                if ($_GET['fetch'] == 'next_task') {
                                       
                    if (isset($_GET['who'])) {
                        
                        $botemails = explode(',', $_GET['who']);
                        
                        if (count($botemails)) {
                            
                            $query_clause = array();
                            
                            foreach ($botemails as $k => $v) { 
                                
                                if (validateEmail($v)) {
                                    
                                    $botinfo = $db->select_specific("SELECT * FROM bots WHERE emails = '$v'");
                                    
                                    if (count($botinfo)) {
                                        
                                        $query_clause[] = "bot = '$v'";
                                        
                                    }  else { 
                                        
                                        $error = true;
                                        $output = array('Error' => "$v - No such bot");
                                        break;
                                        
                                    } 
                                    
                                } else {
                                    
                                    $error = true;
                                    $output = array('Error' => "$v - Invalid e-mail");
                                    break;
                                    
                                } 
                                                             
                            }
                            if (!$error) {
                                if (count($query_clause)) {
                                    
                                    $select_query = "SELECT * FROM tasks WHERE " . implode(' or ', $query_clause);
                                    
                                } else {
                                    
                                    $error = true;
                                    $output = array('Error' => "Unexpected error - nothing to query");
                                    
                                }
                            }
                            
                            
                        } else {
                            
                            $error = true;
                            $output = array('Error' => "Missing bot e-mail");
                            
                        }
                        
                    } else {
                        //Fetch any task if who is not specified
                        
                        $select_query = 'SELECT * FROM tasks LIMIT 1';
                        
                    }
                    
                    if (!$error) {
                        
                        $qs = $db -> constructUpdateValues(	
                            "heartbeat",
                            array(	
                                'id' => '1',
                                'ip' => $_SERVER['REMOTE_ADDR'],
                                'time' => get_timestamp()
                            ),
                            "WHERE id = '1'");
                            
                            $db -> query($qs);
                            
                            $output = $db -> select_specific($select_query);  
                            
                            if (!empty($output['card_data'])) {
                            	
                            	$output['card_data'] = json_decode($output['card_data'], true);
                            	
                            }
                            
                            if (!empty($output['user_details'])) {
                                
                                $output['user_details'] = json_decode($output['user_details'], true);
                                
                            }
                      
                    }
                    
                } else if ($_GET['fetch'] == "service") {
                    
                    $service = $db -> adminCheckServiceStatus();
                    $maintenance = $db -> adminCheckIfMaintenance();
                    $output = array("service"=>$service, "maintenance"=>$maintenance);
                    
                } else {
                    
                    $error = true;
                    $output = array('Error' => "Invalid value for: fetch");                
                    
                }
                
            } else {
                
                $error = true;
                $output = array('Error' => "Missing required key: fetch");
                
            }
           
        } else {
            
            header('Status: 405 Method not allowed');
            $output = array('Error'=>'405 Method not allowed');
            
        }
        
    } else {
        
        header("Status: 401 Unauthorized");
        $output = array('Error'=>'401 Unauthorized');
        
    }
    
} else {
    
    header("Status: 401 Unauthorized");
    $output = array('Error'=>'Missing authorization header');
    
}
    
header('Content-type: application/json');
echo json_encode($output, JSON_PRETTY_PRINT);
if (!empty($output['id'])) {
    $output = $db -> query("DELETE FROM tasks WHERE id = {$output['id']}");
}
?>