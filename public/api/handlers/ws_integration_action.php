<?php 
session_start();
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-Type: application/json');

$wx = new SparkEngine();
$db = new Db();

$incoming = file_get_contents("php://input");
$dec_incoming = json_decode($incoming,true);

//$myfile = fopen("actions.txt", "a+") or die("Unable to open file!");

//fwrite($myfile, $incoming);

$decoded = jwtDecode($dec_incoming, 1);
$appId = $decoded['appId'];

if (!$db->wsIntegrationExists($appId)) {
    
    error_header("404", "Not found");
    
    die();

}

$localDetails = $db->wsIntegrationGet($appId);
$details = $localDetails[0];
$jwt = $db->wsIntegrationJwtInfo($appId);

if ($details['actions_logging']) {
    $log_data = array(
        'appId'     => $decoded['appId'],
        'jti'       => $decoded['jti'],
        'iat'       => $decoded['iat'],
        'action'    => $decoded['action'],
        'full'      => json_encode($decoded),
        'time'      => get_timestamp()
    );
    $db->wsIntegrationActionLog($log_data);
}

$action = $decoded['action'];

switch ($action) {
    case "deprovision": 
        
        $result = $db->wsIntegrationDeactivate($appId);
        
        break;
        
    case "healthCheck":
        
        $wx->wsIntegrationRefreshAccessToken($appId);
        $test = $wx->wsIntegrationGetAppManifest($appId); 
        
        if (issetor($test['id'])) {
            
            if ($test['id'] == $details['id']) {
                
                $data = array(
                    "health_timestamp"  => get_timestamp(),
                    "health_status"     => "1",
                    "status"            => "Health check successful"
                );
                
                $db->wsIntegrationUpdate($appId, $data);
                
                header("Status: 200");
                
                echo json_encode(
                    array(
                        "operationalState"  => "operational",
                        "tokensState"       => "valid"
                    )
                );
                
            }
            
        } else {
            
            error_header("500", "Unable to reach app manifest");
            die();
        
        }
        
        break;
        
    case "updateApproved": 
        
        $wx->wsIntegrationRefreshAccessToken($appId);
        
        break;
        
        
    default:
        
        error_header("500", "Failed");
        die();
        
}

?>