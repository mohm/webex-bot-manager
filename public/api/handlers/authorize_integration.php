<?php 
//Notifications only works if the bot owns the group, a API Token is required in order to use the API.
include '../../includes/functions.inc.php';
header('Content-type: application/json');
$db = new Db();
$spark = new SparkEngine();

$integrationSettings = $db->integrationFetchSettings();
$output = array("status"=>array("response"=>"Error", "description"=>"NA"));

foreach ($integrationSettings as $key => $value) {
	if (empty($value)) {
		$output['status']['response'] = "Error";
		$output['status']['description'] = "Integration settings are not correctly configured in WBM";
		die(json_encode($output));
	}
}

//This will generate an access_token for the configured integration 
if ($_SERVER['REQUEST_METHOD'] == "GET")  {
	$code = issetor($_GET['code']);
	$state = issetor($_GET['state']);
	if ($state) {
		$state_parts = explode(' ', $state);
	}
	if ($code and count($state_parts)) {
		$result = $spark->integrationAuth($integrationSettings['client_id'], $integrationSettings['client_secret'],$code,$integrationSettings['redirect_url']);
		$result = $db->integrationFilterTokenResult($result);
		if (issetor($result['access_token']) and issetor($result['refresh_token'])) {
			$userid = $state_parts[0];
			$user = $db->contactFetchContacts($userid);
			if (count($user)) {
				$result['userid'] = $userid;
				$db->integrationDeleteToken($userid);
				$db->integrationAddToken($result);
				if (count($db->integrationFetchToken($userid))) {
					$db->integrationDeleteRepresentation($userid);
					$spark->integrationCreateBotRepresentation($userid);
					unset($result['userid']);
					$output['status']['response'] = "Success";
					$output['status']['description'] = "Token was successfully attached!";
				} else {
					$output['status']['response'] = "Error";
					$output['status']['description'] = "Unable to attach token. Query failed post user verification!";
				}
				if ($state_parts[1]) {
					$output['status']['response'] = "200 OK - Token was successfully attached!";
					$output['status']['description'] = "Token was attached to the user, you can close this window.";
				}
			} else {
				$output['status']['response'] = "Error";
				$output['status']['description'] = "Unable to attach token. User not found";
			}
		} else {
			$output = $result; 
		}
	} else {
		$output['status']['response'] = "Error";
		$output['status']['description'] = "Something went wrong";
		
		if (isset($_GET['error']) and isset($_GET['error_description'])) {
			$output['status']['error'] = $_GET['error'];
			$output['status']['error_description'] = $_GET['error_description'];
		}
		
		die(json_encode($output));
	}
}

if ($output['status']['description'] != 'NA') {
	echo json_encode($output);
	if ($output['status']['response'] == "Success") {
		$host = getHostUrl();
		redirect("{$host}/index.php?id=contacts&contactid=$userid");
	}
}

?>
	