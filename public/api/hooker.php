<?php

include '../includes/functions.inc.php';

//Start classes
$db_local = new Db();
$spark = new SparkEngine();

$maintenance = ($db_local->adminCheckIfMaintenance()) ? "Enabled" : "Disabled";
$warning_mode = ($db_local->adminCheckIfWarning()) ? "Enabled" : "Disabled";
$unknown_keyword = false;
$default_response = false;

//Incoming data from webhook (Teams)
$raw_webhook_data = file_get_contents("php://input");
$webhook_data_array = json_decode($raw_webhook_data,true);

//Find owner of the webhook
$botid = $db_local->botGetWebhookOwner($webhook_data_array['id']);

//Stop if the webhook does not have a owner
if (!$botid) die();

$botinfo = $db_local->botFetchBots($botid);

//If the bot does not exist, stop
if (!$botinfo) die();

$auth = $spark->authGet($botid);

if (!$auth) die();
	
$access = "granted";

//Get message ID from incoming spark data
$incoming_message_id = $webhook_data_array["data"]["id"];
$incoming_org_id = $webhook_data_array["orgId"];

//If message exist
if ($incoming_message_id) {
	
	//Get the actual message details based on message ID from spark
	$response = $spark->messageGetDetails($botid, $incoming_message_id);
		
	//If the message does not exist, stop
	if (!$response) die();
	
	//Assigning variables based on the incoming message
	$who_sent = $response['personEmail'];
	$botmail = $botinfo[0]['emails'];
	
	//If the message is coming from the same bot, stop
	if ($who_sent == $botmail) die();
	
	$personId = $response['personId'];
	$roomId = $response['roomId'];
	$recepientType = 'roomId';
	$parentId = (isset($response['parentId'])) ? $response['parentId'] : $incoming_message_id;
	$payload = trim($db_local->adminFilterMentions($botinfo[0]['displayName'], $response['text']));
	
	if ($response['files'][0]) {
		
		$files = issetor($response['files'][0]);
		$files_csv = implode(',',$response['files']);
	
	}
	
	$type = "direct";
	
	
	//If the webhook comes from the same bot, stop here
	
	
	//Find the initial response array based on the text input from Spark
	$payload_attributes = explode(" ", $payload);
	$keyword = $db_local->quote(strtolower(array_shift($payload_attributes)));
	$reply = $db_local->responseFetchResponse($botid, $keyword);
			
	//Check if there is a restriction on the bot
	if ($db_local->adminCheckBotRestriction($botid)) {
		
		//If there is a restriction, check if the e-mail domain is valid
	    if ($db_local->botCheckSameOrgResponse($botid)) {
	        
	        $personDetails = $spark->peopleGetDetails($botid, $personId);
	        
	        if (!(issetor($personDetails['orgId']) == $incoming_org_id)) {
	            $spark->messageSendEvent("**{$who_sent}** sent **{$keyword}**:\n- Full command: **" . $payload . "**\n- Response Access: **denied - same org response violation (same org response restriction policy) - No response sent to user**\n- Bot: **{$botmail}**");
	            die();
	        }	        
	        
	    }
		
		if (!$db_local->adminCheckIfValidDomain($who_sent, $botid)) {
		
			$spark->messageSendEvent("**{$who_sent}** sent **{$keyword}**:\n- Full command: **" . $payload . "**\n- Response Access: **denied - domain violation (domain restriction policy) - No response sent to user**\n- Bot: **{$botmail}**");
			die();
		
		}
	
	}
	
	//Check if the user is blocked
	if ($db_local->adminGetBlockedContacts($who_sent)) {
		
		$spark->messageSendEvent("**{$who_sent}** sent **{$keyword}**:\n- Full command: **" . $payload . "**\n- Response Access: **denied - user blocked (blocked user policy) - No response sent to user**\n- Bot: **{$botmail}**");
		die();
	
	}
	
	//Webhook accessgroup checker (check if person has access to use webhook)
	$webhookAccessgroup = $db_local->webhookGetAccessGroup($webhook_data_array['id']);
	
	//0 is no restriction on the webhook
	if($webhookAccessgroup != 0) {
		
		if (!$db_local->groupCheckIfMember($personId, $webhookAccessgroup)) {
			
			//If the user does not have access, check for group space exceptions
			if ($db_local->adminCheckGroupResponseAcl($botid,$roomId) or (count($db_local->adminCheckUserGroupResponseAcl($botid,$roomId,$personId))) or $db_local->groupCheckIfSpaceMember($roomId, $webhookAccessgroup, $botid)) {
				
				$type = "group";
			
			} else {
				
				$access = "denied - webhook restriction (webhook restriction policy) - No response sent to user";
			
			}
		
		}
	
	}
	
	if ($webhook_data_array['name'] == "group") {
		
		$type = "group";
	
	}
	
	//Check the default response
	if (!$reply) {
	    
	    $defres = $botinfo[0]['defres'];
	    
	    if (is_numeric($defres)) {
	        
	        $reply = $db_local->responseFetchResponses($botid, $defres);
	        
	        if (count($reply)) {
	            
	            $reply = $reply[0];
	            
	            $payload_api = preg_replace("/$keyword/", $reply['keyword'], $payload, 1);
	            $keyword_default = $reply['keyword'];
	            
	            $unknown_keyword = true;
	            $default_response = true;
	            
	        }
	        
	    }
	    
	}
	
	//Response to the incoming message
	if ($reply and $access == "granted") {
		
		//Command access verification start ---------------------
		
		if ($reply['accessgroup'] != 0) {
			
			$contactinfo = $db_local->contactFetchContacts($personId);
			
			//Added new access check if the message came from a space part of the access group it will be authorized
			if ($db_local->groupCheckIfMember($contactinfo[0]['id'], $reply['accessgroup']) or $db_local->groupCheckIfSpaceMember($roomId, $reply['accessgroup'], $botid)) { 
			
				//Do nothing (has access to the command)
			
			} else {
				
				//Deny access, reply with access denied
				$access = "denied - command restriction (Command restriction policy)";
				$reply['response'] = "Sorry, I cannot find **{$who_sent}** in the accesslist for this command. Permission was denied.";
			
			}
		
		}
		
		//Command access verification end-----------------------
		
		//Task flag is checked (create a API entry for a listener to pick up if access is granted)
		if ($reply['is_task'] == 1 and $access == "granted") {
			
			if(!$db_local->adminCheckIfMaintenance()) {
				
				if (!$db_local->adminCheckServiceStatus()){
					
					$files_csv = issetor($files_csv);
					
					$user_details = $db_local->contactDetailsAPI($who_sent);
					
					if (count($user_details['items'])) {
					    $user_details = json_encode($user_details['items'][0]);
					} else {
					    $user_details = "";
					}
					
					if (!$db_local->taskAddNewTask(($default_response) ? $keyword_default : $keyword, $payload_attributes, array($who_sent, $user_details, $type, $roomId, $botmail, $payload, $files_csv, $parentId))) {
						
						$reply['response'] = "The request failed a sanity check and was purged, please check your input and try again.";
					
					}
				
				} else {
					
					$reply['response'] = feedbackMsg("Task service status",$db_local->adminCheckServiceStatus('report'), "warning", 1);
				
				}
			
			} else {
				
				$reply['response'] = feedbackMsg("Maintenance mode is enabled!",$db_local->adminGetMaintenanceMessage(), "warning", 1);
			
			}
		
		}
		
		//Feature flag is checked (execute feature and send back reply)
		if ($reply['is_feature'] == 1 and $access == "granted") {
			
			$info = array($botid, $who_sent, $personId, $roomId, $type, $botmail);
			
			//Check if the user attached a file to the request and add it to the feature process
			if (!empty($files) and validateUrl($files)){
				
				$info[] = $files;
			
			}
			
			if (!$result = $spark->featureExecute($keyword, $payload_attributes, $info)) {
				
				$reply['response'] = "Failed to execute feature!";
			
			} else {
				
				//Check if the feature result returns a card (attachments)
				if (isset($result['markdown']) and isset($result['attachments'])) {
					
					$reply['response'] = $result['markdown']; 
					$reply['attachments'] = $result['attachments'];
				
				} 
				
				elseif (is_array($result)) {
				    $reply['response'] = 'Card render: Unable to render card';
				    $reply['attachments'] = $result;
				} 
				
				else {
					
					$reply['response'] = $result;
				
				}
			
			}
		
		}
		
		if ($reply['card_id'] and !$reply['is_feature'] and $access == "granted") {
			
			if (count($cardDetails = $db_local->cardFetchCards($reply['card_id']))) {
				
				$reply['attachments'] = $spark->cardPrepareCard($cardDetails[0]['id'], json_decode($cardDetails[0]['card_body'], true)); 
			
			}
		
		}
		
		//Build the spark response array
		$query = array('recepientType' => 'roomId',
				'recepientValue' => $roomId,
				'sender' => $botid,
				'text' => $reply['response'],
				'type' => $type
		);
		
		//Add the file url to the response		
		if(!empty($reply['file_url']) and $access == "granted") {
			
			$query['files'] = array($reply['file_url']);
		
		}
		//Add the card payload to the response
		if (!empty($reply['attachments'])) {
			
			$query['attachments'] = array($reply['attachments']);
		
		}
		
	} else {
	   
	    //No default response found and the keyword did not match
	   $reply['response'] = "";
	   $reply['keyword'] = ($access == "granted") ? "$keyword (Unknown keyword) -> Default keyword not found":"$keyword ($access)";
	   $access = ($access == "granted") ? "granted - but the keyword was unknown":$access;
	
	}
	
	//Keyword did not match but default response is found
	if ($default_response) {
	    $reply['keyword'] = ($access == "granted") ? "$keyword (Unknown keyword) -> Converted to default keyword ($keyword_default)":"$keyword ($access)";
	    $access = ($access == "granted") ? "granted - but the keyword was unknown":$access;
	}
	
	//Add warning message if warning mode is enabled
	if ($db_local->adminCheckIfWarning()) {
		
		$warning = feedbackMsg("SYSTEM MESSAGE!",$db_local->adminGetWarningMessage(), "warning", 1);
		$query['text'] = $warning . $query['text']; 
	
	}
	
	//Send message back to user (if response is removed, send nothing)
	$reply['response'] = (empty($query['text'])) ? "":$reply['response']; //do not send empty response
	$test = (!empty($reply['response'])) ? json_encode($spark->messageSend($query, $parentId)):false;
	
	//Try to resolve name of the sender and send event notification
	$name = $db_local->contactGetName($who_sent);
	$name = (validateEmail($name)) ? "" : " ($name) ";
	if ($default_response) {
	    $converted_command = "\n- Defaulted to: $payload_api";
	}
	$task_mon = ($db_local->adminCheckIfTaskMonitor()) ? "\n- " . $db_local->adminCheckServiceStatus('report') : "\n- Task monitor is **Disabled**";
	$spark->messageSendEvent("<strong>" . $who_sent . $name . "</strong> sent keyword: <strong>" . $reply['keyword'] . "</strong>:\n- Room Type: <strong>" . $type . "</strong>\n- Full text: <strong>" . $payload . "</strong>\n- Access: <strong>" . $access . "</strong>\n- Conversation with: <strong>" . $botmail . "</strong> $task_mon\n- Maintenance mode: <strong>" . $maintenance . "</strong>\n- Warning mode: <strong>" . $warning_mode . "</strong>");
	
	//Log usage
	$qs = $db_local -> constructInsertValues("activity_log", array('botid'=>$botid,'command'=>$payload,'user'=>$who_sent));
	$db_local -> query($qs);
	
}
?>